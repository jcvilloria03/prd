<?php
class C_ChangePassword extends CI_Controller {

	public function __construct(){
		parent::__construct();
		validate_request();
		$this->load->model('account/M_ChangePassword');
	}
	
	public function index(){
		$data['title'] = 'account';
		$this->load->view('account/V_ChangePassword',$data);
	}
	
	public function UpdatePassword(){
		$return = $this->M_ChangePassword->UpdatePassword($this->input->post());
		switch($return){
			case 1 : echo "success|Your password has beed updated successfully."; break;						
			case 2 : echo "warning|Invalid current password."; break;
			default : echo "error|Upading failed."; break;
		}		
	}
}