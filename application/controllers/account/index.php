<?php
class Index extends CI_Controller {

	public function __construct(){
		parent::__construct();
		validate_request();
	}
	
	public function index(){
		$data['title'] = 'account';
		$this->load->view('account/index.php',$data);
	}
	
}