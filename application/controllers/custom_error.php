<?php
class custom_error extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		validate_request();
	}
	
	public function error_500()
	{
		$data = array('type'=>'error','message'=>"Could not load page properly.\nPlease contact support.");
		$this->load->view('custom_error',$data);
	}
	
	public function error_404()
	{
		$data = array('type'=>'error','message'=>"Could not load page properly.\nPlease contact support.");
		$this->load->view('custom_error',$data);
	}
	
}