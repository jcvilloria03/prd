<?php
class C_PurchaseOrder extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Document/M_PurchaseOrder');
	}
	
	public function index(){
		$this->load->view('Document/V_PurchaseOrder');
	}
	
	public function PRReport(){
		$this->load->view('Document/V_PRReport');
	}
	
	public function SearchData(){
		header('Content-type: application/json');
		echo json_encode($this->M_PurchaseOrder->SearchData($this->input->post()));
	}

}