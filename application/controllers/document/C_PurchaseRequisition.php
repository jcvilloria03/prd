<?php
class C_PurchaseRequisition extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('Document/M_PurchaseRequisition');
	}
	
	public function index(){
		$this->load->view('Document/V_PurchaseRequisition');
	}
	
	public function PRReport(){
		$this->load->view('Document/V_PRReport');
	}
	
	public function Search(){
		header('Content-type: application/json');
		echo json_encode($this->M_PurchaseRequisition->Search($this->input->post()));
	}

}