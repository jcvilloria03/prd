<?php
class C_SaleOrder extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Document/M_SaleOrder');
	}
	
	public function index()
	{		
		$this->load->view('Document/V_SaleOrder');
	}
	
	public function viewSOallData(){
		$return = $this->M_SaleOrder->viewSOData_AllData($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}


}