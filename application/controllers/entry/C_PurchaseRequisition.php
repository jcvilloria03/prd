<?php
class C_PurchaseRequisition extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('entry/M_PurchaseRequisition');
	}
	
	public function index()
	{		
		$this->load->view('entry/V_PurchaseRequisition');
	}
	
	// public function view(){
		// echo $this->M_PurchaseRequisition->get_PRDetail_data($this->input->post());			
	// }
	
	public function PRNumber(){
		echo $this->M_PurchaseRequisition->get_PRNumber($this->input->post());
	}
	
	public function save(){
		$return = $this->M_PurchaseRequisition->save_data($this->input->post());
		switch($return){
			case 1 : echo "success|Saving completed."; break;						
			case 2 : echo "warning|Customer Code already added."; break;
			default : echo "error|Saving failed."; break;
		}		
	}
	
	public function GetPRDetail(){
		echo $this->M_PurchaseRequisition->get_PRDetail($this->input->post());
	}
	
	function __destruct(){
		audittrail();
	}
}