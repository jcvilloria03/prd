<?php
class C_UOM extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		validate_request();
		$this->load->model('entry/M_UOM');
	}
	
	public function index()
	{		
		$this->load->view('entry/V_UOM');
	}
	
	public function view(){
		echo $this->M_UOM->get_data($this->input->post());			
	}
	
	public function search(){
		$return = $this->M_UOM->get_data_by_id($this->input->post());	
		switch($return){
			case 1 : echo "error|No record found."; break;
			default : echo $return;
		}
	}
	
	public function save(){
		$return = $this->M_UOM->save_data($this->input->post());
		switch($return){
			case 1 : echo "success|Saving completed."; break;						
			case 2 : echo "warning|UOM Code already added."; break;
			default : echo "error|Saving failed."; break;
		}		
	}
	
	public function update(){
		$return = $this->M_UOM->update($this->input->post());
		switch($return){
			case 1 : echo "success|Updating completed."; break;						
			case 2 : echo "warning|UOM Code is already added."; break;
			default : echo "error|Updating failed."; break;
		}		
	}
	
	public function delete(){
		$return = $this->M_UOM->delete($this->input->post());
		switch($return){
			case 1 : echo "success|Successfully deleted."; break;						
			case 2 : echo "warning|No record found."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	function __destruct(){
		audittrail();
	}
}