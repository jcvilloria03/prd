<?php
class C_InventoryCount extends CI_Controller{
	public function __construct(){
		parent::__construct();
		validate_request();
		$this->load->model('inventory/M_InventoryCount');
	}
	
	public function index(){
		$this->load->view('inventory/V_InventoryCount');
	}
	
	
	public function viewInventoryCountList(){
		$return = $this->M_InventoryCount->view_InventoryCount($this->input->post('CustomerID'));
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	public function saveInventoryCountMulti(){
		$return = $this->M_InventoryCount->save_InventoryCountMulti($this->input->post());
		switch($return){
			case 0 : echo"success|Adding Complete"; break;
			case 1 : echo "error|PONumber does not exist.";break;
			default : echo "error|could not add. <br> Please contact Support"; break;
		}
	}
}
?>