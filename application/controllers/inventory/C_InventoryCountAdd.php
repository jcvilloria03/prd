<?php
class C_InventoryCountAdd extends CI_Controller{
	public function __construct(){
		parent::__construct();
		validate_request();
		$this->load->model('inventory/M_InventoryCountAdd');
	}
	
	public function index(){
		$this->load->view('inventory/V_InventoryCountAdd');
	}
	
	public function viewInventoryCountList(){

		echo $this->M_InventoryCountAdd->view_InventoryCount($this->input->post()); 
	}
	
	public function viewCustomerList4Inventory(){
		echo $this->M_InventoryCountAdd->view_CustomerList4Inventory($this->input->post()); 
	}
	
	public function saveInventoryCount(){
		$return = $this->M_InventoryCountAdd->save_InventoryCount($this->input->post());
		switch($return){
			case 0 : echo"success|Adding Complete"; break;
			case 1 : echo "success|Updating Complete.";break;
			default : echo "error|could not add. <br> Please contact Support"; break;
		}
	}
}


?>