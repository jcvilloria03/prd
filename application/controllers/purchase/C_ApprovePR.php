<?php
class C_ApprovePR extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		validate_request();
		$this->load->model('purchase/M_ApprovePR');
	}
	
	public function index()
	{		
		$session_data = $this->session->userdata('logged_in');
		$data['name'] = $session_data['name'];
		$data['username'] = $session_data['username'];
		$data['dep_name'] = $session_data['dep_name'];
		$data['is_manager'] = $session_data['is_manager'];
		$data['is_GM'] = $session_data['is_GM'];
		$data['is_BM'] = $session_data['is_BM'];
		$data['is_PRDManager'] = $session_data['is_PRDManager'];
			
		$data['PRNumber'] = $this->input->post('PRNumber');
		$this->load->view('purchase/V_ApprovePR',$data);
	}
	
	public function viewPRData(){
		$return = $this->M_ApprovePR->viewPR_Data($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	function __destruct(){
		audittrail();
	}
}
?>