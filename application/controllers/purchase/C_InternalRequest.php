<?php
class C_InternalRequest extends CI_Controller{
	public function __construct(){
		parent::__construct();
		validate_request();
		$this->load->model('purchase/M_InternalRequest');
	}
	
	public function index(){
		$session_data = $this->session->userdata('logged_in');
		$data['name'] = $session_data['name'];
		$data['username'] = $session_data['username'];
		$data['dep_name'] = $session_data['dep_name'];
		$data['is_manager'] = $session_data['is_manager'];
		$data['is_GM'] = $session_data['is_GM'];
		$data['is_PRDManager'] = $session_data['is_PRDManager'];
		$this->load->view('purchase/V_InternalRequest',$data);
	}
	
	public function createPRFromInterRequest(){
 
		$return = $this->M_InternalRequest->create_PRFromInterRequest($this->input->post());

		switch($return['Result']){
			case 0 : echo $return['PRNumber']; break;						
			default : echo "error"; break;
		}		
	}
	
	
	public function saveInternalRequest(){

		$return = $this->M_InternalRequest->save_InternalRequest($this->input->post());

		switch($return['Result']){
			case 0 : echo "success|Saving completed.|".$return['ID']; break;						
			default : echo "error|Could not save.<br>Please contact Support."; break;
		}		
	}
	
	public function updateInternalRequest(){

		$return = $this->M_InternalRequest->update_InternalRequest($this->input->post());
		switch($return){
			case 0 : echo "success|Saving completed."; break;						
			default : echo "error|Could not save.<br>Please contact Support."; break;
		}		
	}
	
	public function saveIRDetail(){

		$return = $this->M_InternalRequest->save_IRDetail($this->input->post());
		switch($return){
			case 0 : echo "success|Saving completed."; break;						
			default : echo "error|Could not save.<br>Please contact Support."; break;
		}		
	}
	
	public function changeIRType(){
		$return = $this->M_InternalRequest->change_IRType($this->input->post());
		switch($return){
			case 0 : echo "success|Successfully Change IRType."; break;						
			case 1 : echo "warning|No record found."; break;
			default : echo "error|Could not delete.<br>Please contact IT Support."; break;
		}		
	}
	
	public function deleteIRDetail(){
		$return = $this->M_InternalRequest->delete_IRDetail($this->input->post());
		switch($return){
			case 1 : echo "success|Successfully deleted."; break;						
			case 2 : echo "warning|No record found."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	public function completeIRData(){
		$return = $this->M_InternalRequest->complete_IRData($this->input->post());
		switch($return){
			case 0 : echo "success|Saving completed."; break;
			default : echo "error|Could not complete. <br> Please contact Support."; break;
		}
	}
	
	public function search(){
		$return = $this->M_InternalRequest->get_data_by_id($this->input->post());	
		switch($return){
			case 1 : echo "error|No record found."; break;
			default : echo $return;
		}
	}
	
	public function searchIRDetail(){
		$return = $this->M_InternalRequest->get_IRDetail_data($this->input->post());	
		switch($return){
			case 1 : echo "error|No record found."; break;
			default : echo $return;
		}
	}
	
	
	public function view(){
		echo $this->M_InternalRequest->get_data($this->input->post());			
	}
	
	public function viewDetail(){
		echo $this->M_InternalRequest->get_DetailData($this->input->post());			
	}
	
	public function actionInternalRequest(){
		$return = $this->M_InternalRequest->action_InternalRequest($this->input->post());
		switch($return){
			case 0 : echo "success|Saving completed."; break;						
			default : echo "error|Could not save.<br>Please contact Support."; break;
		}		
	}
	
	public function getIRApprovalData(){
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username']; 
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;
		
		$data = $this->M_InternalRequest->get_IRApprovalData($this->input->post());
		$return='';
		$ID = $this->input->post('ID');
		if($data == 1){
			$return= '<table width="100%;"> <tr> <td> No Data</td> </tr></table>';
		}else{
			if(($is_manager == 1 ||  $is_PRDManager == 1) && 
				$data['IRData'][0]['ApprovedBy'] == $username && $data['IRData'][0]['CreatedPR'] == 0  &&
				$data['IRData'][0]['Status'] == 1 ){
				$return .='<table width="100%" style="padding:20px 25px"><tr>
								<td align="right">
									<input type="button" id="Approve1-btn" value="Approve" Onclick="fn_ActionApprove(\''.$ID.'\',\''.$is_manager.'\',\'1\',\'\')" />
									<input type="button" id="Reject1-btn" value="Reject" Onclick="fn_RejectIRData(\''.$ID.'\')"/>
								</td></tr>
							</table>';
			}
			$return .='<table id="subDataTable" width="100%;" style="background:#FCFCFC;border:1px solid #eee; padding:20px 25px">
					<thead><tr style="background:#ddd"> 
					<td> PartNumber</td>
					<td> Part Description1</td>
					<td> Part Description2</td>
					<td> Requested Qty</td> 
					<td> UOM</td><td> PRNumber</td> </tr></thead> <tbody>';
		
			foreach($data['IRDetailData'] as $tmpArray){
				$return .= '<tr><td>'. $tmpArray['PartNumber'] .'</td>'; 
				$return .= '<td>'. $tmpArray['PartDesc'] .'</td>'; 
				$return .= '<td>'. $tmpArray['PartDesc1'] .'</td>'; 
				$return .= '<td>'. $tmpArray['RequestedQty'] .'</td>'; 
				$return .= '<td>'. $tmpArray['UOM'] .'</td>';
				$return .= '<td>'. $tmpArray['PRNumber'] .'</td></tr>';
			}

			$return .=  '</tbody></table>';
		}
		
		echo $return;
		// echo '<table width="100%;"> <tr> <td> PR Detail</td> </tr> 
					// <tr> <td> Department</td>
					// <td> Part Description</td>
					// <td> Requested Qty</td> 
					// <td> Unit Price</td> 
					// <td> Requested By</td> </tr> 
				// </table>';
	}
	public function __deconstruct(){
		
	}
}
?>