<?php
class C_POReceiving extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		validate_request();
		$this->load->model('purchase/M_POReceiving');
	}
	
	public function index()
	{		
		$this->load->view('purchase/V_POReceiving');
	}
	
	public function viewPOReceivingDetailData(){
		echo $this->M_POReceiving->view_POReceivingDetailData($this->input->post());
	}
	
	public function viewPOReceivingData(){
		echo $this->M_POReceiving->view_POReceivingData($this->input->post());
	}
	
	public function checkPONumber(){
		echo $this->M_POReceiving->check_PONumber($this->input->post('PONumber'));
	}
	
	
	public function savePOReceivingDetail(){
		$return = $this->M_POReceiving->save_POReceivingDetail($this->input->post());
		switch($return){
			case 0 : echo "success|Successfully Save."; break;						
			case 1 : echo "warning|Kindly check PartNumber Or Quantity."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	public function savePOReceivingSaveAll(){
		$return = $this->M_POReceiving->save_POReceivingSaveAll($this->input->post());
		switch($return){
			case 0 : echo "success|Successfully Save."; break;						
			case 1 : echo "warning|Kindly check PartNumber Or Quantity."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	// public function viewPOReceivingData(){
		// echo $this->M_POReceiving->view_POReceivingData($this->input->post('PONumber'));
	// }
	
	// public function savePOReceivingData(){
		// $return = $this->M_POReceiving->save_POReceivingData($this->input->post());
		// switch($return){
			// case 0 : echo "success|Successfully Save."; break;						
			// case 1 : echo "warning|No record found."; break;
			// default : echo "error|Could not delete.<br>Please contact Support."; break;
		// }		
	// }
	
	public function updatePOReceivingDetail(){
		$return = $this->M_POReceiving->update_POReceivingDetail($this->input->post());
		switch($return){
			case 0 : echo "success|Successfully Save."; break;						
			case 1 : echo "warning|No record found."; break;
			case 3 : echo "warning|SO Number has been created."; break;
			case 4 : echo "warning|Insufficient quantity."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	// public function savePOReceivingMultiData(){
		// $return = $this->M_POReceiving->save_POReceivingMultiData($this->input->post());
		// switch($return){
			// case 0 : echo "success|Successfully Save."; break;						
			// case 1 : echo "warning|No record found."; break;
			// default : echo "error|Could not delete.<br>Please contact Support."; break;
		// }		
	// }
	
	function __destruct(){
		audittrail();
	}
}
?>
