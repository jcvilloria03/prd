<?php
class C_PurchaseOrder extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		validate_request();
		$this->load->model('purchase/M_PurchaseOrder');
	}
	
	public function index()
	{		
		$this->load->view('purchase/V_PurchaseOrder');
	}
	
	public function viewPendingPurchaseData(){
		$return = $this->M_PurchaseOrder->viewPendingPurchase_Data($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	
	public function viewPRData(){
		$return = $this->M_PurchaseOrder->view_PRData();
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	public function viewAllPRAndPRDetailData()
	{
		$return = $this->M_PurchaseOrder->view_AllPRAndPRDetailData($this->input->post('PONumber'));
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	public function getVendorAddress()
	{
		$return = $this->M_PurchaseOrder->get_VendorAddress($this->input->post('VendorID'));
		echo $return;
	}
	public function createPO()
	{
		 $return = $this->M_PurchaseOrder->create_PO($this->input->post('PRNumber'));
		
		switch($return){
			case 0 : echo"success|Create PO complete"; break;
			case 1 : echo "error|PONumber does not exist.";break;
			default : echo "error|could not Create PO. <br> Please contact Support"; break;
		}
	}
	
	public function updatePO(){
		$return = $this->M_PurchaseOrder->update_PO($this->input->post());
		switch($return){
			case 0 : echo "success|Saving Complete";break;
			case 1 : echo "error|PONumber does already exist";break;
			default : echo "error|Could not save. <br> Please contact Support";break;
		}
	}
	
	public function deletePOData(){
		$return = $this->M_PurchaseOrder->delete_POData($this->input->post());
		switch($return){
			case 0 : echo "success|Successfully deleted."; break;						
			case 1 : echo "warning|No record found."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	public function completePOData(){
		$return = $this->M_PurchaseOrder->complete_POData($this->input->post('PONumber'));
		switch($return){
			case 0 : echo "success|Successfully Complete."; break;						
			case 1 : echo "warning|No record found."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
		
	public function unlockPOData(){
		$return = $this->M_PurchaseOrder->unlock_POData($this->input->post('PONumber'));
		switch($return){
			case 1 : echo "success|Successfully Complete."; break;						
			case 2 : echo "warning|No record found."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	public function getPRDetail(){	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username']; 
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;
		
		$data = $this->M_PurchaseOrder->getPRDetail($this->input->post());
		$return='';
		$Approve = '';
		$PRNumber = $this->input->post('PRNumber');
	
		if($data == 1){
			$return= '<table width="100%;"> <tr> <td> No Data</td> </tr></table>';
		}else{
			$return .='<table id="subDataTable"  width="100%;" style="background:#FCFCFC;border:1px solid #eee; padding:20px 25px">
					<thead><tr style="background:#ddd"> 
					<td> Department</td>
					<td> Part Description1</td>
					<td> Part Description2</td>
					<td align="right"> Requested Qty</td>
					<td align="right"> Unit Price</td>';
		
			// if( $is_PRDManager == 1 || $is_manager == 1 || 
					// $is_BM == 1 || $is_GM== 1 || $dep_name == 'PRD' ){
				// $return .= '<td align="right"> Unit Price</td>';
			// }
			// if($data['PRData'][0]['MultiManagerApproval'] == 1){
				// $return .= '<td> Requested By</td>';
			// }
			$return .= '<td align="right">Total Amount</td>';
			$return .= ' </tr></thead> ';
			foreach($data['PRDetailData'] as $tmpArray){
				$return .= '<tr><td>'. $tmpArray['Department'] .'</td>'; 
				$return .= '<td>'. $tmpArray['PartDesc'] .'</td>'; 
				$return .= '<td>'. $tmpArray['PartDesc1'] .'</td>'; 
				$return .= '<td align="right">'. $tmpArray['RequestedQty'] .'</td>';
				$return .= '<td align="right">'. $tmpArray['UnitPrice'] .'</td>';
				$return .= '<td align="right">'. $tmpArray['TotalAmount'] .'</td>';
				// if( $is_PRDManager == 1 || $is_manager == 1 || 
					// $is_BM == 1 || $is_GM== 1 || $dep_name == 'PRD' ){
					// $return .= '<td align="right">'. $tmpArray['UnitPrice'] .'</td>';
				// }
				// if($data['PRData'][0]['MultiManagerApproval'] == 1){
					// $return .= '<td>'. $tmpArray['RequestedByName'] .'</td>';
				// }
				$return .= '</tr>';
			}

			$return .=  '</table>';
		}
		
		echo $return;
		// echo '<table width="100%;"> <tr> <td> PR Detail</td> </tr> 
					// <tr> <td> Department</td>
					// <td> Part Description</td>
					// <td> Requested Qty</td> 
					// <td> Unit Price</td> 
					// <td> Requested By</td> </tr> 
				// </table>';
	}
	
	function __destruct(){
		audittrail();
	}
}
?>