<?php
class C_PurchaseRequisition extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		validate_request();
		$this->load->model('purchase/M_PurchaseRequisition');
	}
	
	public function index()
	{		
		$session_data = $this->session->userdata('logged_in');
		$data['name'] = $session_data['name'];
		$data['username'] = $session_data['username'];
		$data['dep_name'] = $session_data['dep_name'];
		$data['is_manager'] = $session_data['is_manager'];
		$data['is_GM'] = $session_data['is_GM'];
		$data['is_BM'] = $session_data['is_BM'];
		$data['is_PRDManager'] = $session_data['is_PRDManager'];
			
		$data['PRNumber'] = $this->input->post('PRNumber');
		$this->load->view('purchase/V_PurchaseRequisition',$data);
	}
	
	public function viewPRDataAllData(){
		$return = $this->M_PurchaseRequisition->viewPRData_AllData($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	public function PRNumber(){
		echo $this->M_PurchaseRequisition->get_PRNumber($this->input->post());
	}

	public function savePR(){
		$return = $this->M_PurchaseRequisition->save_PR($this->input->post());
		switch($return){
			case 0 : echo "success|Saving completed."; break;						
			case 1 : echo "error|PRNo does not exist."; break;						
			case 2 : echo "error|This Part number has already added under this PRno."; break;						
			default : echo "error|Could not save.<br>Please contact Support."; break;
		}		
	}
	
	public function updatePRD(){
		$return = $this->M_PurchaseRequisition->update_PRD($this->input->post());
		switch($return){
			case 0 : echo "success|Updating completed."; break;						
			case 1 : echo "error|PRNo does not exist."; break;						
			case 2 : echo "error|This Part number has already added under this PRno."; break;						
			default : echo "error|Could not save.<br>Please contact Support."; break;
		}		
	}
	
	public function deletePRDetailData(){
		$return = $this->M_PurchaseRequisition->delete_PRDetailData($this->input->post('ID'));
		switch($return){
			case 0 : echo "success|Successfully deleted."; break;						
			case 1 : echo "warning|No record found."; break;
			case 2 : echo "warning|This item could not delete"; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	public function completePRData(){
		$return = $this->M_PurchaseRequisition->complete_PRData($this->input->post('PRNumber'));
		switch($return){
			case 0 : echo "success|Successfully Complete."; break;						
			case 1 : echo "warning|No record found."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	public function submitPRData(){
		$return = $this->M_PurchaseRequisition->submit_PRData($this->input->post());
		switch($return){
			case 0 : echo "success|Successfully Complete."; break;						
			case 1 : echo "warning|No record found."; break;
			case 3 : echo "warning|Please save the data before you submit PR to PRD Manager."; break;
			case 4 : echo "warning|Please update Unit Price in PRDetail before you submit PR to PRD Manager."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	public function unlockPRData(){
		$return = $this->M_PurchaseRequisition->unlock_PRData($this->input->post('PRNumber'));
		switch($return){
			case 1 : echo "success|Successfully Complete."; break;						
			case 2 : echo "warning|No record found."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	public function deletePRData(){
		$return = $this->M_PurchaseRequisition->delete_PRData($this->input->post());
		switch($return){
			case 0 : echo "success|Successfully deleted."; break;						
			case 1 : echo "warning|No record found."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	public function savePRDetail(){
		$return = $this->M_PurchaseRequisition->save_PRDetail($this->input->post());
		switch($return){
			case 0 : echo "success|Saving completed."; break;						
			case 1 : echo "error|PRNumber Does not exist."; break;
			case 2 : echo "error|Partnumber already exist."; break;
			default : echo "error|Saving failed."; break;
		}		
	}
	
	public function updatePRDetail(){
		$return = $this->M_PurchaseRequisition->update_PRDetail($this->input->post());
		switch($return){
			case 0 : echo "success|Updating completed."; break;						
			case 1 : echo "error|PRNumber does not exist."; break;						
			case 2 : echo "error|This Part number does not exist in this PRno."; break;						
			case 3 : echo "error|This Part number has already added in this PRno."; break;
			case 4 : echo "error|Unable to update Qty. Some PO Qty are already received.";break;
			case 5 : echo "error|Unable to update Partnumber. Some PO Qty are already received.";break;
			default : echo "error|Could not save.<br>Please contact Support."; break;
		}		
	}
	
	public function addInternalRequest(){
		$return = $this->M_PurchaseRequisition->add_InternalRequest($this->input->post());
		switch($return){
			case 0 : echo "success|Updating completed."; break;						
			default : echo "error|Could not save.<br>Please contact Support."; break;
		}		
	}
	
	public function GetPRData(){
		$return = $this->M_PurchaseRequisition->get_PRData($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	public function getPRDetailData(){
		$return = $this->M_PurchaseRequisition->get_PRDetailData($this->input->post('ID'));
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	public function getPartsPrice(){
		echo $this->M_PurchaseRequisition->get_PartsPrice($this->input->post('PartNumber'));
	}
	
	public function viewPRDetailData(){
		echo $this->M_PurchaseRequisition->view_PRDetailData($this->input->post());
	}
	
	public function viewInternalRequest(){
		echo $this->M_PurchaseRequisition->view_InternalRequest($this->input->post());
	}
	
	public function getPRApprovalData(){	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username']; 
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;
		
		$data = $this->M_PurchaseRequisition->get_PRApprovalData($this->input->post());
		$return='';
		$Approve = '';
		$PRNumber = $this->input->post('PRNumber');
		// echo($is_PRDManager);
		// echo($is_manager); 
		// echo($is_BM);
		// echo($is_GM);
		// echo($dep_name);
	
		
		if($data == 1){
			$return= '<table width="100%;"> <tr> <td> No Data</td> </tr></table>';
		}else{
			if($data['PRData'][0]['Remarks'] <>  '')
			{
				$return = '<b> Remarks </b> : '.$data['PRData'][0]['Remarks'] ;
			}
			if($is_PRDManager== 1){
				$type='PRDManager';
			}else if($is_BM == 1 ){
				$type='BM';
			}else if($is_GM == 1){
				$type='GM';
			}else if($is_manager == 1){
				$type='Manager';
			}else{
				$type='';
			}
			$Approve .='<table width="100%" style="padding:20px 25px"><tr>
						<td align="right">
								<input type="button" id="Approve1-btn" value="Approve" OnClick="fn_SubmitPRData(\''.$PRNumber.'\',\''.$type.'\',\'1\',\'\')" />
								<input type="button" id="Reject1-btn" value="Reject" Onclick="fn_RejectPRData(\''.$PRNumber.'\')"/>
						</td></tr>
					</table>';
			if($is_PRDManager == 1 &&  $data['PRData'][0]['PRDSubmitted'] == 1 
				&&  $data['PRData'][0]['PRDSubmitted'] == 1 && $data['PRData'][0]['ManagerApproved']  == 0 ) {
				if($data['PRData'][0]['MultiManagerApproval'] == 0){
					$return .=$Approve;
				}else{
					$app = 0;
					foreach($data['PRDetailData'] as $tmpArray){
						 if($tmpArray['Approved'] == 1){
							$app = 1;
						 }
					}
					
					if($app == 0){
						$return .=$Approve;
					}
				}
			}else if($is_manager == 1 &&  $data['PRData'][0]['PRDApproved'] == 1 && 
				  $data['PRData'][0]['BMApproved'] == 0){
				if($data['PRData'][0]['MultiManagerApproval'] == 0  &&  
					$data['PRData'][0]['ApprovedBy'] == $username ){
					$return .=$Approve;
				}else{
					$app = false;
					foreach($data['PRDetailData'] as $tmpArray){
						 if($tmpArray['ApprovedBy'] == $username){
							$app = true;
						 }
					}
					if($app){
						$return .=$Approve;
					}
				}
			}else if($is_BM == 1 && $data['PRData'][0]['ManagerApproved'] == 1 
											&& $data['PRData'][0]['Status'] == 0 ){
				 $return .=$Approve;
			}else if($is_GM== 1 && $data['PRData'][0]['GMApproved'] == 1 
											&& $data['PRData'][0]['Status'] == 0 ){
				 $return .=$Approve;
			}else{
				$return .='';
			}

			$return .='<table id="subDataTable"  width="100%;" style="background:#FCFCFC;border:1px solid #eee; padding:20px 25px">
					<thead><tr style="background:#ddd"> 
					<td> Department</td>
					<td> Part Description1</td>
					<td> Part Description2</td>
					<td align="right"> Requested Qty</td>';
			
		
			if( $is_PRDManager == 1 || $is_manager == 1 || 
					$is_BM == 1 || $is_GM== 1 || $dep_name == 'PRD' ){
				$return .= '<td align="right"> Unit Price</td>';
				$return .= '<td align="right"> Total Amount</td>';
				$return .= '<td align="right"> Currency</td>';
			}
			if($data['PRData'][0]['MultiManagerApproval'] == 1){
				$return .= '<td> Requested By</td>';
			}
			$return .= ' </tr></thead> ';
			$grandtotal = 0;
			$totalamt = 0;
			foreach($data['PRDetailData'] as $tmpArray){
				$totalamt =  $tmpArray['RequestedQty'] *  $tmpArray['UnitPrice'];
				$grandtotal = $totalamt + $grandtotal;
				$return .= '<tr><td>'. $tmpArray['Department'] .'</td>'; 
				$return .= '<td>'. $tmpArray['PartDesc'] .'</td>'; 
				$return .= '<td>'. $tmpArray['PartDesc1'] .'</td>'; 
				$return .= '<td align="right">'. $tmpArray['RequestedQty'] .'</td>';
				if( $is_PRDManager == 1 || $is_manager == 1 || 
					$is_BM == 1 || $is_GM== 1 || $dep_name == 'PRD' ){
					$return .= '<td align="right">'. $tmpArray['UnitPrice'] .'</td>';
					$return .= '<td align="right">'.$totalamt.'</td>';
					$return .= '<td align="right">'.$tmpArray['Currency'].'</td>';
					$currency = $tmpArray['Currency'];
				}
				
				if($data['PRData'][0]['MultiManagerApproval'] == 1){
					$return .= '<td>'. $tmpArray['RequestedByName'] .'</td>';
				}
				$return .= '</tr>';
			}
			if( $is_PRDManager == 1 || $is_manager == 1 || 
					$is_BM == 1 || $is_GM== 1 || $dep_name == 'PRD' ){
				$colspan=5;
				if($data['PRData'][0]['MultiManagerApproval'] == 1){
						$colspan=6;
				}
				$gst = 0;
				if($data['PRData'][0]['GST'] ==1){
					$gst  = $grandtotal * 0.07;
					$return .= '<tr><td colspan="'.$colspan.'" align="right">GST</td><td  align="right">'. $gst .'</td><td  align="right">'. $currency .'</td></tr>';
				}
				$grandtotal = $grandtotal + $gst;
				$return .= '<tr><td colspan="'.$colspan.'" align="right">PR Total Amount</td><td  align="right">'. $grandtotal  .'</td><td  align="right">'. $currency .'</td></tr>';
			}
			$return .=  '</table>';
		}
		
		echo $return;
		// echo '<table width="100%;"> <tr> <td> PR Detail</td> </tr> 
					// <tr> <td> Department</td>
					// <td> Part Description</td>
					// <td> Requested Qty</td> 
					// <td> Unit Price</td> 
					// <td> Requested By</td> </tr> 
				// </table>';
	}
	function __destruct(){
		audittrail();
	}
}
?>