<?php
class C_SaleOrder extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		validate_request();
		$this->load->model('purchase/M_SaleOrder');
	}
	
	public function index()
	{		
		$session_data = $this->session->userdata('logged_in');
		$data['name'] = $session_data['name'];
		$this->load->view('purchase/V_SaleOrder',$data);
	}
	
	public function getSOData(){
		$return = $this->M_SaleOrder->get_SOData($this->input->post('SONumber'));
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	public function viewSOallData(){
		$return = $this->M_SaleOrder->viewSOData_AllData($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}

	public function viewPODetail(){
		$return = $this->M_SaleOrder->view_PODetail($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	public function viewSODetail(){
		$return = $this->M_SaleOrder->view_SODetail($this->input->post('SONumber'));
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	public function getSONumber(){
		echo $this->M_SaleOrder->get_SONumber($this->input->post('PONumber'));
	}
	
	

	public function createSO()
	{
		 $return = $this->M_SaleOrder->create_SO($this->input->post('SONumber'));
		
		switch($return){
			case 0 : echo"success|Create PO complete"; break;
			case 1 : echo "error|PONumber does not exist.";break;
			default : echo "error|could not Create PO. <br> Please contact Support"; break;
		}
	}
	
	public function saveSODetail()
	{
		 $return = $this->M_SaleOrder->save_SODetail($this->input->post());
		
		switch($return){
			case 0 : echo"success|Adding Complete"; break;
			case 1 : echo "error|PONumber does not exist.";break;
			default : echo "error|could not add. <br> Please contact Support"; break;
		}
	}
	
	public function updateSODetailQty(){
		 $return = $this->M_SaleOrder->update_SODetailQty($this->input->post());
		
		switch($return){
			case 0 : echo"success|Update Complete"; break;
			case 1 : echo "error|PONumber does not exist.";break;
			default : echo "error|could not Create PO. <br> Please contact Support"; break;
		}
	}
	
	public function saveSO(){
		$return = $this->M_SaleOrder->save_SO($this->input->post());
		switch($return){
			case 0 : echo "success|Saving Complete";break;
			case 1 : echo "error|PONumber does already exist";break;
			default : echo "error|Could not save. <br> Please contact Support";break;
		}
	}
	
	public function deleteSOData(){
		$return = $this->M_SaleOrder->delete_SOData($this->input->post());
		switch($return){
			case 0 : echo "success|Successfully deleted."; break;						
			case 1 : echo "warning|No record found."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	public function deleteSODetailData(){
		$return = $this->M_SaleOrder->delete_SODetailData($this->input->post('SODetailID'));
		switch($return){
			case 0 : echo "warning|No record found."; break;
			case 1 : echo "success|Successfully deleted."; break;						
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	public function completeSOData(){
		$return = $this->M_SaleOrder->complete_SOData($this->input->post('SONumber'));
		switch($return){
			case 0 : echo "success|Successfully Complete."; break;						
			case 1 : echo "warning|No record found."; break;
			default : echo "error|Could not Complete.<br>Please contact Support."; break;
		}		
	}
	
	public function unlockSOData(){
		$return = $this->M_SaleOrder->unlock_SOData($this->input->post('SONumber'));
		switch($return){
			case 1 : echo "success|Successfully Complete."; break;						
			case 2 : echo "warning|No record found."; break;
			default : echo "error|Could not UnLock.<br>Please contact Support."; break;
		}		
	}
	
	public function getSODetail(){	
		$data = $this->M_SaleOrder->get_SODetail($this->input->post());
		$return='';
		if($data == 1){
			$return= '<table width="100%;"> <tr> <td> No Data</td> </tr></table>';
		}else{
			$return .='<table id="subDataTable"  width="100%;" style="background:#FCFCFC;border:1px solid #eee; padding:20px 25px">
					<thead><tr style="background:#ddd"> 
					<td> Part Number</td>
					<td> Description</td>
					<td> Description2</td>
					<td align="right"> Quantity</td>
					<td align="right"> Unit Price </td>
					<td align="right"> Total Amount </td>';
			$return .= ' </tr></thead> ';
			
			foreach($data['SODetailData'] as $tmpArray){
				$return .= '<tr><td>'. $tmpArray['SONumber'] .'</td>'; 
				$return .= '<td>'. $tmpArray['Description'] .'</td>'; 
				$return .= '<td>'. $tmpArray['Description2'] .'</td>'; 
				$return .= '<td align="right">'. $tmpArray['Quantity'] .'</td>';
				$return .= '<td align="right">'. $tmpArray['UnitPrice'] .'</td>'; 
				$return .= '<td align="right">'. $tmpArray['TotalQty'] .'</td>';
				$return .= '</tr>';
			}
			$return .=  '</table>';
		}
		echo $return;
	}
	
	function __destruct(){
		audittrail();
	}
}
?>
