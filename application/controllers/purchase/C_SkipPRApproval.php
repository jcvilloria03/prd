<?php
class C_SkipPRApproval extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		validate_request();
		$this->load->model('purchase/M_SkipPRApproval');
	}
	
	public function index()
	{		
		$this->load->view('purchase/V_SkipPRApproval');
	}
	
	public function viewPRDetailData(){
		echo $this->M_SkipPRApproval->view_PRDetailData($this->input->post());
	}
	
	public function viewPRData(){
		echo $this->M_SkipPRApproval->view_PRData($this->input->post());
	}
	
	
	public function actionSkipPRApproval(){
		echo $this->M_SkipPRApproval->action_SkipPRApproval($this->input->post());
	}
	
	function __destruct(){
		audittrail();
	}
}