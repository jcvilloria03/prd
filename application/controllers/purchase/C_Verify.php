<?php
class C_Verify extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		validate_request();
		$this->load->model('purchase/M_Verify');
	}
	
	public function index()
	{		
		$this->load->view('purchase/V_Verify');
	}
	
	
	public function view_SOSummary(){
		$return = $this->M_Verify->get_SOSummaryReport($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	public function view_ReceivingSummary(){
		$return = $this->M_Verify->get_ReceivingSummaryReport($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	public function saveQASaleOrder(){
		$return = $this->M_Verify->save_QASaleOrder($this->input->post());
		echo $return;
	}
	
	public function saveQAReceiving(){
		$return = $this->M_Verify->save_QAReceiving($this->input->post());
		echo $return;
	}
	
	
	public function removeQASaleOrder(){
		$return = $this->M_Verify->remove_QASaleOrder($this->input->post());
			switch($return){
			case 1 : echo "success|Successfully deleted."; break;						
			case 2 : echo "warning|No record found."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	public function removeQAReceiving(){
		$return = $this->M_Verify->remove_QAReceiving($this->input->post());
			switch($return){
			case 1 : echo "success|Successfully deleted."; break;						
			case 2 : echo "warning|No record found."; break;
			default : echo "error|Could not delete.<br>Please contact Support."; break;
		}		
	}
	
	function __destruct(){
		audittrail();
	}
	
}