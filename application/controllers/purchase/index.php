<?php
class Index extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		validate_request();
	}
	
	public function index()
	{
		$data['title'] = 'Purchase';
		$this->load->model('home_model');
		$data['menu'] = $this->home_model->getMenuByProgramIndex('Purchase');
		$this->load->view('purchase/index.php',$data);
	}
	
}