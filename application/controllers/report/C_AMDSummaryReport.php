<?php
class C_AMDSummaryReport extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('report/M_AMDSummaryReport');
	}
	
	public function index(){
		$this->load->view('report/V_AMDSummaryReport');
	}
	
	public function view_SOSummary(){
		$return = $this->M_AMDSummaryReport->get_SOSummaryReport($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	public function view_POSummaryReport(){
		$return = $this->M_AMDSummaryReport->get_POSummaryReport($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	public function view_POOpeningSummaryReport(){
		$return = $this->M_AMDSummaryReport->get_POOpeningSummaryReport($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	public function saveReportData(){
		$return = $this->M_AMDSummaryReport->save_ReportData($this->input->post());
		echo $return;
	}
	
	public function exportSummaryReport(){
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('test worksheet');
		//set cell A1 content with some text
		
		$this->excel->getActiveSheet()->setCellValue('A1', 'Purchase Order Summary');
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->mergeCells('A1:L1');
		
		$this->excel->getActiveSheet()->setCellValue('A2', 'S/N');
		$this->excel->getActiveSheet()->setCellValue('B2', 'Vender Name');
		$this->excel->getActiveSheet()->setCellValue('C2', 'D/O');
		$this->excel->getActiveSheet()->setCellValue('D2', 'PartNumber');
		$this->excel->getActiveSheet()->setCellValue('E2', 'PartDesc');
		$this->excel->getActiveSheet()->setCellValue('F2', 'Qty');
		$this->excel->getActiveSheet()->setCellValue('G2', 'Unit Price (S$)');
		$this->excel->getActiveSheet()->setCellValue('H2', 'Unit Price (US$)');
		$this->excel->getActiveSheet()->setCellValue('I2', 'GST (S$)');
		$this->excel->getActiveSheet()->setCellValue('J2', 'GST (US$)');
		$this->excel->getActiveSheet()->setCellValue('K2', 'Amt (S$)');
		$this->excel->getActiveSheet()->setCellValue('L2', 'Amt (US$)');
		
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
		
		$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('K2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('F2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('G2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('H2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('I2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('J2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('K2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('L2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		
		$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('E2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('F2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('G2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('H2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('I2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('J2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('K2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('L2')->getFont()->setBold(true);
		
		$return = $this->M_AMDSummaryReport->get_POSummaryReport($this->input->post());
		
		$filename='PO_Summary.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
		
	}
	
}