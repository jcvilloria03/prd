<?php
class C_BMGMSummaryReport extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('report/M_BMGMSummaryReport');
	}
	
	public function index(){
		$this->load->view('report/V_BMGMSummaryReport');
	}
	
	public function view_PurchaseFromSupplier(){
		$return = $this->M_BMGMSummaryReport->get_PurchaseFromSupplier($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	public function view_BillingToCustomer(){
		$return = $this->M_BMGMSummaryReport->get_BillingToCustomer($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	
	public function view_JSICost4Depat(){
		$return = $this->M_BMGMSummaryReport->get_JSICost4Depat($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	public function getSummaryData(){
		$ret = $this->M_BMGMSummaryReport->get_SummaryData($this->input->post());
		print_r($ret);
		return $ret;
	}
	
	
	
	
}