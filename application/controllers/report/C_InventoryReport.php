<?php
class C_InventoryReport extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('report/M_InventoryReport');
	}
	
	public function index(){
		$this->load->view('report/V_InventoryReport');
	}
	
	
	public function viewInventoryReport(){
		$return = $this->M_InventoryReport->viewInventoryReport($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
}