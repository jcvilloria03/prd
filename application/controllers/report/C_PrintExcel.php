<?php
class C_PrintExcel extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function Inventory4WeeklyUsuageReport()
	{
		$this->load->model('report/M_InventoryReport');
		$data =  $this->M_InventoryReport->InventoryExportExcel($this->input->post());

		if($data=='2'){
			die("Record Not Found");
		}
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		$this->excel->getActiveSheet()->setTitle('test worksheet');
		//set cell A1 content with some text
		$this->excel->getActiveSheet()->setCellValue('A1', 'Packaging Material List');
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->mergeCells('A1:E1');
		
		$this->excel->getActiveSheet()->setCellValue('A2', 'S/N');
		$this->excel->getActiveSheet()->setCellValue('B2', 'Part No');
		$this->excel->getActiveSheet()->setCellValue('C2', 'Part Description');
		$this->excel->getActiveSheet()->setCellValue('D2', 'Supplier');
		$this->excel->getActiveSheet()->setCellValue('E2', 'LeadTime');
		
		$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(10);
		$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(50);
		$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
	
		$this->excel->getActiveSheet()->getStyle('A2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('B2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('C2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('D2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$this->excel->getActiveSheet()->getStyle('E2')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
		$this->excel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('B2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('C2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('D2')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('E2')->getFont()->setBold(true);
		
		//change the font size
		//$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(20);
		//make the font become bold
		//$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		//merge cell A1 until D1
		//$this->excel->getActiveSheet()->mergeCells('A1:D1');
		//set aligment to center for that merged cell (A1 to D1)
		//$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		 
		
		$reporttype = $this->input->post("Type");
		
		$currChar = 'F';
	
		foreach($data["Header"] as $tmpArray){
			$this->excel->getActiveSheet()->setCellValue($currChar .'1', $tmpArray);
			$this->excel->getActiveSheet()->getStyle($currChar .'1')->getFont()->setBold(true);
			if($reporttype==1){
				$this->excel->getActiveSheet()->setCellValue($currChar .'2', 'Usuage');
				$this->excel->getActiveSheet()->getStyle($currChar .'2')->getFont()->setBold(true);

				$currChar = chr(ord($currChar)+1);
			}else{
				$megcurrChar = chr(ord($currChar)+2);
				$this->excel->getActiveSheet()->mergeCells($currChar .'1:'.$megcurrChar.'1');
				
				$this->excel->getActiveSheet()->setCellValue($currChar .'2', 'Order');
				$this->excel->getActiveSheet()->setCellValue(chr(ord($currChar)+1) .'2', 'Usuage');
				$this->excel->getActiveSheet()->setCellValue(chr(ord($currChar)+2) .'2', 'Count');
				
				$this->excel->getActiveSheet()->getStyle($currChar .'2')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle(chr(ord($currChar)+1) .'2')->getFont()->setBold(true);
				$this->excel->getActiveSheet()->getStyle(chr(ord($currChar)+2) .'2')->getFont()->setBold(true);
			
				$this->excel->getActiveSheet()->getStyle($currChar .'1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
				$currChar = chr(ord($currChar)+3);
			}
			
		}
		
		
		$CurrentLine = 3;
		
		$sn = 1;
		$oldsn = 0;
		foreach($data["Body"] as $tmpArray){
			foreach($tmpArray[0] as $val){

				$partno =  $val[0];
				$partdesc= $val[1];
				$leadtime =  $val[2];
				//$supplier =  $val[3];
				$orderqty =  $val[4];
				$usuage =  $val[5];
				$countqty =  $val[6];
				$supplier =  $val[7];
				if($sn!=$oldsn){
					$oldsn=$sn;
		
					$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, $sn);
					$this->excel->getActiveSheet()->setCellValue('B'.$CurrentLine, $partno);
					$this->excel->getActiveSheet()->setCellValue('C'.$CurrentLine, $partdesc);
					$this->excel->getActiveSheet()->setCellValue('D'.$CurrentLine, $supplier);
					$this->excel->getActiveSheet()->setCellValue('E'.$CurrentLine, $leadtime);
					$currChar = 'E';
					//table = table  + '<td>' + sn +'</td><td>'+partno+'</td><td>'+partdesc+'</td><td>' + supplier +'</td><td>'+leadtime+'</td>';
				}
				$currChar = chr(ord($currChar)+1);
				if($reporttype== '1'){
					$this->excel->getActiveSheet()->setCellValue($currChar.$CurrentLine, $usuage);
				}else{
					$this->excel->getActiveSheet()->setCellValue($currChar.$CurrentLine, $orderqty);
					$currChar = chr(ord($currChar)+1);
					$this->excel->getActiveSheet()->setCellValue($currChar.$CurrentLine, $usuage);
					$currChar = chr(ord($currChar)+1);
					$this->excel->getActiveSheet()->setCellValue($currChar.$CurrentLine, $countqty);
				}
			}
			$sn = $sn + 1;
			$CurrentLine = $CurrentLine+1;
		}
		
		
		
		$filename='Inventory_Report.xls'; //save our workbook as this file name
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
					 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
	}
	
	
	public function exportSummaryReport(){
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		//load our new PHPExcel library
		$this->load->library('excel');
		//activate worksheet number 1
		$this->excel->setActiveSheetIndex(0);
		//name the worksheet
		
		$headerColor = '92d050';
		$selectedColor ='FFFF00';
		$mysqltime = date('Y-m-d');
		$defaultnumberformat ='0.00';
		$threenumberformat ='0.000';
		$fournumberformat ='0.0000';
		if($this->input->post('reporttype') ==''){
			die('Try Again');
		}else if($this->input->post('reporttype') =='POSummary'){ 
			$this->excel->getActiveSheet()->setTitle('Opening Order Report');
			//set cell A1 content with some text
			$CurrentLine = 1;
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, 'Opening Order Report');
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A'.$CurrentLine.':K'.$CurrentLine);
			$this->cellColor('A'.$CurrentLine,$headerColor);
		
			$CurrentLine++;
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, 'S/N');
			$this->excel->getActiveSheet()->setCellValue('B'.$CurrentLine, 'PONumber');
			$this->excel->getActiveSheet()->setCellValue('C'.$CurrentLine, 'PR Created Date');
			$this->excel->getActiveSheet()->setCellValue('D'.$CurrentLine, 'Vender Name');
			$this->excel->getActiveSheet()->setCellValue('E'.$CurrentLine, 'Oustanding Qty');
			$this->excel->getActiveSheet()->setCellValue('F'.$CurrentLine, 'PartNumber');
			$this->excel->getActiveSheet()->setCellValue('G'.$CurrentLine, 'PartDesc');
			$this->excel->getActiveSheet()->setCellValue('H'.$CurrentLine, 'Unit Price');
			$this->excel->getActiveSheet()->setCellValue('I'.$CurrentLine, 'GST');
			$this->excel->getActiveSheet()->setCellValue('J'.$CurrentLine, 'Total Amount');
			$this->excel->getActiveSheet()->setCellValue('K'.$CurrentLine, 'Currency');
			
			$this->cellColor('A'.$CurrentLine,$headerColor);
			$this->cellColor('B'.$CurrentLine,$headerColor);
			$this->cellColor('C'.$CurrentLine,$headerColor);
			$this->cellColor('D'.$CurrentLine,$headerColor);
			$this->cellColor('E'.$CurrentLine,$headerColor);
			$this->cellColor('F'.$CurrentLine,$headerColor);
			$this->cellColor('G'.$CurrentLine,$headerColor);
			$this->cellColor('H'.$CurrentLine,$headerColor);
			$this->cellColor('I'.$CurrentLine,$headerColor);
			$this->cellColor('J'.$CurrentLine,$headerColor);
			$this->cellColor('K'.$CurrentLine,$headerColor);
				
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(40);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
			
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('B'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('C'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('D'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('E'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('F'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

			
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('B'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('C'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('D'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('E'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('F'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('G'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('H'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('I'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getFont()->setBold(true);
			
			$this->load->model('report/M_PRDSummaryReport');
			
			$data = $this->M_PRDSummaryReport->exportSummaryReport($this->input->post());
			//print_r($data );
			$CurrentLine++;
			$SN = 1;
			foreach ($data as $obj) { 
				$UnitPrice = ($obj['UnitPrice']==0)?'-':$obj['UnitPrice'];
				$GST = ($obj['GST']==0)?'-':$obj['GST'];
				$TotalAmount = ($obj['TotalAmount']==0)?'-':$obj['TotalAmount'];

				
				$this->excel->getActiveSheet()->getStyle('E'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle('H'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle('I'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

				$this->excel->getActiveSheet()->getStyle('H'.$CurrentLine)->getNumberFormat()->setFormatCode($threenumberformat);
				$this->excel->getActiveSheet()->getStyle('I'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				
				$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, $SN);
				$this->excel->getActiveSheet()->setCellValue('B'.$CurrentLine, $obj['PONumber']);
				$this->excel->getActiveSheet()->setCellValue('C'.$CurrentLine, $obj['CompletedPODate']);
				$this->excel->getActiveSheet()->setCellValue('D'.$CurrentLine, $obj['VendorName']);
				$this->excel->getActiveSheet()->setCellValue('E'.$CurrentLine, $obj['POBalanceQty']);
				if($obj['PRType'] == 'JSIEXPENSE'){
					$this->excel->getActiveSheet()->setCellValue('F'.$CurrentLine, '');
					$this->excel->getActiveSheet()->setCellValue('G'.$CurrentLine, $obj['PartNumber'] .' '. $obj['PartDesc1']);
				}else{
					$this->excel->getActiveSheet()->setCellValue('F'.$CurrentLine, $obj['PartNumber']);
					$this->excel->getActiveSheet()->setCellValue('G'.$CurrentLine, $obj['PartDesc1']);
				}
				$this->excel->getActiveSheet()->setCellValue('H'.$CurrentLine, $UnitPrice);
				$this->excel->getActiveSheet()->setCellValue('I'.$CurrentLine, $GST);
				$this->excel->getActiveSheet()->setCellValue('J'.$CurrentLine, $TotalAmount);
				$this->excel->getActiveSheet()->setCellValue('K'.$CurrentLine, $obj['Currency']);
				$CurrentLine++;
				$SN++;
			} 
			
			//print_r($data );
			$filename='Opening_Order'.$mysqltime.'.xls'; //save our workbook as this file name
		}else if($this->input->post('reporttype') =='SOSummary'){ 
		
		
			$this->excel->getActiveSheet()->setTitle('SO Summary Report');
			//set cell A1 content with some text
			$CurrentLine = 1;
			
		
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A'.$CurrentLine.':S'.$CurrentLine);
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine,'JSI LOGISTICS (S) PTE LTD');
			//$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine.':S'.$CurrentLine)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

			$CurrentLine++;
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A'.$CurrentLine.':S'.$CurrentLine);
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine,'Packaging Materials - Sales Summary Report');
			//$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine.':S'.$CurrentLine)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
		
			$CurrentLine++;
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A'.$CurrentLine.':S'.$CurrentLine);
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine,'From - '.$this->input->post('from').' to '. $this->input->post('to'));
			//$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine.':S'.$CurrentLine)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

					
			$CurrentLine++;
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, 'Sale Order Summary Report');
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->cellColor('A'.$CurrentLine,$headerColor);
			$this->excel->getActiveSheet()->mergeCells('A'.$CurrentLine.':S'.$CurrentLine);
			$this->borderColor('A'.$CurrentLine.':S'.$CurrentLine);
			//$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine.':S'.$CurrentLine)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

					
			$CurrentLine++;
			$this->excel->getActiveSheet()->mergeCells('J'.$CurrentLine.':N'.$CurrentLine);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('J'.$CurrentLine, 'Cost Of Sale');
			$this->cellColor('J'.$CurrentLine,$headerColor);
			$this->borderColor('J'.$CurrentLine.':N'.$CurrentLine);
			//$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine.':S'.$CurrentLine)->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);

					
			$this->excel->getActiveSheet()->mergeCells('O'.$CurrentLine.':S'.$CurrentLine);
			$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('O'.$CurrentLine, 'Selling');
			$this->cellColor('O'.$CurrentLine,$headerColor);
			$this->borderColor('O'.$CurrentLine.':S'.$CurrentLine);
				
			
			
			// $this->cellColor('A'.$CurrentLine,$headerColor);
			// $this->cellColor('B'.$CurrentLine,$headerColor);
			// $this->cellColor('C'.$CurrentLine,$headerColor);
			// $this->cellColor('D'.$CurrentLine,$headerColor);
			// $this->cellColor('E'.$CurrentLine,$headerColor);
			// $this->cellColor('F'.$CurrentLine,$headerColor);
			// $this->cellColor('G'.$CurrentLine,$headerColor);
			// $this->cellColor('H'.$CurrentLine,$headerColor);
			// $this->cellColor('I'.$CurrentLine,$headerColor);
			
			$CurrentLine++;
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, 'S/N');
			$this->excel->getActiveSheet()->setCellValue('B'.$CurrentLine, 'Customer');
			$this->excel->getActiveSheet()->setCellValue('C'.$CurrentLine, 'P/O #');
			$this->excel->getActiveSheet()->setCellValue('D'.$CurrentLine, 'Venders');
			$this->excel->getActiveSheet()->setCellValue('E'.$CurrentLine, 'Inv#');
			$this->excel->getActiveSheet()->setCellValue('F'.$CurrentLine, 'D/O Date');
			$this->excel->getActiveSheet()->setCellValue('G'.$CurrentLine, 'Qty');
			$this->excel->getActiveSheet()->setCellValue('H'.$CurrentLine, 'P/N');
			$this->excel->getActiveSheet()->setCellValue('I'.$CurrentLine, 'Description');
			$this->excel->getActiveSheet()->setCellValue('J'.$CurrentLine, 'Currency');
			$this->excel->getActiveSheet()->setCellValue('K'.$CurrentLine, 'Unit');
			$this->excel->getActiveSheet()->setCellValue('L'.$CurrentLine, 'Cost');
			$this->excel->getActiveSheet()->setCellValue('M'.$CurrentLine, 'GST 7%');
			$this->excel->getActiveSheet()->setCellValue('N'.$CurrentLine, 'Total');
			$this->excel->getActiveSheet()->setCellValue('O'.$CurrentLine, 'Unit');
			$this->excel->getActiveSheet()->setCellValue('P'.$CurrentLine, 'Sale');
			$this->excel->getActiveSheet()->setCellValue('Q'.$CurrentLine, 'Admin');
			$this->excel->getActiveSheet()->setCellValue('R'.$CurrentLine, 'GST	7%');
			$this->excel->getActiveSheet()->setCellValue('S'.$CurrentLine, 'Total');
			
			$this->borderColor('A'.$CurrentLine);
			$this->borderColor('B'.$CurrentLine);
			$this->borderColor('C'.$CurrentLine);
			$this->borderColor('D'.$CurrentLine);
			$this->borderColor('E'.$CurrentLine);
			$this->borderColor('F'.$CurrentLine);
			$this->borderColor('G'.$CurrentLine);
			$this->borderColor('H'.$CurrentLine);
			$this->borderColor('I'.$CurrentLine);
			$this->borderColor('J'.$CurrentLine);
			$this->borderColor('K'.$CurrentLine);
			$this->borderColor('L'.$CurrentLine);
			$this->borderColor('M'.$CurrentLine);
			$this->borderColor('N'.$CurrentLine);
			$this->borderColor('O'.$CurrentLine);
			$this->borderColor('P'.$CurrentLine);
			$this->borderColor('Q'.$CurrentLine);
			$this->borderColor('R'.$CurrentLine);
			$this->borderColor('S'.$CurrentLine);
			
			$this->cellColor('A'.$CurrentLine,$headerColor);
			$this->cellColor('B'.$CurrentLine,$headerColor);
			$this->cellColor('C'.$CurrentLine,$headerColor);
			$this->cellColor('D'.$CurrentLine,$headerColor);
			$this->cellColor('E'.$CurrentLine,$headerColor);
			$this->cellColor('F'.$CurrentLine,$headerColor);
			$this->cellColor('G'.$CurrentLine,$headerColor);
			$this->cellColor('H'.$CurrentLine,$headerColor);
			$this->cellColor('I'.$CurrentLine,$headerColor);
			$this->cellColor('J'.$CurrentLine,$headerColor);
			$this->cellColor('K'.$CurrentLine,$headerColor);
			$this->cellColor('L'.$CurrentLine,$headerColor);
			$this->cellColor('M'.$CurrentLine,$headerColor);
			$this->cellColor('N'.$CurrentLine,$headerColor);
			$this->cellColor('O'.$CurrentLine,$headerColor);
			$this->cellColor('P'.$CurrentLine,$headerColor);
			$this->cellColor('Q'.$CurrentLine,$headerColor);
			$this->cellColor('R'.$CurrentLine,$headerColor);
			$this->cellColor('S'.$CurrentLine,$headerColor);
			
			
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(60);
			$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(10);
			
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('B'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('C'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('D'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('E'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('F'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('N'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('P'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('Q'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('R'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('S'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('B'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('C'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('D'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('E'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('F'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('G'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('H'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('I'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('L'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('M'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('N'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('P'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('Q'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('R'.$CurrentLine)->getFont()->setBold(true);
		
			$this->load->model('report/M_PRDSummaryReport');
			
			$data = $this->M_PRDSummaryReport->exportSummaryReport($this->input->post());
			
			$CurrentLine++;
			$SN=1;
	
			foreach ($data as $obj) { 

				$BPrice = ($obj['BPrice']==0)?'-':$obj['BPrice'];
				$BCurrency = ($obj['BCurrency']=='')?'-':$obj['BCurrency'];
				$BuyAmount = ($obj['BuyAmount']==0)?'-':$obj['BuyAmount'];
				$BuyGST = ($obj['BuyGST']==0)?'-':$obj['BuyGST'];
				$BuyTotalAmount = ($obj['BuyTotalAmount']==0)?'-':$obj['BuyTotalAmount'];
				//$BCurrency = ($obj['BCurrency']==0)?'-':$obj['BCurrency'];
				
				$SPrice = ($obj['SPrice']==0)?'-':$obj['SPrice'];
				$SCurrency = ($obj['SCurrency']=='')?'-':$obj['SCurrency'];
				$SellAmount = ($obj['SellAmount']==0)?'-':$obj['SellAmount'];
				//$SCurrency = ($obj['SCurrency']==0)?'-':$obj['SCurrency'];
				$AdminCost = ($obj['AdminCost']==0)?'-':$obj['AdminCost'];
				$GST = ($obj['GST']==0)?'-':$obj['GST'];
				$TotalAmount = ($obj['TotalAmount']==0)?'-':$obj['TotalAmount'];
				//$SCurrency = ($obj['SCurrency']==0)?'-':$obj['SCurrency'];
				if($obj['QAConfirm'] == '1'){
					$this->cellColor('A'.$CurrentLine,$selectedColor);
					$this->cellColor('B'.$CurrentLine,$selectedColor);
					$this->cellColor('C'.$CurrentLine,$selectedColor);
					$this->cellColor('D'.$CurrentLine,$selectedColor);
					$this->cellColor('E'.$CurrentLine,$selectedColor);
					$this->cellColor('F'.$CurrentLine,$selectedColor);
					$this->cellColor('G'.$CurrentLine,$selectedColor);
					$this->cellColor('H'.$CurrentLine,$selectedColor);
					$this->cellColor('I'.$CurrentLine,$selectedColor);
					$this->cellColor('J'.$CurrentLine,$selectedColor);
					$this->cellColor('K'.$CurrentLine,$selectedColor);
					$this->cellColor('L'.$CurrentLine,$selectedColor);
					$this->cellColor('M'.$CurrentLine,$selectedColor);
					$this->cellColor('N'.$CurrentLine,$selectedColor);
					$this->cellColor('O'.$CurrentLine,$selectedColor);
					$this->cellColor('P'.$CurrentLine,$selectedColor);
					$this->cellColor('Q'.$CurrentLine,$selectedColor);
					$this->cellColor('R'.$CurrentLine,$selectedColor);
					$this->cellColor('S'.$CurrentLine,$selectedColor);
				}
				$this->borderColor('A'.$CurrentLine);
				$this->borderColor('B'.$CurrentLine);
				$this->borderColor('C'.$CurrentLine);
				$this->borderColor('D'.$CurrentLine);
				$this->borderColor('E'.$CurrentLine);
				$this->borderColor('F'.$CurrentLine);
				$this->borderColor('G'.$CurrentLine);
				$this->borderColor('H'.$CurrentLine);
				$this->borderColor('I'.$CurrentLine);
				$this->borderColor('J'.$CurrentLine);
				$this->borderColor('K'.$CurrentLine);
				$this->borderColor('L'.$CurrentLine);
				$this->borderColor('M'.$CurrentLine);
				$this->borderColor('N'.$CurrentLine);
				$this->borderColor('O'.$CurrentLine);
				$this->borderColor('P'.$CurrentLine);
				$this->borderColor('Q'.$CurrentLine);
				$this->borderColor('R'.$CurrentLine);
				$this->borderColor('S'.$CurrentLine);
				
				$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, $SN);
				$this->excel->getActiveSheet()->setCellValue('B'.$CurrentLine, $obj['CustomerName']);
				$this->excel->getActiveSheet()->setCellValue('C'.$CurrentLine, $obj['SONumber']);
				$this->excel->getActiveSheet()->setCellValue('D'.$CurrentLine, $obj['VendorName']);
				$this->excel->getActiveSheet()->setCellValue('E'.$CurrentLine, $obj['Invoice']);
				$this->excel->getActiveSheet()->setCellValue('F'.$CurrentLine, $obj['DeliveryDate']);
				$this->excel->getActiveSheet()->setCellValue('G'.$CurrentLine, $obj['SOQty']);
				$this->excel->getActiveSheet()->setCellValue('H'.$CurrentLine, $obj['PartNumber']);
				$this->excel->getActiveSheet()->setCellValue('I'.$CurrentLine, $obj['Description']);
				$this->excel->getActiveSheet()->setCellValue('J'.$CurrentLine, $BCurrency);
				$this->excel->getActiveSheet()->setCellValue('K'.$CurrentLine, $BPrice);
				$this->excel->getActiveSheet()->setCellValue('L'.$CurrentLine, $BuyAmount);
				$this->excel->getActiveSheet()->setCellValue('M'.$CurrentLine, $BuyGST);
				$this->excel->getActiveSheet()->setCellValue('N'.$CurrentLine, $BuyTotalAmount);
				$this->excel->getActiveSheet()->setCellValue('O'.$CurrentLine, $SPrice);
				$dataMoldCost = array();
				$dataMoldCost = $this->M_PRDSummaryReport->getMoldCost($obj['SONumber']);
				
				if (count($dataMoldCost) < 1 and isset($dataMoldCost)){
					$this->excel->getActiveSheet()->setCellValue('P'.$CurrentLine, $SellAmount);
				} else {
					$this->excel->getActiveSheet()->setCellValue('P'.$CurrentLine, '=O'.$CurrentLine.'*G'.$CurrentLine);
				}
				
				if (count($dataMoldCost) < 1 and isset($dataMoldCost)){
					$this->excel->getActiveSheet()->setCellValue('Q'.$CurrentLine, $AdminCost);
				} else {
					$this->excel->getActiveSheet()->setCellValue('Q'.$CurrentLine, '=P'.$CurrentLine.'*0.1');
				}
				
				if (count($dataMoldCost) < 1 and isset($dataMoldCost)){
					$this->excel->getActiveSheet()->setCellValue('R'.$CurrentLine, $GST);
				} else {
					$this->excel->getActiveSheet()->setCellValue('R'.$CurrentLine, '=(P'.$CurrentLine.'+Q'.$CurrentLine.')*0.07');
				}

				if (count($dataMoldCost) < 1 and isset($dataMoldCost)){
					$this->excel->getActiveSheet()->setCellValue('S'.$CurrentLine, $TotalAmount);
				} else {
					$this->excel->getActiveSheet()->setCellValue('S'.$CurrentLine, '=P'.$CurrentLine.'*1.1*1.07');
				}
						
				//$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getNumberFormat()->setFormatCode($threenumberformat );
				$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
				$this->excel->getActiveSheet()->getStyle('L'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('M'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('N'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getNumberFormat()->setFormatCode($threenumberformat );
				$this->excel->getActiveSheet()->getStyle('P'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('Q'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('R'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('S'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				
				$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine.':S'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
				$CurrentLine++;
				$SN++;
				
				/** mold Cost **/
				foreach ($dataMoldCost as $objMoldCost) {
					$this->borderColor('A'.$CurrentLine);
					$this->borderColor('B'.$CurrentLine);
					$this->borderColor('C'.$CurrentLine);
					$this->borderColor('D'.$CurrentLine);
					$this->borderColor('E'.$CurrentLine);
					$this->borderColor('F'.$CurrentLine);
					$this->borderColor('G'.$CurrentLine);
					$this->borderColor('H'.$CurrentLine);
					$this->borderColor('I'.$CurrentLine);
					$this->borderColor('J'.$CurrentLine);
					$this->borderColor('K'.$CurrentLine);
					$this->borderColor('L'.$CurrentLine);
					$this->borderColor('M'.$CurrentLine);
					$this->borderColor('N'.$CurrentLine);
					$this->borderColor('O'.$CurrentLine);
					$this->borderColor('P'.$CurrentLine);
					$this->borderColor('Q'.$CurrentLine);
					$this->borderColor('R'.$CurrentLine);
					$this->borderColor('S'.$CurrentLine);
					
					$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, $SN);
					$this->excel->getActiveSheet()->setCellValue('B'.$CurrentLine, $obj['CustomerName']);
					$this->excel->getActiveSheet()->setCellValue('C'.$CurrentLine, $obj['SONumber']);
					$this->excel->getActiveSheet()->setCellValue('D'.$CurrentLine, $obj['VendorName']);
					$this->excel->getActiveSheet()->setCellValue('E'.$CurrentLine, $obj['Invoice']);
					$this->excel->getActiveSheet()->setCellValue('F'.$CurrentLine, $obj['DeliveryDate']);
					$this->excel->getActiveSheet()->setCellValue('G'.$CurrentLine, '1');
					$this->excel->getActiveSheet()->setCellValue('H'.$CurrentLine, 'N/A');
					$this->excel->getActiveSheet()->setCellValue('I'.$CurrentLine, 'Mold Cost');
					$this->excel->getActiveSheet()->setCellValue('J'.$CurrentLine, $BCurrency);
					$this->excel->getActiveSheet()->setCellValue('K'.$CurrentLine, '-');
					$this->excel->getActiveSheet()->setCellValue('L'.$CurrentLine, '-');
					
					$this->excel->getActiveSheet()->setCellValue('M'.$CurrentLine, '-');
					$this->excel->getActiveSheet()->setCellValue('N'.$CurrentLine, '-');
					$this->excel->getActiveSheet()->setCellValue('O'.$CurrentLine, $objMoldCost['OneTimeCharge']);
					$this->excel->getActiveSheet()->setCellValue('P'.$CurrentLine, '=O'.$CurrentLine.'*G'.$CurrentLine);
					$this->excel->getActiveSheet()->setCellValue('Q'.$CurrentLine, '=P'.$CurrentLine.'*0.1');
					$this->excel->getActiveSheet()->setCellValue('R'.$CurrentLine, '=(P'.$CurrentLine.'+Q'.$CurrentLine.')*0.07');
					$this->excel->getActiveSheet()->setCellValue('S'.$CurrentLine, '=P'.$CurrentLine.'*1.1*1.07');
					
					$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine.':S'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getNumberFormat()->setFormatCode($threenumberformat );
					$this->excel->getActiveSheet()->getStyle('P'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
					$this->excel->getActiveSheet()->getStyle('Q'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
					$this->excel->getActiveSheet()->getStyle('R'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
					$this->excel->getActiveSheet()->getStyle('S'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
					
					$CurrentLine++;
					$SN++;
				}
				/***************/
				
			}

	
			
			//print_r($data );
			$filename='SaleOrderSummary'.$mysqltime.'.xls'; //save our workbook as this file name
		}else if($this->input->post('reporttype') =='SOCNSummary'){ 
		
		
			$this->excel->getActiveSheet()->setTitle('SO with CN Summary Report');
			//set cell A1 content with some text
			$CurrentLine = 1;
			
			//set cell A1 content with some text
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A'.$CurrentLine.':S'.$CurrentLine);
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine,'JSI LOGISTICS (S) PTE LTD');

			$CurrentLine++;
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A'.$CurrentLine.':S'.$CurrentLine);
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine,'Packaging Materials - Sales Summary w/ CN Report');

			$CurrentLine++;
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A'.$CurrentLine.':S'.$CurrentLine);
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine,'From - '.$this->input->post('from').' to '. $this->input->post('to'));
					
			$CurrentLine++;
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, 'Sale Order Summary Report');
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->cellColor('A'.$CurrentLine,$headerColor);
			$this->excel->getActiveSheet()->mergeCells('A'.$CurrentLine.':S'.$CurrentLine);
			$this->borderColor('A'.$CurrentLine.':S'.$CurrentLine);
					
			$CurrentLine++;
			$this->excel->getActiveSheet()->mergeCells('J'.$CurrentLine.':N'.$CurrentLine);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('J'.$CurrentLine, 'Cost Of Sale');
			$this->cellColor('J'.$CurrentLine,$headerColor);
			$this->borderColor('J'.$CurrentLine.':N'.$CurrentLine);
		
			$this->excel->getActiveSheet()->mergeCells('O'.$CurrentLine.':S'.$CurrentLine);
			$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('O'.$CurrentLine, 'Selling');
			$this->cellColor('O'.$CurrentLine,$headerColor);
			$this->borderColor('O'.$CurrentLine.':S'.$CurrentLine);			
			
			$CurrentLine++;
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, 'S/N');
			$this->excel->getActiveSheet()->setCellValue('B'.$CurrentLine, 'Customer');
			$this->excel->getActiveSheet()->setCellValue('C'.$CurrentLine, 'P/O #');
			$this->excel->getActiveSheet()->setCellValue('D'.$CurrentLine, 'Venders');
			$this->excel->getActiveSheet()->setCellValue('E'.$CurrentLine, 'Inv#');
			$this->excel->getActiveSheet()->setCellValue('F'.$CurrentLine, 'D/O Date');
			$this->excel->getActiveSheet()->setCellValue('G'.$CurrentLine, 'Qty');
			$this->excel->getActiveSheet()->setCellValue('H'.$CurrentLine, 'P/N');
			$this->excel->getActiveSheet()->setCellValue('I'.$CurrentLine, 'Description');
			$this->excel->getActiveSheet()->setCellValue('J'.$CurrentLine, 'Currency');
			$this->excel->getActiveSheet()->setCellValue('K'.$CurrentLine, 'Unit');
			$this->excel->getActiveSheet()->setCellValue('L'.$CurrentLine, 'Cost');
			$this->excel->getActiveSheet()->setCellValue('M'.$CurrentLine, 'GST 7%');
			$this->excel->getActiveSheet()->setCellValue('N'.$CurrentLine, 'Total');
			$this->excel->getActiveSheet()->setCellValue('O'.$CurrentLine, 'Unit');
			$this->excel->getActiveSheet()->setCellValue('P'.$CurrentLine, 'Sale');
			$this->excel->getActiveSheet()->setCellValue('Q'.$CurrentLine, 'Admin');
			$this->excel->getActiveSheet()->setCellValue('R'.$CurrentLine, 'GST	7%');
			$this->excel->getActiveSheet()->setCellValue('S'.$CurrentLine, 'Total');			
			$this->excel->getActiveSheet()->setCellValue('T'.$CurrentLine, 'B Price');
			$this->excel->getActiveSheet()->setCellValue('U'.$CurrentLine, 'S Price');
			$this->excel->getActiveSheet()->setCellValue('V'.$CurrentLine, 'CN');
			$this->excel->getActiveSheet()->setCellValue('W'.$CurrentLine, 'Difference');
			$this->excel->getActiveSheet()->setCellValue('X'.$CurrentLine, 'GST %7');
			$this->excel->getActiveSheet()->setCellValue('Y'.$CurrentLine, 'Total');
			
			$this->borderColor('A'.$CurrentLine);
			$this->borderColor('B'.$CurrentLine);
			$this->borderColor('C'.$CurrentLine);
			$this->borderColor('D'.$CurrentLine);
			$this->borderColor('E'.$CurrentLine);
			$this->borderColor('F'.$CurrentLine);
			$this->borderColor('G'.$CurrentLine);
			$this->borderColor('H'.$CurrentLine);
			$this->borderColor('I'.$CurrentLine);
			$this->borderColor('J'.$CurrentLine);
			$this->borderColor('K'.$CurrentLine);
			$this->borderColor('L'.$CurrentLine);
			$this->borderColor('M'.$CurrentLine);
			$this->borderColor('N'.$CurrentLine);
			$this->borderColor('O'.$CurrentLine);
			$this->borderColor('P'.$CurrentLine);
			$this->borderColor('Q'.$CurrentLine);
			$this->borderColor('R'.$CurrentLine);
			$this->borderColor('S'.$CurrentLine);			
			$this->borderColor('T'.$CurrentLine);
			$this->borderColor('U'.$CurrentLine);
			$this->borderColor('V'.$CurrentLine);
			$this->borderColor('W'.$CurrentLine);
			$this->borderColor('X'.$CurrentLine);
			$this->borderColor('Y'.$CurrentLine);
			
			$this->cellColor('A'.$CurrentLine,$headerColor);
			$this->cellColor('B'.$CurrentLine,$headerColor);
			$this->cellColor('C'.$CurrentLine,$headerColor);
			$this->cellColor('D'.$CurrentLine,$headerColor);
			$this->cellColor('E'.$CurrentLine,$headerColor);
			$this->cellColor('F'.$CurrentLine,$headerColor);
			$this->cellColor('G'.$CurrentLine,$headerColor);
			$this->cellColor('H'.$CurrentLine,$headerColor);
			$this->cellColor('I'.$CurrentLine,$headerColor);
			$this->cellColor('J'.$CurrentLine,$headerColor);
			$this->cellColor('K'.$CurrentLine,$headerColor);
			$this->cellColor('L'.$CurrentLine,$headerColor);
			$this->cellColor('M'.$CurrentLine,$headerColor);
			$this->cellColor('N'.$CurrentLine,$headerColor);
			$this->cellColor('O'.$CurrentLine,$headerColor);
			$this->cellColor('P'.$CurrentLine,$headerColor);
			$this->cellColor('Q'.$CurrentLine,$headerColor);
			$this->cellColor('R'.$CurrentLine,$headerColor);
			$this->cellColor('S'.$CurrentLine,$headerColor);			
			$this->cellColor('T'.$CurrentLine,$headerColor);
			$this->cellColor('U'.$CurrentLine,$headerColor);
			$this->cellColor('V'.$CurrentLine,$headerColor);
			$this->cellColor('W'.$CurrentLine,$headerColor);
			$this->cellColor('X'.$CurrentLine,$headerColor);			
			$this->cellColor('Y'.$CurrentLine,$headerColor);			
			
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(40);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
			$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(60);
			$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('P')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('Q')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('R')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('S')->setWidth(10);			
			$this->excel->getActiveSheet()->getColumnDimension('T')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('U')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('V')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('W')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('X')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('Y')->setWidth(10);
			
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('B'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('C'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('D'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('E'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('F'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('G'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('N'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('P'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('Q'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('R'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('S'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);			
			$this->excel->getActiveSheet()->getStyle('T'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('U'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('V'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('W'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('X'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('Y'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('B'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('C'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('D'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('E'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('F'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('G'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('H'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('I'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('L'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('M'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('N'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('P'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('Q'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('R'.$CurrentLine)->getFont()->setBold(true);			
			$this->excel->getActiveSheet()->getStyle('T'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('U'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('V'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('W'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('X'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('Y'.$CurrentLine)->getFont()->setBold(true);
		
			$this->load->model('report/M_PRDSummaryReport');
			
			$data = $this->M_PRDSummaryReport->exportSummaryReport($this->input->post());
			
			$CurrentLine++;
			$SN=1;
	
			foreach ($data as $obj) { 

				$BPrice = ($obj['BPrice']==0)?'-':$obj['BPrice'];
				$BCurrency = ($obj['BCurrency']=='')?'-':$obj['BCurrency'];
				$BuyAmount = ($obj['BuyAmount']==0)?'-':$obj['BuyAmount'];
				$BuyGST = ($obj['BuyGST']==0)?'-':$obj['BuyGST'];
				$BuyTotalAmount = ($obj['BuyTotalAmount']==0)?'-':$obj['BuyTotalAmount'];
				//$BCurrency = ($obj['BCurrency']==0)?'-':$obj['BCurrency'];
				
				$SPrice = ($obj['SPrice']==0)?'-':$obj['SPrice'];
				$SCurrency = ($obj['SCurrency']=='')?'-':$obj['SCurrency'];
				$SellAmount = ($obj['SellAmount']==0)?'-':$obj['SellAmount'];
				//$SCurrency = ($obj['SCurrency']==0)?'-':$obj['SCurrency'];
				$AdminCost = ($obj['AdminCost']==0)?'-':$obj['AdminCost'];
				$GST = ($obj['GST']==0)?'-':$obj['GST'];
				$TotalAmount = ($obj['TotalAmount']==0)?'-':$obj['TotalAmount'];
				//$SCurrency = ($obj['SCurrency']==0)?'-':$obj['SCurrency'];
				if($obj['QAConfirm'] == '1'){
					$this->cellColor('A'.$CurrentLine,$selectedColor);
					$this->cellColor('B'.$CurrentLine,$selectedColor);
					$this->cellColor('C'.$CurrentLine,$selectedColor);
					$this->cellColor('D'.$CurrentLine,$selectedColor);
					$this->cellColor('E'.$CurrentLine,$selectedColor);
					$this->cellColor('F'.$CurrentLine,$selectedColor);
					$this->cellColor('G'.$CurrentLine,$selectedColor);
					$this->cellColor('H'.$CurrentLine,$selectedColor);
					$this->cellColor('I'.$CurrentLine,$selectedColor);
					$this->cellColor('J'.$CurrentLine,$selectedColor);
					$this->cellColor('K'.$CurrentLine,$selectedColor);
					$this->cellColor('L'.$CurrentLine,$selectedColor);
					$this->cellColor('M'.$CurrentLine,$selectedColor);
					$this->cellColor('N'.$CurrentLine,$selectedColor);
					$this->cellColor('O'.$CurrentLine,$selectedColor);
					$this->cellColor('P'.$CurrentLine,$selectedColor);
					$this->cellColor('Q'.$CurrentLine,$selectedColor);
					$this->cellColor('R'.$CurrentLine,$selectedColor);
					$this->cellColor('S'.$CurrentLine,$selectedColor);
				}
				$this->borderColor('A'.$CurrentLine);
				$this->borderColor('B'.$CurrentLine);
				$this->borderColor('C'.$CurrentLine);
				$this->borderColor('D'.$CurrentLine);
				$this->borderColor('E'.$CurrentLine);
				$this->borderColor('F'.$CurrentLine);
				$this->borderColor('G'.$CurrentLine);
				$this->borderColor('H'.$CurrentLine);
				$this->borderColor('I'.$CurrentLine);
				$this->borderColor('J'.$CurrentLine);
				$this->borderColor('K'.$CurrentLine);
				$this->borderColor('L'.$CurrentLine);
				$this->borderColor('M'.$CurrentLine);
				$this->borderColor('N'.$CurrentLine);
				$this->borderColor('O'.$CurrentLine);
				$this->borderColor('P'.$CurrentLine);
				$this->borderColor('Q'.$CurrentLine);
				$this->borderColor('R'.$CurrentLine);
				$this->borderColor('S'.$CurrentLine);
				
				$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, $SN);
				$this->excel->getActiveSheet()->setCellValue('B'.$CurrentLine, $obj['CustomerName']);
				$this->excel->getActiveSheet()->setCellValue('C'.$CurrentLine, $obj['SONumber']);
				$this->excel->getActiveSheet()->setCellValue('D'.$CurrentLine, $obj['VendorName']);
				$this->excel->getActiveSheet()->setCellValue('E'.$CurrentLine, $obj['Invoice']);
				$this->excel->getActiveSheet()->setCellValue('F'.$CurrentLine, $obj['DeliveryDate']);
				$this->excel->getActiveSheet()->setCellValue('G'.$CurrentLine, $obj['SOQty']);
				$this->excel->getActiveSheet()->setCellValue('H'.$CurrentLine, $obj['PartNumber']);
				$this->excel->getActiveSheet()->setCellValue('I'.$CurrentLine, $obj['Description']);
				$this->excel->getActiveSheet()->setCellValue('J'.$CurrentLine, $BCurrency);
				$this->excel->getActiveSheet()->setCellValue('K'.$CurrentLine, $BPrice);
				$this->excel->getActiveSheet()->setCellValue('L'.$CurrentLine, $BuyAmount);
				$this->excel->getActiveSheet()->setCellValue('M'.$CurrentLine, $BuyGST);
				$this->excel->getActiveSheet()->setCellValue('N'.$CurrentLine, $BuyTotalAmount);
				$this->excel->getActiveSheet()->setCellValue('O'.$CurrentLine, $SPrice);				
				$this->excel->getActiveSheet()->setCellValue('T'.$CurrentLine, $obj['LoadingPrice']);
				$this->excel->getActiveSheet()->setCellValue('U'.$CurrentLine, $obj['BasePrice']);
				$this->excel->getActiveSheet()->setCellValue('V'.$CurrentLine, $obj['CreditNote']);
				$this->excel->getActiveSheet()->setCellValue('W'.$CurrentLine, ($obj['SOQty'] * $obj['CreditNote']));
				$this->excel->getActiveSheet()->setCellValue('X'.$CurrentLine, (($obj['SOQty'] * $obj['CreditNote']))*0.07);
				$this->excel->getActiveSheet()->setCellValue('Y'.$CurrentLine, ($obj['SOQty'] * $obj['CreditNote']) + ((($obj['SOQty'] * $obj['CreditNote']))*0.07));
				
				$dataMoldCost = array();
				$dataMoldCost = $this->M_PRDSummaryReport->getMoldCost($obj['SONumber']);
				
				if (count($dataMoldCost) < 1 and isset($dataMoldCost)){
					$this->excel->getActiveSheet()->setCellValue('P'.$CurrentLine, $SellAmount);
				} else {
					$this->excel->getActiveSheet()->setCellValue('P'.$CurrentLine, '=O'.$CurrentLine.'*G'.$CurrentLine);
				}
				
				if (count($dataMoldCost) < 1 and isset($dataMoldCost)){
					$this->excel->getActiveSheet()->setCellValue('Q'.$CurrentLine, $AdminCost);
				} else {
					$this->excel->getActiveSheet()->setCellValue('Q'.$CurrentLine, '=P'.$CurrentLine.'*0.1');
				}
				
				if (count($dataMoldCost) < 1 and isset($dataMoldCost)){
					$this->excel->getActiveSheet()->setCellValue('R'.$CurrentLine, $GST);
				} else {
					$this->excel->getActiveSheet()->setCellValue('R'.$CurrentLine, '=(P'.$CurrentLine.'+Q'.$CurrentLine.')*0.07');
				}

				if (count($dataMoldCost) < 1 and isset($dataMoldCost)){
					$this->excel->getActiveSheet()->setCellValue('S'.$CurrentLine, $TotalAmount);
				} else {
					$this->excel->getActiveSheet()->setCellValue('S'.$CurrentLine, '=P'.$CurrentLine.'*1.1*1.07');
				}
						
				//$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getNumberFormat()->setFormatCode($threenumberformat );
				$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
				$this->excel->getActiveSheet()->getStyle('L'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('M'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('N'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getNumberFormat()->setFormatCode($threenumberformat );
				$this->excel->getActiveSheet()->getStyle('P'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('Q'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('R'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('S'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );				
				$this->excel->getActiveSheet()->getStyle('T'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('U'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('V'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('W'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('X'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('Y'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				
				$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine.':S'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		
				$CurrentLine++;
				$SN++;
				
				/** mold Cost **/
				foreach ($dataMoldCost as $objMoldCost) {
					$this->borderColor('A'.$CurrentLine);
					$this->borderColor('B'.$CurrentLine);
					$this->borderColor('C'.$CurrentLine);
					$this->borderColor('D'.$CurrentLine);
					$this->borderColor('E'.$CurrentLine);
					$this->borderColor('F'.$CurrentLine);
					$this->borderColor('G'.$CurrentLine);
					$this->borderColor('H'.$CurrentLine);
					$this->borderColor('I'.$CurrentLine);
					$this->borderColor('J'.$CurrentLine);
					$this->borderColor('K'.$CurrentLine);
					$this->borderColor('L'.$CurrentLine);
					$this->borderColor('M'.$CurrentLine);
					$this->borderColor('N'.$CurrentLine);
					$this->borderColor('O'.$CurrentLine);
					$this->borderColor('P'.$CurrentLine);
					$this->borderColor('Q'.$CurrentLine);
					$this->borderColor('R'.$CurrentLine);
					$this->borderColor('S'.$CurrentLine);					
					$this->borderColor('T'.$CurrentLine);
					$this->borderColor('U'.$CurrentLine);
					$this->borderColor('V'.$CurrentLine);
					$this->borderColor('W'.$CurrentLine);
					$this->borderColor('X'.$CurrentLine);
					$this->borderColor('Y'.$CurrentLine);
					
					$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, $SN);
					$this->excel->getActiveSheet()->setCellValue('B'.$CurrentLine, $obj['CustomerName']);
					$this->excel->getActiveSheet()->setCellValue('C'.$CurrentLine, $obj['SONumber']);
					$this->excel->getActiveSheet()->setCellValue('D'.$CurrentLine, $obj['VendorName']);
					$this->excel->getActiveSheet()->setCellValue('E'.$CurrentLine, $obj['Invoice']);
					$this->excel->getActiveSheet()->setCellValue('F'.$CurrentLine, $obj['DeliveryDate']);
					$this->excel->getActiveSheet()->setCellValue('G'.$CurrentLine, '1');
					$this->excel->getActiveSheet()->setCellValue('H'.$CurrentLine, 'N/A');
					$this->excel->getActiveSheet()->setCellValue('I'.$CurrentLine, 'Mold Cost');
					$this->excel->getActiveSheet()->setCellValue('J'.$CurrentLine, $BCurrency);
					$this->excel->getActiveSheet()->setCellValue('K'.$CurrentLine, '-');
					$this->excel->getActiveSheet()->setCellValue('L'.$CurrentLine, '-');
					
					$this->excel->getActiveSheet()->setCellValue('M'.$CurrentLine, '-');
					$this->excel->getActiveSheet()->setCellValue('N'.$CurrentLine, '-');
					$this->excel->getActiveSheet()->setCellValue('O'.$CurrentLine, $objMoldCost['OneTimeCharge']);
					$this->excel->getActiveSheet()->setCellValue('P'.$CurrentLine, '=O'.$CurrentLine.'*G'.$CurrentLine);
					$this->excel->getActiveSheet()->setCellValue('Q'.$CurrentLine, '=P'.$CurrentLine.'*0.1');
					$this->excel->getActiveSheet()->setCellValue('R'.$CurrentLine, '=(P'.$CurrentLine.'+Q'.$CurrentLine.')*0.07');
					$this->excel->getActiveSheet()->setCellValue('S'.$CurrentLine, '=P'.$CurrentLine.'*1.1*1.07');
					$this->excel->getActiveSheet()->setCellValue('T'.$CurrentLine, $obj['LoadingPrice']);
					$this->excel->getActiveSheet()->setCellValue('U'.$CurrentLine, $obj['BasePrice']);
					$this->excel->getActiveSheet()->setCellValue('V'.$CurrentLine, $obj['CreditNote']);
					$this->excel->getActiveSheet()->setCellValue('W'.$CurrentLine, ($obj['SOQty'] * $obj['CreditNote']));
					$this->excel->getActiveSheet()->setCellValue('X'.$CurrentLine, (($obj['SOQty'] * $obj['CreditNote']))*0.07);
					$this->excel->getActiveSheet()->setCellValue('Y'.$CurrentLine, ($obj['SOQty'] * $obj['CreditNote']) + ((($obj['SOQty'] * $obj['CreditNote']))*0.07));
					
					$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine.':S'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
					$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getNumberFormat()->setFormatCode($threenumberformat );
					$this->excel->getActiveSheet()->getStyle('P'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
					$this->excel->getActiveSheet()->getStyle('Q'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
					$this->excel->getActiveSheet()->getStyle('R'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
					$this->excel->getActiveSheet()->getStyle('S'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );					
					$this->excel->getActiveSheet()->getStyle('T'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
					$this->excel->getActiveSheet()->getStyle('U'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
					$this->excel->getActiveSheet()->getStyle('V'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
					$this->excel->getActiveSheet()->getStyle('W'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
					$this->excel->getActiveSheet()->getStyle('X'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
					$this->excel->getActiveSheet()->getStyle('Y'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
					
					$CurrentLine++;
					$SN++;
				}
				/***************/
				
			}

	
			
			//print_r($data );
			$filename='SaleOrderCNSummary'.$mysqltime.'.xls'; //save our workbook as this file name
		}else if($this->input->post('reporttype') =='POOpeningSummary'){ 
			$this->excel->getActiveSheet()->setTitle('Received Summary Report');
			//set cell A1 content with some text
			$CurrentLine = 1;
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, 'Received Summary Report');
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->mergeCells('A'.$CurrentLine.':O'.$CurrentLine);
					
			$this->cellColor('A'.$CurrentLine,$headerColor);
			
			
			$CurrentLine++;
			$this->excel->getActiveSheet()->mergeCells('J'.$CurrentLine.':K'.$CurrentLine);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('J'.$CurrentLine, 'Unit Price');
			
			$this->excel->getActiveSheet()->mergeCells('L'.$CurrentLine.':M'.$CurrentLine);
			$this->excel->getActiveSheet()->getStyle('L'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('L'.$CurrentLine, 'GST');

			$this->excel->getActiveSheet()->mergeCells('N'.$CurrentLine.':O'.$CurrentLine);
			$this->excel->getActiveSheet()->getStyle('N'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('N'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->setCellValue('N'.$CurrentLine, 'Total Amount');
			
			$this->cellColor('J'.$CurrentLine,$headerColor);
			$this->cellColor('L'.$CurrentLine,$headerColor);
			$this->cellColor('N'.$CurrentLine,$headerColor);

			$CurrentLine++;
			$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, 'S/N');
			$this->excel->getActiveSheet()->setCellValue('B'.$CurrentLine, 'PO #');
			$this->excel->getActiveSheet()->setCellValue('C'.$CurrentLine, 'Department');
			$this->excel->getActiveSheet()->setCellValue('D'.$CurrentLine, 'Vendo Name');
			$this->excel->getActiveSheet()->setCellValue('E'.$CurrentLine, 'Delivery Date');
			$this->excel->getActiveSheet()->setCellValue('F'.$CurrentLine, 'Inv#');
			$this->excel->getActiveSheet()->setCellValue('G'.$CurrentLine, 'Qty');
			$this->excel->getActiveSheet()->setCellValue('H'.$CurrentLine, 'P/N');
			$this->excel->getActiveSheet()->setCellValue('I'.$CurrentLine, 'Description');
			$this->excel->getActiveSheet()->setCellValue('J'.$CurrentLine, 'Unit Price (S$)');
			$this->excel->getActiveSheet()->setCellValue('K'.$CurrentLine, 'Unit Price (US$)');
			$this->excel->getActiveSheet()->setCellValue('L'.$CurrentLine, 'GST (S$)');
			$this->excel->getActiveSheet()->setCellValue('M'.$CurrentLine, 'GST (US$)');
			$this->excel->getActiveSheet()->setCellValue('N'.$CurrentLine, 'Amt (S$)');
			$this->excel->getActiveSheet()->setCellValue('O'.$CurrentLine, 'Amt (US$)');
			
					
			$this->cellColor('A'.$CurrentLine,$headerColor);
			$this->cellColor('B'.$CurrentLine,$headerColor);
			$this->cellColor('C'.$CurrentLine,$headerColor);
			$this->cellColor('D'.$CurrentLine,$headerColor);
			$this->cellColor('E'.$CurrentLine,$headerColor);
			$this->cellColor('F'.$CurrentLine,$headerColor);
			$this->cellColor('G'.$CurrentLine,$headerColor);
			$this->cellColor('H'.$CurrentLine,$headerColor);
			$this->cellColor('I'.$CurrentLine,$headerColor);
			$this->cellColor('J'.$CurrentLine,$headerColor);
			$this->cellColor('K'.$CurrentLine,$headerColor);
			$this->cellColor('L'.$CurrentLine,$headerColor);
			$this->cellColor('M'.$CurrentLine,$headerColor);
			$this->cellColor('N'.$CurrentLine,$headerColor);
			$this->cellColor('O'.$CurrentLine,$headerColor);
			
			$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
			$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(30);
			$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
			$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(40);
			$this->excel->getActiveSheet()->getColumnDimension('I')->setWidth(50);
			$this->excel->getActiveSheet()->getColumnDimension('J')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('K')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('L')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('M')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('N')->setWidth(10);
			$this->excel->getActiveSheet()->getColumnDimension('O')->setWidth(10);
			
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('B'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('C'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('D'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('E'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('F'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$this->excel->getActiveSheet()->getStyle('G'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('H'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('I'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('L'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('M'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('N'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
			
			$this->excel->getActiveSheet()->getStyle('A'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('B'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('C'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('D'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('E'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('F'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('G'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('H'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('I'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('L'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('M'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('N'.$CurrentLine)->getFont()->setBold(true);
			$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getFont()->setBold(true);
			
			$this->load->model('report/M_PRDSummaryReport');
			
			$data = $this->M_PRDSummaryReport->exportSummaryReport($this->input->post());
			// print_r($data );
			// die();
			$CurrentLine++;
			$SN=1;
			foreach ($data as $obj) { 
				if($obj['QAConfirm'] == '1'){
					$this->cellColor('A'.$CurrentLine,$selectedColor);
					$this->cellColor('B'.$CurrentLine,$selectedColor);
					$this->cellColor('C'.$CurrentLine,$selectedColor);
					$this->cellColor('D'.$CurrentLine,$selectedColor);
					$this->cellColor('E'.$CurrentLine,$selectedColor);
					$this->cellColor('F'.$CurrentLine,$selectedColor);
					$this->cellColor('G'.$CurrentLine,$selectedColor);
					$this->cellColor('H'.$CurrentLine,$selectedColor);
					$this->cellColor('I'.$CurrentLine,$selectedColor);
					$this->cellColor('J'.$CurrentLine,$selectedColor);
					$this->cellColor('K'.$CurrentLine,$selectedColor);
					$this->cellColor('L'.$CurrentLine,$selectedColor);
					$this->cellColor('M'.$CurrentLine,$selectedColor);
					$this->cellColor('N'.$CurrentLine,$selectedColor);
					$this->cellColor('O'.$CurrentLine,$selectedColor);
				}
				
				$SGD_UnitPrice = ($obj['SGD_UnitPrice']==0)?'-':$obj['SGD_UnitPrice'];
				$USD_UnitPrice = ($obj['USD_UnitPrice']==0)?'-':$obj['USD_UnitPrice'];
				
				$SGD_GST = ($obj['SGD_GST']==0)?'-':$obj['SGD_GST'];
				$USD_GST = ($obj['USD_GST']==0)?'-':$obj['USD_GST'];
				
				$SGD_TotalAmount = ($obj['SGD_TotalAmount']==0)?'-':$obj['SGD_TotalAmount'];
				$USD_TotalAmount = ($obj['USD_TotalAmount']==0)?'-':$obj['USD_TotalAmount'];
				
				$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle('L'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle('M'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle('N'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);
				
				$this->excel->getActiveSheet()->getStyle('J'.$CurrentLine)->getNumberFormat()->setFormatCode($fournumberformat );
				$this->excel->getActiveSheet()->getStyle('K'.$CurrentLine)->getNumberFormat()->setFormatCode($threenumberformat );
				$this->excel->getActiveSheet()->getStyle('L'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('M'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('N'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				$this->excel->getActiveSheet()->getStyle('O'.$CurrentLine)->getNumberFormat()->setFormatCode($defaultnumberformat );
				
				$this->excel->getActiveSheet()->setCellValue('A'.$CurrentLine, $SN);
				$this->excel->getActiveSheet()->setCellValue('B'.$CurrentLine, $obj['PONumber']);
				$this->excel->getActiveSheet()->setCellValue('C'.$CurrentLine, $obj['Department']);
				$this->excel->getActiveSheet()->setCellValue('D'.$CurrentLine, $obj['VendorName']);
				$this->excel->getActiveSheet()->setCellValue('E'.$CurrentLine, $obj['DeliveryDate']);				
				$this->excel->getActiveSheet()->setCellValue('F'.$CurrentLine, $obj['Invoice']);
				$this->excel->getActiveSheet()->setCellValue('G'.$CurrentLine, $obj['Qty']);
				if($obj['PRType'] == 'JSIEXPENSE'){
					$this->excel->getActiveSheet()->setCellValue('H'.$CurrentLine, '');
					$this->excel->getActiveSheet()->setCellValue('I'.$CurrentLine, $obj['PartNumber'] .' '. $obj['PartDesc1']);
				}else{
					$this->excel->getActiveSheet()->setCellValue('H'.$CurrentLine, $obj['PartNumber']);
					$this->excel->getActiveSheet()->setCellValue('I'.$CurrentLine, $obj['PartDesc1']);
				}
				$this->excel->getActiveSheet()->setCellValue('J'.$CurrentLine, $SGD_UnitPrice);
				$this->excel->getActiveSheet()->setCellValue('K'.$CurrentLine, $USD_UnitPrice);
				$this->excel->getActiveSheet()->setCellValue('L'.$CurrentLine, $SGD_GST);
				$this->excel->getActiveSheet()->setCellValue('M'.$CurrentLine, $USD_GST);
				$this->excel->getActiveSheet()->setCellValue('N'.$CurrentLine, $SGD_TotalAmount);
				$this->excel->getActiveSheet()->setCellValue('O'.$CurrentLine, $USD_TotalAmount);
				
				$CurrentLine++;
				$SN++;
			} 
			//print_r($data );
			$filename='ReceivingSummary'.$mysqltime.'.xls'; //save our workbook as this file name
		}
		
		header('Content-Type: application/vnd.ms-excel'); //mime type
		header('Content-Disposition: attachment;filename="'.$filename.'"'); //tell browser what's the file name
		header('Cache-Control: max-age=0'); //no cache
		 
		//save it to Excel5 format (excel 2003 .XLS file), change this to 'Excel2007' (and adjust the filename extension, also the header mime type)
		//if you want to save it as .XLSX Excel 2007 format
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');  
		//force user to download the Excel file without writing it to server's HD
		$objWriter->save('php://output');
		
	}
	
	function cellColor($cells,$color){
       	$this->excel->getActiveSheet()->getStyle($cells)->getFill()->applyFromArray(
			array(
			'type'       => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array('rgb' => $color)
			)
		);
    }
	
	function borderColor($cells){
		$styleArray = array(
			   'borders' => array(
					 'outline' => array(
							'style' => PHPExcel_Style_Border::BORDER_THIN,
							'color' => array('argb' => '0000000'),
					 ),
			   ),
		);
       	$this->excel->getActiveSheet()->getStyle($cells)->applyFromArray($styleArray);
    }
	
}