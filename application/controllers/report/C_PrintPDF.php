<?php
class C_PrintPDF extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->helper(array('dompdf', 'file'));
		$this->load->model('Report/M_PrintPDF');
		
	}
	
	public function index()
	{	
		
		$html = 'test';
		//$html = $this->load->view('report/PurchaseRequisition/PRReport', $data, true);
		//$html = file_get_content('http://www.prd.loc/report/PurchaseRequisition/PRReport');
		pdf_create($html, 'filename','A4','landscape',false);
		
	}
	
	public function PRReport()
	{
		
		ini_set('memory_limit','2048M');
		$url = parse_url($_SERVER['REQUEST_URI']);
		$array =  explode("/", $url['path']);
		$key = array_pop(array_keys($array));
		$PRNumber =  $array[$key-1]."/".$array[$key];
		$data = $this->M_PrintPDF->loadPRData($PRNumber);
		
		// var_dump($data);
		// die($data);
		
		//$temp_array['PRData'] = $data['PRData'];
		//$temp_array['PRDetail'] = array_chunk($data['PRDetail'],10,true);

		$this->load->library('mpdf/mpdf');
		$this->mpdf->mPDF('',    // mode - default ''
					 'A4-L',    // format - A4, for example, default ''
					 0,     // font size - default 0
					 '',    // default font family
					 5,    // margin_left
					 5,    // margin right
					 5,     // margin top
					 5,    // margin bottom
					 5,     // margin header
					 5,     // margin footer
					 'L');  // L - landscape, P - portrait
		if (!isset($data['PRDetail'])){
			die();
		}

		$tempArrays = array_chunk($data['PRDetail'],10,true);
		$i = 1;
		$len = count($tempArrays);
		foreach($tempArrays as $tmpArray){
			$temp_array['PRData'] = $data['PRData'];
			$temp_array['PRDetail'] = $tmpArray;
			// echo "<pre>";$temp_array['PRData']->FirstRow ='1';
			// print_r($temp_array);
			 // echo "</pre>";
			 // die();
			if ($i == 1) {
				$temp_array['PRData']->FirstPage ='1';
			}else{
				$temp_array['PRData']->FirstPage ='0';
			}
			if ($i == $len) {
				$temp_array['PRData']->LastPage ='1';
			}else{
				$temp_array['PRData']->LastPage ='0';
			}
			
			$temp_array['PRData']->PageNumber = $i;
			$temp_array['PRData']->PageCount = $len;
			$temp_array['PRData']->PatchGST = $this->M_PrintPDF->GetPatchGST($PRNumber);
			$temp_array['PRData']->PatchGSTAmount = $this->M_PrintPDF->GetPatchGSTAMOUNT($PRNumber);
			$html = $this->load->view('Report/V_PRReport',$temp_array,true);
			//select GST,TotalAmount from tblPatchPRPODetails where PDDetailID = 'J309/13'
			$this->mpdf->WriteHTML($html);
			if ($tmpArray === end($tempArrays)){
			
			}else{
			$this->mpdf->AddPage('','NEXT-ODD','','','','','','','','','','','','','',-1,-1,-1,-1);
			}
			//pdf_create($html, 'POReport','A4','landscape',false);
			unset($temp_array);
			$i++;
		}
		$this->mpdf->Output();

		// echo "<pre>";
		// print_r( $temp_array);
		// echo "</pre>";
		// $html = $this->load->view('Report/V_PRReport',$data,true);
		// pdf_create($html, 'POReport','A4','portrait',false);
		
	}

	public function PRWithoutPriceReport()
	{
		ini_set('memory_limit','512M');
		$url = parse_url($_SERVER['REQUEST_URI']);
		$array =  explode("/", $url['path']);
		$key = array_pop(array_keys($array));
		$PRNumber =  $array[$key-1]."/".$array[$key];
		$data = $this->M_PrintPDF->loadPRData($PRNumber);
	
		$this->load->library('mpdf/mpdf');
		$this->mpdf->mPDF('',    // mode - default ''
					 'A4-L',    // format - A4, for example, default ''
					 0,     // font size - default 0
					 '',    // default font family
					 5,    // margin_left
					 5,    // margin right
					 5,     // margin top
					 5,    // margin bottom
					 5,     // margin header
					 5,     // margin footer
					 'L');  // L - landscape, P - portrait
		if (!isset($data['PRDetail'])){
			die();
		}
		
		$tempArrays = array_chunk($data['PRDetail'],10,true);
		$i = 1;
		$len = count($tempArrays);
		foreach($tempArrays as $tmpArray){
			$temp_array['PRData'] = $data['PRData'];
			$temp_array['PRDetail'] = $tmpArray;
			if ($i == 1) {
				$temp_array['PRData']->FirstPage ='1';
			}else{
				$temp_array['PRData']->FirstPage ='0';
			}
			if ($i == $len) {
				$temp_array['PRData']->LastPage ='1';
			}else{
				$temp_array['PRData']->LastPage ='0';
			}
			
			$temp_array['PRData']->PageNumber = $i;
			$temp_array['PRData']->PageCount = $len;
			$html = $this->load->view('Report/V_PRWithoutPriceReport',$temp_array,true);
			

			$this->mpdf->WriteHTML($html);
			if ($tmpArray === end($tempArrays)){
			
			}else{
			$this->mpdf->AddPage('','NEXT-ODD','','','','','','','','','','','','','',-1,-1,-1,-1);
			}
			unset($temp_array);
			$i++;
		}
		$this->mpdf->Output();


		}
	
	public function POReport()
	{
		ini_set('memory_limit','1024M');
		$url = parse_url($_SERVER['REQUEST_URI']);
		$array =  explode("/", $url['path']);
		$key = array_pop(array_keys($array));
		$PONumber =  $array[$key-1]."/".$array[$key];
		$data = $this->M_PrintPDF->loadPOData($PONumber);
		$this->load->library('mpdf/mpdf');
		$this->mpdf->mPDF('',    // mode - default ''
					 'A4',    // format - A4, for example, default ''
					 0,     // font size - default 0
					 '',    // default font family
					 5,    // margin_left
					 5,    // margin right
					 5,     // margin top
					 5,    // margin bottom
					 5,     // margin header
					 5,     // margin footer
					 'P');  // L - landscape, P - portrait
		
		$data['PRData']->PatchGST = $this->M_PrintPDF->GetPatchGST($PONumber);
		$data['PRData']->PatchGSTAmount = $this->M_PrintPDF->GetPatchGSTAMOUNT($PONumber);
		
		$html = $this->load->view('Report/V_POReport',$data,true);
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output();
	}
	
	public function POReportDomPDF()
	{
		$url = parse_url($_SERVER['REQUEST_URI']);
		$array =  explode("/", $url['path']);
		$key = array_pop(array_keys($array));
		$PONumber =  $array[$key-1]."/".$array[$key];
		//$this->load->model('Report/M_PurchaseOrder');
		$data = $this->M_PrintPDF->loadPOData($PONumber);
		//$this->load->view('Report/V_POReport',$data,true);
		
		$html = $this->load->view('Report/V_POReport_domPDF',$data,true);
		pdf_create($html, 'POReport','A4','portrait',false);
	}
	
	public function SOReport()
	{
		ini_set('memory_limit','1024M');
		$url = parse_url($_SERVER['REQUEST_URI']);
		$array =  explode("/", $url['path']);
		$key = array_pop(array_keys($array));
		$SONumber =  $array[$key-1]."/".$array[$key];
		$data = $this->M_PrintPDF->loadSOData($SONumber);
		$this->load->library('mpdf/mpdf');
		$this->mpdf->mPDF('',    // mode - default ''
					 'A4',    // format - A4, for example, default ''
					 0,     // font size - default 0
					 '',    // default font family
					 5,    // margin_left
					 5,    // margin right
					 5,     // margin top
					 5,    // margin bottom
					 5,     // margin header
					 5,     // margin footer
					 'P');  // L - landscape, P - portrait
		
		$html = $this->load->view('Report/V_SOReport',$data,true);
		
		$this->mpdf->WriteHTML($html);
		$this->mpdf->Output();
	}

	public function SOReportDomPDF()
	{
		$url = parse_url($_SERVER['REQUEST_URI']);
		$array =  explode("/", $url['path']);
		$key = array_pop(array_keys($array));
		$SONumber =  $array[$key-1]."/".$array[$key];
		//$this->load->model('Report/M_SaleOrder');
		$data = $this->M_PrintPDF->loadSOData($SONumber);

		///$this->load->view('Report/V_SOReport',$data,true);
		
		$html = $this->load->view('Report/V_SOReport_domPDF',$data,true);
		
		pdf_create($html, 'SOReport','A4','portrait',false);
	}
}