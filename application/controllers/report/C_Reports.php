<?php
class C_Reports extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('report/M_Reports');
	}
	
	public function index(){
		$this->load->view('report/V_Reports');
	}
	
	public function populate(){
		header('Content-type: application/json');
		echo json_encode($this->M_Reports->populate($this->input->post()));
	}
	
	public function searchData(){
		header('Content-type: application/json');
		echo json_encode($this->M_Reports->searchData($this->input->post()));
	}
	
}