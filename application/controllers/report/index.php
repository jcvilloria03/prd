<?php
class Index extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		validate_request();
	}
	
	public function index()
	{
		$data['title'] = 'Home';
		$this->load->view('report/index.php',$data);
	}
	
}