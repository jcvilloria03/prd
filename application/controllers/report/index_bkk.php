<?php
class Index extends CI_Controller {

	public function __construct(){
		parent::__construct();
		validate_request();
	}
	
	public function index(){
		$data['title'] = 'report';
		$this->load->view('report/index.php',$data);
	}
	
	public function populate(){
		header('Content-type: application/json');
		echo json_encode($this->Index->populate($this->input->post()));
	}
	
}