<?php
class C_DataTables extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('services/M_DataTables');
	}
	
	public function Index()
	{		
		echo 'Default';			
	}
	
	public function viewPRDataAllData(){
		$return = $this->M_DataTables->viewPRData_AllData($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	public function viewSOallData(){
		$return = $this->M_DataTables->viewSOData_AllData($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	public function viewPendingPurchaseData(){
		$return = $this->M_DataTables->viewPendingPurchase_Data($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	public function viewPODataAllData(){
		$return = $this->M_DataTables->viewPOData_AllData($this->input->post());
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	
	function __destruct(){
		audittrail();
	}
}