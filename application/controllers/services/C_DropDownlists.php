<?php
class C_DropDownlists extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('services/M_DropDownlists');
	}
	
	public function Index()
	{		
		echo 'Default';			
	}
	
	public function paymentTerms()
	{		
		echo $this->M_DropDownlists->paymentTerms_data();			
	}
	
	public function deliveryTerms(){
		echo $this->M_DropDownlists->deliveryTerms_data();
	}
	
	public function customer(){
		echo $this->M_DropDownlists->customer_data();
	}
	
	public function department(){
		echo $this->M_DropDownlists->department_data();
	}
	
	public function partnumberByCustomer(){
		echo $this->M_DropDownlists->partnumberByCustomer_data($this->input->post());
	}
	
	public function partnumberByPONumber(){
		echo $this->M_DropDownlists->partnumberByPONumber_data($this->input->post());
	}
	
	
	public function PRNumberOnlyApprovalPRDMgr(){
		echo $this->M_DropDownlists->PRNumberOnlyApprovalPRDMgr_data($this->input->post());
	}
		
		
	public function getComboBoxdata(){
		echo $this->M_DropDownlists->getComboBox_data($this->input->post());
	}
	
	function __destruct(){
		audittrail();
	}
}