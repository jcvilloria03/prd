<?php
class C_Setup extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		validate_request();
		$this->load->model('setting/M_Setup');
	}
	
		
	public function index()
	{		
		$this->load->view('setting/V_Setup');
	}
	
	public function getSetupdata(){
		$return = $this->M_Setup->get_SetUpData();
		switch($return){
			case 1 : echo "1"; break;
			default : echo $return;
		}
	}
	
	
	public function saveSetupData()
	{
		 $return = $this->M_Setup->save_SetupData($this->input->post());
		
		switch($return){
			case 0 : echo"success|Successfully Completed"; break;
			case 1 : echo "error|PONumber does not exist.";break;
			default : echo "error|could not add. <br> Please contact Support"; break;
		}
	}
}