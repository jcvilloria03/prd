<?php
class User_Model extends CI_Model {
	var $masterdb;
	
	function __construct(){
        parent::__construct();
		$this->load->library('cipher');
		$this->masterdb = $this->load->database('masterdata',true);
    }   

	function login($post){		
		$username = $post['logname'];
		$password = $post['logpass'];
		$encryptedPass = $this->cipher->encrypt($password);
		$result = $this->masterdb->query("execute LogMeIn2 '$username', '$encryptedPass'");
		if($result->num_rows() > 0){			
			$result = $result->row(0);
			if ($result->Result == 'Success'){			
				$result = $this->CheckFirstLogin($username);
				if($result == 'success' || is_numeric($result)){
					$this->masterdb->query("UPDATE [User] SET lastlogin = '".date('Y-m-d H:i:s')."' WHERE [Username] = '$username' ");
					
//$username = '14/1079';

					$result = $this->masterdb->get_where('[User]', array('Username'=>$username ));
					$row = $result->row(0);
					$sess_array = array(
						'name' => $row->Name,
						'username' => $row->Username,
						'is_admin' => $this->is_admin($row->Username),
						'is_manager' => $this->is_manager($row->Username),
						'dep_name' => $this->get_DepartmentName($row->Username),
						'is_GM' => $this->is_GM($row->Username),
						'is_BM' => $this->is_BM($row->Username),
						'is_PRDManager' => $this->is_PRDManager($row->Username)
					);
					$this->session->set_userdata('logged_in', $sess_array);
					//var_dump($sess_array);
					$return = 1;
				}
			}else{
				$return = 2;
			}
			
		}else {	
			$return = 2;
		}
		$this->masterdb->close();
		return $return;
	}
	
	function CheckFirstLogin($username){				
		$result = $this->masterdb->query("SELECT DATEDIFF(day,GETDATE(),PasswordValidity) as DateDifference, * FROM [User] WHERE Username = '$username' "); 
		if($result->num_rows() > 0){
			$row = $result->row(0);
			if($row->DateDifference <= 0 && $row->FirstLogin){
				$return = 'expired'; 
			}else if($row->DateDifference <= PASSWORD_VALIDITY_TRESHOLD && $row->DateDifference > 0 && $row->FirstLogin){
				$return = $row->DateDifference;
			}else if($row->DateDifference > PASSWORD_VALIDITY_TRESHOLD && $row->FirstLogin){
				$return = 'success'; 
			}else{
				$return = 'first'; 
			}
		}else {	
			$return = 'failed';
		}
		$return = 'success';
		$this->masterdb->close();
		return $return;
	}
	
	function is_admin($username){		
		$result = $this->masterdb->query("SELECT UsergroupName FROM [v_UserDep&Group] WHERE Username = '" . trim($username) ."'");		
		if($result->num_rows() > 0){
			$row = $result->row(0); 	
			if(strtolower($row->UsergroupName) == 'admin' || strtolower($row->UsergroupName) == 'administrator' || strtolower($row->UsergroupName) == 'administrators'){
				return true;
			}else{
				return false;		
			}
		}else{ 
			return false;
		}		  
	}
	
	function is_GM($username){	
		if ($username == 'atan') {
			return true;
		}
				
		$sql ="SELECT UsergroupName FROM [v_UserDep&Group]  WHERE Username = '" . trim($username) ."'";
		
		$result = $this->masterdb->query($sql);		
		if($result->num_rows() > 0){
			$row = $result->row(0); 	
			if(strtolower($row->UsergroupName) == 'gen. manager' || strtolower($row->UsergroupName) == 'gm' || strtolower($row->UsergroupName) == 'general manager'){
				return true;
			}else{
				return false;		
			}
		}else{ 
			return false;
		}		  
	}
	
	function is_BM($username){		
		$sql = "SELECT dbo.[fn_CheckBranchManagerUsername]('" . trim($username) ."') as Result";
		$result = $this->db->query($sql);
		if($result->num_rows() > 0){
			$row = $result->row(0); 	
			if($row->Result){
				return true;
			}else{
				return false;
			}
		}else{ 
			return false;
		}		  
		// $result = $this->masterdb->query("SELECT UsergroupName FROM [v_UserDep&Group]  WHERE Username = '" . trim($username) ."'");		
		// if($result->num_rows() > 0){
			// $row = $result->row(0); 	
			// if(strtolower($row->UsergroupName) == 'branch manager' || strtolower($row->UsergroupName) == 'bm'){
				// return true;
			// }else{
				// return false;		
			// }
		// }else{ 
			// return false;
		// }	
	}
	
	function is_manager($username){		
		$sql ="SELECT UsergroupName FROM [v_UserDep&Group] WHERE Username = '" . trim($username) ."' and UsergroupName = 'Manager' ";
		$result = $this->masterdb->query($sql);			

		if($result->num_rows() > 0){
			return true;
			// $row = $result->row(0); 	
			// if(strtolower($row->UsergroupName) == 'manager' || strtolower($row->UsergroupName) == 'managers' || strtolower($row->UsergroupName) == 'supervisor' || strtolower($row->UsergroupName) == 'supervisors'){
				// return true;
			// }else{
				// return false;		
			// }
		}else{ 
			return false;
		}		  
	}
	
	function is_PRDManager($username){
		$result = $this->masterdb->query("Select * from PRD.Dbo.GroupEmail WHERE GroupName ='PRD' and GroupType ='manager' and Username = '" . trim($username) ."'");		
		if($result->num_rows() > 0){
			return true;
		}else{
			return false;	
		}  
	}
	function get_DepartmentName($username){
		$result= $this->masterdb->query ("Select Department.Name  from [user],Department WHERE [User].DeptID = Department.ID and [USER].Username = '" . trim($username) ."'");
		if($result->num_rows() > 0){
			$row = $result->row(0); 	
			return $row->Name;
		}else{
			return "No Dept";
		}
	}
	
	function forgot($post){	
		extract($post);	
		$this->masterdb->select(array('EMail','Name','Password'));
		
		$result = $this->masterdb->get_where("[User]", array('username'=>$forgotname,'Disabled'=>0));
		if($result->num_rows() > 0){	
			$row = $result->row();
			if ($row->EMail){
				// SEND EMAIL

				$decryptedPass = $this->cipher->decrypt($row->Password);
				
				$this->email->from(MAIL_SUPPORT,MAIL_SUPPORT_NAME);
				$this->email->to($row->EMail);
				$this->email->subject("PRD - Forgot Password");
				$this->email->message('
					<p>Hi '.$row->Name.',</p>
					<p>Forgot password has been requested on your account.</p>
					<p>Your current password is <strong>'.$decryptedPass.'</strong>.</p>
					<p>If you didn\'t requested for this transaction please contact support.</p>
					<p>For more details, please check on PRD.</p>
					<p>Thank you.</p>
					<p>JSI Notifier</p> 	 				
				');
				if($this->email->send()){
					return 1;					
				}else{
					return 3; 
				}
			}else{
				return 4;
			}
			return 1;
		}else {	
			return 2;
		}
	}
}