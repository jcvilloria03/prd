<?php
class M_ChangePassword extends CI_Model {

	public function __construct()
	{
		$this->load->library('cipher');
		$this->masterdb = $this->load->database('masterdata',true);
		$this->load->database();
	}
	
	public function UpdatePassword($post){
		$curpassword = $this->cipher->encrypt($post['curpassword']);
		$newpassword = $this->cipher->encrypt($post['newpassword']);
		$conpassword = $this->cipher->encrypt($post['conpassword']);
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['username'];
		$result = $this->masterdb->get_where('[User]', array('Username' => $username,'[Password]'=>$curpassword));
		if($result->num_rows() == 1){
			$data = array('[Password]'=>$newpassword);
			return $this->masterdb->update('[User]', $data, array('Username' => $username,'[Password]'=>$curpassword));
		}else{
		//Incorrect Password
			return 2;
		}
	}

}
