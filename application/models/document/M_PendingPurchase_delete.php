<?php
class M_PendingPurchase extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function loadPOData($PONumber)
	{
	
		$result = $this->db->query(" Select pr.PRType,po.PONumber,po.VendorID,v.VendorName,v.VendorAddress1,v.VendorAddress2,v.VendorAddress3,
									v.ContactPerson as VendorContactperson, v.TelNo as VendorTelNo,v.FaxNo as VendorFaxNo,
									po.PaymentTerms,pt.PaymentTerms as PaymentTermName,po.DeliveryTerms, dt.DeliveryTerms as DeliveryTermName,
									po.ShipToAddress,po.BillingAddress,po.Attention, po.Ext,po.ReqDeliveryDate,
									po.Remarks,po.Buyer,po.BuyerExt,po.EntryDate,po.[Status],po.Deleted
									from PendingPurchase po, PurchaseRequisition pr,Vendors v, PaymentTerms pt,DeliveryTerms dt 
									WHERE po.PONumber = pr.PRNumber and po.VendorID = v.VendorID and po.PaymentTerms = pt.ID and po.DeliveryTerms= dt.ID
									and PONumber = '$PONumber'");
		if($result->num_rows() > 0){
			$data['POData'] = $result->row(0);
			
				if($data['POData']->PRType =='NORMAL'){
				$result = $this->db->query(" Select pod.PONumber,pod.PartNumber,p.[Description] as PartName, pod.Quantity,pod.UnitPrice,(pod.Quantity * pod.UnitPrice) as Amount
											from PODetail pod, Parts p 
											where pod.PartNumber = p.PartNumber and PONumber= '$PONumber'");
				if($result->num_rows() > 0){	
					$data['PODetail'] = $result->result_array();
				}
			}ELSE{
				$result = $this->db->query("Select pod.PONumber,pod.PartNumber,pod.Quantity,pod.UnitPrice,(Quantity*UnitPrice) as Amount 
											from PODetail pod 
											where PONumber= '$PONumber'");
				if($result->num_rows() > 0){	
					$data['PODetail'] = $result->result_array();
				}
			}
			
			
			
			return $data;
		}
	}
	
}
