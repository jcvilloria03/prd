<?php
class M_PurchaseOrder extends CI_Model {

	public function __construct(){
		$this->load->database();
	}
	
	public function SearchData($vars){
		if (trim($vars['PONumber']) != ''){
			$whereClause = " where PONumber = '".trim($vars['PONumber'])."'";
		}
		
		if (trim($vars['ReqDeliveryDate']) != ''){
			if (isset($whereClause)){
				$whereClause.= " and ReqDeliveryDate = '".trim($vars['ReqDeliveryDate'])."'";
			} else {
				$whereClause = " where ReqDeliveryDate = '".trim($vars['ReqDeliveryDate'])."'";
			}
		}
		
		if (isset($vars['VendorID'])){
			if (trim($vars['VendorID']) != ''){
				if (isset($whereClause)){
					$whereClause.= " and ConcatVendor = '".trim($vars['VendorID'])."'";
				} else {
					$whereClause = " where ConcatVendor = '".trim($vars['VendorID'])."'";
				}
			}
		}
		
		if (isset($vars['PaymentTerms'])){
			if (trim($vars['PaymentTerms']) != ''){
				if (isset($whereClause)){
					$whereClause.= " and ConcatPaymentTerms = '".trim($vars['PaymentTerms'])."'";
				} else {
					$whereClause = " where ConcatPaymentTerms = '".trim($vars['PaymentTerms'])."'";
				}
			}
		}
		
		if (isset($vars['DeliveryTerms'])){
			if (trim($vars['DeliveryTerms']) != ''){
				if (isset($whereClause)){
					$whereClause.= " and ConcatDeliveryTerms = '".trim($vars['DeliveryTerms'])."'";
				} else {
					$whereClause = " where ConcatDeliveryTerms = '".trim($vars['DeliveryTerms'])."'";
				}
			}
		}
		
		if (isset($whereClause)){
			$result = $this->db->query("select * from v_ol_DocPurchaseOrder$whereClause");
		} else {
			$result = $this->db->query("select * from v_ol_DocPurchaseOrder");
		}
		
		return $result->result_array();
	}
	
}
