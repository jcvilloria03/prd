<?php
class M_PurchaseRequisition extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	// public function loadPRData($PRNumber){
		// $result = $a->db->query(" Select pr.PRNumber,pr.PRType,pr.Department,d.Department as DepartmentName,pr.DateRequired,pr.RequestedBy,
									 // pr.RequestedDate,pr.PurchasedBy,pr.ApprovedBy,pr.Comment,pr.Remarks,pr.[Status],pr.[Deleted] 
									 // from PurchaseRequisition pr,Department d 
									// WHERE pr.Department = d.ID And PRNumber = '$PRNumber'");
		
		// if($result->num_rows() > 0){		
			// $data['PRData'] = $result->row(0);
			// if($data['PRData']->PRType =='NORMAL'){
				// $result = $this->db->query(" SELECT prd.ID,prd.RequestedQty,prd.PartNumber,p.[Description] as partname, prd.VendorID,v.VendorName,prd.UnitPrice,
											// (prd.RequestedQty*prd.UnitPrice) as Amount 
											// FROM PRDetail prd,Vendors v, Parts p 
											// WHERE prd.VendorID = v.VendorID and prd.PartNumber = p.PartNumber 
											// And prd.PRNumber = '$PRNumber'");
				// if($result->num_rows() > 0){	
					// $data['PRDetail'] = $result->result_array();
				// }
			// }ELSE{
				// if($data['PRData']->Department==1){
					// $result = $this->db->query(" SELECT prd.ID,prd.RequestedQty,dep.Department as Department,prd.PartNumber,'' as partname, prd.VendorID,prd.VendorID as VendorName,prd.UnitPrice,
											// (prd.RequestedQty*prd.UnitPrice) as Amount 
											// FROM PRDetail prd,Department dep WHERE prd.Department = dep.ID and prd.PRNumber = '$PRNumber'");
				// }else{
					// $result = $this->db->query("SELECT prd.ID,prd.RequestedQty,Department,prd.PartNumber,'' as partname, prd.VendorID,prd.VendorID as VendorName,prd.UnitPrice,
											// (prd.RequestedQty*prd.UnitPrice) as Amount 
											// FROM PRDetail prd WHERE prd.PRNumber = '$PRNumber' Order by Department,prd.PartNumber");
				// }
				
				// if($result->num_rows() > 0){	
					// $data['PRDetail'] = $result->result_array();
				// }
			// }
			
			// return $data;
			
		// }		
	// }
	
	public function Search($vars){
		$whereClause = "";
		
		if (trim($vars['PRType']) != 'ALL'){
			$whereClause.= " and pr.PRType = '".trim($vars['PRType'])."'";
		}
		
		if (trim($vars['PRStatus']) != 'ALL'){
			if (trim($vars['PRStatus']) == 1){
				$whereClause.= " and pr.[Status] = 1";
			} else if(trim($vars['PRStatus']) == 2){
				$whereClause.= " and pr.DELETED = 1";
			} else {
				$whereClause.= " and pr.[Status] = 0 and pr.DELETED = 0";
			}
		}
		
		if (trim($vars['PRNumber']) != ''){
			$whereClause.= " and pr.PRNumber = '".trim($vars['PRNumber'])."'";
		}
		
		if (trim($vars['Department-combox']) != ''){
			$whereClause.= " and (d.Department + ' - ' + convert(varchar,d.ID))  = '".trim($vars['Department-combox'])."'";
		}
		$sql = "Select PRNumber,PRType,Department,DateRequired, RequestedBy,DateRequired,PurchasedBy,ApprovedBy,Comment,Remarks,
				[Status],PRDeleted as [Deleted] 
				 from v_ol_DocPurchaseRequisition WHERE PRNumber <> '' $whereClause";
		//$result = $this->db->query("Select pr.PRNumber,pr.PRType,d.Department,pr.DateRequired, pr.RequestedBy,pr.PurchasedBy,pr.ApprovedBy, CASE pr.DELETED WHEN 1 THEN 'Deleted' Else CASE pr.[Status] WHEN 1 THEN 'Completed'	ELSE '' END END as Status from PurchaseRequisition pr,Department d where pr.Department = d.ID$whereClause");
		$result = $this->db->query("Select pr.PRNumber,pr.PRType,d.Department,pr.DateRequired, pr.RequestedBy,pr.PurchasedBy,pr.ApprovedBy, CASE pr.DELETED WHEN 1 THEN 'Deleted' Else CASE pr.[Status] WHEN 1 THEN 'Completed'	ELSE '' END END as Status from PurchaseRequisition pr,Department d where pr.Department = d.ID$whereClause");
		
		return $result->result_array();
	}
}
