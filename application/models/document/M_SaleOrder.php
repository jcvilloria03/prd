<?php
class M_SaleOrder extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function viewSOData_AllData($vars){
		$whereClause = "";
		if (trim($vars['SOType']) != 'ALL'){
			$whereClause.= " and SOType = '".trim($vars['SOType'])."'";
		} else {
			$whereClause.= " and SOType like '%'";
		}
		
		if (trim($vars['PONumber']) != ''){
			$whereClause.= " and PONumber = '".trim($vars['PONumber'])."'";
		}
		
		if (trim($vars['SONumber']) != ''){
			$whereClause.= " and SONumber = '".trim($vars['SONumber'])."'";
		}
		
		if (isset($vars['CustomerID'])){
			if (trim($vars['CustomerID']) != ''){
				$arr = explode("-",trim($vars['CustomerID']));
				$whereClause.= " and CustomerID = '".trim($arr[1])."'";
			}
		}
		$sql = "Select SONumber,PONumber,SOType,CustomerID,
									CustomerName,Name,SODate,TTCharges,FreightCharges,
									EntryDate,[Status],Deleted 
									FROM v_ol_DocSaleOrder
									WHERE SONumber <> '' $whereClause";
		
		$result = $this->db->query($sql);
		if($result->num_rows() > 0){
			return json_encode($result->result_array());	
		}else{
			return 1;
		}
	}
	
	// public function loadPOData($SONumber)
	// {
		// $sql = "Select SONumber,PONumber,CustomerID,CustomerName, CustomerAddress1,CustomerAddress2,CustomerAddress3,
				// CustomerTelNo, CustomerFaxNo,AdminCost,CustomerContactPerson,EmailAddress,
				// Name,SODate,TTCharges,FreightCharges,Remarks,EntryDate 
				// from v_ol_DocSaleOrder WHERE SONumber = '$SONumber'";
		// $result = $this->db->query($sql);
		// // $result = $this->db->query(" Select so.SONumber,so.PONumber,so.CustomerID,c.CustomerName, c.CustomerAddress1,c.CustomerAddress2,c.CustomerAddress3,
									// // c.TelNo as CustomerTelNo, c.FaxNo as CustomerFaxNo,c.AdminCost,c.ContactPerson as CustomerContactPerson, c.EmailAddress,
									// // so.Name,so.SODate,so.TTCharges,so.FreightCharges,so.Remarks,so.EntryDate from SaleOrder so,Customers c
									// // WHERE so.CustomerID = c.CustomerID and SONumber = '$SONumber'");
		// if($result->num_rows() > 0){
			// $data['SOData'] = $result->row(0);
			// $result = $this->db->query(" SELECT so.SONumber,so.PartNumber,p.Description as PartName, so.Quantity,so.UnitPrice,(so.Quantity*so.UnitPrice) as Amount
										// FROM SODetail so,Parts p 
										// where so.PartNumber = p.PartNumber and SONumber ='$SONumber'");
				// if($result->num_rows() > 0){	
					// $data['SODetail'] = $result->result_array();
				// }
			
		// }
			
		// return $data;
	// }
	
}
