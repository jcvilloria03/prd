<?php
class M_BankCharges extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_data($post){
		$sIndexColumn = "ID";
		$sTable = "v_BankCharges";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		
		$Status = isset($post['Status'])?$post['Status']:'';
			
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
			   array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
		if ( isset( $post['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		 
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		 
		
		if($Status==''){}
		Else{
			$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
			$sTWhere = " WHERE  Status = '$Status'";
			$sWhere .= "  Status = '$Status'";
		}
		
		
		/* Data set length after filtering */
		
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
	   
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$rResult = $this->db->query("SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo");
		 
		 
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable");
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		 
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
					
				}else if ( $aColumns[$i] == 'isfixedrate' ){
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ] ? 'Yes' : 'No';
				}else if ( $aColumns[$i] != ' ' ){
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
				$row[] = '<a title="Edit"  class="edit-btn" href="#" onclick="todo(\'edit\',\''.$custID.'\');" >Edit</a>
						 <a title="Delete" class="delete-btn" href="#" onclick="todo(\'delete\',\''.$custID.'\');" >Delete</a>';

			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	public function save_data($post){
		extract($post);
		$result = $this->db->get_where('BankCharges', array('VendorID' => $VendorID,'CustomerID' => $CustomerID,'ChargeMonth'=>$month,'ChargeYear'=>$year));
		$days	= cal_days_in_month(CAL_GREGORIAN,$post['month'],$post['year']);

		if($result->num_rows() <= 0){
			$data = array(
					'VendorID' => $post['VendorID'],
					'CustomerID' => $post['CustomerID'],
					'ChargeMonth' => $post['month'],
					'ChargeYear' => $post['year'],
					'ChargeAmount' => $post['ChargeAmount'],
					'currency'	=>	$post['currency']
				);
			return $this->db->insert('BankCharges', $data);	
		}else{
			return 2;
		}		
	}
	
	public function get_data_by_id($post){
		extract($post);
		$result = $this->db->get_where('BankCharges', array('ID' => $ID));
		if($result->num_rows() > 0){
			return json_encode($result->row());	
		}else{
			return 1;
		}
	}
	
	public function update($post){
		extract($post);
		$result = $this->db->get_where('BankCharges', array('VendorID' => $VendorID,'CustomerID' => $CustomerID,'ChargeMonth'=>$month,'ChargeYear'=>$year));
		$days	= cal_days_in_month(CAL_GREGORIAN,$post['month'],$post['year']);		
		if($result->num_rows() == 1){
			$data = array(
					'VendorID' => $post['vendorid'],
					'CustomerID' => $post['customerid'],
					'ChargeMonth' => $post['month'],
					'ChargeYear' => $post['year'],
					'ChargeAmount' => $post['ChargeAmount'],
					'currency'	=>	$post['currency']					
				);				
			return $this->db->update('BankCharges', $data, array('ID'=>$ID)); 
		}else{
			return 2;
		}
		
	}
	
	public function delete($post){
		extract($post);
		$result = $this->db->get_where('BankCharges', array('ID'=>$ID ));		
		if($result->num_rows() ==1){
			//$data = array('deleted' => 1);				
			return $this->db->delete('BankCharges', array('ID'=>$ID)); 
		}else{
			return 2;
		}
		
	}

	public function view_SODetail($post){
		extract($post);
		$SONumber = $post['SONumber'];		
		$rResult = $this->db->query("SELECT * FROM BankCharges;");
				
		if($rResult->num_rows() > 0){
			return json_encode($rResult->result_array());	
		}else{
			return 1;
		}
	}	
}