<?php
class M_ProductType extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_data($post){
		
		$sIndexColumn = "ProductCode";
		$sTable = "ProductType";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
				
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
			   array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
		if ( isset( $post['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		 
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		 
		
		/* Data set length after filtering */
		
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
	   
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$rResult = $this->db->query("SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo");
		 
		 
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable");
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		 
		foreach (  $rResult->result_array() as $aRow )
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
			  
				if ( $aColumns[$i] == $sIndexColumn )
				{
					/* Special output formatting */
					$row[] = '<a title="Edit" class="edit-btn" href="#" onclick="todo(\'edit\',\''.$aRow[ trim($aColumns[$i],'[]') ].'\');" >Edit</a>
							  <a title="Delete" class="delete-btn" href="#" onclick="todo(\'delete\',\''.$aRow[ trim($aColumns[$i],'[]') ].'\');" >Delete</a>';
				}
				else if ( $aColumns[$i] != ' ' )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	public function save_data($post){
		extract($post);
		$result = $this->db->get_where('ProductType', array('PDesc' => $PDesc));
		if($result->num_rows() <= 0){
			$data = array(
				   'PDesc' => $PDesc
				);
			return $this->db->insert('ProductType', $data);	
		}else{
			return 2;
		}		
	}
	
	public function get_data_by_id($post){
		extract($post);
		$result = $this->db->get_where('ProductType', array('ProductCode' => $ProductCode));
		if($result->num_rows() > 0){
			return json_encode($result->row());	
		}else{
			return 1;
		}
	}
	
	public function update($post){
		extract($post);
		$result = $this->db->get_where('ProductType', array('ProductCode'=>$ProductCode));		
		if($result->num_rows() == 1){
			$data = array(
				   'PDesc' => $PDesc
				);				
			return $this->db->update('ProductType', $data, array('ProductCode'=>$ProductCode)); 
		}else{
			return 2;
		}
		
	}
	
	public function delete($post){
		extract($post);
		$result = $this->db->get_where('ProductType', array('ProductCode'=>$ProductCode ));		
		if($result->num_rows() ==1){
			//$data = array('deleted' => 1);				
			return $this->db->delete('ProductType', array('ProductCode'=>$ProductCode)); 
		}else{
			return 2;
		}
		
	}
}