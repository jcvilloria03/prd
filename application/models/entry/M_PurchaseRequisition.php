<?php
class M_PurchaseRequisition extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	// public function get_PRDetail_data($post){
		// $PRType = $post['PRType'];
		// $rResult = $this->db->query("execute sp_ol_GetPRNumber '$PRType'");
		// return $rResult[0]['Result'];
	// }
	
	public function get_PRNumber($post){
		extract($post);
		$PRType = $post['PRType'];
		
		$rResult = $this->db->query("execute sp_ol_GetPRNumber '$PRType'");
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function get_PRDetail($post){
		extract($post);
		$PRType = $post['PRType'];
		
		$rResult = $this->db->query("execute sp_ol_GetPRDetail '$PRNumber'");
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	
	public function save_data($post){
		extract($post);
		$result = $this->db->get_where('PurchaseRequisition', array('PRNumber' => $PRNumber));
		if($result->num_rows() <= 0){
		
			$rResult = $this->db->query("execute sp_ol_PRSave '$PRNumber'");
			
			return $this->db->insert('Customers', $data);	
		}else{
			return 2;
		}		
	}
	
}