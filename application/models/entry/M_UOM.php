<?php
class M_UOM extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	

	public function get_data($post){
		
		$sIndexColumn = "[Code]";
		$sTable = "UOM";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
				
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
		if ( isset( $post['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		 
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		 
		
		/* Data set length after filtering */
		
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
	   
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$rResult = $this->db->query("SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo");
		 
		 
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable");
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		 
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
					
				}
				else if ( $aColumns[$i] != ' ' )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
				$row[] = '<a title="Edit" class="edit-btn" href="#" onclick="todo(\'edit\',\''.$custID.'\');" >Edit</a>
						 <a title="Delete" class="delete-btn" href="#" onclick="todo(\'delete\',\''.$custID.'\');" >Delete</a>';

			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	
	public function save_data($post){
		extract($post);
		$result = $this->db->get_where('UOM', array('[Code]' => $Code));
		if($result->num_rows() <= 0){
			$data = array(
					'[Code]'=>$Code,
					'Description' => $Description
				);
			return $this->db->insert('UOM', $data);	
		}else{
			return 2;
		}		
	}
	
	public function get_data_by_id($post){
		extract($post);
		$this->db->select(' [Code],[Description]');
		$result = $this->db->get_where('UOM', array('[Code]' => $ID));
		if($result->num_rows() > 0){
			return json_encode($result->row());	
		}else{
			return 1;
		}
	}
	
	public function update($post){
		extract($post);
		$session_data = $this->session->userdata('logged_in');
		$name = $session_data['name'];
		$result = $this->db->get_where('UOM', array('[Code]'=>$ID));		
		if($result->num_rows() == 1){			
			$data = array(
					'Description' => $Description
				);				
			return $this->db->update('UOM', $data, array('[Code]'=>$ID)); 
		}else{
			return 2;
		}
		
	}
	
	public function delete($post){
		extract($post);
		$result = $this->db->get_where('UOM', array('[Code]'=>$ID ));		
		if($result->num_rows() ==1){
			return $this->db->delete('UOM', array('[Code]'=>$ID)); 
		}else{
			return 2;
		}
		
	}
}