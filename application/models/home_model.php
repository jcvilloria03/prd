<?php
class Home_model extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}

	function getMenu(){
		 //$session_data = $this->session->userdata('logged_in');
		 //$username = $session_data['username'];
		$result = $this->db->query("SELECT ProgramName,MenuName,MenuURL 
														FROM PRD.dbo.v_ol_Menu 
													WHERE [Status] = 1 and UserID ='$username' 
													ORDER BY ProgramID,OrderInCategory");
		$result = $result->result_array();				
		return $result;
	}
	function getMenuByProgramIndex($programindex){
		 $session_data = $this->session->userdata('logged_in');
		  $username = $session_data['username'];
		$result = $this->db->query("SELECT ProgramName,ProgramIndex,MenuName,MenuIndex,MenuURL 
									FROM PRD.dbo.v_ol_Menu 
									WHERE [Status] = 1 and ProgramIndex='$programindex' and UserID ='$username' 
									ORDER BY ProgramID,OrderInCategory");
		$result = $result->result_array();				
		return $result;
	}
}