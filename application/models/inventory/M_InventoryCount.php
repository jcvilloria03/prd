<?php
class M_InventoryCount extends CI_Model{
	public function __construct(){
		$this->load->database();
	}
	
	public function view_InventoryCount($CustomerID){
		$result = $this->db->query("SELECT  PartNumber,[Description],Description2,UOM,Balance,InventoryID
									FROM v_ol_InventoryCount4Parts 
									WHERE (InventoryDate IS NULL OR DATEPART(dw,getdate()) = DayOfCount) 
									AND  CustomerID = '$CustomerID'");
		if($result->num_rows() > 0){
			return json_encode($result->result_array());	
		}else{
			return 1;
		}
	}
	
	
	public function save_InventoryCountMulti($post){
		extract($post);
		$partnumbers = explode("|", $partnumbers);
		$balances = explode("|", $balances);
		$inventoryids = explode("|", $inventoryids);
		for($i = 0; $i < count($partnumbers) - 1; $i++){
			if($this->saveInventoryCount($inventoryids[$i],$partnumbers[$i],$balances[$i]) != 0){
				return 2;
			}
		}		
		return 0;
	}
	
	public function saveInventoryCount($inventoryid,$partnumber,$balance){
		if($balance != '0'){
			if($inventoryid != 'undefined'){
				$rResult = $this->db->query ("DECLARE @Flag Int;
											 execute sp_ol_InventoryCountSave '$inventoryid','$partnumber','$balance',@Flag OUTPUT;
											 Select @Flag as Result;");
				$rResult = $rResult->result_array();
				return $rResult[0]['Result'];
			}else{
				return 0;
			}
		}else{
			return 0;
		}
		return 1;
		
	}
	
	
}
?>