<?php
class M_InventoryCountAdd extends CI_Model{
	public function __construct(){
		$this->load->database();
	}
	
	// public function view_InventoryCount($CustomerID){
		// $result = $this->db->query("SELECT  PartNumber,[Description],Description2,UOM,Balance,InventoryID
									// FROM v_ol_fm_InventoryCount4Parts 
									// WHERE (InventoryDate IS NULL OR DATEPART(dw,getdate()) = DayOfCount) 
									// AND  CustomerID = '$CustomerID'");
		// if($result->num_rows() > 0){
			// return json_encode($result->result_array());	
		// }else{
			// return 1;
		// }
	// }
	
	
	public function view_InventoryCount($post){
		
		$sIndexColumn = "ID";
		$sTable = "v_ol_fm_InventoryCount4Parts";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		
		$CustomerID = isset($post['CustomerID'])?$post['CustomerID']:'';
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
			   array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
		if ( isset( $post['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		 
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		$sTWhere = " WHERE   CustomerID = '$CustomerID' "; 
		$sWhere .= " CustomerID = '$CustomerID' ";  
		
		
		/* Data set length after filtering */
		
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
	   
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
	
		$rResult = $this->db->query($sql);
		 
		 
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable");
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		 
		 $j = 0;
		foreach (  $rResult->result_array() as $aRow )
		{
			$row = array();
			$j++;
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
			  
				if ( $aColumns[$i] == $sIndexColumn )
				{
					/* Special output formatting */
					$row[] = $j;//$aRow[ $aColumns[$i] ];
				}
				else if ( $aColumns[$i] != ' ' )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	
	public function view_CustomerList4Inventory($post){
		
		$sIndexColumn = "CustomerID";
		$sTable = "v_ol_frm_CustomerList4Inventory";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		
		$CustomerID = isset($post['CustomerID'])?$post['CustomerID']:'';
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
			   array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
		if ( isset( $post['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		 
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		// $sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		// $sTWhere = " WHERE   CustomerID = '$CustomerID' "; 
		// $sWhere .= " CustomerID = '$CustomerID' ";  
		
		
		/* Data set length after filtering */
		
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
	   
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
	
		$rResult = $this->db->query($sql);
		 
		 
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable");
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		 
		 $j = 0;
		foreach (  $rResult->result_array() as $aRow )
		{
			$row = array();
			$j++;
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
			  
				if ( $aColumns[$i] == $sIndexColumn )
				{
					/* Special output formatting */
					$row[] = $aRow[ $aColumns[$i] ];
				}else if ( $aColumns[$i] == 'Status' ){
					if($aRow[ $aColumns[$i] ]=='1'){
						$row[] = "<span class='status-online'></span>";
					}else{
						$row[] = "<span class='status-offline'></span>";
					}
				}
				else if ( $aColumns[$i] != ' ' )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	public function save_InventoryCount($post){
		extract($post);
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['username']; 
		$CustomerID = $post['CustomerID'];
		$PartNumber =  $post['PartNumber'];
		$Balance = $post['Balance'];
		$rResult = $this->db->query ("DECLARE @Flag Int;
						 execute sp_ol_InventoryCountSave '$CustomerID','$PartNumber',$Balance,'$username',@Flag OUTPUT;
						 Select @Flag as Result;");
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];

	}
}
?>