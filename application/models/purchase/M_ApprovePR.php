<?php
class M_ApprovePR extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	public function viewPR_Data($post){
	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;
		

		
		$sIndexColumn = "PRNumber";
		$sTable = "v_ol_frm_ApprovePR";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		array_push($aColumns, "ID", "PRDApproved");
		
		$PRStatus = isset($post['PRStatus'])?$post['PRStatus']:'';
		
		$PRType = isset($post['PRType'])?$post['PRType']:'';
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
			
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
	
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		
		$sTWhere = " WHERE  ((dbo.fn_CheckMultiRequestedByName(PRNumber,'".$username."') =1 OR dbo.fn_CheckMultiApprovedByName(PRNumber,'".$username."') = 1 OR '$dep_name' ='PRD' OR $is_PRDManager = 1) ";    
		$sWhere .= " ((dbo.fn_CheckMultiRequestedByName(PRNumber,'".$username."') =1 OR dbo.fn_CheckMultiApprovedByName(PRNumber,'".$username."') = 1 OR '$dep_name' ='PRD' OR $is_PRDManager = 1) ";  
		if($is_GM==1){
			$sTWhere .= " OR  (BMApproved=1 or RequiredGMApproved=1)) ";
			$sWhere .= " OR  (BMApproved=1 or RequiredGMApproved=1)) ";
		}else if( $is_BM == 1 && $is_manager  == 0){
			$sTWhere .= " OR (ManagerApproved=1)) ";
			$sWhere .= "  OR (ManagerApproved=1))  ";
		}else if( $is_BM == 1 && $is_manager  == 1){
			$sTWhere .= " And  PRDApproved =1 OR (ManagerApproved=1 )) ";
			$sWhere .= "  And  PRDApproved =1 OR (ManagerApproved=1 ))  ";
		}else if( $is_manager  == 1 && $is_PRDManager  == 0){
			$sTWhere .= " And PRDApproved =1) ";
				$sWhere .= " And  PRDApproved=1) ";
		} else if( $is_PRDManager  == 1){
			$sTWhere .= " And PRDSubmitted =1) ";
			$sWhere  .= " And PRDSubmitted =1) ";
		} else{
			$sTWhere .= " ) ";
			$sWhere  .= " ) ";
		
		}
		
		if($PRStatus ==''){
			
		}else{
			if(($is_BM == 1 && $is_manager  == 1) && ($PRStatus == 9 || $PRStatus == 7)){
				$sTWhere .= " AND PRDStatus in(9,7) " ;
				$sWhere .= " AND PRDStatus in(9,7) ";
			}else{
				$sTWhere .= " AND PRDStatus ='$PRStatus' " ;
				$sWhere .= " AND PRDStatus ='$PRStatus' ";
			}
		}
			
		if($PRType==''){}
		Else{
			$sTWhere .= " AND  PRType = '$PRType'";
			$sWhere .= " AND  PRType = '$PRType'";
		}
		
		
		$sTWhere .= " AND  YearRequested = '".$post['YearRequested']."'";
		$sWhere .= " AND  YearRequested = '".$post['YearRequested']."'";
		
		
		
		
		$sql = "SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere";
		$rResultCnt = $this->db->query($sql);
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
		
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
		
		$rResult = $this->db->query($sql);
		 
		 
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					
					//ORIGINAL 2016-06-27
					// if($is_PRDManager== 1 && ($aRow['PRDStatus'] == 11 || 
						// $aRow['PRDStatus'] == 10 || $aRow['PRDStatus'] == 9)){
						// $Allow = true;
					// }else if($is_manager == 1  && ($aRow['PRDStatus'] == 9 
						 // || $aRow['PRDStatus'] == 7 || $aRow['PRDStatus'] == 8)){
						// $Allow = true;
					// }else if($is_BM == 1 && ($aRow['PRDStatus'] == 7 || $aRow['PRDStatus'] != 1 
						 // && $aRow['PRDStatus'] == 5 || $aRow['PRDStatus'] == 6 )){
						// $Allow = true;
					// }else if($is_GM == 1 && $aRow['PRDStatus'] == 5 ){
						// $Allow = true;
					// }else{
						// $Allow = false;
					// }

					if($is_PRDManager== 1 && ($aRow['PRDStatus'] == 11 || 
						$aRow['PRDStatus'] == 10 || $aRow['PRDStatus'] == 9)){
						$Allow = true;
					}else if($is_manager == 1  && ($aRow['PRDStatus'] == 9 
						 || $aRow['PRDStatus'] == 7 || $aRow['PRDStatus'] == 8)){
						$Allow = true;
					}else if($is_BM == 1 && ($aRow['PRDStatus'] == 7 || $aRow['PRDStatus'] != 1 
						 && $aRow['PRDStatus'] == 5 || $aRow['PRDStatus'] == 6 )){
						$Allow = true;
					}else if($is_GM == 1 && ($aRow['PRDStatus'] == 5 || $aRow['PRDStatus'] == 7) ){
						$Allow = true;
					}else{
						$Allow = false;
					}

					
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					//$row[] = $custID;
					$ID= $aRow['ID'];

					if($Allow){
						$row[] = '<input type="checkbox" name="Approved" id="chkID_'.$ID.'" data="'.$custID.'" value="'.$ID.'" OnClick="fn_todoPR(\'Checked\',this);">'. $aRow['PRNumber'] .'<br>';
					}else{
						$row[] = $ID;//$custID;
					}
					/* Special output formatting */
					
				}else if ( $aColumns[$i] == 'PRDStatus'){
					switch ($aRow[ $aColumns[$i] ]) {
					   case 1: $row[] = '<span class="status green">Completed</font>';break;
					   case 2: $row[] = '<span class="status red">Deleted</font>';break;
					   case 3: $row[] = '<span class="status orange">Approved By GM</font>';break;
					   case 4: $row[] = '<span class="status red">Rejected By GM</font>';break;
					   case 5: $row[] = '<span class="status orange">Approved By BM</font>';break;
					   case 6: $row[] = '<span class="status red">Rejected By BM</font>';break;
					   case 7: $row[] = '<span class="status orange">Approved By Dept. Manager</font>';break;
					   case 8: $row[] = '<span class="status red">Rejected By Dept. Manager</font>';break;
					   case 9: $row[] = '<span class="status orange">Approved By PRD Manager</font>';break;
					   case 10:$row[] = '<span class="status red">Rejected By PRD Manager</font>';break;
					   case 11:$row[] = '<span class="status orange">Submitted to PRD Manager</font>';break;
					   case 12:$row[] = '<span  class="status grey">Draft</font>';break; 
					   default:$row[] = '';Break;
					}
						//$row[] = $aRow[ $aColumns[$i] ];
				}else if ( $aColumns[$i] != ' ' && $aColumns[$i] != 'ID' && $aColumns[$i] != 'PRDApproved'  )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			
			if($is_PRDManager== 1){
				$type='PRDManager';
			}else if($is_BM  == 1){
				$type='BM';
			}else if($is_GM == 1){
				$type='GM';
			}else if($is_manager == 1){
				$type='Manager';
			}else{
				$type='';
			}
			
			$ApproveHtml = '<a title="Approve" class="approve-btn" href="#" onclick="fn_ApprovePRData(\''.$custID.'\',\''.$type.'\',\'1\',\'\')" >Print PO</a>';
			$RejectHtml = '<a title="Reject" class="reject-btn" href="#" onclick="fn_RejectPRData(\''.$custID.'\')" >Print PO</a>';
				
			$Action  = '';
			if($type != ''){
				if($Allow){
					$Action .= $ApproveHtml;
					$Action .=$RejectHtml;
				}
			}
			
				if(strtolower($dep_name) =='prd' || $is_GM == 1 
					|| $is_BM == 1 || $is_manager == 1 || $is_PRDManager == 1){  
					$Action .= '<a title="Print" class="print-btn" href="#" onclick="fn_todoPR(\'PrintPR\',this,\''.$custID.'\');" >Print PO</a>';
				}else  if( $aRow[ "PRDApproved" ] == '1'){
					$Action .= '<a title="Print" class="print-btn" href="#" onclick="fn_todoPR(\'PrintPRWithoutIMEI\',this,\''.$custID.'\');" >Print PO</a>';
				} 
			 
			//$Action .= '<a title="Detail" class="details-open" href="#" onclick="fn_todoPR(\'detail\',this,\''.$custID.'\');" > Detail</a>';
		
			$row[] = $Action;
					
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
}
?>