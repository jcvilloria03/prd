<?php
class M_InternalRequest extends CI_Model{
	public function __construct(){
		$this->load->database();
	}
	
	
	public function get_data($post){
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username']; 
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;
	
		$sIndexColumn = "ID";
		$sTable = "v_ol_frm_InternalRequest";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		array_push($aColumns, "Status", "ManagerApproved","CreatedPR","ApprovedBy");
		
		$IRStatus = isset($post['IRStatus'])?$post['IRStatus']:'All';
		
		$IRType = isset($post['IRType'])?$post['IRType']:'';
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
		if ( isset( $post['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
			
			
		}
		 
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
	
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		
		// $sTWhere = " WHERE  (Department= '".$dep_name."' OR '$dep_name' ='PRD')  "; 
		// $sWhere .= " (Department= '".$dep_name."' OR '$dep_name' ='PRD') ";  
		
		// $sTWhere = " WHERE  ((Department= '".$dep_name."' and $is_manager =1) OR RequestedBy='$username'  OR '$dep_name' ='PRD' OR $is_PRDManager ='1')  "; 
		// $sWhere .= " ((Department= '".$dep_name."' and $is_manager =1 ) OR RequestedBy='$username' OR '$dep_name' ='PRD' OR $is_PRDManager ='1') "; 
		
		$sTWhere = " WHERE  ((ApprovedBy= '".$username."' and $is_manager =1) OR RequestedBy='$username'  OR '$dep_name' ='PRD' OR $is_PRDManager ='1')  "; 
		$sWhere .= " ((ApprovedBy= '".$username."' and $is_manager =1 ) OR RequestedBy='$username' OR '$dep_name' ='PRD' OR $is_PRDManager ='1') ";  

		if($IRStatus == 'CancelIR'){
			$sTWhere .= ' AND  Deleted =1';
			$sWhere .=' AND  Deleted = 1';
		}else  if($IRStatus == 'Draft'){
			$sTWhere .= ' AND ManagerApproved = 0 and Deleted =0 and Status = 0'; 
			$sWhere .=' AND ManagerApproved = 0 and Deleted =0 and Status = 0';
		}else if($IRStatus == 'Pending'){
			$sTWhere .= ' AND ManagerApproved = 0 and Deleted =0 and Status = 1'; 
			$sWhere .=' AND ManagerApproved = 0 and Deleted =0  and Status = 1';
		}else if($IRStatus == 'Approved'){
			$sTWhere .= ' AND ManagerApproved = 1';
			$sWhere .=' AND ManagerApproved = 1';
		}else if($IRStatus == 'Rejected'){
			$sTWhere .= ' AND ManagerApproved = 2';
			$sWhere .=' AND ManagerApproved = 2';
		} else if($IRStatus == 'ActivatedPR'){
			$sTWhere .= ' AND  CreatedPR =1';
			$sWhere .=' AND  CreatedPR = 1';
		} else if($IRStatus == 'NonActivatedPR'){
			$sTWhere .= ' AND ManagerApproved = 1 AND  CreatedPR =0';
			$sWhere .='  AND ManagerApproved = 1 AND  CreatedPR = 0';
		}  
		
		if($IRType==''){}
		Else{
			$sTWhere .= " AND  IRType = '$IRType'";
			$sWhere .= " AND  IRType = '$IRType'";
		}
		
		/* Data set length after filtering */
		$sql = "SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere";
		$rResultCnt = $this->db->query($sql);
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
		
	   
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
	
		$rResult = $this->db->query($sql);

		/* Total data set length */
		$sql = "SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere";
		$rResultTotal = $this->db->query($sql );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
	
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$Status = $aRow["Status"] ;
					$ManagerApproved  = $aRow["ManagerApproved"] ;
					$ApprovedBy  = $aRow["ApprovedBy"] ;
					//$row[] = $custID;
					if($ManagerApproved == 0 && $Status  ==1 && $is_manager == 1 && $ApprovedBy == $username){
						$chk = '<input type="checkbox" id="chkID_'.$custID.'" value="'.$custID.'" />';
					}else{
						$chk =$custID;
					}
					$row[] = $chk ;
					/* Special output formatting */
					
				}else if($aColumns[$i] == 'IRType' ){
					$IRType= $aRow[ $aColumns[$i] ];
					if($IRType =='Normal'){
						$row[] = 'Packing materials';
					}Else{
						$row[] = $IRType;
					}
				}
				else if ( $aColumns[$i] != ' ' && $aColumns[$i] != 'Status' 
					 && $aColumns[$i] != 'ManagerApproved' && $aColumns[$i] != 'CreatedPR' && 
						 $aColumns[$i] != 'ApprovedBy')
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			$Action =  '<a title="View Detail" class="detail-btn" href="#" onclick="todo(\'view\',\''.$custID.'\',\'\');" >View</a> ';
			if (strtolower($dep_name) =='prd' 
				&& $aRow["ManagerApproved"] == '1' && $aRow["Status"] == '1' && $aRow["CreatedPR"]== '0'){
				$Action .=  ' <a title="Create PR" class="create-btn" href="#" onclick="todo(\'gotopr\',\''.$custID.'\',\''.$IRType.'\');" >Create PR</a> ';
			}
			$Action .= '<a title="Detail" class="details-open" href="#" onclick="todo(\'detail\',this,\''.$custID.'\');" > Detail</a>';
			
			$row[] = $Action;
			
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	public function get_DetailData($post){
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$is_GM = $session_data['is_GM'];
		
		$IRID = ($post['IRID']=='')?'0':$post['IRID'];
		$sIndexColumn = "ID";
		$sTable = "v_ol_frm_IRDetail";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		
		
		
		array_push($aColumns, "Status","IRDetail_Status", "Deleted","CustomerID");
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
		if ( isset( $post['iSortCol_0'] ) )
		{
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
	
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				}
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		
		/* Data set length after filtering */
		
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";

		$sTWhere = " WHERE  IRID = $IRID  ";
		$sWhere .= " IRID = $IRID ";  
	
	
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
	   
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$rResult = $this->db->query("SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo");

		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable  $sWhere ");
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		 
		foreach (  $rResult->result_array() as $aRow )
		{
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$PartNumber = $aRow["PartNumber"];
					$PartDesc1 = $aRow["PartDesc1"];
					$RequestedQty = $aRow["RequestedQty"];
					$UOM = $aRow["UOM"];
					$row[] = $custID;
					/* Special output formatting */
					
				}else if($aColumns[$i] == 'CustomerID'){
					$CustomerID = $aRow[ $aColumns[$i] ];
				}else if ( $aColumns[$i] != ' ' && $aColumns[$i] != 'Status' && $aColumns[$i] != 'Deleted' 
						&& $aColumns[$i] != 'IRDetail_Status')
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			$Action='';
			if($aRow[ "Status" ] == '0' && $aRow[ "Deleted" ] =='0'){
				$Action .=  '<a class="edit-btn" href="#" onclick="todo(\'editIRDetail\',\''.$custID.'\');" >Edit</a> '; 
				//$Action =  '<a class="edit-btn" href="#" onclick="todoIRDetailEdit(\'editIRDetail\',\''.$custID.'\',\''.$CustomerID.'\',\''.$PartNumber.'\',\''.$PartDesc1.'\',\''.$RequestedQty.'\',\''.$UOM.'\');" >Edit</a> '; 
				$Action .= '<a class="delete-btn" href="#" onclick="todo(\'deleteIRDetail\',\''.$custID.'\');" >Delete</a> ';
			}else if($dep_name == 'PRD' && $aRow[ "IRDetail_Status" ] == '0' && $aRow[ "Deleted" ] =='0' ){
				$Action .= '<a class="delete-btn" href="#" onclick="todo(\'deleteIRDetail\',\''.$custID.'\');" >Delete</a> ';
			}else{
				$Action=''; 
			}
			$row[] = $Action;
					
			
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
		
	public function create_PRFromInterRequest($post){
		$session_data = $this->session->userdata('logged_in');
		extract($post);
		$ID = $post['ID'];
		$PRType = $post['PRType'];
		$username = $session_data['username'];
		
		$sql = "DECLARE @Flag Int; Declare @PRNumber varchar(20);
				 execute sp_ol_CreatePRFromIR '$ID','$PRType', '$username',@PRNumber OUTPUT,@Flag OUTPUT;
				Select @Flag as Result,@PRNumber as PRNumber";
		 
		$rResult = $this->db->query($sql);
									 
		$rResult = $rResult->result_array();
		// if($rResult[0]['Result'] == '0'){
			// return $rResult[0]['PRNumber'];
		// }else{
			// return $rResult[0]['Result'];
		// }
		return $rResult[0];
		
	}
	
	public function save_InternalRequest($post){
	
		$session_data = $this->session->userdata('logged_in');
		extract($post);
		
		$ID = ($post['ID']=='')?'0':$post['ID'];
		$IRType = isset($post['IRType'])?$post['IRType']:'';
		//$CustomerID = isset($post['CustomerID'])?$post['CustomerID']:'';

		$DateRequired = $post['DateRequired'];
		$Remarks = str_replace("'", "", $post['Remarks']);
		
		$Department = $session_data['dep_name'];
		$RequestedBy =  $session_data['username'];
		
	
		$sql = "DECLARE @Flag Int;DECLARE @ID Int; SET @ID = $ID;
				 execute sp_ol_InternalRequestSave '$IRType','$DateRequired','$Remarks', '$Department',
										'$RequestedBy',@Flag OUTPUT,@ID OUTPUT;
				 Select @Flag as Result,@ID as ID;";
		$rResult = $this->db->query($sql);
		$rResult = $rResult->result_array();

		return $rResult[0];
	}
	
	// public function update_InternalRequest($post){
		// $session_data = $this->session->userdata('logged_in');
		// extract($post);
		// $IRType = $post['IRType'];
		// $CustomerID = $post['CustomerID'];
		// $DateRequired = $post['DateRequired'];
		// $Remarks = $post['Remarks'];

		// $Department = $session_data['dep_name'];
		// $RequestedBy =  $session_data['username'];
		
		// $sql = "DECLARE @Flag Int;
				 // execute sp_ol_InternalRequestUpdate '$IRType','$CustomerID','$DateRequired','$Remarks', '$Department',  
										// '$RequestedBy',@Flag OUTPUT;
				 // Select @Flag as Result;";
		// $rResult = $this->db->query($sql);
		
		// $rResult = $rResult->result_array();
		// return $rResult[0]['Result'];
	// }
	
	
	public function save_IRDetail($post){ 
		$session_data = $this->session->userdata('logged_in');
		extract($post);
		$IRID = $post['IRID'];
		$IRDetailID = ($post['IRDetailID'])?$post['IRDetailID']:0;
		$CustomerID = ($post['CustomerID'])?$post['CustomerID']:0;
		if($post['IRType'] =='NORMAL'){
			$PartNumber = isset($post['PartNumber'])? str_replace("'", "", $post['PartNumber']):'';
		}else{
			$PartNumber = isset($post['PartNumber-txt'])? str_replace("'", "", $post['PartNumber-txt']):'';
		}
		$PartDesc1 = str_replace("'", "", $post['PartDesc1']);
		$RequesetedQty = $post['RequestedQty']; 
		$UOM = $post['UOM']; 
		
		$sql = "DECLARE @Flag Int; 
				 execute sp_ol_IRDetailSave  $IRID,'$CustomerID',$IRDetailID ,'$PartNumber','$PartDesc1','$RequesetedQty', '$UOM',@Flag OUTPUT;
				 Select @Flag as Result;";
		$rResult = $this->db->query($sql);
		$rResult = $rResult->result_array();  
		
		return $rResult[0]['Result'];
	}
	
	public function complete_IRData($post){
		$ID = $post['ID'];
		$sql = "DECLARE @Flag int; 
			 execute sp_ol_CompleteIR $ID,@Flag Output;
			 SELECT @Flag as Result;";
		 
		 $rResult=$this->db->query($sql);
		 $rResult = $rResult->result_array();

		 return $rResult[0]['Result'];
		
	}
	
	public function action_InternalRequest($post)
	{
		$session_data = $this->session->userdata('logged_in');
		extract($post);
		$ID = $post['ID'];
		$manager = ($post['manager'] == '1')?1:0;
		$type = $post['type'];
		$reason = str_replace("'", "", $post['reason']);
		$actionby = $session_data['username'];
		
		$sql = "DECLARE @Flag Int;
				 execute sp_ol_InternalRequestAction $ID,$manager,$type,'$actionby','$reason',@Flag OUTPUT;
				 Select @Flag as Result;";

		$rResult = $this->db->query($sql);
									 
		$rResult = $rResult->result_array();
		 
		return $rResult[0]['Result'];
		
		
	}
	public function get_data_by_id($post){
		extract($post); 
		$this->db->select(' [ID],IRType,DateRequired,Department,dbo.fn_GetUserFullName(RequestedBy) as RequestedBy,RequestedBy as RequestedByOrg,
						Approved,dbo.fn_GetUserFullName(ApprovedBy) as ApprovedBy,Approved,ApprovedDate,ApprovedReason,Remarks,Status,Deleted,CreatedPR');
		$result = $this->db->get_where('InternalRequest', array('[ID]' => $ID));
		if($result->num_rows() > 0){
			return json_encode($result->row());	
		}else{
			return 1;
		}
	}
	public function get_IRDetail_data($post){
		extract($post); 
		$this->db->select('ID,IRID,CustomerID,PartNumber,PartDesc1,RequestedQty,UOM,EntryDate,Status ');
		$result = $this->db->get_where('IRDetail', array('[ID]' => $ID));
		if($result->num_rows() > 0){
			return json_encode($result->row());	
		}else{
			return 1;
		}
	}
	
	public function change_IRType($post){
		extract($post);
		$ID = $post['ID'];
		
		$sql = "DECLARE @Flag Int;
				 execute sp_ol_InternalRequestTypeChange $ID,@Flag OUTPUT;
				 Select @Flag as Result;";

		$rResult = $this->db->query($sql);
									 
		$rResult = $rResult->result_array();
		 
		return $rResult[0]['Result'];
	}
	
	public function delete_IRDetail($post){
		extract($post);
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['username']; 
		$dep_name = $session_data['dep_name'];
		
		$result = $this->db->get_where('IRDetail', array('ID'=>$ID ));		
		if($result->num_rows() ==1){
			$sql = "INSERT IRDetailHistory(ID,IRID,CustomerID,PartNumber,PartDesc1,RequestedQty,UOM,EntryDate,[Status],DeletedBy)
				SELECT ID,IRID,CustomerID,PartNumber,PartDesc1,RequestedQty,UOM,EntryDate,[Status],'$username' FROM IRDetail
				WHERE ID =$ID";
			if($this->db->query($sql)){
				if(strtolower($dep_name) =='prd' ){
					$return = $this->db->delete('IRDetail', array('ID'=>$ID ));
					$sql ="Select * from IRDetail where IRID in (Select IRID from IRDetailHistory where ID ='$ID')";
					$result = $this->db->query($sql);
					if($result->num_rows()==0){
						$sql = "update InternalRequest  set Approved=0, Deleted=1,Status=0,DeletedReason='Deleted By PRD User' where ID in
								(Select IRID from IRDetailHistory where ID ='$ID') and Status = 1 and Approved= 1 and Deleted=0";
						
						$this->db->query($sql);
					}else{
						$sql = " Select * from IRDetail where IRID in 
								(Select IRID from IRDetailHistory where ID ='$ID') and Status=0";
						$result = $this->db->query($sql);
						if($result->num_rows()==0){
							$sql =" update InternalRequest  set CreatedPR=1 where ID in
									(Select IRID from IRDetailHistory where ID ='$ID') and  Status = 1 and CreatedPR = 0";
							$this->db->query($sql);
						}
					}
					return $return;
				}else{
					return $this->db->delete('IRDetail', array('ID'=>$ID ));
				}
			}else{
				return 2;
			}	
		}else{
			return 2;
		}
	}
	
	
	public function get_IRApprovalData($post){
		extract($post);
		$ID = $post['ID'];
		
		$sql = "SELECT * FROM InternalRequest WHERE ID ='$ID' ";
		$rResult = $this->db->query($sql);
		if($rResult->num_rows() > 0){
					 
			$data['IRData'] = $rResult->result_array();
			$sql  = "Select *,dbo.[fn_GetPRNumberByIRDetailID](ID) as PRNumber FROM v_ol_frm_IRDetail WHERE IRID = '$ID'";
	
			$rResult = $this->db->query($sql);
			$data['IRDetailData'] = $rResult->result_array();

		}else{
			$data =1;
		}
		return $data;
	}
	function Email($content,$title,$mailto,$mailcc){ 	
		$this->email->from(MAIL_SUPPORT,MAIL_SUPPORT_NAME);
		
		$this->email->to($mailto);
		$this->email->cc($mailcc); 
		$this->email->subject("PRD - InternalRequest");
		$this->email->message($content);
		if($this->email->send()){
			return 1;					
		}else{
			return 3; 
		}
	}
	
	public function __deconstruct(){
	
	}
}
?>