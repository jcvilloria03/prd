<?php
class M_POReceiving extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function view_POReceivingDetailData($post){
		extract($post);
		
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['username'];
		
		$PONumber = $post['PONumber'];
		$sIndexColumn = "POReceivingDetailID";
		$sTable  = "v_ol_frm_POReceivingDetail";
				
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		//array_push($aColumns, "Action");
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		
		$sTWhere = "  [PONumber]='$PONumber' ";
		$sWhere .= "   [PONumber]='$PONumber' ";
			
		/* Data set length after filtering */
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
	  
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
		
		$rResult = $this->db->query($sql);
		  
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		$j =0;
		foreach (  $rResult->result_array() as $aRow )
		{
			$j++;
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $j; //$custID;
					/* Special output formatting */
					
				}else if ( $aColumns[$i] == 'Invoice' || $aColumns[$i] == 'DONumber' || $aColumns[$i] == 'Qty' )
				{
					
					$row[] =  '<div id="'.$custID.'" class="edit" name="'.$aColumns[$i].'">'.$aRow[ $aColumns[$i] ].'</div>';
					/* Special output formatting */
					
				}
				else if ( $aColumns[$i] != ' ' )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}

					
			
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	public function view_POReceivingData($post){
		extract($post);
		
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['username'];
		
		$PONumber = $post['PONumber'];
		$Type = (isset($post['Type']))?$post['Type']:'';
		
		$sIndexColumn = "PONumber";
		$sTable  = "v_ol_frm_POReceiving";
				
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		//array_push($aColumns, "Action");
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		
		$sTWhere = "  [PONumber]='$PONumber' ";
		$sWhere .= "   [PONumber]='$PONumber' ";
		
		if($Type == "RemainQty"){
			$sWhere .= "  And RemainQty<> 0 ";
			$sTWhere = " And RemainQty<> 0 ";
		}else if($Type == "CompleteQty"){
			$sWhere .= "  And RemainQty = 0 ";
			$sTWhere = " And RemainQty = 0 ";
		}
		/* Data set length after filtering */
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
	  
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
		
		$rResult = $this->db->query($sql);
		  
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
					
				}
				else if ( $aColumns[$i] != ' ' )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}

					
			
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	
	// public function view_POReceivingData($PONumber){
		// $sql = "Select POReceivingID ,PONumber,PartNumber ,[Description], 
								// RequestQty ,ReceivedQty ,QtyOut ,(RequestQty-ReceivedQty) as RemainQty 
								// FROM v_ol_frm_POReceivingList WHERE PONumber ='$PONumber'";
		// $result = $this->db->query($sql); 

		// if($result->num_rows() > 0){
			// return json_encode($result->result_array());	
		// }else{
			// return 1;
		// }
	// }
	
	public function check_PONumber($PONumber){
		$sql ="Select * from POReceiving WHERE PONumber = '$PONumber'";
		$result = $this->db->query($sql);
		if($result->num_rows() > 0){
			$sql =" Select COUNT(*) as RCount from POReceiving where PONumber ='$PONumber' And RequestQty - ReceivedQty <> 0 ";
			$rResult = $this->db->query($sql);
			$rResult = $rResult->result_array();
			if($rResult[0]['RCount'] == 0)
			{
				return '1|0';	
			}Else
			{
				return '1|1';
			}	
		}else{
			return 0;
		}
	}
	public function save_POReceivingDetail($post){
		extract($post);
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['username'];
		$PONumber =$post['PONumber'];
		$PartNumber =$post['PartNumber'];
		$Invoice  =$post['Invoice'];
		$DONumber  =$post['DONumber'];
		$DeliveryDate =$post['DeliveryDate'];
		$Qty =  $post['Qty'];
		
		$rResult = $this->db->query ("DECLARE @Flag Int;
				 execute [sp_ol_POReceivingDetailSave] '$PONumber','$PartNumber','$Invoice','$DONumber','$DeliveryDate','$Qty','$username',@Flag OUTPUT;
				 Select @Flag as Result;");
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
		
		// $rResult = $this->db->query ("DECLARE @Flag Int;
									 // execute sp_ol_POReceivingDetailSave '$poreceivingid','$invoice','$ddate','$dqty',@Flag OUTPUT;
									 // Select @Flag as Result;");
		// $rResult = $rResult->result_array();
		// return $rResult[0]['Result'];
	}
	
	// public function save_POReceivingData($post){
		// extract($post);
		// $poreceivingid =$post['poreceivingid'];
		// $ddate  =$post['ddate'];
		// $dqty =$post['dqty'];
		// $invoice =  $post['invoice'];
		
		// return $this->savePOReceiving($poreceivingid,$invoice,$ddate,$dqty);
		// // $rResult = $this->db->query ("DECLARE @Flag Int;
									 // // execute sp_ol_POReceivingDetailSave '$poreceivingid','$invoice','$ddate','$dqty',@Flag OUTPUT;
									 // // Select @Flag as Result;");
		// // $rResult = $rResult->result_array();
		// // return $rResult[0]['Result'];
	// }
	
	public function save_POReceivingSaveAll($post){
		extract($post);
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['username'];
		$PONumber =$post['PONumber'];
		$Invoice  =$post['Invoice'];
		$DONumber  =$post['DONumber'];
		$DeliveryDate =$post['DeliveryDate'];
		$Qty =  $post['Qty'];
		
		$rResult = $this->db->query ("DECLARE @Flag Int;
				 execute [sp_ol_POReceivingSaveAll] '$PONumber','$Invoice','$DONumber','$DeliveryDate','$Qty','$username',@Flag OUTPUT;
				 Select @Flag as Result;");
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	// public function save_POReceivingMultiData($post){
		// extract($post);
		// $poreceivingids = explode("|", $poreceivingids);
		// $ddates = explode("|", $ddates);
		// $dqtys = explode("|", $dqtys);
		// $invoices = explode("|", $invoices);
		// for($i = 0; $i < count($poreceivingids) - 1; $i++){
			// if($this->savePOReceiving($poreceivingids[$i],$invoices[$i],$ddates[$i],$dqtys[$i]) != 0){
				// echo('error');
				// return 2;
			// }
		// }		
		// return 0;
	// }
	
	// public function savePOReceiving($id,$invoice,$date,$qty){
		// $rResult = $this->db->query ("DECLARE @Flag Int;
									 // execute sp_ol_POReceivingDetailSave '$id','$invoice','$date','$qty',@Flag OUTPUT;
									 // Select @Flag as Result;");
		// $rResult = $rResult->result_array();
		// return $rResult[0]['Result'];
	// }
	
	public function update_POReceivingDetail($post){
		extract($post);
			$result = $this->db->get_where('POREceivingDetail', array('POReceivingDetailID'=>$id));		
		if($result->num_rows() == 1){
			$sql ='Update POREceivingDetail SET ';
			if($fieldname =='Invoice'){
				$sql .= " Invoice= '$value' ";
			}else if($fieldname =='DONumber'){
				$sql .= " DONumber= '$value' ";
			}else if($fieldname =='SONumber'){
				$sql .= " SONumber= '$value' ";
			}
			if($fieldname <> ''){
				$sql .= ' WHERE POReceivingDetailID = '.$id;
			}
			if($fieldname =='Qty'){
				 $rResult = $this->db->query ("exec sp_UpdatePOReceivingQty '".$id."','".$value."'");
				$rResult = $rResult->result_array();
				return $rResult[0]['Result'];
			}else{
				$this->db->query ($sql);
			}
		}else{
			return 2;
		}
		
	}
}
?>