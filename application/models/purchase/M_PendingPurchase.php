<?php
class M_PendingPurchase extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function view_PRData(){
		$result = $this->db->query('Select pr.PRNumber,pr.PRType,d.Department,pr.DateRequired,
										   pr.RequestedBy,pr.PurchasedBy,pr.ApprovedBy 
									from PurchaseRequisition pr,Department d
									where pr.Department = d.ID');
		if($result->num_rows() > 0){
			return json_encode($result->result_array());	
		}else{
			return 1;
		}
	}
	
	public function create_PO($PRNumber){

		$rResult = $this->db->query("DECLARE @Flag Int;
									 execute sp_ol_CreatePO '$PRNumber',@Flag OUTPUT;
									 Select @Flag as Result;");
									 
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function get_VendorAddress($VendorID)
	{
		$this->db->from('vendors');
		$this->db->select('VendorAddress1,VendorAddress2,VendorAddress3');
		$this->db->where('VendorID',$VendorID);	
		$result =  $this->db->get();
		
		if($result->num_rows() > 0){
			return json_encode($result->result());
		}
	}
	
	public function update_PO($post){
		extract($post);
		$PONumber =$post['PONumber'];
		$VendorID  =$post['VendorID'];
		$PaymentTerms =$post['PaymentTerms'];
		$DeliveryTerms =  $post['DeliveryTerms'];
		$ShipToAddress = $post['ShipToAddress'];
		$BillingAddress = $post['BillingAddress'];
		$Attention  =$post['Attention'];
		$ReqDeliveryDate = $post['ReqDeliveryDate'];
		$Ext = $post['Ext'];
		$Remarks ='';
		$Buyer  =$post['Buyer'];
		$BuyerExt  =$post['BuyerExt'];
		
		$rResult = $this->db->query ("DECLARE @Flag Int;
									 execute sp_ol_POupdate '$PONumber','$VendorID','$PaymentTerms','$DeliveryTerms','$ShipToAddress','$BillingAddress',
															'$Attention','$Ext','$ReqDeliveryDate','$Remarks','$Buyer','$BuyerExt',@Flag OUTPUT;
									 Select @Flag as Result;");
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function complete_POData($PONumber){
		$result = $this->db->get_where('PurchaseOrder', array('PONumber'=>$PONumber));		
		
		if($result->num_rows() == 1){
			$data = array(
				   'Status' => 1
				);				
			return $this->db->update('PurchaseOrder', $data, array('PONumber'=>$PONumber)); 
		}else{
			return 2;
		}
		
	}
	
	public function delete_POData($PONumber){
			$rResult = $this->db->query("DECLARE @Flag Int;
									 execute sp_ol_PODelete '$PONumber',@Flag OUTPUT;
									 Select @Flag as Result;");
									 
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
		
	}
	
	public function view_AllPRAndPRDetailData($PONumber)
	{	
		$this->db->from('PurchaseOrder');
		$this->db->select('[PONumber],[VendorID],[PaymentTerms],[DeliveryTerms], [ShipToAddress],[BillingAddress],
						 [Attention],[Ext],[ReqDeliveryDate],[Remarks],[Buyer],[BuyerExt],[EntryDate],[Status],[Deleted]');
		$this->db->where('PONumber',$PONumber);	
		$result =  $this->db->get();
		if($result->num_rows() > 0){
			$Data['PurchaseOrder'] = $result->result();
		}else{
			$Data['PurchaseOrder'] = '1';
		}
		
		$this->db->from('PODetail');
		$this->db->select('[PONumber],[PartNumber],[Quantity],[UnitPrice]');
		$this->db->where('PONumber',$PONumber);	
		$result =  $this->db->get();
		if($result->num_rows() > 0){
			$Data['PODetail'] = $result->result();
		}else{
			$Data['PODetail'] = '1';
		}
		
		return json_encode($Data);
	}
}
?>