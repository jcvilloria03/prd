<?php
class M_PurchaseOrder extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function viewPendingPurchase_Data($post){
		extract($post);
		
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0; 
		
		$PRStatus = isset($post['PRStatus'])?$post['PRStatus']:'';
		$PRType = isset($post['PRType'])?$post['PRType']:'';
		
		$sIndexColumn = "PRNumber";
		$sTable  = "v_ol_frm_PendingPurchase";
				
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		//array_push($aColumns, "Action");
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		
		$sTWhere = "";
		if($PRStatus != ''){
			$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
			$sTWhere = "  [Status]='$PRStatus' ";
			$sWhere .= "   [Status]='$PRStatus' ";
			if($PRStatus =='Pending'){
			}else if($PRStatus =='Draft'){
			}else if($PRStatus =='Completed'){
			}else if($PRStatus =='Deleted'){
			}
		}
		
		if($PRType==''){}
		Else{
			$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
			$sTWhere .= (  $sTWhere == "" ||  $sTWhere == " " )?" ": " AND ";	
			$sTWhere .= " PRType = '$PRType'";
			$sWhere .= " PRType = '$PRType'";
		}
		
		/* Data set length after filtering */
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
	  
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
		
		$rResult = $this->db->query($sql);
		  
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
					
				}else if ( $aColumns[$i] == 'Status'){
				
					switch ($aRow[ $aColumns[$i] ]) {
						case 'Completed': 
							$Status = '<span class="status green" >Completed</font>	';
							$Action = '<a title="Purchase Order Detail"  class="detail-btn" href="#" onclick="fn_todoPR(\'PODetail\',\''.$custID.'\');" >PO Detail</a>';
							$Action .= '<a title="Print Purchase Order"  class="print-btn" href="#" onclick="fn_todoPR(\'PrintPo\',\''.$custID.'\');" >Print PO</a>';
							break;
						case 'Deleted': 
							$Status = '<span class="status red" > Deleted</font>';
							$Action = '';
							break;
						case 'Pending': 
							$Status = '<span class="status grey" >Pending</font>';
							$Action = '<a title="Purchase Order Detail" class="create-btn" href="#" onclick="fn_todoPR(\'PODetail\',\''.$custID.'\');" >Create PO</a>';
							break;
						case 'Draft': 
							$Status = '<span  class="status grey" >Draft</font>';
							$Action = '<a title="Create Purchase Order"  class="detail-btn" href="#" onclick="fn_todoPR(\'PODetail\',\''.$custID.'\');" >Create PO</a>'; 
							break;	
						default: 
							$Status =  '';
							$Action = '';
							Break;   
					}
					$Action .= '<a title="Detail" class="details-open" href="#" onclick="fn_todoPR(\'detail\',this);" > Detail</a>';
					$row[] = $Status;
					$row[] = $Action;
				}
				else if ( $aColumns[$i] != ' ' )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}

					
			
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	
	// public function viewPendingPurchase_Data(){
		// $result = $this->db->query("Select PRNumber,PRType,Department,DateRequired,RequestedBy,PurchasedBy,ApprovedBy,
									// [PRStatus],POStatus,[PODeleted]
									// from v_ol_PendingPurchase ORDER BY EntryDate,RequestedDate");
		// if($result->num_rows() > 0){
			// return json_encode($result->result_array());	
		// }else{
			// return 1;
		// }
	// }
	
	public function view_PRData(){
		$result = $this->db->query('Select pr.PRNumber,pr.PRType,d.Department,pr.DateRequired,
										   pr.RequestedBy,pr.PurchasedBy,pr.ApprovedBy 
									from PurchaseRequisition pr,Department d
									where pr.Department = d.ID');
		if($result->num_rows() > 0){
			return json_encode($result->result_array());	
		}else{
			return 1;
		}
	}
	

	public function create_PO($PRNumber){

		$rResult = $this->db->query("DECLARE @Flag Int;
									 execute sp_ol_CreatePO '$PRNumber',@Flag OUTPUT;
									 Select @Flag as Result;");
									 
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function get_VendorAddress($VendorID)
	{
		$this->db->from('vendors');
		$this->db->select('VendorAddress1,VendorAddress2,VendorAddress3');
		$this->db->where('VendorID',$VendorID);	
		$result =  $this->db->get();
		
		if($result->num_rows() > 0){
			return json_encode($result->result());
		}
	}
	
	public function update_PO($post){
		extract($post);
		$PONumber =$post['PONumber'];
		$VendorID  =$post['VendorID'];
		$PaymentTerms =$post['PaymentTerms'];
		$DeliveryTerms =  $post['DeliveryTerms'];
		$ShipToAddress = $post['ShipToAddress'];
		$BillingAddress = $post['BillingAddress'];
		$Attention  =$post['Attention'];
		$ReqDeliveryDate = $post['ReqDeliveryDate'];
		$Ext = $post['Ext'];
		$Remarks = $post['Remarks'];
		$Buyer  =$post['Buyer'];
		$BuyerExt  =$post['BuyerExt'];
		
		$rResult = $this->db->query ("DECLARE @Flag Int;
									 execute sp_ol_POupdate '$PONumber','$VendorID','$PaymentTerms','$DeliveryTerms','$ShipToAddress','$BillingAddress',
															'$Attention','$Ext','$ReqDeliveryDate','$Remarks','$Buyer','$BuyerExt',@Flag OUTPUT;
									 Select @Flag as Result;");
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function complete_POData($PONumber){
		// $result = $this->db->get_where('PurchaseOrder', array('PONumber'=>$PONumber));		
		// if($result->num_rows() == 1){
			// $data = array(
				   // 'Status' => 1
				// );				
			// return $this->db->update('PurchaseOrder', $data, array('PONumber'=>$PONumber)); 
		// }else{
			// return 2;
		// }

		$rResult = $this->db->query ("DECLARE @Flag Int;
									 execute sp_ol_CompletePO '$PONumber',@Flag OUTPUT;
									 Select @Flag as Result;");
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];

	}
	
	public function unlock_POData($PONumber){
		$result = $this->db->get_where('PurchaseOrder', array('PONumber'=>$PONumber,'Status' => 1));		
		
		if($result->num_rows() == 1){
			$data = array(
				   'Status' => 0
				);				
			return $this->db->update('PurchaseOrder', $data, array('PONumber'=>$PONumber)); 
		}else{
			return 2;
		}
	}
	
	public function delete_POData($post){
		extract($post);
		$PONumber =$post['PONumber'];
		$DeletedReason  =$post['DeletedReason'];
		
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['username'];
		
		$rResult = $this->db->query("DECLARE @Flag Int;
									 execute sp_ol_PODelete '$PONumber','$DeletedReason','$username',@Flag OUTPUT;
									 Select @Flag as Result;");
									 
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
		
	}
	
	public function view_AllPRAndPRDetailData($PONumber)
	{	
		$this->db->from('PurchaseOrder');
		// $this->db->select('[PONumber],[VendorID],[PaymentTerms],[DeliveryTerms], [ShipToAddress],[BillingAddress],
						 // [Attention],[Ext],[ReqDeliveryDate],[Remarks],[Buyer],[BuyerExt],[EntryDate],[Status],[Deleted]');
		// $this->db->where('PONumber',$PONumber);	
		$result =$this->db->query("SELECT    PONumber, VendorID, PaymentTerms, DeliveryTerms, ShipToAddress, BillingAddress, Attention, Ext, ReqDeliveryDate, 
									Remarks,Buyer, BuyerExt, EntryDate, [Status], [Deleted], VendorAddress
									FROM         v_ol_PurchaseOrderList
									WHere  PONumber ='$PONumber'");
		//$result =  $this->db->get();
		if($result->num_rows() > 0){
			$Data['PurchaseOrder'] = $result->result_array();
		}else{
			$Data['PurchaseOrder'] = '1';
		}
		
		// $this->db->from('PODetail');
		// $this->db->select('[PONumber],[PartNumber],[Quantity],[UnitPrice]');
		// $this->db->where('PONumber',$PONumber);	
		$result =$this->db->query("SELECT [PONumber],[PartNumber],[PartDesc1],[Quantity],[UnitPrice],UOM 
								FROM v_ol_PODetailList4JSI	WHere  PONumber ='$PONumber'");
		//$result =  $this->db->get();
		if($result->num_rows() > 0){
			$Data['PODetail'] = $result->result_array();
		}else{
			$Data['PODetail'] = '1';
		}
		
		return json_encode($Data);
	}
	
	
	public function getPRDetail($post){
		extract($post);
		$PRNumber = $post['PRNumber'];
		
		$sql ="SELECT [PRNumber],[PRType],[Department],[VendorID],[DateRequired],[RequestedBy],[RequestedDate]
			  ,[PurchasedBy],[ApprovedBy],[Comment],[Remarks],[GST],[InternalRequest],[PRDApproved],[PRDApprovedBy]
			  ,[PRDApprovedReason] ,[ManagerApproved],[ManagerApprovedBy],[ManagerApprovedReason],[GMApproved]
			  ,[GMApprovedBy],[GMApprovedReason],[BMApproved],[BMApprovedBy],[BMApprovedReason],[Status],[Deleted],
			  [PRDSubmitted],[RequiredGMApproved],[DeletedReason],[MultiManagerApproval],
			  dbo.fn_GetUserFullName(RequestedBy) as RequestedByName, dbo.fn_GetUserFullName(PRDApprovedBy) as PRDApprovedByName,
			  dbo.fn_GetUserFullName(ManagerApprovedBy) as ManagerApprovedByByName, dbo.fn_GetUserFullName(BMApprovedBy) as BMApprovedByName,
			  dbo.fn_GetUserFullName(GMApprovedBy) as GMApprovedByName
		from PurchaseRequisition where PRNumber ='$PRNumber'";  
		
		$rResult = $this->db->query($sql);
		if($rResult->num_rows() > 0){
					 
			$data['PRData'] = $rResult->result_array();
			
			$sql = "SELECT * FROM PurchaseRequisition WHERE PRNumber = '$PRNumber' AND PRType =  'NORMAL'";
			$rResult = $this->db->query($sql);
			if($rResult->num_rows() > 0){
				$sql  = "Select *,RequestedQty*UnitPrice as TotalAmount from v_ol_PRDetailList4Normal where PRNumber ='$PRNumber'";
			}else{
				$sql  = "Select *,RequestedQty*UnitPrice as TotalAmount from v_ol_PRDetailList4JSI where PRNumber ='$PRNumber'";
			}
			$rResult = $this->db->query($sql);
			$data['PRDetailData'] = $rResult->result_array();
			// foreach($data['PRDetailData'] as $tmpArray){
				// echo($tmpArray->PartNumber); 
			// }
		}else{
			$data =1;
		}
		return $data;
	}
	
}
?>