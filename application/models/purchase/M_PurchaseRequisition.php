<?php
class M_PurchaseRequisition extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function viewPRData_AllData($post){
	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;

		$sIndexColumn = "PRNumber";
		$sTable = "v_ol_frm_PurchaseRequisition";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		array_push($aColumns, "PRDApproved", "Status");
		
		$PRStatus = isset($post['PRStatus'])?$post['PRStatus']:'';
		
		$PRType = isset($post['PRType'])?$post['PRType']:'';
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
	
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		
		$sTWhere = " WHERE  ((dbo.fn_CheckMultiRequestedByName(PRNumber,'".$username."') =1 OR dbo.fn_CheckMultiApprovedByName(PRNumber,'".$username."') = 1 OR 'PRD' = 'PRD' OR $is_PRDManager = 1) ";    
		//$sTWhere .= " OR (PRDSubmitted =1 ANd $is_PRDManager = 1) ";
		$sWhere .= " ((dbo.fn_CheckMultiRequestedByName(PRNumber,'".$username."') =1 OR dbo.fn_CheckMultiApprovedByName(PRNumber,'".$username."') = 1 OR 'PRD' = 'PRD' OR $is_PRDManager = 1) ";  
		//$sWhere .= " OR (PRDSubmitted =1 ANd $is_PRDManager = 1) ";  
		/* Data set length after filtering */
		//echo("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");  
		if($is_GM==1){
			$sTWhere .= " OR  (BMApproved=1 and RequiredGMApproved=1)) ";
			$sWhere .= " OR  (BMApproved=1 and RequiredGMApproved=1)) ";
		}else if( $is_BM == 1 && $is_manager  == 0){
			$sTWhere .= " OR (ManagerApproved=1)) ";
			$sWhere .= "  OR (ManagerApproved=1))  ";
		}else if( $is_BM == 1 && $is_manager  == 1){
			$sTWhere .= " And  PRDApproved =1 OR (ManagerApproved=1 )) ";
			$sWhere .= "  And  PRDApproved =1 OR (ManagerApproved=1 ))  ";
		}else if( $is_manager  == 1 && $is_PRDManager  == 0){
			$sTWhere .= " And PRDApproved =1) ";
				$sWhere .= " And  PRDApproved=1) ";
		} else if( $is_PRDManager  == 1){
			$sTWhere .= " And PRDSubmitted =1) ";
			$sWhere  .= " And PRDSubmitted =1) ";
		} else{
			$sTWhere .= " ) ";
			$sWhere  .= " ) ";
		
		}
		
		if($PRStatus ==''){
			
		}else{
			if(($is_BM == 1 && $is_manager  == 1) && ($PRStatus == 9 || $PRStatus == 7)){
				$sTWhere .= " AND PRDStatus in(9,7) " ;
				$sWhere .= " AND PRDStatus in(9,7) ";
			}else{
				$sTWhere .= " AND PRDStatus ='$PRStatus' " ;
				$sWhere .= " AND PRDStatus ='$PRStatus' ";
			}
		}
		
		if($PRType==''){}
		Else{
			
			$sTWhere .= " AND  PRType = '$PRType'";
			$sWhere .= " AND  PRType = '$PRType'";
		}
		
		if ($dep_name == 'PRD' or $dep_name == 'HOD'){
			$sWhere .= " AND  Department like '%'";
		} else {
			if($is_manager==1 and $dep_name <> 'QA'){
				$sWhere .= " and RequestedBy in (select Distinct [Name] from MasterData.dbo.[User] where DeptID = (select id from MasterData.dbo.Department where [Name] = '$dep_name')) OR ( Department = 'MULTIPLE' AND dbo.fn_CheckMultiApprovedByName(PRNumber,'$username') = 1 ) ";
			} elseif($is_manager==1 and $dep_name == 'QA'){
				// do nothing
			} else {
				$sWhere .= " AND  Department like '%$dep_name%'";
			}
		}
		// else if($PRStatus =='2'){ 
			// $sTWhere .= " AND PRDeleted =1 " ;
			// $sWhere .= " AND PRDeleted =1 ";
		// }else if($PRStatus =='Completed'){
			// $sTWhere .= " AND CompletedPR =1 " ;
			// $sWhere .= " AND CompletedPR =1 ";
		// }else if($PRStatus =='ApprovedByBM'){
			// $sTWhere .= " AND BMApproved =1 " ;
			// $sWhere .= " AND BMApproved =1 ";
		// }else if($PRStatus =='RejectedByBM'){
			// $sTWhere .= " AND BMApproved =2 " ;
			// $sWhere .= " AND BMApproved =2 ";
		// }else if($PRStatus =='ApprovedByDeptManager'){
			// $sTWhere .= " AND ManagerApproved =1 " ;
			// $sWhere .= " AND ManagerApproved =1 ";
		// }else if($PRStatus =='RejectedByDeptManager'){
			// $sTWhere .= " AND ManagerApproved =2 " ;
			// $sWhere .= " AND ManagerApproved =2 ";
		// }else if($PRStatus =='ApprovedByPRDManager'){
			// $sTWhere .= " AND PRDApproved =1 " ;
			// $sWhere .= " AND PRDApproved =1 ";
		// }else if($PRStatus =='RejectedByPRDManager'){
			// $sTWhere .= " AND PRDApproved =2 " ;
			// $sWhere .= " AND PRDApproved =2 ";
		// }else if($PRStatus =='SubmittedPRD'){
			// $sTWhere .= " AND PRDSubmitted =1 " ;
			// $sWhere .= " AND PRDSubmitted =1 ";
		// }else if($PRStatus =='Pending'){ 
			// $sTWhere .= " AND PRDSubmitted =0 and  PRDeleted =0" ;
			// $sWhere .= " AND PRDSubmitted =0  and  PRDeleted =0";
		// }
		
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
		
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
		//echo $sql;
		$rResult = $this->db->query($sql);
		 
		 
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
					
				}else if ( $aColumns[$i] == 'ID'){
					if($is_PRDManager== 1 && ($aRow['PRDStatus'] == 11 || 
						$aRow['PRDStatus'] == 10 || $aRow['PRDStatus'] == 9)){
						$Allow = true;
					}else if($is_BM == 1 && $is_manager  == 1 &&  ($aRow['PRDStatus'] == 7 || $aRow['PRDStatus'] != 1 
						 && $aRow['PRDStatus'] == 5 || $aRow['PRDStatus'] == 6 || $aRow['PRDStatus'] == 9 )){
						$Allow = true;
					}else if($is_manager == 1 && $is_BM == 0 && ($aRow['PRDStatus'] == 9 
						 || $aRow['PRDStatus'] == 7 || $aRow['PRDStatus'] == 8)){
						$Allow = true;
					}else if($is_BM == 1 && $is_manager  == 0 &&  ($aRow['PRDStatus'] == 7 || $aRow['PRDStatus'] != 1 
						 && $aRow['PRDStatus'] == 5 || $aRow['PRDStatus'] == 6 )){
						$Allow = true;
					}else if($is_GM == 1 && $aRow['PRDStatus'] == 5 ){
						$Allow = true;
					}else{
						$Allow = false;
					}
					
					$ID= $aRow[ trim($aColumns[$i],'[]') ];
					if($Allow){
						$row[] = '<input type="checkbox" name="Approved" id="chkID_'.$ID.'" value="'.$ID.'"><br>';
					}else{
						$row[] = $ID;
					}
				}else if ( $aColumns[$i] == 'PRDStatus'){
					switch ($aRow[ $aColumns[$i] ]) {
					   case 1: $row[] = '<span class="status green" >Completed</font>';break;
					   case 2: $row[] = '<span class="status red" > Deleted</font>';break;
					   case 3: $row[] = '<span class="status orange" >Approved By GM</font>';break;
					   case 4: $row[] = '<span class="status red" > Rejected By GM</font>';break;
					   case 5: $row[] = '<span class="status orange" >Approved By BM</font>';break;
					   case 6: $row[] = '<span class="status red" > Rejected By BM</font>';break;
					   case 7: $row[] = '<span class="status orange" >Approved By Dept. Manager</font>';break;
					   case 8: $row[] = '<span class="status red" > Rejected By Dept. Manager</font>';break;
					   case 9: $row[] = '<span class="status orange" >Approved By PRD Manager</font>';break;
					   case 10:$row[] = '<span class="status red" > Rejected By PRD Manager</font>';break;
					   case 11:$row[] = '<span class="status orange" >Submitted to PRD Manager By PRD User</font>';break;
					   case 12:$row[] = '<span  class="status grey"  >Draft</font>';break; 
					   default:$row[] = '';Break;
					}
				}else if ( $aColumns[$i] != ' ' && $aColumns[$i] != 'PRDApproved'  && $aColumns[$i] != 'Status')
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			
			
			$Action  = '';
			 if (strtolower($dep_name) =='prd' ){
				$Action = '<a title="Edit" class="edit-btn" href="#" onclick="fn_todoPR(\'edit\',\''.$custID.'\');" > Edit</a>';
				
				if( strtolower($aRow["Status"]) != 'completed'){
					//$Action .= '<a title="Delete" class="delete-btn" href="#" onclick="fn_todoPR(\'delete\',\''.$custID.'\');" >Delete</a>';
				}  
			 }else if( $aRow[ "ApprovedBy" ] == $username  || $is_GM ==1  || $is_BM == 1 || $is_manager == 1){
				$Action = '<a title="Edit" class="edit-btn" href="#" onclick="fn_todoPR(\'edit\',\''.$custID.'\');" > Edit</a>';
				//$Action .= '<img src="assets/images/details_open.png">';	
				
			}
			 if( $aRow[ "PRDApproved" ] == '1'){
				if($aRow[ "ApprovedBy" ]== $username || strtolower($dep_name) =='prd' || $is_GM == 1  || $is_BM == 1 || $is_manager == 1){  
					$Action .= '<a title="Print" class="print-btn" href="#" onclick="fn_todoPR(\'PrintPR\',\''.$custID.'\');" >Print PO</a>';
			}else { //if($aRow[ "RequestedBy" ]== $username)  
					$Action .= '<a title="Print" class="print-btn" href="#" onclick="fn_todoPR(\'PrintPRWithoutIMEI\',\''.$custID.'\');" >Print PO</a>';
				} 
			}

			$Action .= '<a title="Detail" class="details-open" href="#" onclick="fn_todoPR(\'detail\',this);" > Detail</a>';
			$row[] = $Action;
					
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
		
	public function view_PRDetailData($post){
		extract($post);
		$PRNumber = $post['PRNumber'];
		$PRDStatus = $post['PRDStatus'];
		
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0; 
		
		$sIndexColumn = "ID";

		$sql = "SELECT * FROM PurchaseRequisition WHERE PRNumber = '$PRNumber' AND PRType =  'NORMAL'";
		$rResult = $this->db->query($sql);
		if($rResult->num_rows() > 0){
			$sTable  = "v_ol_PRDetailList4Normal";
		}else{
			$sTable  = "v_ol_PRDetailList4JSI";
		}
				
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		//array_push($aColumns, "PRDApproved");
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
	
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		
		$sTWhere = " WHERE   PRNumber = '$PRNumber'  ";    
		$sWhere .= "  PRNumber = '$PRNumber'  ";  
	
	
		/* Data set length after filtering */
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
	  
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql ="SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";

		$rResult = $this->db->query($sql);
		  
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
					
				}
				else if ( $aColumns[$i] != ' ' )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			
			
			
			if($PRDStatus == 'Deleted' || $PRDStatus== 'Completed'){
				$Action  = ''; 
			}else if($is_PRDManager ==1 || $dep_name=="PRD"){
				$Action = '<a class="edit-btn" href="#" onclick="fn_todoPRDetail(\'edit\',\''.$custID.'\');" > edit </a>';
				$Action .= '<a class="delete-btn" href="#" onclick="fn_todoPRDetail(\'delete\',\''.$custID.'\');" >delete</a>'; 
			}else{
				$Action  = ''; 
			}
			$row[] = $Action;
					
			
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	// public function view_InternalRequest($post){
		// extract($post);
		// $PRNumber = $post['PRNumber'];
		// $result = $this->db->query("SELECT ID, PartNumber, RequestedQty, UOM,Department, RequestedBy, RequestedDate 
										// FROM v_ol_frm_viewIRDetail4PR 
									// WHERE [status] = 1 and Approved = 1 and  IRDetailStatus = 0 "); 
		// if($result->num_rows() > 0){
			// return json_encode($result->result_array());	 
		// }else{
			// return 1; 
		// } 
	// }
	
	public function view_InternalRequest($post){
		extract($post);
		$PRNumber = $post['PRNumber'];
		
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0; 
		
		$sIndexColumn = "ID";

		$sTable  = "v_ol_frm_viewIRDetail4PR";

		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		array_push($aColumns, "Status");
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
			
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
	
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		
		$sTWhere = " WHERE  [status] = 1 and Approved = 1 and  IRDetailStatus = 0   ";    
		$sWhere .= "  [status] = 1 and Approved = 1 and  IRDetailStatus = 0   ";  
	
	
		/* Data set length after filtering */
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
	  
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
		
		$rResult = $this->db->query($sql);
		  
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					//$row[] = $custID;
					$row[] = $custID;
					/* Special output formatting */
					
				}else if ( $aColumns[$i] == 'RequestedQty'){
					$RequestedQty =  '<input type="text" id="UnitPrice_'.$custID.'" readonly="readonly" class="readonly" value='. $aRow[ $aColumns[$i] ] .' />';
					$row[] = $RequestedQty;
				}else if ( $aColumns[$i] == 'Status'){
					$Status = '<input type="checkbox" id="chkID_'.$custID.'" value="'.$custID.'" />';
					$row[] = $Status;
				}
				else if ( $aColumns[$i] != ' ' )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
				
			}
			
			
			
			// if($PRDStatus == 'Deleted' || $PRDStatus== 'Completed'){
				// $Action  = ''; 
			// }else if($is_PRDManager ==1 || $dep_name=="PRD"){
				// $Action = '<a class="edit-btn" href="#" onclick="fn_todoPRDetail(\'edit\',\''.$custID.'\');" > edit </a>';
				// $Action .= '<a class="delete-btn" href="#" onclick="fn_todoPRDetail(\'delete\',\''.$custID.'\');" >delete</a>'; 
			// }else{
				// $Action  = ''; 
			// }
			// 
			
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	// public function viewPRData_AllData(){
		// $sql = "Select PRNumber,PRType,Department,DateRequired, RequestedBy,PurchasedBy,ApprovedBy,
										// [Status] from v_ol_PurchaseRequisition
										// WHERE     ([PRStatus] = 0 AND PRDeleted = 0) 
										// ORDER BY RequestedDate";
		// $result = $this->db->query($sql);
		// if($result->num_rows() > 0){
			// return json_encode($result->result_array());	
		// }else{
			// return 1;
		// }
	// }

	public function get_PRNumber($post){
		extract($post);
		$PRType = $post['PRType'];
		$sql = "DECLARE @PRNumber VARCHAR(20);
				execute sp_ol_GetPRNumber '$PRType',@PRNumber output;
				Select @PRNumber as Result";
		$rResult = $this->db->query($sql);
									
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function get_PRData($post){
		extract($post);
		$sql ="SELECT [PRNumber],[PRType],[Department],[VendorID],[DateRequired],[RequestedBy],[RequestedDate]
					  ,[PurchasedBy],[ApprovedBy],[Comment],CONVERT(TEXT,[Remarks]) AS [Remarks],[GST],[InternalRequest],[PRDApproved],[PRDApprovedBy]
					  ,[PRDApprovedReason] ,[ManagerApproved],[ManagerApprovedBy],[ManagerApprovedReason],[GMApproved]
					  ,[GMApprovedBy],[GMApprovedReason],[BMApproved],[BMApprovedBy],[BMApprovedReason],[Status],[Deleted],
					  [PRDSubmitted],[RequiredGMApproved],[DeletedReason],[MultiManagerApproval],
					  dbo.fn_GetUserFullName(RequestedBy) as RequestedByName, dbo.fn_GetUserFullName(PRDApprovedBy) as PRDApprovedByName,
					  dbo.fn_GetUserFullName(ManagerApprovedBy) as ManagerApprovedByByName, dbo.fn_GetUserFullName(BMApprovedBy) as BMApprovedByName,
					  dbo.fn_GetUserFullName(GMApprovedBy) as GMApprovedByName,Discount
				from PurchaseRequisition where PRNumber ='$PRNumber'";  
		 $result = $this->db->query($sql);
		if($result->num_rows() > 0){
			$data['PR'] = $result->row(0);
			if($data['PR']->MultiManagerApproval ==1){
				$sql =" SELECT Approved,ApprovedBy,dbo.fn_GetUserFullName(ApprovedBy) as ApprovedByName from PRDetail where PRNumber ='$PRNumber'
						Group By Approved,ApprovedBy ";
				 $result = $this->db->query($sql);
				 if($result->num_rows() > 0){
					$data['PRDetail']= $result->result_array();
				 }else{
					$data['PRDetail'] = 1;
				 }
			}
			//return json_encode_convert();	
		}else{
			$data = 1;
		}
		
		return  json_encode_convert($data);
	}
	
	public function get_PRDetailData($ID){
		$result = $this->db->get_where('PRDetail', array('ID'=>$ID));	
		if($result->num_rows() > 0){
			return json_encode($result->row());	
		}else{
			return 1;
		}
	}
	
	

	
	public function save_PRDetail($post){
		extract($post);
		$PRNumber = $post['PRNumber'];
		$PartDesc1 = $post['PartDesc1'];
		$PRType = $post['PRType'];
		
		$Department = $post['Department-Detail'];
		
		if($PRType =='NORMAL'){
			$PartNumber = $post['PartNumber-cbx'];
		}else{
			$PartNumber = $post['PartNumber'];
		}
		
		$CustomerID = isset($post['CustomerID'])?$post['CustomerID']:'';
		$RequestedQty = $post['RequestedQty'];
		$UOM = $post['UOM'];
		
		//Add Currency
		$Currency = $post['Currency'];
		
		$UnitPrice = $post['UnitPrice'];

		$sql = "DECLARE @Flag Int;
				 execute sp_ol_PRDetailSave '$PRNumber','$CustomerID','$PRType','$Department','$PartNumber', '$PartDesc1','$RequestedQty',
						'$UnitPrice','$UOM','$Currency',@Flag OUTPUT;
				 Select @Flag as Result;";
		$rResult = $this->db->query($sql);
									 
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function save_PR($post){
		extract($post);
		$PRNumber = $post['PRNumber'];
		$PRType = $post['PRType'];
		$Department = $post['Department'];
		$VendorID = $post['VendorID'];
		$DateRequired = $post['DateRequired'];
		$PurchasedBy = $post['PurchasedBy'];
		$RequestedBy = $post['RequestedBy'];
		$ApprovedBy = $post['ApprovedBy'];
		$Comment = $post['Comment'];
		$Remarks = $post['Remarks'];
		$GST =  ($post['GST']=='1')?1:0;
		$Discount =  $post['Discount'];
		$RequiredGMApproved = ($post['RequiredGMApproved']=='1')?1:0;
		
		$sql = "DECLARE @Flag Int;
				 execute sp_ol_PRSave '$PRNumber','$PRType', '$Department','$VendorID', '$DateRequired',
						'$PurchasedBy','$RequestedBy','$ApprovedBy','$Comment','$Remarks',$GST,$RequiredGMApproved,
						$Discount, @Flag OUTPUT;
				 Select @Flag as Result;";
		$rResult = $this->db->query($sql);
									 
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	} 
	
	public function update_PRD($post){
		extract($post);
		$PRNumber = $post['PRNumber'];
		$Department = $post['Department'];
		$VendorID = $post['VendorID'];
		$DateRequired = $post['DateRequired'];
		$PurchasedBy = $post['PurchasedBy'];
		$RequestedBy = $post['RequestedBy'];
		$ApprovedBy = $post['ApprovedBy'];
		$Comment = $post['Comment'];
		$Remarks = $post['Remarks'];
		$GST = ($post['GST']=='1')?1:0;
		$Discount =  $post['Discount'];
		$RequiredGMApproved = ($post['RequiredGMApproved']=='1')?1:0;
		
		$sql = "DECLARE @Flag Int;
				 execute sp_ol_PRUpdate '$PRNumber', '$Department','$VendorID' , '$DateRequired',
						'$PurchasedBy','$RequestedBy','$ApprovedBy','$Comment','$Remarks',$GST,$RequiredGMApproved,
						$Discount, @Flag OUTPUT;
				 Select @Flag as Result;";
		$rResult = $this->db->query($sql);
		
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function update_PRDetail($post){
		extract($post);
		$PRNumber = $post['PRNumber'];
		$PartDesc1 = $post['PartDesc1'];
		$ID = $post['ID'];
		$PartNumber = $post['PartNumber-hid'];
		$Department = $post['Department-Detail'];
		$PRType = $post['PRType'];
		if($PRType =='NORMAL'){
			$NewPartNumber = $post['PartNumber-cbx'];
		}else{
			$NewPartNumber = $post['PartNumber'];
		}		
		
		$CustomerID = isset($post['CustomerID'])?$post['CustomerID']:'';
		$RequestedQty = $post['RequestedQty'];
		$UOM = $post['UOM'];
		
		//Add Currency
		$Currency = $post['Currency'];
		$UnitPrice = $post['UnitPrice'];
		
		$sql ="DECLARE @Flag Int;
				 execute sp_ol_PRDetailUpdate '$ID','$PRNumber','$CustomerID','$PRType','$Department','$PartNumber','$NewPartNumber',
						'$PartDesc1','$RequestedQty','$UnitPrice','$UOM','$Currency',@Flag OUTPUT;
				 Select @Flag as Result;";
		$rResult = $this->db->query($sql);

		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function add_InternalRequest($post){
		extract($post);
		$PRNumber = $post['PRNumber'];
		$ID = $post['ID'];
		$UnitPrice = $post['UnitPrice'];
		
		$sql ="DECLARE @Flat Int;
				 execute [sp_ol_InternalRequestAdd4PR] '$PRNumber','$ID','$UnitPrice',@Flat OUTPUT;  
				 Select @Flat as Result;";

		$rResult = $this->db->query($sql); 

		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function complete_PRData($PRNumber){
		$sql = "DECLARE @Flag Int;
					 execute sp_ol_CompletePR '$PRNumber',@Flag OUTPUT;
					 Select @Flag as Result;";
		$rResult = $this->db->query($sql);
		
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function submit_PRData($post){
		extract($post);
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$is_GM = $session_data['is_GM'];
		$username = $session_data['username'];
		
		$PRNumber=$post['PRNumber'];
		$Type=$post['Type'];
		$Approved=$post['Approved'];
		$ApprovedBy=$username;
		$ApprovedReason=$post['ApprovedReason'];
		
		$sql = "DECLARE @Flag Int;
				 execute [sp_ol_SubmitPR] '$PRNumber','$Type',$Approved,'$ApprovedBy','$ApprovedReason',@Flag OUTPUT; 
				 Select @Flag as Result;";
		$rResult = $this->db->query($sql);
		  
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}

	public function unlock_PRData($PRNumber){
		$result = $this->db->get_where('PurchaseRequisition', array('PRNumber'=>$PRNumber,'Status' => 1));		
		
		if($result->num_rows() == 1){
			$data = array(
				   'Status' => 0
				);				
			return $this->db->update('PurchaseRequisition', $data, array('PRNumber'=>$PRNumber)); 
		}else{
			return 2;
		}
	}
	
	public function delete_PRDetailData($id){

		$sql ="DECLARE @Flag Int;
				execute sp_ol_DeletePRDetail '$id',@Flag OUTPUT;
				Select @Flag as Result;";
		$rResult = $this->db->query($sql);
		
		
		$rResult = $rResult->result_array();
		return $rResult[0]['Result']; 
	}
	
	public function get_PartsPrice($PartNumber){
		$this->db->select('UOM,UnitPrice,LoadingPrice as BasePrice,BaseCurrency as Currency');

		$result = $this->db->get_where('Parts',array('PartNumber' => $PartNumber));
		if($result->num_rows() == 1){
			return json_encode($result->row());	
		}else{
			return 2;
		}
	}
	
	public function get_PRApprovalData($post){
		extract($post);
		$PRNumber = $post['PRNumber'];
		
		$sql ="SELECT [PRNumber],[PRType],[Department],[VendorID],[DateRequired],[RequestedBy],[RequestedDate]
			  ,[PurchasedBy],[ApprovedBy],[Comment],[Remarks],[GST],[InternalRequest],[PRDApproved],[PRDApprovedBy]
			  ,[PRDApprovedReason] ,[ManagerApproved],[ManagerApprovedBy],[ManagerApprovedReason],[GMApproved]
			  ,[GMApprovedBy],[GMApprovedReason],[BMApproved],[BMApprovedBy],[BMApprovedReason],[Status],[Deleted],
			  [PRDSubmitted],[RequiredGMApproved],[DeletedReason],[MultiManagerApproval],
			  dbo.fn_GetUserFullName(RequestedBy) as RequestedByName, dbo.fn_GetUserFullName(PRDApprovedBy) as PRDApprovedByName,
			  dbo.fn_GetUserFullName(ManagerApprovedBy) as ManagerApprovedByByName, dbo.fn_GetUserFullName(BMApprovedBy) as BMApprovedByName,
			  dbo.fn_GetUserFullName(GMApprovedBy) as GMApprovedByName
		from PurchaseRequisition where PRNumber ='$PRNumber'";  
		
		$rResult = $this->db->query($sql);
		if($rResult->num_rows() > 0){
					 
			$data['PRData'] = $rResult->result_array();
			
			$sql = "SELECT * FROM PurchaseRequisition WHERE PRNumber = '$PRNumber' AND PRType =  'NORMAL'";
			$rResult = $this->db->query($sql);
			if($rResult->num_rows() > 0){
				$sql  = "Select * from v_ol_PRDetailList4Normal where PRNumber ='$PRNumber'";
			}else{
				$sql  = "Select * from v_ol_PRDetailList4JSI where PRNumber ='$PRNumber'";
			}
			$rResult = $this->db->query($sql);
			$data['PRDetailData'] = $rResult->result_array();
			// foreach($data['PRDetailData'] as $tmpArray){
				// echo($tmpArray->PartNumber); 
			// }
		}else{
			$data =1;
		}
		return $data;
	}
	public function delete_PRData($post){
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['username'];
		
		extract($post);
		$PRNumber = $post['PRNumber'];
		$DeletedReason = $post['DeletedReason'];
		
		$sql = "DECLARE @Flag Int;
				 execute sp_ol_PRDelete '$PRNumber','$DeletedReason','$username',@Flag OUTPUT;
				 Select @Flag as Result;";
				 
		$rResult = $this->db->query($sql);
								 
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
		
	}
	
}
?>