<?php
class M_SaleOrder extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_SOData($SONumber){
		$result = $this->db->query("Select top 1 SONumber,PONumber,SOType,CustomerID,Name,
						Convert(varchar(10),SODate,101) as SODate,OneTimeCharge,transportationCharge,
					TTCharges,FreightCharges,Remarks,EntryDate,Status,[Deleted],DeletedReason  
					from SaleOrder WHERE SONumber = '$SONumber'");
		if($result->num_rows() > 0){
			return json_encode($result->result_array());	
		}else{
			return 1;
		}
	}
	
	// public function viewSOData_AllData(){
		// $result = $this->db->query("Select SONumber,PONumber,SOType,CustomerID,CustomerName,Name,SODate,TTCharges,FreightCharges,
									// EntryDate,[Status],Deleted FROM v_ol_frm_SaleOrder 
									// WHERE [Status] = 0 and Deleted = 0");
		// if($result->num_rows() > 0){
			// return json_encode($result->result_array());	
		// }else{
			// return 1;
		// }
	// }
	
	public function viewSOData_AllData($post){
	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		
		$sIndexColumn = "SONumber";
		$sTable = "v_ol_frm_SaleOrder";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		

		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
	
			// $sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		
		// $sTWhere = " WHERE  (RequestedBy= '".$username."' OR ApprovedBy= '".$username."' OR '$dep_name' ='PRD') OR (($is_GM =1  OR $is_BM = 1 )AND ManagerApproved=1) ";    
		// $sWhere .= " (RequestedBy= '".$username."' OR ApprovedBy= '".$username."' OR '$dep_name' ='PRD')  OR (($is_GM =1  OR $is_BM = 1 )AND ManagerApproved=1) ";  
		 
		/* Data set length after filtering */
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
	  
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$rResult = $this->db->query("SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo");
		  
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
					
				}else if ($aColumns[$i] == 'SOStatus'){
					if($aRow[ "SOStatus" ]== 'Draft'){
						$status='<span  class="status grey" >Draft</font>';
						$action = '<a title="Edit" class="edit-btn" href="#" onclick="fn_todoSO(\'edit\',\''.$custID.'\');" > Edit </a>';
						$action .= '<a title="Delete" class="delete-btn" href="#" onclick="fn_todoSO(\'delete\',\''.$custID.'\');" >Delete</a>';
					}else if($aRow[ "SOStatus" ]== 'Completed'){
						$action = '<a title="View"  class="detail-btn" href="#" onclick="fn_todoSO(\'edit\',\''.$custID.'\');" > Edit </a>';
						$action .= '<a class="print-btn" href="#" onclick="fn_todoSO(\'print\',\''.$custID.'\');" > print</a>';
						$status ='<span class="status green" >Completed</font>	';
					}else if ($aRow[ "Deleted" ] =='Deleted'){
						$status ='<span class="status red" > Deleted</font>';;
						$action='';
					}else{
						$status ='';
						$action='';
					 }
					$action .= '<a title="Detail" class="details-open" href="#" onclick="fn_todoSO(\'detail\',this);" > Detail</a>';
				
					 $row[] =  $status;
					 $row[] =  $action;
				}else if ( $aColumns[$i] != ' ')
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			
			
			// $Action  = '';
			// if($aRow[ "Status" ]== ''){
					// }else if($aRow[ "Status" ]== 'Completed'){
			// }else if ($aRow[ "Status" ] =='Deleted'){

			// }else{
				// $status ='';
				// $action='';
			 // }
		
			// $row[] = $action; 
					
			
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	
	public function create_SO($SONumber){

		$rResult = $this->db->query("DECLARE @Flag Int;
									 execute [sp_ol_CreateSO] '$SONumber',@Flag OUTPUT;
									 Select @Flag as Result;");
									 
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function save_SODetail($post){
		extract($post);
		$SONumber =$post['SONumber'];
		$PONumber =$post['PONumber'];
		$PartNumber =$post['PartNumber'];
		$Quantity =$post['Quantity'];
		$sql = "DECLARE @Flag Int;
				  execute [sp_ol_SODetailSave] '$SONumber','$PONumber','$PartNumber','$Quantity',@Flag OUTPUT;
				  Select @Flag as Result;";

		$rResult = $this->db->query($sql);
									 
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	public function update_SODetailQty($post){
		extract($post);
		$SODetailID =$post['SODetailID'];
		$Quantity =$post['Quantity'];
	
		$rResult = $this->db->query("DECLARE @Flag Int;
									  execute [sp_ol_SODetailUpdateQty] '$SODetailID','$Quantity',@Flag OUTPUT;
									  Select @Flag as Result;");
								
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function get_SONumber($PONumber){
		
		$rResult = $this->db->query("DECLARE @SONumber VARCHAR(20);
									execute sp_ol_GetSoNumber '$PONumber',@SONumber output;
									Select @SONumber as Result");
									
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];

	}

	public function save_SO($post){
		extract($post);
		$session_data = $this->session->userdata('logged_in');
		$Name = $session_data['name'];
		
		$SONumber =$post['SONumber'];
		$PONumber  =$post['PONumber'];
		$SOType  =$post['SOType'];
		$SODate =$post['SODate'];
		$CustomerID = $post['CustomerID'];
		$OneTimeCharge = ($post['OneTimeCharge'] == '')? 0 : $post['OneTimeCharge'];
		$FreightCharges = ($post['FreightCharges'] == '')? 0 : $post['FreightCharges'];
		$TTCharges  = ($post['TTCharges'] == '')? 0 : $post['TTCharges'];
		$Remarks =$post['Remarks'];
		
		$rResult = $this->db->query ("DECLARE @Flag Int;execute sp_ol_SOSave '$SONumber','$PONumber','$SOType','$SODate','$CustomerID',$OneTimeCharge,
									$FreightCharges,$TTCharges,'$Remarks','$Name',@Flag OUTPUT;Select @Flag as Result;");
					
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
	
	public function delete_SOData($post){
		extract($post);
		$SONumber =$post['SONumber'];
		$DeletedReason  =$post['DeletedReason'];
		
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['username'];
		
		$rResult = $this->db->query("DECLARE @Flag Int;
						 execute sp_ol_SODelete '$SONumber','$DeletedReason','$username',@Flag OUTPUT;
						 Select @Flag as Result;");
						 
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
		
	}
	
	public function delete_SODetailData($SODetailID){
		$result = $this->db->get_where('SODetail', array('SODetailID'=>$SODetailID));	
		if($result->num_rows() ==1){
			$sql = "Update por set por.QtyOut = por.QtyOut - dbo.SODetail.Quantity
							FROM dbo.POReceiving por INNER JOIN  dbo.SODetail
							ON dbo.SODetail.PartNumber = por.PartNumber 
							INNER JOIN dbo.SaleOrder ON dbo.SODetail.SONumber = dbo.SaleOrder.SONumber 
							AND por.PONumber = dbo.SaleOrder.PONumber
							WHERE dbo.SODetail.SODetailID = $SODetailID";
			$this->db->query($sql);
			$sql = "Update PORD SET PORD.SONumber = Null FROM POReceivingDetail PORD,SODetail sod,POReceiving POR
					WHERE PORD.POReceivingID = POR.POReceivingID 
					AND sod.PartNumber = POR.PartNumber And sod.SONumber = PORD.SONumber
					And sod.SODetailID = $SODetailID ";
			$this->db->query($sql);
			return $this->db->delete('SODetail', array('SODetailID'=>$SODetailID));
		}else{
			return 2;
		}
		
	}
	
	public function complete_SOData($SONumber){
		// $result = $this->db->get_where('SaleOrder', array('SONumber'=>$SONumber));		

		// if($result->num_rows() == 1){
			// $data = array(
				   // 'Status' => 1
				// );				
			// return $this->db->update('SaleOrder', $data, array('SONumber'=>$SONumber)); 
		// }else{
			// return 2;
		// }
		$sql = "DECLARE @Flag Int;
									 execute sp_ol_CompleteSO '$SONumber',@Flag OUTPUT;
									 Select @Flag as Result;";
							 
		$rResult = $this->db->query($sql); 
									 
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
		
	}
	
	public function unlock_SOData($SONumber){
		$result = $this->db->get_where('SaleOrder', array('SONumber'=>$SONumber,'Status' => 1));		
		
		if($result->num_rows() == 1){
			$data = array(
				   'Status' => 0
				);				
			return $this->db->update('SaleOrder', $data, array('SONumber'=>$SONumber)); 
		}else{
			return 2;
		}
	}
	
	public function view_PODetail($post){
		extract($post);
		$SONumber = $post['SONumber'];
		$PONumber = $post['PONumber'];		
		$rResult = $this->db->query("execute sp_ol_GetPODetailData '$SONumber','$PONumber';");
		if($rResult->num_rows() > 0){
			return json_encode($rResult->result_array());	
		}else{
			return 1;
		}
	}
	
	public function view_SODetail($SONumber){
		$rResult = $this->db->query("execute sp_ol_GetSODetailData '$SONumber';");
		if($rResult->num_rows() > 0){
			return json_encode($rResult->result_array());	
		}else{
			return 1;
		}
	}
	public function get_SODetail($post){
		extract($post);
		$SONumber = $post['SONumber'];
		$sql = "  Select So.SONumber,p.Description,p.Description2,So.UnitPrice,So.Quantity,So.UnitPrice*So.Quantity as TotalQty FROM SODetail So,Parts P
					WHERE So.PartNumber = p.PartNumber AND So.SONumber = '$SONumber'   ";
		$rResult = $this->db->query($sql);
		if($rResult->num_rows() > 0){
			$data['SODetailData'] = $rResult->result_array();
			
		}else{
			return 1;
		}
	
		return($data); 
	}
	
	
}
?>