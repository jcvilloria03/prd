<?php
class M_SkipPRApproval extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	
		
	public function view_PRDetailData($post){
		extract($post);
		$PRNumber = $post['PRNumber'];

		$sIndexColumn = "ID";

		$sql = "SELECT * FROM PurchaseRequisition WHERE PRNumber = '$PRNumber' AND PRType =  'NORMAL'";
		$rResult = $this->db->query($sql);
		if($rResult->num_rows() > 0){
			$sTable  = "v_ol_PRDetailList4Normal";
		}else{
			$sTable  = "v_ol_PRDetailList4JSI";
		}
				
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		//array_push($aColumns, "PRDApproved");
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
	
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		
		$sTWhere = " WHERE   PRNumber = '$PRNumber'  ";    
		$sWhere .= "  PRNumber = '$PRNumber'  ";  
	
	
		/* Data set length after filtering */
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
	  
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql ="SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";

		$rResult = $this->db->query($sql);
		  
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
					
				}
				else if ( $aColumns[$i] != ' ' )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
			
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	
	public function view_PRData($post){
		$PRNumber =$post['PRNumber'];
		$sql = "SELECT * FROM v_ol_frm_PRData4SkipPRApproval WHERE PRNumber ='$PRNumber'";
		$result = $this->db->query($sql);	
		if($result->num_rows() > 0){
			return json_encode($result->row());	
		}else{
			return 1;
		}
	}
	
	public function action_SkipPRApproval($post){
		$PRNumber =$post['PRNumber'];
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['username'];
		$sql = "DECLARE @Flag Int;
					 execute [sp_ol_SkipManagerApproval] '$PRNumber','$username',@Flag OUTPUT;
					 Select @Flag as Result;";
		$rResult = $this->db->query($sql);
		
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
}
?>