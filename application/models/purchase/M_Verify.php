<?php
class M_Verify extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_SOSummaryReport($post){
	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;

		$sIndexColumn = "SONumber";
		$sTable = "v_ol_rpt_ACMSummaryReport";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
		array_push($aColumns, "SODetailID");
		
		$CustomerID = isset($post['CustomerID'])?$post['CustomerID']:'';
		$VendorID = isset($post['VendorID'])?$post['VendorID']:'';
		$from = isset($post['from'])?$post['from']:'';
		$to = isset($post['to'])?$post['to']:'';
		$soConfirmType = isset($post['soConfirmType'])?$post['soConfirmType']:'';
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		
		
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		$sTWhere = " WHERE  DeliveryDate between '$from' and  '$to'  ";
		$sWhere .= " DeliveryDate between '$from' and '$to' ";
		if($CustomerID != ''){
			$sTWhere .= " and CustomerID ='$CustomerID'";
			$sWhere .= " and CustomerID ='$CustomerID'";
		}
		if($VendorID != ''){
			$sTWhere .= " and VendorID ='$VendorID'";
			$sWhere .= " and VendorID ='$VendorID'";
		}
		if($soConfirmType =='Confirm'){
			$sTWhere .= " and QAConfirm =1";
			$sWhere .= " and QAConfirm =1";
		}else if($soConfirmType =='NotConfirm'){
			$sTWhere .= " and QAConfirm =0";
			$sWhere .= " and QAConfirm =0";
		}
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
		
		
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
		
		$rResult = $this->db->query($sql);
		 
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = '<a href="#" onclick="fn_todoPR(\'PrintSO\',\''.$custID.'\')">'.$custID.'</a>';
					/* Special output formatting */
				}else if ( $aColumns[$i] == 'PONumber' )
				{
					/* General output */
					$PONumber = $aRow[ $aColumns[$i] ];
					$row[] = '<a href="#" onclick="fn_todoPR(\'PrintPO\',\''.$PONumber.'\')">'.$PONumber.'</a>';
					
				}else if( $aColumns[$i] == 'QAConfirm' ){
					$ID =  $aRow['SODetailID'];
					if($aRow[ $aColumns[$i] ] == '1'){
						$row[] = '<a  href="#" onclick="fn_todoPR(\'SOSummary\',\''.$ID.'\')">Remove Confirmed</a>';
					}else{
						
						$row[] = '<input type="checkbox" name="Approved" id="chkID_'.$ID.'" data="'.$ID.'" value="'.$ID.'" OnClick="fn_todoPR(\'Checked\',this);"><br>';
						
					}
				}else if ( $aColumns[$i] != ' ' &&  $aColumns[$i] !=  'SODetailID' )
				{
					if(is_numeric( $aRow[ $aColumns[$i] ]) && $aColumns[$i] != 'Invoice' && $aColumns[$i] != 'SOQty' && $aColumns[$i] != 'Qty'  ){
						if ($aColumns[$i] == 'UnitPrice' || $aColumns[$i] == 'BPrice' || $aColumns[$i] == 'SPrice'	){
							$row[] = number_format($aRow[ $aColumns[$i] ], 3, '.', '');
						}else{
							$row[] = number_format($aRow[ $aColumns[$i] ], 2, '.', '');
						}
					}else{
						/* General output */
						$row[] = $aRow[ $aColumns[$i] ];
					}
				}
			}
					
			$output['aaData'][] = $row;
		}
		 
		
		return json_encode( $output );
	}
	
			
	public function get_ReceivingSummaryReport($post){
	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;

		$sIndexColumn = "POReceivingDetailID";
		$sTable = "v_ol_rpt_POOpeningSummaryReport";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();
		array_push($aColumns, "POReceivingDetailID");
		
		$CustomerID = isset($post['CustomerID'])?$post['CustomerID']:'';
		$VendorID = isset($post['VendorID'])?$post['VendorID']:'';
		$from = isset($post['from'])?$post['from']:'';
		$to = isset($post['to'])?$post['to']:'';
		
		$ReceivingConfirmType = isset($post['ReceivingConfirmType'])?$post['ReceivingConfirmType']:'';
		
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		
		
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		$sTWhere = " WHERE  DeliveryDate between '$from' and '$to' ";
		$sWhere .= " DeliveryDate between '$from' and '$to' ";
		
		if($ReceivingConfirmType =='Confirm'){
			$sTWhere .= " and QAConfirm =1";
			$sWhere .= " and QAConfirm =1";
		}else if($ReceivingConfirmType =='NotConfirm'){
			$sTWhere .= " and QAConfirm =0";
			$sWhere .= " and QAConfirm =0";
		}
		
		if($VendorID != ''){
			$sTWhere .= " and VendorID ='$VendorID'";
			$sWhere .= " and VendorID ='$VendorID'";
		}
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
		
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
		
		$rResult = $this->db->query($sql);
		 
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
				}else if ( $aColumns[$i] == 'PRNumber' )
				{
					/* General output */
					$PRNumber = $aRow[ $aColumns[$i] ];
					$row[] = '<a href="#" onclick="fn_todoPR(\'PrintPR\',\''.$PRNumber.'\')">'.$PRNumber.'</a>';
				}else if ( $aColumns[$i] == 'PONumber' )
				{
					/* General output */
					$PONumber = $aRow[ $aColumns[$i] ];
					$row[] = '<a href="#" onclick="fn_todoPR(\'PrintPO\',\''.$PONumber.'\')">'.$PONumber.'</a>';
					
				}else if( $aColumns[$i] == 'QAConfirm' ){
					$ID =  $aRow['POReceivingDetailID'];
					if($aRow[ $aColumns[$i] ] == '1'){
						$row[] = '<a  href="#" onclick="fn_todoPR(\'ReceivingSummary\',\''.$ID.'\')">Remove Confirmed</a>';
					}else{
						$row[] = '<input type="checkbox" name="Approved" id="chkID_'.$ID.'" data="'.$ID.'" value="'.$ID.'" OnClick="fn_todoPR(\'Checked\',this);"><br>';
						
					}
				}
				else if ( $aColumns[$i] != ' ' && $aColumns[$i] !=  'POReceivingDetailID' )
				{
					if(is_numeric( $aRow[ $aColumns[$i] ]) && $aColumns[$i] != 'Invoice' && $aColumns[$i] != 'PartDesc1' 
								&& $aColumns[$i] != 'Invoice' && $aColumns[$i] != 'SOQty' && $aColumns[$i] != 'Qty'){
						if ($aColumns[$i] == 'UnitPrice' || $aColumns[$i] == 'BPrice' || $aColumns[$i] == 'SPrice'	){
							$row[] = number_format($aRow[ $aColumns[$i] ], 3, '.', '');
						}else{
							$row[] = number_format($aRow[ $aColumns[$i] ], 2, '.', '');
						}
					}else{
						/* General output */
						$row[] = $aRow[ $aColumns[$i] ];
					}
					/* General output */
						//$row[] = $aRow[ $aColumns[$i] ];
				}
			}
					
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	
	
	public function save_QASaleOrder($post){
		$valid =true;
		$SODetailIDs = $post['SODetailIDs'];
		$SODetailIDs = explode("|", $SODetailIDs);
		$session_data = $this->session->userdata('logged_in');
		$Name = $session_data['name'];
		foreach($SODetailIDs  as $SODetailID){
			if($SODetailID != ''){
				$rResult = $this->db->query ("DECLARE @Flag Int;execute sp_QASaleOrderSave '$SODetailID','$Name',@Flag OUTPUT;Select @Flag as Result;");
				$rResult = $rResult->result_array();
				if($rResult[0]['Result'] <> '0'){
					$valid = false;
				}
			}
		}
		if($valid){
			return 0;
		}else{
			return 1;
		}
	}
	
	public function save_QAReceiving($post){
		$valid =true;
		$POReceivingDetailIDs = $post['POReceivingDetailIDs'];
		$POReceivingDetailIDs = explode("|", $POReceivingDetailIDs);
		$session_data = $this->session->userdata('logged_in');
		$Name = $session_data['name'];
		foreach($POReceivingDetailIDs  as $POReceivingDetailID){
			if($POReceivingDetailID != ''){
				$rResult = $this->db->query ("DECLARE @Flag Int;execute sp_QAReceivingSave '$POReceivingDetailID','$Name',@Flag OUTPUT;Select @Flag as Result;");
				$rResult = $rResult->result_array();
				if($rResult[0]['Result'] <> '0'){
					$valid = false;
				}
			}
		}
		if($valid){
			return 0;
		}else{
			return 1;
		}
	}
	
	public function remove_QASaleOrder($post){
		extract($post);
		$result = $this->db->get_where('QASaleOrder', array('SODetailID'=>$ID ));		
		if($result->num_rows() ==1){
			//$data = array('deleted' => 1);				
			return $this->db->delete('QASaleOrder', array('SODetailID'=>$ID)); 
		}else{
			return 2;
		}
	}
	
	public function remove_QAReceiving($post){
		extract($post);
		$result = $this->db->get_where('QAReceiving', array('POReceivingDetailID'=>$ID ));		
		if($result->num_rows() ==1){
			//$data = array('deleted' => 1);				
			return $this->db->delete('QAReceiving', array('POReceivingDetailID'=>$ID)); 
		}else{
			return 2;
		}
	}
}
?>