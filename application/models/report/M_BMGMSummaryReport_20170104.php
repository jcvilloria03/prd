<?php
class M_BMGMSummaryReport extends CI_Model {

	public function __construct(){
		$this->load->database();
		$this->masterdb = $this->load->database('masterdata',true);
	}
	
		
	public function get_PurchaseFromSupplier($post){
	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;

		$sIndexColumn = "VendorName";
		$sTable = "v_ol_rpt_PurchaseFromSupplierReport";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
	
		$from = (isset($post['from']))?$post['from']:'';
		$to =(isset($post['to']))?$post['to']:'';
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		
		
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		$sTWhere = " WHERE  CompletedPODate between CONVERT(datetime,'$from') and  CONVERT(datetime,'$to')  ";
		$sWhere .= " CompletedPODate between CONVERT(datetime,'$from') and  CONVERT(datetime,'$to')  ";
		
		$sql = "SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere  Group By $sIndexColumn";

		$rResultCnt = $this->db->query($sql);
		$iFilteredTotal = $rResultCnt->num_rows();
		//$iFilteredTotal = $aResultCnt->row;
		
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $iFilteredTotal: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields  FROM ( SELECT VendorName, Sum(SGD) as SGD ,sum(USD) as USD, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere Group By VendorName ) a 
				WHERE row > $sLimitFrom and row <= $sLimitTo";
		
	
		$rResult = $this->db->query($sql);
		 
		/* Total data set length */
		$sql  = "SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere  Group By $sIndexColumn" ;

		$rResultTotal = $this->db->query($sql );
		$iTotal = $rResultTotal->num_rows();
		//$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
				}else if ( $aColumns[$i] != ' ' )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
					
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}

		
		
	public function get_BillingToCustomer($post){
	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;

		$sIndexColumn = "CustomerName";
		$sTable = "v_ol_rpt_BillingToCustomer";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
	
		$from = (isset($post['from']))?$post['from']:'';
		$to =(isset($post['to']))?$post['to']:'';
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		
		
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		$sTWhere = " WHERE  DeliveryDate between CONVERT(datetime,'$from') and  CONVERT(datetime,'$to')  ";
		$sWhere .= " DeliveryDate between CONVERT(datetime,'$from') and  CONVERT(datetime,'$to')  ";
		
		$sql = "SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere  Group By $sIndexColumn";
	
		$rResultCnt = $this->db->query($sql);
		$iFilteredTotal = $rResultCnt->num_rows();
		//$iFilteredTotal = $aResultCnt->row;
		
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $iFilteredTotal: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields  FROM ( SELECT CustomerName, Sum(SGD) as SGD ,sum(USD) as USD, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere Group By CustomerName ) a 
				WHERE row > $sLimitFrom and row <= $sLimitTo";

		$rResult = $this->db->query($sql);
		 
		/* Total data set length */
		$sql  = "SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere  Group By $sIndexColumn" ;

		$rResultTotal = $this->db->query($sql );
		$iTotal = $rResultTotal->num_rows();
		//$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
				}else if ( $aColumns[$i] != ' ' )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
					
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}

	
	public function get_JSICost4Depat($post){
	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;

		$sIndexColumn = "Department";
		$sTable = "v_ol_rpt_JSICost4EachDepartment";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
	
		$from = (isset($post['from']))?$post['from']:'';
		$to =(isset($post['to']))?$post['to']:'';
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		
		
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		$sTWhere = " WHERE  DeliveryDate between CONVERT(datetime,'$from') and  CONVERT(datetime,'$to')  ";
		$sWhere .= " DeliveryDate between CONVERT(datetime,'$from') and  CONVERT(datetime,'$to')  ";
		
		$sql = "SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere  Group By $sIndexColumn";

		$rResultCnt = $this->db->query($sql);
		$iFilteredTotal = $rResultCnt->num_rows();
		//$iFilteredTotal = $aResultCnt->row;
		
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $iFilteredTotal: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields  FROM ( SELECT Department, Sum(SGD) as SGD ,sum(USD) as USD, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere Group By Department ) a 
				WHERE row > $sLimitFrom and row <= $sLimitTo";
		
		$rResult = $this->db->query($sql);
		 
		/* Total data set length */
		$sql  = "SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere  Group By $sIndexColumn" ;

		$rResultTotal = $this->db->query($sql );
		$iTotal = $rResultTotal->num_rows();
		//$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
				}else if ( $aColumns[$i] != ' ' )
				{
					/* General output */
					$row[] = $aRow[ $aColumns[$i] ];
				}
			}
					
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	function get_SummaryData($post){
		$from = $post['from'];
		$to = $post['to'];
		$sql = "Select COUNT(*) as PONORMAL from PurchaseOrder,PurchaseRequisition 
			WHERE PurchaseRequisition.PRNumber = PurchaseOrder.PONumber 
			AND PurchaseRequisition.PRType ='NORMAL' And PurchaseOrder.Status = 1
			AND convert(date,PurchaseOrder.CompletedPODate) between convert(date,'".$from."') and convert(date,'".$to."')";
			
		$rResult=$this->db->query($sql);
		$rResult = $rResult->result_array();
		$data['PO']['PONORMAL'] =$rResult[0]['PONORMAL'];
		
		$sql = "Select COUNT(*) as JSICOST from PurchaseOrder,PurchaseRequisition 
				WHERE PurchaseRequisition.PRNumber = PurchaseOrder.PONumber 
				AND PurchaseRequisition.PRType ='JSICOST' And PurchaseOrder.Status = 1
				AND convert(date,PurchaseOrder.CompletedPODate) between convert(date,'".$from."') and convert(date,'".$to."')";
				
		$rResult=$this->db->query($sql);
		$rResult = $rResult->result_array();
		$data['PO']['JSICOST'] =$rResult[0]['JSICOST'];
		
		$sql = "Select COUNT(*) as POJSIEXPENSE from PurchaseOrder,PurchaseRequisition 
				WHERE PurchaseRequisition.PRNumber = PurchaseOrder.PONumber 
				AND PurchaseRequisition.PRType ='JSIEXPENSE' And PurchaseOrder.Status = 1
				AND convert(date,PurchaseOrder.CompletedPODate) between convert(date,'".$from."') and convert(date,'".$to."')";
				
		$rResult=$this->db->query($sql);
		$rResult = $rResult->result_array();
		$data['PO']['POJSIEXPENSE'] =$rResult[0]['POJSIEXPENSE'];

		$sql = "Select COUNT(*) as SOCount FROM SaleOrder WHERE Status = 1
				and convert(date,SODate) between convert(date,'".$from."') and convert(date,'".$to."')";
				
		$rResult=$this->db->query($sql);
		$rResult = $rResult->result_array();
		$data['PO']['SOCount'] =$rResult[0]['SOCount'];
		
		
		$sql = "Select SUM(SGD) as SGD,SUM(USD) as USD from v_ol_rpt_BillingToCustomer 
				WHERE convert(date,DeliveryDate) between convert(date,'".$from."') and convert(date,'".$to."')";
		$rResult=$this->db->query($sql);
		$rResult = $rResult->result_array();
		$data['PO']['Billing_SGD'] =$rResult[0]['SGD'];
		$data['PO']['Billing_USD'] =$rResult[0]['USD'];
			
		$sql = "Select SUM(SGD) as SGD,SUM(USD) as USD from v_ol_rpt_JSICost4EachDepartment 
				WHERE convert(date,DeliveryDate) between convert(date,'".$from."') and convert(date,'".$to."')";
		$rResult=$this->db->query($sql);
		$rResult = $rResult->result_array();
		$data['PO']['JSI_SGD'] =$rResult[0]['SGD'];
		$data['PO']['JSI_USD'] =$rResult[0]['USD'];
		
		return json_encode($data['PO']);
	}
	

}
