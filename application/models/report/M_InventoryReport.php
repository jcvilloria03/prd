<?php
class M_InventoryReport extends CI_Model {

	public function __construct(){
		$this->load->database();
	}
	
	public function viewInventoryReport($vars){
		$return = $this->getInventoryData($vars);
		if($return != 2){
			return json_encode($return);
		}else{
			return $return;
		}
	}
	
	public function InventoryExportExcel($vars){
			
		$return = $this->getInventoryData($vars);
	
		return $return;
		// // $type = $vars['Type'];
		// // $customer = $vars['Customer'];
		// // $from = $vars['From'];
		// // $to = $vars['To'];
		// //$sql = "execute sp_ol_rpt_Inventory  '$from','$to','$customer';";
		// $sql = "execute sp_ol_rpt_Inventory  '01-01-2013','01-31-2013','26';";
		// $rResult = $this->db->query($sql);
		// $array = $rResult->result_array();
		// //print_r($array);
		// if(count($array)){
			// $split = $array['0']['ColumnCount'];
			// //echo($split);
			// $arrItems = array();
			// foreach($array as $key=>$val){
				// $PartNumber = $val['PartNumber'];
				// $LeadTime = $val['LeadTime'];
				// $PartDescription = $val['PartDescription'];
				// $tempArray = array();
				
				// $ReportDateArr = explode('|', $val['ReportDate']);
				// array_pop($ReportDateArr);
				
				// $CountQtyArr = explode('|', $val['CountQty']);
				// array_pop($CountQtyArr);
				
				// $OrderQtyArr = explode('|', $val['OrderQty']);
				// array_pop($OrderQtyArr);
				
				// for($i=0;$i < $split;$i++){
					// $usage  = 0;
					// if ($i == 0){
						// $usage = ($OrderQtyArr[$i] - $CountQtyArr[$i]);
					// } else {
						// $usage = (($OrderQtyArr[$i] + $CountQtyArr[$i - 1]) - $CountQtyArr[$i]);
					// }
					// array_push($tempArray,array($PartNumber,$PartDescription,
									// $LeadTime,$ReportDateArr[$i],$OrderQtyArr[$i],$usage,$CountQtyArr[$i]));
				// }
				// //print_r($tempArray);
				// if(!isset($arrItems[$key])){
					// $arrItems[$key] = array();
				// }
				// array_push($arrItems[$key],$tempArray);
				// /*
				// $date = explode('|', $val['ReportDate']);
				// array_pop($date);
				// echo "Item: ".$key;
				// */
			// }
			// $return["Header"] =$ReportDateArr;
			// $return["Body"] = $arrItems;
			// return $return;
		// }else{
			// return 2;
		// }
	}
	public function getInventoryData($post){
		$type = $post['Type'];
		$customer = $post['Customer'];
		$from = $post['From'];
		$to = $post['To'];
		$sql = "execute sp_ol_rpt_Inventory  '$from','$to','$customer';";
		//$sql = "execute sp_ol_rpt_Inventory  '01-01-2013','01-31-2013','26';";
		$rResult = $this->db->query($sql);
		$array = $rResult->result_array();
		//print_r($array);
		if(count($array)){
			$split = $array['0']['ColumnCount'];
			//echo($split);
			$arrItems = array();
			foreach($array as $key=>$val){
				$PartNumber = $val['PartNumber'];
				$LeadTime = $val['LeadTime'];
				$PartDescription = $val['PartDescription'];
				$Supplier = $val['VendorName'];
				$Balance = $val['Balance'];
				$tempArray = array();
				
				$ReportDateArr = explode('|', $val['ReportDate']);
				array_pop($ReportDateArr);
				
				$CountQtyArr = explode('|', $val['CountQty']);
				array_pop($CountQtyArr);
				
				$OrderQtyArr = explode('|', $val['OrderQty']);
				array_pop($OrderQtyArr);
				
				for($i=0;$i < $split;$i++){
					$usage  = 0;
					
					if($OrderQtyArr[$i]){
						$OrderQty = $OrderQtyArr[$i];
					}else{
						$OrderQty = '0';
					}
					
					if($CountQtyArr[$i]){
						$CountQty = $CountQtyArr[$i];
					}else{
						$CountQty = '0';
					}
					
					if ($i == 0){
						$usage = (($OrderQty+ $Balance) - $CountQty);
					} else {
						$usage = (($OrderQty + $CountQty) - $CountQty);
					}
					
					
					
					array_push($tempArray,array($PartNumber,$PartDescription,
									$LeadTime,$ReportDateArr[$i],$OrderQty,$usage,$CountQty,$Supplier,$Balance));
					// array_push($tempArray,array($PartNumber,$PartDescription,
									// $LeadTime,$ReportDateArr[$i],$OrderQtyArr[$i],$usage,$CountQtyArr[$i],$Supplier,$Balance));
				}
				//print_r($tempArray);
				if(!isset($arrItems[$key])){
					$arrItems[$key] = array();
				}
				array_push($arrItems[$key],$tempArray);
				/*
				$date = explode('|', $val['ReportDate']);
				array_pop($date);
				echo "Item: ".$key;
				*/
			}
			$return["Header"] =$ReportDateArr;
			$return["Body"] = $arrItems;

			return $return;
		}else{
			return 2;
		}
	}
	
}
