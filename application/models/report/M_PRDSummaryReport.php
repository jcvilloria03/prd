<?php
class M_PRDSummaryReport extends CI_Model {

	public function __construct(){
		$this->load->database();
		$this->masterdb = $this->load->database('masterdata',true);
	}
	
	public function get_OpenOrderReport($post){
	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;

		$sIndexColumn = "ID";
		$sTable = "v_ol_rpt_OpeningClosingPOSummary";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
	
		
		$CustomerID = isset($post['CustomerID'])?$post['CustomerID']:'';
		$VendorID = isset($post['VendorID'])?$post['VendorID']:'';
		$from = isset($post['from'])?$post['from']:'';
		$to = isset($post['to'])?$post['to']:'';
		
		$PRType = isset($post['PRType'])?$post['PRType']:'';
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		
		
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		#if($PRType != 'JSIEXPENSE'){
		#	$sTWhere = " WHERE POBalanceQty = 0 ";
		#} else {
			$sTWhere = " WHERE POBalanceQty <> 0 ";
		#}
		
		#if($PRType != 'JSIEXPENSE'){
		#	$sWhere .= " POBalanceQty = 0 ";
		#} else {
			$sWhere .= " POBalanceQty <> 0 ";
		#}
		// $sTWhere = " WHERE  OpeningDate between '$from' and '$to' ";
		// $sWhere .= " OpeningDate between '$from' and '$to' ";
		
		//$sTWhere = " WHERE  CONVERT(DATETIME,CompletedPODate) between '$from' and '$to' ";
		//$sWhere .= " AND CONVERT(DATETIME,CompletedPODate) between '$from' and '$to' ";
		
		$sTWhere = " WHERE  CompletedPODate between CONVERT(DATETIME,'$from') and CONVERT(DATETIME,'$to') ";
		$sWhere .= " AND  CompletedPODate between CONVERT(DATETIME,'$from') and CONVERT(DATETIME,'$to') ";
		
		if($PRType != ''){
			$sTWhere = " And PRType = '$PRType' ";
			$sWhere .= " And PRType = '$PRType' ";
		}
		if($VendorID != ''){
			$sTWhere .= " and VendorID ='$VendorID'";
			$sWhere .= " and VendorID ='$VendorID'";
		}
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
		
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
		
		$rResult = $this->db->query($sql);
		 
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
				}else if ( $aColumns[$i] == 'PRNumber' )
				{
					/* General output */
					$PRNumber = $aRow[ $aColumns[$i] ];
					$row[] = '<a href="#" onclick="fn_todoPR(\'PrintPR\',\''.$PRNumber.'\')">'.$PRNumber.'</a>';
				}else if ( $aColumns[$i] == 'PONumber' )
				{
					/* General output */
					$PONumber = $aRow[ $aColumns[$i] ];
					$row[] = '<a href="#" onclick="fn_todoPR(\'PrintPO\',\''.$PONumber.'\')">'.$PONumber.'</a>';
					
				}
				else if ( $aColumns[$i] != ' ' )
				{
					if(is_numeric( $aRow[ $aColumns[$i] ]) && $aColumns[$i] != 'Invoice' && $aColumns[$i] != 'PartDesc1'
					&& $aColumns[$i] != 'Invoice' && $aColumns[$i] != 'SOQty' && $aColumns[$i] != 'Qty' 
					&& $aColumns[$i] != 'POBalanceQty'){
						if ($aColumns[$i] == 'BPrice' || $aColumns[$i] == 'SPrice'	){
							$row[] = number_format($aRow[ $aColumns[$i] ], 3, '.', '');
						}else if($aColumns[$i] == 'UnitPrice' ){
							$row[] = number_format($aRow[ $aColumns[$i] ], 4, '.', '');
						}else{
							$row[] = number_format($aRow[ $aColumns[$i] ], 2, '.', '');
						}
					}else{
						/* General output */
						$row[] = $aRow[ $aColumns[$i] ];
					}
					/* General output */
					//$row[] = $aRow[ $aColumns[$i] ];
				}
			}
					
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	public function get_SOSummaryReport($post){
	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;

		$sIndexColumn = "SONumber";
		$sTable = "v_ol_rpt_ACMSummaryReport";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
	
		$CustomerID = isset($post['CustomerID'])?$post['CustomerID']:'';
		$VendorID = isset($post['VendorID'])?$post['VendorID']:'';
		$from = isset($post['from'])?$post['from']:'';
		$to = isset($post['to'])?$post['to']:'';
		
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}

			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		
		
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		$sTWhere = " WHERE  DeliveryDate between CONVERT(datetime,'$from') and  CONVERT(datetime,'$to')  ";
		$sWhere .= " DeliveryDate between CONVERT(datetime,'$from') and CONVERT(datetime,'$to') ";
		if($CustomerID != ''){
			$sTWhere .= " and CustomerID ='$CustomerID'";
			$sWhere .= " and CustomerID ='$CustomerID'";
		}
		if($VendorID != ''){
			$sTWhere .= " and VendorID ='$VendorID'";
			$sWhere .= " and VendorID ='$VendorID'";
		}
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
		
		
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
		
		
		$rResult = $this->db->query($sql);
		 
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = '<a href="#" onclick="fn_todoPR(\'PrintSO\',\''.$custID.'\')">'.$custID.'</a>';
					/* Special output formatting */
				}else if ( $aColumns[$i] == 'PONumber' )
				{
					/* General output */
					$PONumber = $aRow[ $aColumns[$i] ];
					$row[] = '<a href="#" onclick="fn_todoPR(\'PrintPO\',\''.$PONumber.'\')">'.$PONumber.'</a>';
					
				}
				else if ( $aColumns[$i] != ' ' )
				{
					if(is_numeric( $aRow[ $aColumns[$i] ]) && $aColumns[$i] != 'Invoice' && $aColumns[$i] != 'PartDesc1'
							&& $aColumns[$i] != 'Invoice' && $aColumns[$i] != 'SOQty' && $aColumns[$i] != 'Qty'){
						if ($aColumns[$i] == 'BPrice' || $aColumns[$i] == 'SPrice'	){
							$arr = explode(".",$aRow[ $aColumns[$i] ]);
							if (count($arr) > 1){
								if (strlen($arr[1]) > 3){
									$decimalPoint = strlen($arr[1]);
								} else {
									$decimalPoint = 3;
								}
							} else {
								$decimalPoint = 3;
							}
							//$row[] = number_format($aRow[ $aColumns[$i] ], 3, '.', '');
							$row[] = number_format($aRow[ $aColumns[$i] ], $decimalPoint, '.', '');
						}else if($aColumns[$i] == 'UnitPrice' ){
							$row[] = number_format($aRow[ $aColumns[$i] ], 4, '.', '');
						}else{
							$row[] = number_format($aRow[ $aColumns[$i] ], 2, '.', '');
						}
					}else{
						/* General output */
						$row[] = $aRow[ $aColumns[$i] ];
					}
					/* General output */
					//$row[] = $aRow[ $aColumns[$i] ];
				}
			}
					
			$output['aaData'][] = $row;
		}
		 
		
		return json_encode( $output );
	}
	
	public function get_SOSummaryCNReport($post){
	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;

		$sIndexColumn = "SONumber";
		$sTable = "v_ol_rpt_ACMSummaryReport_withCN";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
	
		$CustomerID = isset($post['CustomerID'])?$post['CustomerID']:'';
		$VendorID = isset($post['VendorID'])?$post['VendorID']:'';
		$from = isset($post['from'])?$post['from']:'';
		$to = isset($post['to'])?$post['to']:'';
		
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		
		
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		$sTWhere = " WHERE  DeliveryDate between CONVERT(datetime,'$from') and  CONVERT(datetime,'$to')  ";
		$sWhere .= " DeliveryDate between CONVERT(datetime,'$from') and CONVERT(datetime,'$to') ";
		if($CustomerID != ''){
			$sTWhere .= " and CustomerID ='$CustomerID'";
			$sWhere .= " and CustomerID ='$CustomerID'";
		}
		if($VendorID != ''){
			$sTWhere .= " and VendorID ='$VendorID'";
			$sWhere .= " and VendorID ='$VendorID'";
		}
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
		
		
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
		
		
		$rResult = $this->db->query($sql);
		 
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = '<a href="#" onclick="fn_todoPR(\'PrintSO\',\''.$custID.'\')">'.$custID.'</a>';
					/* Special output formatting */
				}else if ( $aColumns[$i] == 'PONumber' )
				{
					/* General output */
					$PONumber = $aRow[ $aColumns[$i] ];
					$row[] = '<a href="#" onclick="fn_todoPR(\'PrintPO\',\''.$PONumber.'\')">'.$PONumber.'</a>';
					
				}
				else if ( $aColumns[$i] != ' ' )
				{
					if(is_numeric( $aRow[ $aColumns[$i] ]) && $aColumns[$i] != 'Invoice' && $aColumns[$i] != 'PartDesc1'
							&& $aColumns[$i] != 'Invoice' && $aColumns[$i] != 'SOQty' && $aColumns[$i] != 'Qty'){
						if ($aColumns[$i] == 'BPrice' || $aColumns[$i] == 'SPrice'	){
							$arr = explode(".",$aRow[ $aColumns[$i] ]);
							if (count($arr) > 1){
								if (strlen($arr[1]) > 3){
									$decimalPoint = strlen($arr[1]);
								} else {
									$decimalPoint = 3;
								}
							} else {
								$decimalPoint = 3;
							}
							//$row[] = number_format($aRow[ $aColumns[$i] ], 3, '.', '');
							$row[] = number_format($aRow[ $aColumns[$i] ], $decimalPoint, '.', '');
						}else if($aColumns[$i] == 'UnitPrice' ){
							$row[] = number_format($aRow[ $aColumns[$i] ], 4, '.', '');
						}else{
							$row[] = number_format($aRow[ $aColumns[$i] ], 2, '.', '');
						}
					}else{
						/* General output */
						$row[] = $aRow[ $aColumns[$i] ];
					}
					/* General output */
					//$row[] = $aRow[ $aColumns[$i] ];
				}
			}
					
			$output['aaData'][] = $row;
		}
		 
		
		return json_encode( $output );
	}
	
	public function get_ReceivedSummaryReport($post){
	
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0;

		$sIndexColumn = "POReceivingDetailID";
		$sTable = "v_ol_rpt_POOpeningSummaryReport";		
		$aColumns = strlen($post['Fields']) > 0 ? explode(',',$post['Fields']) : array();		
	
		
		$CustomerID = isset($post['CustomerID'])?$post['CustomerID']:'';
		$VendorID = isset($post['VendorID'])?$post['VendorID']:'';
		$from = isset($post['from'])?$post['from']:'';
		$to = isset($post['to'])?$post['to']:'';
		
		$PRType = isset($post['PRType'])?$post['PRType']:'';
		
		/* If column not given */
		if(count($aColumns) < 1){
			$cResult = $this->db->query("SELECT Column_Name as colname FROM Information_schema.Columns WITH (nolock) WHERE Table_Name LIKE '".$sTable."'");
			foreach ($cResult->result_array() as $row)
			{
				array_push( $aColumns, $row['colname'] );
			}			
		}
		
			 
		/* Paging */ 
		$sLimitFrom = isset($post['iDisplayStart']) ? $post['iDisplayStart'] : 0;
		$sLimitTo =(int)$post['iDisplayStart'] + (int)$post['iDisplayLength'];	 
		 
		/* Ordering */ 
		$sOrder = "";
	
		if ( isset( $post['iSortCol_0'] ) )
		{
		
			$sOrder = "ORDER BY  ";
			for ( $i=0 ; $i<intval( $post['iSortingCols'] ) ; $i++ )
			{
				if ( $post[ 'bSortable_'.intval($post['iSortCol_'.$i]) ] == "true" )
				{
					$sOrder .= $aColumns[ intval( $post['iSortCol_'.$i] ) ]."
						".addslashes( $post['sSortDir_'.$i] ) .", ";
				}
			}
			 
			$sOrder = substr_replace( $sOrder, "", -2 );
			if ( $sOrder == "ORDER BY" )
			{
				$sOrder = "";
			}
		}
		
		
		/* Filtering */
		$sWhere = " ";
		if ( isset($post['sSearch']) && $post['sSearch'] != "" )
		{
			$sWhere .= "WHERE  (";
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes( $post['sSearch'] )."%' OR ";
			}
			$sWhere = substr_replace( $sWhere, "", -3 );
			$sWhere .= ')';
		}
		 
		/* Individual column filtering */     
		for ( $i=0 ; $i<count($aColumns) ; $i++ )
		{
			if ( isset($post['bSearchable_'.$i]) && $post['bSearchable_'.$i] == "true" && $post['sSearch_'.$i] != '' )
			{
				if ( $sWhere == "" )
				{
					$sWhere = "WHERE ";
				} 
				else
				{
					$sWhere .= " AND ";
				}
				$sWhere .= $aColumns[$i]." LIKE '%".addslashes($post['sSearch_'.$i])."%' ";
			}
		}
		
		
		$sWhere .= (  $sWhere == "" ||  $sWhere == " " )?" WHERE ": " AND ";	
		$sTWhere = " WHERE  DeliveryDate between CONVERT(datetime,'$from') and  CONVERT(datetime,'$to')  ";
		$sWhere .= " DeliveryDate between CONVERT(datetime,'$from') and CONVERT(datetime,'$to') ";
		
		if($PRType != ''){
			$sTWhere = " And PRType = '$PRType' ";
			$sWhere .= " And PRType = '$PRType' ";
		}
		if($VendorID != ''){
			$sTWhere .= " and VendorID ='$VendorID'";
			$sWhere .= " and VendorID ='$VendorID'";
		}
		$rResultCnt = $this->db->query("SELECT count(".$sIndexColumn.") as counter FROM $sTable $sWhere");
		$aResultCnt = $rResultCnt->row();
		$iFilteredTotal = $aResultCnt->counter;
		
		/* Get data to display */  
		$sLimitTo = $sLimitTo < 0 ? $aResultCnt->counter: $sLimitTo;
		$sFields = implode(',',$aColumns);
		$sql = "SELECT $sFields FROM ( SELECT *, ROW_NUMBER() OVER ($sOrder) as row FROM $sTable $sWhere ) a WHERE row > $sLimitFrom and row <= $sLimitTo";
		
		$rResult = $this->db->query($sql);
		 
		/* Total data set length */
		$rResultTotal = $this->db->query("SELECT COUNT(".$sIndexColumn.") as counter FROM   $sTable $sWhere" );
		$aResultTotal = $rResultTotal->row();
		$iTotal = $aResultTotal->counter;
		 
		/* Output */ 
		$output = array(
			"sEcho" => intval($post['sEcho']),
			"iTotalRecords" => $iTotal,
			"iTotalDisplayRecords" => $iFilteredTotal,
			"aaData" => array()
		);
		
		foreach (  $rResult->result_array() as $aRow )
		{
			
			$row = array();
			for ( $i=0 ; $i<count($aColumns) ; $i++ )
			{
				if ( $aColumns[$i] == $sIndexColumn )
				{
					$custID= $aRow[ trim($aColumns[$i],'[]') ];
					$row[] = $custID;
					/* Special output formatting */
				}else if ( $aColumns[$i] == 'PRNumber' )
				{
					/* General output */
					$PRNumber = $aRow[ $aColumns[$i] ];
					$row[] = '<a href="#" onclick="fn_todoPR(\'PrintPR\',\''.$PRNumber.'\')">'.$PRNumber.'</a>';
				}else if ( $aColumns[$i] == 'PONumber' )
				{
					/* General output */
					$PONumber = $aRow[ $aColumns[$i] ];
					$row[] = '<a href="#" onclick="fn_todoPR(\'PrintPO\',\''.$PONumber.'\')">'.$PONumber.'</a>';
					
				}
				else if ( $aColumns[$i] != ' ' )
				{
					if(is_numeric( $aRow[ $aColumns[$i] ]) && $aColumns[$i] != 'Invoice' && $aColumns[$i] != 'PartDesc1'
						&& $aColumns[$i] != 'Invoice' && $aColumns[$i] != 'SOQty' && $aColumns[$i] != 'Qty' ){
						if ($aColumns[$i] == 'BPrice' || $aColumns[$i] == 'SPrice'	){
							$row[] = number_format($aRow[ $aColumns[$i] ], 3, '.', '');
						}else if($aColumns[$i] == 'UnitPrice' ){
							$row[] = number_format($aRow[ $aColumns[$i] ], 4, '.', '');
						}else{
							$row[] = number_format($aRow[ $aColumns[$i] ], 2, '.', '');
						}
					}else{
						/* General output */
						$row[] = $aRow[ $aColumns[$i] ];
					}
					/* General output */
					//$row[] = $aRow[ $aColumns[$i] ];
				}
			}
					
			$output['aaData'][] = $row;
		}
			 
		return json_encode( $output );
	}
	
	public function save_ReportData($post){
		
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['username'];
		$CustomerID = isset($post['CustomerID'])?$post['CustomerID']:'';
		$VendorID = isset($post['VendorID'])?$post['VendorID']:'';
		
		
		$reporttype = isset($post['reporttype'])?$post['reporttype']:'POSummary';
		$from = isset($post['from'])?$post['from']:'';
		$to = isset($post['to'])?$post['to']:'';
		$PRType = isset($post['PRType'])?$post['PRType']:'';
		$paramPO = "username:=$username|*|DateFrom:=$from|*|DateTo:=$to";
		$paramSO = "username:=$username|*|DateFrom:=$from|*|DateTo:=$to";
		

		$sWhere  = '';
		if($CustomerID != ''){
			$sWhere .= " and CustomerID =''$CustomerID''";
		}
		if($VendorID != ''){
			$sWhere .= " and VendorID =''$VendorID''";
			
			$paramSO .= "|*|SupplierName:=$VendorID";
		}else{
			$paramSO .= "|*|SupplierName:=All";
		}
		
		if($PRType != ''){
			$sWhere .= " And PRType = ''$PRType'' ";
		}
		
		
		if($reporttype == 'POSummary'){
			$sql = "DECLARE @myid uniqueidentifier;
						SET @myid = NEWID();
						insert into ReportHistory
						([PID]
							  ,[systemname]
							  ,[systemtype]
							  ,[reportname]
							  ,[reporttype]
							  ,[params]
							  ,[status]
							  ,[wawiindex]
							  ,[sqlstatement]
							  ,[savetofile]
							  ,[filename]) 
						values
							(@myid,'PRD','Summary','POSummaryReport','XLS','$paramPO',1,
								'PRD','Select * from v_ol_rpt_POSummaryReport WHERE  RequestedDate between ''$from'' and  ''$to'' $sWhere  ',0,'');
						select convert(char(255),@myid) as 'UNIQ';";
		}else if($reporttype == 'SOSummary'){
			$sql = "DECLARE @myid uniqueidentifier;
					SET @myid = NEWID();
					insert into ReportHistory
					([PID]
						  ,[systemname]
						  ,[systemtype]
						  ,[reportname]
						  ,[reporttype]
						  ,[params]
						  ,[status]
						  ,[wawiindex]
						  ,[sqlstatement]
						  ,[savetofile]
						  ,[filename]) 
					values
						(@myid,'PRD','Summary','AMDSummaryReport','XLS','$paramSO',1,
							'PRD','Select * from v_ol_rpt_ACMSummaryReport WHERE  DeliveryDate between CONVERT(datetime,'$from') and CONVERT(datetime,'$to')' $sWhere ',0,'');
					select convert(char(255),@myid) as 'UNIQ';";
		}else if($reporttype == 'POOpeningSummary'){
			$sql = "DECLARE @myid uniqueidentifier;
						SET @myid = NEWID();
						insert into ReportHistory
						([PID]
							  ,[systemname]
							  ,[systemtype]
							  ,[reportname]
							  ,[reporttype]
							  ,[params]
							  ,[status]
							  ,[wawiindex]
							  ,[sqlstatement]
							  ,[savetofile]
							  ,[filename]) 
						values
							(@myid,'PRD','Summary','POOPeningSummaryReport','XLS','$paramPO',1,
								'PRD','Select * from v_ol_rpt_POOpeningSummaryReport WHERE  DeliveryDate between CONVERT(datetime,'$from') and CONVERT(datetime,'$to')' $sWhere ',0,'');
						select convert(char(255),@myid) as 'UNIQ';";
		}
		$rResult =  $this->masterdb->query($sql);
		$rResult = $rResult->result_array();
	
		return $rResult[0]['UNIQ'];
	
	}
	
	public function exportSummaryReport($post){
			
		$session_data = $this->session->userdata('logged_in');
		$username = $session_data['username'];
		$CustomerID = isset($post['CustomerID'])?$post['CustomerID']:'';
		$VendorID = isset($post['VendorID'])?$post['VendorID']:'';
		
		
		$reporttype = isset($post['reporttype'])?$post['reporttype']:'POSummary';
		$from = isset($post['from'])?$post['from']:'';
		$to = isset($post['to'])?$post['to']:'';
		$PRType = isset($post['PRType'])?$post['PRType']:'';
		$paramPO = "username:=$username|*|DateFrom:=$from|*|DateTo:=$to";
		$paramSO = "username:=$username|*|DateFrom:=$from|*|DateTo:=$to";
		

		$sWhere  = '';
		if($CustomerID != ''){
			$sWhere .= " and CustomerID ='$CustomerID'";
		}
		if($VendorID != ''){
			$sWhere .= " and VendorID ='$VendorID'";
			
			$paramSO .= "|*|SupplierName:=$VendorID";
		}else{
			$paramSO .= "|*|SupplierName:=All";
		}
		
		if($PRType != ''){
			$sWhere .= " And PRType = '$PRType' ";
		}
		
		if($reporttype == 'POSummary'){
			$sql = "Select * from v_ol_rpt_OpeningClosingPOSummary WHERE POBalanceQty <> 0 AND CompletedPODate between CONVERT(datetime,'$from') and CONVERT(datetime,'$to') $sWhere ";
		}else if($reporttype == 'SOSummary'){
			$sql = "Select * from v_ol_rpt_ACMSummaryReport WHERE  DeliveryDate between CONVERT(datetime,'$from') and CONVERT(datetime,'$to') $sWhere Order by CustomerName,VendorName ";
		}else if($reporttype == 'SOCNSummary'){
			$sql = "Select * from v_ol_rpt_ACMSummaryReport_withCN WHERE  DeliveryDate between CONVERT(datetime,'$from') and CONVERT(datetime,'$to') $sWhere Order by CustomerName,VendorName ";
		}else if($reporttype == 'POOpeningSummary'){
			$sql = "Select * from v_ol_rpt_POOpeningSummaryReport WHERE  DeliveryDate between CONVERT(datetime,'$from') and CONVERT(datetime,'$to') $sWhere ";
		}

		$query = $this->db->query($sql);
		$return = $query->result_array();
		return $return;
	}
	
	public function getMoldCost($SONumber){
		$query = $this->db->query("select * from saleorder where SONumber = '".$SONumber."' and OneTimeCharge > 0 and SOType = 'Multi' and [Status] = 1 and [Deleted] = 0");
		$return = $query->result_array();
		return $return;
	}
	
}
