<?php
class M_PrintPDF extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function loadPRData($PRNumber)
	{
		$result = $this->db->query(" SELECT * FROM v_pdf_PurchaseRequisition WHERE  PRNumber = '$PRNumber' ");
		
		// return $result->result();
		
		if($result->num_rows() > 0){		
			$data['PRData'] = $result->row(0);
			if($data['PRData']->PRType =='NORMAL'){
				$result = $this->db->query(" SELECT * FROM v_pdf_PRDetail4Normal WHERE PRNumber = '$PRNumber'");
				if($result->num_rows() > 0){	
					$data['PRDetail'] = $result->result_array();
				}
			}ELSE{
				// if($data['PRData']->Department==1){
					// $result = $this->db->query(" SELECT prd.ID,prd.RequestedQty,dep.Department as Department,prd.PartNumber,'' as partname,[PartDesc] as PartName2, prd.VendorID,prd.VendorID as VendorName,prd.UnitPrice,
											// (prd.RequestedQty*prd.UnitPrice) as Amount  ,prd.UOM, 'SGD' as BaseCurrency
											// FROM PRDetail prd,Department dep WHERE prd.Department = dep.ID and prd.PRNumber = '$PRNumber'");
				// }else{
					// $result = $this->db->query("SELECT prd.ID,prd.RequestedQty,Department,prd.PartNumber,'' as partname,[PartDesc] as PartName2	, prd.VendorID,prd.VendorID as VendorName,prd.UnitPrice,
											// (prd.RequestedQty*prd.UnitPrice) as Amount ,prd.UOM, 'SGD' as BaseCurrency
											// FROM PRDetail prd WHERE prd.PRNumber = '$PRNumber' Order by Department,prd.PartNumber");
				// }
				$result = $this->db->query(" SELECT * FROM v_pdf_PRDetail4JSI where	 PRNumber = '$PRNumber' ORDER BY Department");
				if($result->num_rows() > 0){	
					$data['PRDetail'] = $result->result_array();
				}
			}
			
			return $data;
			
		}		
	}
	
	public function GetPatchGST($PRNumber){
		$result = $this->db->query(" SELECT top 1 GST FROM tblPatchPRPODetails where PDDetailID = '$PRNumber'");
		if($result->num_rows() > 0){
			$data = $result->row(0);
			return $data->GST;
		} else {
			return 0;
		}
	}
	
	public function GetPatchGSTAMOUNT($PRNumber){
		$result = $this->db->query(" SELECT top 1 TotalAmount FROM tblPatchPRPODetails where PDDetailID = '$PRNumber'");
		if($result->num_rows() > 0){
			$data = $result->row(0);
			return $data->TotalAmount;
		} else {
			return 0;
		}
	}
	
	public function loadPOData($PONumber)
	{
		
		$session_data = $this->session->userdata('logged_in');
		$name = $session_data['name'];
		
		$result = $this->db->query(" SELECT * FROM v_pdf_PurchaseOrder where  PONumber = '$PONumber'");
		if($result->num_rows() > 0){
			$data['POData'] = $result->row(0);
				$data['POData']->CurrentName = $name;
				
			if($data['POData']->PRType =='NORMAL'){
				$result = $this->db->query(" SELECT * FROM v_pdf_PODetail4Normal Where PONumber= '$PONumber'");
				if($result->num_rows() > 0){	
					$data['PODetail'] = $result->result_array();
				}
			}ELSE{
				$result = $this->db->query("Select * from v_pdf_PODetail4JSI  WHERE  PONumber= '$PONumber' ORDER BY Department");
				if($result->num_rows() > 0){	
					$data['PODetail'] = $result->result_array();
				}
			}
			return $data;
		}
	}
	

	
	public function loadSOData($SONumber)
	{
		$result = $this->db->query(" Select * from v_pdf_SaleOrder WHERE SONumber = '$SONumber'");
		if($result->num_rows() > 0){
			$data['SOData'] = $result->row(0);
				// $result = $this->db->query(" SELECT so.SONumber,so.PartNumber,p.Description as PartName,p.Description2 as PartName2,p.SellingCurrency,p.UOM, so.Quantity,so.UnitPrice,(so.Quantity*so.UnitPrice) as Amount
											// FROM SODetail so,Parts p 
											// where so.PartNumber = p.PartNumber and SONumber ='$SONumber'");
				$sql = "SELECT SONumber,PartNumber,PartName,PartName2,SellingCurrency,UOM,Quantity,UnitPrice,
											Amount,DeliveryDate,Invoice,DONumber
											FROM v_pdf_SODetail
											where SONumber ='$SONumber'";

				$result = $this->db->query($sql);
				if($result->num_rows() > 0){	
					$data['SODetail'] = $result->result_array();
				}
			
		}
			
		return $data;
	}

	public function loadSOCNData($SONumber)
	{
		$result = $this->db->query(" Select * from v_pdf_SaleOrder WHERE SONumber = '$SONumber'");
		if($result->num_rows() > 0){
			$data['SOData'] = $result->row(0);
				// $result = $this->db->query(" SELECT so.SONumber,so.PartNumber,p.Description as PartName,p.Description2 as PartName2,p.SellingCurrency,p.UOM, so.Quantity,so.UnitPrice,(so.Quantity*so.UnitPrice) as Amount
											// FROM SODetail so,Parts p 
											// where so.PartNumber = p.PartNumber and SONumber ='$SONumber'");
				$sql = "SELECT SONumber,PartNumber,PartName,PartName2,SellingCurrency,UOM,Quantity,UnitPrice,
											Amount,DeliveryDate,Invoice,DONumber,UnitPrice AS BasePrice,LoadingPrice,CreditNote
											FROM v_pdf_SODetail
											where SONumber ='$SONumber' and PartNumber LIKE '%Cheng%'";

				$result = $this->db->query($sql);
				if($result->num_rows() > 0){	
					$data['SODetail'] = $result->result_array();
				}
			
		}
			
		return $data;
	}	

	public function loadSOERData($SONumber)
	{
		$result = $this->db->query(" Select * from v_pdf_SaleOrder WHERE SONumber = '$SONumber'");
		if($result->num_rows() > 0){
			$data['SOData'] = $result->row(0);
				$sql = "SELECT SONumber,PartNumber,PartName,PartName2,SellingCurrency,UOM,Quantity,UnitPrice,
											Amount,DeliveryDate,Invoice,DONumber,ToCurrency, ToValue
											FROM v_pdf_SOERDetail
											where SONumber ='$SONumber'";

				$result = $this->db->query($sql);
				if($result->num_rows() > 0){	
					$data['SODetail'] = $result->result_array();
				}
			
		}
			
		return $data;
	}	
	
}
