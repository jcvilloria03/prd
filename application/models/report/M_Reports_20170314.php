<?php
class M_Reports extends CI_Model {

	public function __construct(){
		$this->load->database();
	}

	public function populate($vars){
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0; 
		
		$sql = "select ReportName,ReportColumns from reportslist where [Status] = 1";
		if($is_GM == 1){
			$sql .= " AND GM = 1 " ;
		}else if($is_BM == 1){
			$sql .= " AND BM = 1 " ;
		}else if($is_manager == 1){
			$sql .= " AND Manager = 1 " ;
		}else if($dep_name== 'PRD' || $is_PRDManager == 1){
			$sql .= " AND PRD = 1 " ;
		}else{
			$sql .= " AND UserLevel = 1 " ;
		}
		
		// if($is_PRDManager == 1){
			// $sql .= " OR PRD = 1 " ;
		// }
		$sql .= "  order by orderlist ASC ";
		
		$ReportsListSql = $this->db->query($sql);
		
	
		if ($ReportsListSql->num_rows() > 0){
			$ReportsListArray = array();
			$ReportsFilter		= array();
			foreach($ReportsListSql->result_object() as $ReportsListRow){
				array_push($ReportsListArray,$ReportsListRow);
				if (!isset($ReportsFilter[$ReportsListRow->ReportColumns])){
					$ReportsFilter[$ReportsListRow->ReportColumns] = array();
				}
				
				$ReportsFilterSql = $this->db->query("select viewname,fieldname,fieldalias,fieldtype from reportfilters where viewname = '".$ReportsListRow->ReportColumns."' and [status] = 1 order by orderlist ASC");
				if ($ReportsFilterSql->num_rows() > 0){
					foreach($ReportsFilterSql->result_object() as $ReportsFilterRow){
						array_push($ReportsFilter[$ReportsListRow->ReportColumns],$ReportsFilterRow);
					}
				} else {
					array_push($ReportsFilter[$ReportsListRow->ReportColumns],'no data');
				}
			}
			return array($ReportsListArray,$ReportsFilter);
		} else {
			return "no data";
		}
	}
	
	public function searchData($vars){
		$session_data = $this->session->userdata('logged_in');
		$is_manager = $session_data['is_manager'];
		$dep_name = $session_data['dep_name'];
		$username = $session_data['username'];
		
		$is_manager = ($session_data['is_manager']==1)?1:0;
		$is_GM =  ($session_data['is_GM']==1)?1:0;
		$is_BM = ($session_data['is_BM']==1)?1:0; 
		$is_PRDManager = ($session_data['is_PRDManager']==1)?1:0; 
		
		$chkDept = 0;
		if($is_GM == 1){
		}else if($is_BM == 1){
		}else if($dep_name== 'PRD' || $is_PRDManager == 1){
		}else if($is_manager == 1){
			$chkDept = $this->fnCheckDepartment($vars['report'],'Department');
		}else{
			$chkDept = $this->fnCheckDepartment($vars['report'],'Department');
		}

		$cols = $this->getColumn($vars['report']);
		
		
		if ($cols == 'nodata'){
			return array('nodata','nodata');
		} else {
		
			$sqlString = "Select ".$cols." from ".$vars['report'];
			if (trim($vars['filtertype']) == 'range'){
				if (trim($vars['range_from']) != ''){
					$sqlwhere = " where ".$vars['field']." between '".$vars['range_from']."' and '".$vars['range_to']."'";
				}
			} else {
				if (trim($vars['filter']) != ''){
					switch (trim($vars['filtertype'])){
						case 'is':
							$sqlwhere= " where [".$vars['field']."] = '".trim($vars['filter'])."'";
						break;
						case 'is not':
							$sqlwhere= " where [".$vars['field']."] <> '".trim($vars['filter'])."'";
						break;
						case 'contain':
							$sqlwhere= " where [".$vars['field']."] like '%".trim($vars['filter'])."%'";
						break;
						case 'notcontain':
							$sqlwhere= " where [".$vars['field']."] not like '%".trim($vars['filter'])."%'";
						break;
						case 'begin':
							$sqlwhere= " where [".$vars['field']."] like '".trim($vars['filter'])."%'";
						break;
						case 'end':
							$sqlwhere= " where [".$vars['field']."] like '%".trim($vars['filter'])."'";
						break;
						case 'greater than':
							$sqlwhere= " where [".$vars['field']."] > '".trim($vars['filter'])."'";
						break;
						case 'less than':
							$sqlwhere= " where [".$vars['field']."] < '".trim($vars['filter'])."'";
						break;
						case 'greater than or equal to':
							$sqlwhere= " where [".$vars['field']."] >= '".trim($vars['filter'])."'";
						break;
						case 'less than or equal to':
							$sqlwhere= " where [".$vars['field']."] <= '".trim($vars['filter'])."'";
						break;
					}
				}
				if($chkDept){
					if(trim($vars['filter']) != ''){
						$sqlwhere .= " And Department = '$dep_name'";
					}else{
						$sqlwhere = " WHERE Department = '$dep_name'";
					}	
					$sqlString .= $sqlwhere; 
				}else{
					if(trim($vars['filter']) != ''){
							$sqlString .= $sqlwhere; 
					}	
				}
			}
			
			
			$sqlString.= " order by ".$vars['sort_by']." ".$vars['sort_type'];

			$rs = $this->db->query($sqlString);
			return array($rs->result_array(),$cols);
		}
	}
	
	function getColumn($viewName){
		$Column = "";
		$rs = $this->db->query("select fieldname from reportfilters where viewname = '".$viewName."' and [status] = 1 order by orderlist ASC");
		if ($rs->num_rows() > 0){
			foreach($rs->result_object() as $rsField){
				$Column.= ",[".$rsField->fieldname."]";
			}
			return substr($Column,1);
		} else {
			return 'nodata';
		}
		
	}
	
	function fnCheckDepartment($viewName,$colname){
		$rs = $this->db->query("select fieldname from reportfilters where viewname = '".$viewName."' and  fieldname ='".$colname."' and [status] = 1 ");
		if ($rs->num_rows() > 0){
			return 1;
		}else{
			return 0;
		}
	}
	
}
