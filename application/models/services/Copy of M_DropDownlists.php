<?php
class M_DropDownLists extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}	
	
	public function paymentTerms_data(){
		$this->db->from('PaymentTerms');
		$this->db->select('ID,PaymentTerms');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return json_encode($result->result());	
		}else{
			return 1;
		}
	}
	
	public function deliveryTerms_data(){
		$this->db->from('DeliveryTerms');
		$this->db->select('ID,DeliveryTerms');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return json_encode($result->result());	
		}else{
			return 1;
		}
	}
	public function customer_data(){
		$this->db->from('Customers');
		$this->db->select('CustomerID,CustomerName');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return json_encode($result->result());	
		}else{
			return 1;
		}
	}

}