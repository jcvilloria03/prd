<?php
class M_DataTables extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}	

	public function viewPRData_AllData(){
		$result = $this->db->query("Select pr.PRNumber,pr.PRType,d.Department,pr.DateRequired,
										   pr.RequestedBy,pr.PurchasedBy,pr.ApprovedBy,
										   CASE pr.DELETED WHEN 1 THEN 'Deleted'
															Else 
																CASE pr.[Status] WHEN 1 THEN 'Completed'
																ELSE '' END
															END as Status
									from PurchaseRequisition pr,Department d
									where pr.Department = d.ID");
		if($result->num_rows() > 0){
			return json_encode($result->result_array());	
		}else{
			return 1;
		}
	}
	
	public function viewSOData_AllData($vars){
		$whereClause = "";
		if (trim($vars['SOType']) != 'ALL'){
			$whereClause.= " and so.SOType = '".trim($vars['SOType'])."'";
		} else {
			$whereClause.= " and so.SOType like '%'";
		}
		
		if (trim($vars['PONumber']) != ''){
			$whereClause.= " and so.PONumber = '".trim($vars['PONumber'])."'";
		}
		
		if (trim($vars['SONumber']) != ''){
			$whereClause.= " and so.SONumber = '".trim($vars['SONumber'])."'";
		}
		
		if (isset($vars['CustomerID'])){
			if (trim($vars['CustomerID']) != ''){
				$arr = explode("-",trim($vars['CustomerID']));
				$whereClause.= " and so.CustomerID = '".trim($arr[1])."'";
			}
		}
		$result = $this->db->query("Select so.SONumber,so.PONumber,so.SOType,so.CustomerID,
									cust.CustomerName,so.Name,so.SODate,so.TTCharges,so.FreightCharges,
									so.EntryDate,so.[Status],so.Deleted 
									FROM SaleOrder so,Customers cust
									WHERE so.CustomerID = cust.CustomerID$whereClause");
		if($result->num_rows() > 0){
			return json_encode($result->result_array());	
		}else{
			return 1;
		}
	}
	
	public function viewPendingPurchase_Data(){
		$result = $this->db->query("Select pr.PRNumber,pr.PRType,d.Department,pr.DateRequired,
		pr.RequestedBy,pr.PurchasedBy,pr.ApprovedBy,pr.[Status] as [PRStatus], Case WHEN po.[Status] is NULL THen 0 ELSE po.[Status] END as POStatus
			from PurchaseRequisition pr LEFT JOIN Department d ON pr.Department = d.ID  
				LEFT JOIN PurchaseOrder po on pr.PRNumber = po.PONumber			
			where pr.Deleted =0 and pr.Status=1");
		if($result->num_rows() > 0){
			return json_encode($result->result_array());	
		}else{
			return 1;
		}
	}
	
	public function viewPOData_AllData(){
		$result = $this->db->query('Select PONumber,v.VendorName,pt.PaymentTerms,dt.DeliveryTerms,po.ReqDeliveryDate 
									from PurchaseOrder po,Vendors v, PaymentTerms pt,DeliveryTerms dt,
									WHERE po.VendorID = v.VendorID and 
									po.PaymentTerms = pt.ID and po.DeliveryTerms = dt.ID');
		if($result->num_rows() > 0){
			return json_encode($result->result_array());	
		}else{
			return 1;
		}
	}
	
	
	

}