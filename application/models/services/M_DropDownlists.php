<?php
class M_DropDownLists extends CI_Model {

	public function __construct()
	{
		$this->load->database();
		$this->masterdb = $this->load->database('masterdata',true);
	}	
	
	public function paymentTerms_data(){
		$this->db->from('PaymentTerms');
		$this->db->select('ID,PaymentTerms as Value');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return json_encode($result->result());	
		}else{
			return 1;
		}
	}
	
	public function deliveryTerms_data(){
		$this->db->from('DeliveryTerms');
		$this->db->select('ID,DeliveryTerms as Value');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return json_encode($result->result());	
		}else{
			return 1;
		}
	}
	public function customer_data(){
		$this->db->from('Customers');
		$this->db->where('Status',1);
		$this->db->select('CustomerID as ID,CustomerName as Value');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return json_encode($result->result());	
		}else{
			return 1;
		}
	}

	public function department_data(){
		$this->db->from('Department');
		$this->db->select('ID,Department as Value');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return json_encode($result->result());	
		}else{
			return 1;
		}
	}
	
	public function partnumberByCustomer_data($post){
		$CustomerID = $post['CustomerID'];
		$this->db->from('Parts');
		$this->db->where('Status',1);
		$this->db->where('CustomerID',$CustomerID);
		$this->db->select('PartNumber as ID,Description as Value');
		$result = $this->db->get();
		if($result->num_rows() > 0){
			return json_encode($result->result());	
		}else{
			return 1;
		}
	}
	
	public function partnumberByPONumber_data($post){
		$PONumber = $post['PONumber'];
		
			$result = $this->db->query("SELECT * FROM PurchaseRequisition WHERE PRNumber = '$PONumber' and PRType  = 'NORMAL'");
		if($result->num_rows() > 0){
			$sql = "Select Parts.PartNumber as ID,Parts.Description as Value from Parts,POReceiving
					WHERE Parts.PartNumber = POReceiving.PartNumber and POReceiving.PONumber = '$PONumber'";
		}else{
			$sql = "SELECT PartNumber as ID, PartDesc1 as Value FROM PODetail  WHERE PONumber =  '$PONumber'";
		}
		$result = $this->db->query($sql);
		if($result->num_rows() > 0){
			return json_encode($result->result());	
		}else{
			return 1;
		}

	}
	
	public function PRNumberOnlyApprovalPRDMgr_data($post){
		$PRType = $post['PRType'];
		$result = $this->db->query("SELECT PRNumber as ID,PRNumber as Value 
										FROM PurchaseRequisition
									WHERE PRDSubmitted=1 And PRDApproved = 1
									and ManagerApproved =0
									and MultiManagerApproval = 1
									And PRType ='$PRType'");
		if($result->num_rows() > 0){
			return json_encode($result->result());	
		}else{
			return 1;
		}
	}
	public function getComboBox_data($post){
		foreach ($post as $value)
		{
			if($value=='Customer'){
				$this->db->from('Customers');
				$this->db->where('Status',1);
				$this->db->select('CustomerID as ID,CustomerName as Value');
				$result = $this->db->get();
				if($result->num_rows() > 0){
					$Data['Customer'] = $result->result();
				}else{
					$Data['Customer'] ='';
				}
			}
			
			if($value=='AllCustomer'){
				$this->db->from('Customers');
				$this->db->select('CustomerID as ID,CustomerName as Value');
				$result = $this->db->get();
				if($result->num_rows() > 0){
					$Data['Customer'] = $result->result();
				}else{
					$Data['Customer'] ='';
				}
			}
			
			if($value=='CustomerByCountDay'){
				$sql = "SELECT CustomerID as ID,CustomerName as Value FROM Customers 
						WHERE Status = 1 and DayOfCount = DATEPART(dw,getdate())";
				$result = $this->db->query($sql);
				if($result->num_rows() > 0){
					$Data['Customer'] = $result->result();
				}else{
					$Data['Customer'] ='';
				}
			}
			
			if($value=='Department'){
				$this->db->from('Department');
				$this->db->select('ID,Department as Value');
				$result = $this->db->get();
				if($result->num_rows() > 0){
					$Data['Department'] = $result->result();
				}else{
					$Data['Department'] ='';
				}
			}
			
			if($value=='Part'){
				$this->db->from('Parts');
				$this->db->where('Status',1);
				$this->db->select('PartNumber as ID,Description as Value');
				$result = $this->db->get();
				if($result->num_rows() > 0){
					$Data['Part'] = $result->result();
				}else{
					$Data['Part'] ='';
				}
			}
			
			if($value=='Vendor'){
				$this->db->from('Vendors');
				$this->db->where('Status',1);
				$this->db->select('VendorID as ID,VendorName as Value');
				$result = $this->db->get();
				if($result->num_rows() > 0){
					$Data['Vendor'] = $result->result();
				}else{
					$Data['Vendor'] ='';
				}
			}
			
			if($value=='DeliveryTerm'){
				$this->db->from('DeliveryTerms');
				$this->db->select('ID,DeliveryTerms as Value');
				$result = $this->db->get();
				if($result->num_rows() > 0){
					$Data['DeliveryTerm'] = $result->result();
				}else{
					$Data['DeliveryTerm'] ='';
				}
			}
			
			if($value=='PaymentTerm'){
				$this->db->from('PaymentTerms');
				$this->db->select('ID,PaymentTerms as Value');
				$result = $this->db->get();
				if($result->num_rows() > 0){
					$Data['PaymentTerm'] = $result->result();
				}else{
					$Data['PaymentTerm'] = '';
				}
			}			
			
			if($value=='PONumber4SO'){
				
				$result = $this->db->query("Select po.PONumber as ID,po.PONumber as Value From PurchaseOrder PO,PurchaseRequisition PR WHERE 
								po.PONumber = PR.PRNumber and po.Deleted = 0 And po.[Status] = 1  and PR.PRType = 'NORMAL'
								And po.PONumber Not In( Select PONumber From SaleOrder WHERE SOType ='Single')");
				if($result->num_rows() > 0){
					$Data['PurchaseOrder'] = $result->result();
				}else{
					$Data['PurchaseOrder'] = '';
				}
			}
			if($value=='Addresses'){
				$result = $this->db->query("SELECT [Index] as Value,
												(CASE 
													WHEN [Address4] <> ''  THEN [Address1] + CHAR(13) + [Address2] + CHAR(13) + [Address3] + CHAR(13) + [Address4]
													WHEN [Address3] <> ''  THEN [Address1] + CHAR(13) + [Address2] + CHAR(13) + [Address3]
													WHEN [Address2] <> ''  THEN [Address1] + CHAR(13) + [Address2]
													ELSE [Address1]
												END)  as ID
												FROM [PRD].[dbo].[Addresses]  
											  WHERE [Status] = 1");
				if($result->num_rows() > 0){
					$Data['Addresses'] = $result->result();
				}else{
					$Data['Addresses'] = '';
				}
			}
			
			if($value=='PRType'){
				$result = $this->db->query("SELECT ID,PRType as Value From PRType");
				if($result->num_rows() > 0){
					$Data['PRType'] = $result->result();
				}else{
					$Data['PRType'] = '';
				}
			}
			
			if($value=='UOM'){
				$result = $this->db->query("SELECT Code as ID,[Description] as Value FROM UOM");
				if($result->num_rows() > 0){
					$Data['UOM'] = $result->result();
				}else{
					$Data['UOM'] = '';
				}
			}
			
			if($value=='PRType'){
				$result = $this->db->query("SELECT ID,PRType as Value FROM PRType Order by Orderby ");
				if($result->num_rows() > 0){
					$Data['PRType'] = $result->result();
				}else{
					$Data['PRType'] = '';
				}
			}
			
			if($value=='PRTypeJSI'){
				$result = $this->db->query("SELECT ID,PRType as Value FROM PRType where ID <> 'NORMAL' Order by Orderby ");
				if($result->num_rows() > 0){
					$Data['PRType'] = $result->result();
				}else{
					$Data['PRType'] = '';
				}
			}
			
			if($value=='RequestedBy'){
				$result = $this->masterdb->query("Select Username as ID,Name as Value from [User] WHERE [Disabled] = 0 ");
				if($result->num_rows() > 0){
					$Data['RequestedBy'] = $result->result();
				}else{
					$Data['RequestedBy'] = '';
				}
			}
			
			if($value=='PurchasedBy'){
				$result = $this->masterdb->query("Select  Username as ID,Name as Value from [User] WHERE [Disabled] = 0 and DeptID = 13 ");
				if($result->num_rows() > 0){
					$Data['PurchasedBy'] = $result->result();
				}else{
					$Data['PurchasedBy'] = '';
				}
			}
			
			if($value=='ApprovedBy'){
				$result = $this->masterdb->query("Select  Username as ID,Name as Value  from [User] WHERE [Disabled] = 0 and UserGroupID = 2 ");
				if($result->num_rows() > 0){
					$Data['ApprovedBy'] = $result->result();
				}else{
					$Data['ApprovedBy'] = '';
				}
			}
			
			
		} 
		return json_encode($Data);
	}
	
	
}