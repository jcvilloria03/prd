<?php
class M_Setup extends CI_Model {

	public function __construct()
	{
		$this->load->database();
	}
	
	public function get_SetUpData()
	{
		$result = $this->db->query("Select * from Setup WHERE Entry = 'PRFORM' And 
							Value in ('NORMAL/'+Right(Year(getDate()),2),'JSICOST/'+Right(Year(getDate()),2) ,'JSIEXPENSE/'+Right(Year(getDate()),2) )");
		if($result->num_rows() > 0){
			return json_encode($result->result_array());	
		}else{
			return 1;
		}
	}
	
	public function save_SetupData($post){
		$Normal =$post['Normal'];
		$JSICost =$post['JSICost'];
		$JSIExpense =$post['JSIExpense'];
		
		$sql = "DECLARE @Flag Int; 
				execute sp_SaveSetupData $Normal,$JSICost,$JSIExpense,@Flag OUTPUT;
				 Select @Flag as Result;";
		
		$rResult = $this->db->query($sql);
		$rResult = $rResult->result_array();
		return $rResult[0]['Result'];
	}
}