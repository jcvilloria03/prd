<div class="newentry">
<h3 class="form-title">Change Password</h3>
	<form class="globalform" method="post" action="#"  name="changepassword" >
		<ol>
			<li>
				<label>Current Password*</label> 
				<input type="password" class="text" name="curpassword" id="curpassword" />
				<span class="req">*</span>
			</li>
			<li>
				<label>New Password*</label> 
				<input type="password" class="text" name="newpassword" id="newpassword" />
				<span class="req">*</span>
			</li>
			<li>
				<label>Confirm New Password*</label> 
				<input type="password" class="text" name="conpassword" id="conpassword" />
				<span class="req">*</span>
			</li>
			<li>
				<label>&nbsp;</label> 
				<input type="reset" class="button" value="Clear" id="clear"  />&nbsp; 
				<input type="button" value="Change" id="changepassword-btn"  />
			</li>
		</ol>
		<br class="clear">		
    </form>
</div>


<script type="text/javascript">
$(document).ready(function() {
	$( "input:submit, input:button, button, .button").button();
	
	$('#changepassword-btn').click(function(){
			var frules =	[
					"required,curpassword,Please enter your current password.",
					"required,newpassword,Please enter new password.",
					"required,conpassword,Please confirm new password.",
					"same_as,newpassword,conpassword,Please ensure the new passwords you enter are the same.",
					"length>=6,newpassword,Please enter a value that is at least 6 characters long"
					];
			rsv.customErrorHandler = formError;	
			rsv.onCompleteHandler = function(theForm){
				var formValues = $(theForm).serialize();
				//ProcessRequest('type=UpdateUserFirstLogin'+ '&' + formValues,'ChangePasswordCallback');	
				ProcessRequest('account/ChangePassword/UpdatePassword',formValues,'ChangePasswordCallback');					
			};
			rsv.validate(document.forms['changepassword'],frules);
		
	});
	
	function formError(f, errorInfo){		
		var fieldName;
		if(errorInfo.length > 0){
			if (errorInfo[0][0].type == undefined)
				fieldName = errorInfo[0][0][0].name;
			else
				fieldName = errorInfo[0][0].name;				
			msgbox('Error Message','<p class="warning">'+errorInfo[0][1]+'</p>',fieldName);
		}
	}
	
	ChangePasswordCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		// if(data['0'] == 'success'){		
			// msgbox('Error Message','<p class="warning">Your password has beed updated successfully.</p>','iWMS Confirmation','redirect'); 
		// }else if(data == 'samepassword'){
			// msgbox('Error Message','<p class="warning">New password should not be the same with your current password.</p>','newpassword');
		// }else if(data == 'invalid'){
			// msgbox('Error Message','<p class="warning">Invalid current password.</p>','curpassword');
		// }else {
			// msgbox('Error Message','<p class="warning">Cannot update your password.Please contact Dev Team</p>','curpassword');
		// }
	}
	$('#curpassword').focus();
});

</script>