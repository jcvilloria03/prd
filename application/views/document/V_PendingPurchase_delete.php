<h2 class="title">Purchase &raquo; Order Report</h2>
<div class="newentry">
	<h3 class="form-title">Search Purchase Order</h3>
	<form class="globalform" id="rptPR-frm" name="rptPR">
		<ol>
			<div class="lfpane" >
				<li>
					<label>PO No</label>
					<input type="text" id="PONumber" name="PurchasedBy" value=""  class="readonly"/>
				</li>
				<li>
					<label> Required Delivery Date</label>
					<input type="text" id="ReqDeliveryDate" name="ReqDeliveryDate" value="" class="datetime"  readonly="readonly"/>
				</li>
				<li>
					<label> &nbsp;</label>
				</li>
			</div>
			<div class="rfpane" >
				<li>
					<label> Supplier</label>
					<select id="VendorID" name="VendorID">
					</select>
				</li>
				<li>
					<label>Payment Terms</label>
					<select id="PaymentTerms" name="PaymentTerms">
					</select>
				</li>
				<li>
					<label>Delivery Terms</label>
					<select id="DeliveryTerms" name="DeliveryTerms">
					</select>
				</li>
			</div>
			<div style="width: 100%;">
				<li style="float: right; margin-bottom: 15px; margin-right: 110px;">
					<input type="button" id="Search-btn" value="Search" />
				</li>
			</div>		
		</ol>
		<br class="clear">		
	</form>
</div>
<div id="prdiv" name="prdiv"  title="Purchase Requisition List" > 
	<table class="display" id="tbl_PO" >
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>

<script type="text/javascript">
var tbl_PO;

$(document).ready(function() {
	$("input:submit, input:button, button, .button").button();

	fn_GetComboBoxdata = function(data){
		 var obj = jQuery.parseJSON(data);
		 loadComboBoxData("VendorID",obj.Vendor,true);
		 loadComboBoxData("PaymentTerms",obj.PaymentTerm,true);
		 loadComboBoxData("DeliveryTerms",obj.DeliveryTerm,true);			
	}
	
	ProcessRequest('services/DropDownLists/getComboBoxdata',
					{'Vendor':'Vendor','PaymentTerm':'PaymentTerm','DeliveryTerm':'DeliveryTerm'},'fn_GetComboBoxdata');
					
		
	var PO_fields = {'No':'No',
					  'PRNumber':'PRNumber',
					  'PRType':'PRType',
					  'Department':'Department',
					  'DateRequired':'DateRequired',
					  'RequestedBy':'RequestedBy',
					  'PurchasedBy':'PurchasedBy',
					  'ApprovedBy':'ApprovedBy'};
	
		
	$.each(PO_fields,function(i,e){
		$('#tbl_PO thead tr').append('<th>'+e+'</th>');
		$('#tbl_PO tfoot tr').append('<th>'+e+'</th>');
	});
	
	tbl_PO = $('#tbl_PO').dataTable({
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bProcessing: true
	});
	
	setTableHeader('tbl_PO','Purchase Order List');
	
	//ProcessRequest('services/DataTables/viewPendingPurchaseData',{PRNumber:''},'fn_viewPRData');
	
	// fn_viewPRData  = function(data){
		// tbl_PO.fnClearTable(); 
		// if(data){
			// var obj = jQuery.parseJSON(data);
			// var no = 0;
			// var prnumber;
			// var newArray = [];
			// var action = '';
			// for (var i=0; i<obj.length; i++){
				// prnumber = obj[i]['PRNumber'];
					
				// no = no + 1;
				// newArray.push([no,
					// prnumber,
					// obj[i]['PRType'],
					// obj[i]['Department'],
					// obj[i]['DateRequired'],
					// obj[i]['RequestedBy'],
					// obj[i]['PurchasedBy'],
					// obj[i]['ApprovedBy']
				// ]);
			// }
			// tbl_PO.fnAddData(newArray); 
			// tbl_PO.fnAdjustColumnSizing();
		// }
	// }
	
});

</script>