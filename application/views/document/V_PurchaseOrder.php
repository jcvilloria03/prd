<h2 class="title">Document &raquo;Purchase Order Report</h2>
<div class="newentry">
	<h3 class="form-title">Search Purchase Order</h3>
	<form class="globalform" id="rptPR-frm" name="rptPR">
		<ol>
			<div class="lfpane" >
				<li>
					<label>PO No</label>
					<input type="text" id="PONumber" name="PONumber" value="" />
				</li>
				<li>
					<label> Required Delivery Date</label>
					<input type="text" id="ReqDeliveryDate" name="ReqDeliveryDate" value="" class="datetime" readonly />
				</li>
				<li>
					<label> &nbsp;</label>
				</li>
			</div>
			<div class="rfpane" >
				<li>
					<label> Supplier</label>
					<select id="VendorID" name="VendorID">
					</select>
				</li>
				<li>
					<label>Payment Terms</label>
					<select id="PaymentTerms" name="PaymentTerms">
					</select>
				</li>
				<li>
					<label>Delivery Terms</label>
					<select id="DeliveryTerms" name="DeliveryTerms">
					</select>
				</li>
			</div>
			<div style="width: 100%;">
				<li style="float: right; margin-bottom: 15px; margin-right: 110px;">
					<input type="button" id="Search-btn" value="Search" />
				</li>
			</div>		
		</ol>
		<br class="clear">		
	</form>
</div>
<div id="prdiv" name="prdiv"  title="Purchase Requisition List" > 
	<table class="display" id="tbl_PO" >
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>

<script type="text/javascript">
var tbl_PO;

$(document).ready(function() {
	$("input:submit, input:button, button, .button").button();

	fn_GetComboBoxdata = function(data){
		 var obj = jQuery.parseJSON(data);
		 loadComboBoxData("VendorID",obj.Vendor,true);
		 loadComboBoxData("PaymentTerms",obj.PaymentTerm,true);
		 loadComboBoxData("DeliveryTerms",obj.DeliveryTerm,true);			
	}
	
	ProcessRequest('services/DropDownLists/getComboBoxdata',
					{'Vendor':'Vendor','PaymentTerm':'PaymentTerm','DeliveryTerm':'DeliveryTerm'},'fn_GetComboBoxdata');
					
		
	var PO_fields = {'No':'No',
					  'PONumber':'PONumber',
					  'VendorName':'Vendor Name',
					  'PaymentTerms':'Payment Terms',
					  'DeliveryTerms':'Delivery Terms',
					  'Attention':'Attention',
					  'Ext':'Ext',
					  'ReqDate':'Req Date',
					  'Buyer':'Buyer',
					  'BuyerExt':'Buyer Ext',
					  'Date':'Date',
					  'Status':'Status',
					  'Action':'Action',
					  };
	
		
	$.each(PO_fields,function(i,e){
			$('#tbl_PO thead tr').append('<th>'+e+'</th>');
			$('#tbl_PO tfoot tr').append('<th>'+e+'</th>');
	});
	
	tbl_PO = $('#tbl_PO').dataTable({
		"sDom": '<"H"lTf>r<"F"tip>',
		"oTableTools": {
		                "aButtons": [
		                	 {
		                        "sExtends": "xls",
		                        "sFileName": "PurchaseOrder.xls",
		                        "bFooter": false
		                    }
		                ]
		            },
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bProcessing: true
	});
	$('.DTTT_container').css({'float':'left','margin-left':20});
	$('.DTTT_container a').css({'padding':'0 5px','margin-right':10}).button();
	
	setTableHeader('tbl_PO','Purchase Order List');
	
	$( ".datetime" ).datepicker({
		dateFormat: "yy-mm-dd"
	});
	
	SearchData = function(obj){
		tbl_PO.fnClearTable();
		var no = 0;
		var newArray = [];
		var action = '';
		var status ='';
		for (var i=0; i<obj.length; i++){
			no = no + 1;
			if(obj[i]['POStatus'] =='Completed'){
				action = '<a class="print-btn" onclick="fn_PrintPO(\'' + obj[i]['PONumber'] + '\');" href="#">Print PO</a>' 
				status = '<font style="color:green">Completed</font>';
			}else if(obj[i]['POStatus'] =='Deleted'){
				action = ' - ';
				status = '<font style="color:red">Deleted</font>'
			}else{
				action = ' - ';
				status ='<font style="color:#cecece">Pending</font>';
			}
		
			newArray.push([no,
				obj[i]['PONumber'],
				obj[i]['VendorName'],
				obj[i]['PaymentTerms'],
				obj[i]['DeliveryTerms'],
				obj[i]['Attention'],
				obj[i]['Ext'],
				obj[i]['ReqDeliveryDate'],
				obj[i]['Buyer'],
				obj[i]['BuyerExt'],
				obj[i]['EntryDate'],
				status,
				action
			]);
		}
		//(obj[i]['POStatus'] == 'Completed' ? '<a class="print-btn" onclick="fn_PrintPO(\'' + obj[i]['PONumber'] + '\');" href="#">Print PO</a>' : '-')
		
		
		tbl_PO.fnAddData(newArray); 
		tbl_PO.fnAdjustColumnSizing();
	}
	
	$('#Search-btn').click(function(){
		ProcessRequest('document/PurchaseOrder/SearchData',{'PONumber':$('#PONumber').val(),'ReqDeliveryDate':$('#ReqDeliveryDate').val(),'VendorID':$('#VendorID-combox').val(),'PaymentTerms':$('#PaymentTerms-combox').val(),'DeliveryTerms':$('#DeliveryTerms-combox').val()},'SearchData');
	});
	
	fn_PrintPO = function(ponumber){
		window.open("report/PrintPDF/POREPORT/"+ponumber,'_blank',false);
	}
	
});

</script>