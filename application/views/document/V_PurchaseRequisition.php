<h2 class="title">Document &raquo; Purchase Requisition Report</h2>
<div class="newentry">
	<h3 class="form-title">Search Requisition</h3>
	<form class="globalform" id="rptPR-frm" name="rptPR">
		<ol>
			<div class="lfpane" >
				<li>
				<label>PR Status</label>
				<select name="PRStatus" id="PRStatus">
					<option value="ALL">ALL</option>
					<option value="1">Completed</option>
					<option value="2">Deleted</option>
					<option value="3">Processing</option>
				</select>
				</li>
				<li>
				<label>PR Type</label>
					<select name="PRType" id="PRType">
						<option value="ALL">ALL</option>
						<option value="NORMAL">NORMAL</option>
						<option value="JSIEXPENSE">JSI EXPENSE</option>
						<option value="JSICOST">JSI COST</option>
					</select>
				</li>
			</div>
			<div class="rfpane" >
				<li>
					<label>PRNumber</label>
					<input type="text" id="PRNumber" name="PRNumber" />
				</li>
				<li>
					<label>Department</label>
					<select name="Department" id="Department">
					</select>
				</li>
			</div>
			<div style="width: 100%;">
				<li style="float: right; margin-bottom: 15px; margin-right: 110px;">
					<input type="button" id="Search-btn" value="Search" />
				</li>
			</div>		
		</ol>
		<br class="clear">		
	</form>
</div>
<div id="prdiv" name="prdiv"  title="Purchase Requisition List" > 
	<table class="display" id="tbl_PR" >
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>

<script type="text/javascript">
var tbl_PR;

$(document).ready(function() {
	$("input:submit, input:button, button, .button").button();
	
	/* Create Table For PR */
	var PR_fields = {'No':'No',
					  'PRNumber':'PR No',
					  'PRType':'PRType',
					  'Department':'Department',
					  'DateRequired':'DateRequired',
					  'RequestedBy':'RequestedBy',
					  'PurchasedBy':'PurchasedBy',
					  'ApprovedBy':'ApprovedBy',
					  'Status':'Status',
					  'Action':'Action'};
	
	$.each(PR_fields,function(i,e){
		$('#tbl_PR thead tr').append('<th>'+e+'</th>');
		$('#tbl_PR tfoot tr').append('<th>'+e+'</th>');
	});
	
	tbl_PR = $('#tbl_PR').dataTable({
		"sDom": '<"H"lTf>r<"F"tip>',
		"oTableTools": {
	                  "aButtons": [
	                  	 {
	                          "sExtends": "xls",
	                          "sFileName": "PurchaseRequisition.xls",
	                          "bFooter": false
	                      }
	                  ]
	              },
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bProcessing: true
	});
	$('.DTTT_container').css({'float':'left','margin-left':20});
	$('.DTTT_container a').css({'padding':'0 5px','margin-right':10}).button();
	
	//ProcessRequest('services/DataTables/viewPRDataAllData',{PRNumber:''},'fn_viewPRData');
	
	fn_viewPRData  = function(data){
		tbl_PR.fnClearTable(); 
		PrintMyData(jQuery.parseJSON(data));
	}
	
	fn_todoPR = function(action,prnumber){
		switch(action){
			case 'print' : 
					fn_PrintPR(prnumber);
				break
		}
	}
	
	SearchData = function(data){
		tbl_PR.fnClearTable();
		PrintMyData(data);
	}
	
	function PrintMyData(data){
			var obj = data;
			var no = 0;
			var prnumber;
			var status;
			var action = '';
			var newArray = [];
			for (var i=0; i<obj.length; i++){
				prnumber = obj[i]['PRNumber'];
				no = no + 1;
				status = obj[i]['Status'];
				if(status ==' '){
					status='<font style="color:#cecece">Pending</font>';
					action = '';
				}else if (status =='Completed'){
					action = '<a class="print-btn" href="#" onclick="fn_todoPR(\'print\',\''+prnumber+'\');" > print</a>';
					status = '<font style="color:green">'+status+'</font>'
				}else if (status =='Deleted'){
					status = '<font style="color:red">'+status+'</font>'
					action='';
				}else{
					action='';
				}
				newArray.push([no,
					prnumber,
					obj[i]['PRType'],
					obj[i]['Department'],
					obj[i]['DateRequired'],
					obj[i]['RequestedBy'],
					obj[i]['PurchasedBy'],
					obj[i]['ApprovedBy'],
					status,
					action
				]);
			}
			tbl_PR.fnAddData(newArray); 
			tbl_PR.fnAdjustColumnSizing();
	}

	$('#Search-btn').click(function(){
		ProcessRequest('document/PurchaseRequisition/Search',{'PRType':$('#PRType').val(),'PRStatus':$('#PRStatus').val(),'PRNumber':$('#PRNumber').val(),'Department-combox':$('#Department-combox').val()},'SearchData');
	});
		
	$('#printpr-btn').click(function(){
			fn_PrintPR($('#PRNumber').val());
	});
	 
	 fn_PrintPR = function(PRNumber){
		window.open("report/PrintPDF/PRREPORT/"+PRNumber,'_blank',false);
	 }
	fn_ComboBoxload = function(data){
		 var obj = jQuery.parseJSON(data);

		loadComboBoxData("Department",obj.Department,true);
	}
	
	ProcessRequest('services/DropDownLists/getComboBoxdata',{'Department':'Department'},'fn_ComboBoxload');
});
	
</script>