<h2 class="title">Document &raquo; Sale Order Report</h2>
<div class="newentry">
	<h3 class="form-title">Search Sale Order</h3>
	<form class="globalform" id="rptSO-frm" name="rptPR">
		<ol>
			<div class="lfpane" >
				<li>
					<label>SO Type</label>
					<select name="SOType" id="SOType">
						<option value="ALL">ALL</option>
						<option value="Single">Single</option>
						<option value="Multi">Multi</option>
					</select>
				</li>
				<li>
					<label>PO No</label>
					<input type="text" id="PONumber" style="width:100px;" name="PONumber" value="" />
				</li>
			</div>
			<div class="rfpane" >
				<li>
					<label>SO NO</label>
					<input type="text" id="SONumber" style="width:100px;" name="SONumber" value=""/>
				</li>
				<li>
					<label>CustomerID</label>
					<select id="CustomerID" name="CustomerID">
					</select>
				</li>
			</div>
			<div style="width: 100%;">
				<li style="float: right; margin-bottom: 15px; margin-right: 110px;">
					<input type="button" id="Search-btn" value="Search" />
				</li>
			</div>		
		</ol>
		<br class="clear">
	<div id="sodiv" name="sodiv" title='Sale Order'>
		<table class="display" id="tbl_SO">
			<thead><tr></tr></thead>
			<tbody></tbody>
			<tfoot><tr></tr></tfoot>
		</table>
	</div>
	</form>

</div>

<script type="text/javascript">
var tbl_SO;

$(document).ready(function() {
	$("input:submit, input:button, button, .button").button();
	
	fn_GetComboBoxdata = function(data){
		var obj = jQuery.parseJSON(data);
		loadComboBoxData("CustomerID",obj.Customer,true);
	}

	ProcessRequest('services/DropDownLists/getComboBoxData',{'Customer':'Customer'},'fn_GetComboBoxdata');
		/* Create Table For SO */
	var SO_fields = {'No':'No',
					  'SONumber':'SONumber',
					  'PONumber':'PONumber',
					  'SOType':'SOType',
					  'Customer':'Customer',
					  'SODate':'SODate',
					  'TTCharges':'TTCharges',
					  'FreightCharges':'FreightCharges',
					  'Status':'Status',
					  'Action':'Action'
					  };
	$.each(SO_fields,function(i,e){
		$('#tbl_SO thead tr').append('<th>'+e+'</th>');
		$('#tbl_SO tfoot tr').append('<th>'+e+'</th>');
	});
	
	tbl_SO = $('#tbl_SO').dataTable( {
		"sDom": '<"H"lTf>r<"F"tip>',
		"oTableTools": {
		                "aButtons": [
		                	 {
		                        "sExtends": "xls",
		                        "sFileName": "SalesOrder.xls",
		                        "bFooter": false
		                    }
		                ]
		            },
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bProcessing: true
	});
	$('.DTTT_container').css({'float':'left','margin-left':'-5px','margin-top':'5px'});
	$('.DTTT_container a').css({'padding':'0 5px','margin-right':10}).button();
	tbl_SO.fnAdjustColumnSizing();
			
	fn_viewSOData = function(data){
		tbl_SO.fnClearTable();
		if(data){
			var no = 0;
			var status='',action='',SONumber ='';
			var obj = jQuery.parseJSON(data);
			var newArray = [];			
			for (var i=0; i<obj.length; i++){
				no = no + 1;	
				SONumber=obj[i]['SONumber'];
				if(obj[i]['Status'] ==1 || obj[i]['Deleted'] ==1){
					if(obj[i]['Deleted'] == 1){
						status='<font style="color:red">Deleted</font>'
						action='';
					}else if(obj[i]['Status'] ==1){
						status='<font style="color:green">Completed</font>';
						//action = '<a class="print-btn" href="#" onclick="fn_todoSO(\'print\',\''+SONumber+'\');" > print</a>';
						action ='<a class="print-btn" onclick="fn_PrintSO(\'' + SONumber + '\');" href="#"> print</a>';
					}
				}else{
					 status='<font style="color:#cecece">Pending</font>';
					 //action = '<a class="edit-btn" href="#" onclick="fn_todoSO(\'edit\',\''+SONumber+'\');" > Edit</a>'+ 
					 //'<a class="delete-btn" href="#" onclick="fn_todoSO(\'delete\',\''+SONumber+'\');" >Delete</a>';
				}
				 newArray.push([no,
					   SONumber,
					   '<a href="javascript:void(0);" onclick="ShowPODetails(\''+ obj[i]['PONumber'] +'\'); return false;">' + obj[i]['PONumber'] + '</a>',
					   obj[i]['SOType'],
					   obj[i]['CustomerName'],
					   obj[i]['SODate'],
					   obj[i]['TTCharges'],
					   obj[i]['FreightCharges'],
					   status,action]);
			
			}
			
			tbl_SO.fnAddData(newArray);
			tbl_SO.fnAdjustColumnSizing();
		}
	}

	fn_PrintSO = function(sonumber){	
		window.open("report/PrintPDF/SOREPORT/"+sonumber,'_blank',false);
	}
	
	ShowPODetails = function(PONumber){
		ProcessRequest('document/PurchaseOrder/SearchData',{'PONumber':PONumber,'ReqDeliveryDate':'','VendorID':'','PaymentTerms':'','DeliveryTerms':''},'SearchPOData');
	}
	
	SearchPOData = function(obj){
		if (obj.length > 0){
			var status='';
			if(obj[0]['Status'] ==1 || obj[0]['Deleted'] ==1){
				if(obj[0]['Deleted'] == 1){
					status='<font style="color:red">Deleted</font>'
				}else if(obj[0]['Status'] ==1){
					status='<font style="color:green">Completed</font>';
				}
			}else{
				 status='<font style="color:#cecece">Pending</font>';
			}
			msgbox("PO Number Details",'<div style="width:630px;"><table width="620" border="0" cellspacing="0" cellpadding="2" style="border-collapse:collapse"><tr><td width="117"><strong>PO Number:</strong></td><td width="165">' + obj[0]['PONumber'] +'</td><td width="46">&nbsp;</td><td width="119"><strong>Attention:</strong></td><td width="153">' + obj[0]['Attention'] +'</td></tr><tr><td height="54" valign="top"><strong>Vendor Name:</strong></td><td valign="top">' + obj[0]['VendorName'] +'</td><td>&nbsp;</td><td valign="top"><strong>Ext:</strong></td><td valign="top">' + obj[0]['Ext'] +'</td></tr><tr><td><strong>Payment Terms:</strong></td><td>' + obj[0]['PaymentTerms'] +'</td><td>&nbsp;</td><td><strong>Requisition Date:</strong></td><td>' + obj[0]['ReqDeliveryDate'] +'</td></tr><tr><td><strong>Delivery Terms</strong></td><td>' + obj[0]['DeliveryTerms'] +'</td><td>&nbsp;</td><td><strong>Buyer:</strong></td><td>' + obj[0]['Buyer'] +'</td></tr><tr><td><strong>Date:</strong></td><td>' + obj[0]['EntryDate'] +'</td><td>&nbsp;</td><td><strong>Buyer Ext:</strong></td><td>' + obj[0]['BuyerExt'] +'</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td><strong>Status:</strong></td><td>' + status +'</td></tr></table></div>',false,false,false,650,350);
		} else {
			msgbox("PO Number Details","No available PO Details");
		}
	}
	
	$('#Search-btn').click(function(){
		ProcessRequest('document/SaleOrder/viewSOallData',{SOType:$('#SOType').val(),PONumber:$('#PONumber').val(),SONumber:$('#SONumber').val(),CustomerID:$('#CustomerID-combox').val()},'fn_viewSOData');
	});
	
	//ProcessRequest('services/DataTables/viewSOallData',{SOType:$('#SOType').val(),PONumber:$('#PONumber').val(),SONumber:$('#SONumber').val(),CustomerID:$('#CustomerID-combox').val()},'fn_viewSOData');
});
	
</script>