<p>Dear Manager,</p>		
<p style="margin-left: 20px;"><strong>New Internal Request:</strong></p>
<table style="border-collapse: collapse; margin-left: 20px;">
	<tr> 
		<td style="border:1px solid #C4E4F2; background:#E1EEF4; padding: 2px 10px; font-size: 15px;">Date Required</td>
		<td style="border:1px solid #C4E4F2; padding: 2px 10px; font-size: 15px;">{DateRequired}</td>
	</tr>
	<tr> 
		<td style="border:1px solid #C4E4F2; background:#E1EEF4; padding: 2px 10px; font-size: 15px;">Department</td>
		<td style="border:1px solid #C4E4F2; padding: 2px 10px; font-size: 15px;">{Department}</td>
	</tr>
	<tr> 
		<td style="border:1px solid #C4E4F2; background:#E1EEF4; padding: 2px 10px; font-size: 15px;">Requested Date</td>
		<td style="border:1px solid #C4E4F2; padding: 2px 10px; font-size: 15px;">{RequestedDate}</td> 
	</tr>
	<tr> 
		<td style="border:1px solid #C4E4F2; background:#E1EEF4; padding: 2px 10px; font-size: 15px;">Request By</td>
		<td style="border:1px solid #C4E4F2; padding: 2px 10px; font-size: 15px;">{RequestedBy}</td>
	</tr>
</table>		
<p>&nbsp;</p>
<p>***This is an auto generated email. Please do not reply.***</p>