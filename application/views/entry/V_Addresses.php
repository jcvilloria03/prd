<h2 class="title">Entry &raquo; Addresses</h2>
<div class="top-button">
	<button id="add-new"><span class="ui-icon ui-icon-plusthick"></span>Add New Addresses</button>
	<button id="back-to-list" style="display:none"><span class="ui-icon ui-icon-arrowthick-1-w"></span>Back to List</button>
</div>
<div class="clear"></div>
<div class="list">
	<table class="display" id="tbl_Addresses">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>
<div class="newentry" style="display:none">
	<h3 class="form-title">Add Addresses</h3>
	<form class="globalform" id="addresses-frm" name="addresses">
	<em style="font-size: 13px; color:#888"><span class="req">*</span> Required field</em>
			<ol>
				<div class="lfpane" >
					<li>
						<label>Index</label>
						<input type="text" id="Index" name="Index" value="" />
					</li>
					<li>
						<label>Address1</label>
						<input type="text" id="Address1" name="Address1" value="" />
						<span class="req">*</span>
					</li>
					<li>
						<label>Address2</label>
						<input type="text" id="Address2" name="Address2" value="" />
							<span class="req">*</span>
					</li>
				</div>
				<div class="rfpane" >
					<li>
						<label>Address3</label>
						<input type="text" id="Address3" name="Address3" value="" />
							<span class="req">*</span>
					</li>
					<li>
						<label>Address4</label>
						<input type="text" id="Address4" name="Address4" value="" />
							<span class="req">*</span>
					</li>
					<li class="radio">
						<label>Status</label>
						<input type="radio" id="Active" name="Status" value="1" class="retain" checked>Active
						<input type="radio" id="In-Active" name="Status" value="0" class="retain">In-Active
					</li>
				</div>
				<li class="buttons" style="margin-left: 5%;">
					<label>&nbsp;</label>
					<input type="button" id="save-btn" value="Save" data="" />
					<input type="button" id="viewlist-btn" value="View List" />
					<input type="button" id="delete-btn" value="Delete" />
				</li>
			</ol>
	<div class="clear"></div>
	</form>
</div>
<script type="text/javascript">
var tbl_Addresses;
$(document).ready(function() {
	
	$("input:submit, input:button, button, .button").button();
	
// Field to fetch from DB {'DB Column':'Table Header'}
	var Addresses_fields = {'[Index]':'ID',
							  'Address1':'Address1',
							  'Address2':'Address2',
							   'Address3':'Address3',
							    'Address4':'Address4',
							    'Status':'Status'
							};
	$.each(Addresses_fields,function(i,e){
		$('#tbl_Addresses thead tr').append('<th>'+e+'</th>');
		$('#tbl_Addresses tfoot tr').append('<th>'+e+'</th>');
	});
	$('#tbl_Addresses thead tr').append('<th>Action</th>');
	$('#tbl_Addresses tfoot tr').append('<th>Action</th>');	


	// Table Initialization
	tbl_Addresses = $('#tbl_Addresses').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true,
		sAjaxSource: "entry/Addresses/view",
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "WawiID", "value": "" } );
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(Addresses_fields)) } );
			aoData.push( { "name": "Status", "value": $("#radiotype input:radio:checked").val() } ); 
			$.ajax( {
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					//showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	} );
	setTableHeaderHTML('tbl_Addresses',' <div id="radiotype" style="width:300px;float:left;margin-left: 30px;"> Status : '+
					' <input type="radio" id="All" name="radiotype" checked="checked" value=""/><label for="All">All</label> '+
					' <input type="radio" id="1" name="radiotype" value="1" /><label for="1">Active</label> '+
					' <input type="radio" id="0" name="radiotype" value="0"/><label for="0">InActive</label> </div>');
	
	$( "#radiotype" ).buttonset();
		
  $("#radiotype input:radio").change(function(){
		//console.log( $("#radiotype input:radio:checked").val());
		tbl_Addresses.fnDraw();
		
		// Do something interesting here
	});
	
 // Buttons
	$('#add-new').click(function(){
		$(this).fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#Index').focus().select();});});
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Add New Payment Terms');});
		$('#save-btn').val('Save').attr('data','');
		$('#addresses-frm').clearForm();
		$('#delete-btn').hide();
		$('#viewlist-btn').show();
	});
		
	$('#viewlist-btn').click(function(){
		$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
	});
	
	$('#back-to-list').click(function(){
		tbl_Addresses.fnDraw();
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#viewlist-btn').click(function(){
		tbl_Addresses.fnDraw();
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#cancel-btn').click(function(){
		tbl_Addresses.fnDraw();
		$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#save-btn').click(function(){
		var rules =	[
				"required,Index,Please enter Index.",
				"required,Address1,Please enter Address.",
				"required,Address2,Please enter Address2.",
				"required,Address3,Please enter Address3.",
				"required,Address4,Please enter Address4."
				];
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = formSubmit;
		rsv.validate(document.forms['addresses'],rules); 
	});
	
	// Form Submission (add/update)
	function formSubmit(theForm){
		var formValues = $(theForm).serializeArray();
			formValues.push({ name: "ID", value: $('#save-btn').attr('data') });
		var action = $('#save-btn').attr('data') == '' ? 'save' : 'update';
		ProcessRequest('entry/Addresses/'+action,formValues,'submitCallback');
	}
	
	// Form Submission Callback
	submitCallback = function(data){
		var data = explode("|",data);
		if(data[0] == 'success'){
			tbl_Addresses.fnDraw();
			$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
			$('.newentry').slideToggle(function(){$('.list').slideToggle();});
		}
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','Index');	
	}
	
	// Todo Function
	todo = function(action,id){
		switch(action){
			case 'edit' : ProcessRequest('entry/Addresses/search',{ID:id},'searchCountryCallback'); break;	
			case 'delete' : 
				  var confirm  = ["deleteItem",id];
				  msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirm);				
				break
		}
			
	}
	
	deleteItem = function(id){
		ProcessRequest('entry/Addresses/delete',{ID:id},'deleteCountryCallback');
	}
	
	
	$('#delete-btn').click(function(){
		var confirm  = ["deleteItem",$('#save-btn').attr('data')];
		msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirm);				
	});
// Search Record Callback	
	searchCountryCallback = function(data){
		$('#add-new').fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#Addresses').focus().select();});});
		$('#delete-btn').show();
		$('#viewlist-btn').show();
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Update Payment Terms');});
		if(data.length <= 1){
			var data = explode("|",data);
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');		
		}else{
			
			var data = json_decode(data);
			$.each(data,function(i,e){
				if(i == 'Status'){
					if(e =='1') $('#Active').attr("checked", "checked");
					else $('#In-Active').attr("checked", "checked");
				}else{
					$("#"+i).val(e);
				}
			});
			
			$('#save-btn').val('Save Changes').attr('data',data.Index);				
		}
	}
	
	//Delete Callback
	deleteCountryCallback = function(data){
		var data = explode("|",data);
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
			tbl_Addresses.fnDraw();
	}
	
});
</script>
