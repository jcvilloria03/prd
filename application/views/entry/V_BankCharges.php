<h2 class="title">Entry &raquo;Bank Charges</h2>
<div class="top-button">
	<button id="add-new"><span class="ui-icon ui-icon-plusthick"></span>Add Bank Charges</button>
	<button id="back-to-list" style="display:none"><span class="ui-icon ui-icon-arrowthick-1-w"></span>Back to List</button>
</div>
<div class="clear"></div> 
<div class="list">
	<table class="display" id="tbl_BankCharge">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>
<div class="newentry" style="display:none">
	<h3 class="form-title">Add Bank Charges</h3>
	<form class="globalform" id="newBankCharges-frm" name="newBankCharges">
	<em style="font-size: 13px; color:#888"><span class="req">*</span> Required field</em>
		<ol>
			<div class="lfpane" >	
				<li>
					<label>Vendor</label>
					<select id="VendorID" name="VendorID">				
					</select>					
					<span class="req">*</span>	
				</li>	
				<li>
					<label>Customer</label>
					<select id="CustomerID" name="CustomerID">				
					</select>					
					<span class="req">*</span>	
				</li>					
				<li>
					<label>Month</label>
					<select id="month" name="month">
						<option value="1">January</option>
						<option value="2">February</option>
						<option value="3">March</option>
						<option value="4">April</option>
						<option value="5">May</option>
						<option value="6">June</option>
						<option value="7">July</option>
						<option value="8">August</option>
						<option value="9">September</option>
						<option value="10">October</option>
						<option value="11">November</option>
						<option value="12">December</option>
					</select>					
					<span class="req">*</span>	
				</li>			

				<li>
					<label>Year</label>
					<select id="year" name="year">
					</select>
				</li>
				
				<li>
					<label>Amount</label>
					<input type="text" id="ChargeAmount" name="ChargeAmount" value="" />
				</li>			
				<li>
					<label>Currency</label>
					<select id="currency" name="currency">
						<option value="USD">USD</option>
						<option value="SGD">SGD</option>
					</select>
				</li>				
			</div>
			<li class="buttons" style="margin-left: 5%;">
				<label></label>
				<input type="button" id="save-btn" value="Save" data="" />
				<input type="button" id="viewlist-btn" value="View List" />
				<input type="button" id="delete-btn" value="Delete" />
			</li>
		</ol>
		<div class="clear"></div>
	</form>
</div>
<script type="text/javascript">
var tbl_BankCharge;
$(document).ready(function() {
	
	fn_GetComboBoxdata = function(data){
		 var obj = jQuery.parseJSON(data);
		 loadComboBoxData("VendorID",obj.Vendor,true);	
		loadComboBoxData("CustomerID",obj.Customer,true);		 		 
	}

	ProcessRequest('services/DropDownLists/getComboBoxdata',
					{'Vendor':'Vendor','Customer':'Customer'},'fn_GetComboBoxdata');					
			
	$("input:submit, input:button, button, .button").button();

	for (i = new Date().getFullYear(); i > new Date().getFullYear() - 5; i--)
	{
		$('#year').append($('<option />').val(i).html(i));
	}	
	var myOptions = {
		val1 : 'N/A'
	};
	var mySelect = $('#CustomerID');
	$.each(myOptions, function(val, text) {
		mySelect.append(
			$('<option></option>').val(val).html(text)
		);
	});
// Field to fetch from DB {'DB Column':'Table Header'}
	var BankCharges_fields = {'ID':'ID',
							'VendorName':'VendorName',
							'ChargeMonth':'Month',
							'ChargeYear':'Year',
							'ChargeAmount':'ChargeAmount',
							'Currency':'Currency'};
							
	
	$.each(BankCharges_fields,function(i,e){
		$('#tbl_BankCharge thead tr').append('<th>'+e+'</th>');
		$('#tbl_BankCharge tfoot tr').append('<th>'+e+'</th>');
	});
	$('#tbl_BankCharge thead tr').append('<th>Action</th>');
	$('#tbl_BankCharge tfoot tr').append('<th>Action</th>');	


// Table Initialization
	tbl_BankCharge = $('#tbl_BankCharge').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true,
		sAjaxSource: "entry/BankCharges/view",
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "WawiID", "value": "" } );
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(BankCharges_fields)) } );
			$.ajax( {
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					//showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});			
		}
	} );
	
    // Buttons
	$('#add-new').click(function(){
		$(this).fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#SONumber').focus().select();});});
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Add New Exchange Rate');});
		$('#save-btn').val('Save').attr('data','');
		$('#newBankCharges-frm').clearForm();
		$('#delete-btn').hide();
		$('#viewlist-btn').show();
	});
	
	
	$('#viewlist-btn').click(function(){
		$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
	});
	
	$('#back-to-list').click(function(){
		tbl_BankCharge.fnDraw();
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#viewlist-btn').click(function(){
		tbl_BankCharge.fnDraw();
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#cancel-btn').click(function(){
		tbl_BankCharge.fnDraw();
		$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#save-btn').click(function(){
		var rules =	[
				"required,VendorID,Please enter Vendor",
				"required,month,Please enter Month",
				"required,year,Please enter Year.",
				"required,currency,Please enter Currency."
				];
		rsv.VendorrorHandler = formError;	
		rsv.onCompleteHandler = formSubmit;
		rsv.validate(document.forms['newBankCharges'],rules); 
	});
	
	// Form Submission (add/update)
	function formSubmit(theForm){
		var formValues = $(theForm).serializeArray();
		var action = $('#save-btn').attr('data') == '' ? 'save' : 'update';
		ProcessRequest('entry/BankCharges/'+action,formValues,'submitCallback');
	}
	
	// Form Submission Callback
	submitCallback = function(data){
		var data = explode("|",data);
		if(data[0] == 'success'){
			tbl_BankCharge.fnDraw();
			$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
			$('.newentry').slideToggle(function(){$('.list').slideToggle();});
		}
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','SONumber');	
	}
	
	// Todo Function
	todo = function(action,id){
		switch(action){
			case 'edit' : ProcessRequest('entry/BankCharges/search',{ID:id},'searchSOCallback'); break;	
			case 'delete' : 
				  var confirm  = ["deleteItem",id];
				  msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirm);				
				break
		}
	}
	
	deleteItem = function(id){
		ProcessRequest('entry/BankCharges/delete',{ID:id},'deleteCountryCallback');
	}	
	
	$('#delete-btn').click(function(){
		var confirm  = ["deleteItem",$('#save-btn').attr('data')];
		msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirm);				
	});
// Search Record Callback	
	searchSOCallback = function(data){
		$('#add-new').fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#SONumber').focus().select();});});
		$('#delete-btn').show();
		$('#viewlist-btn').show();
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Update Bank Charge');});
		
		if(data.length <= 1){
			var data = explode("|",data);
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','VendorID');		
		}else{
			var data = json_decode(data);
			$.each(data,function(i,e){
				if(i == 'VendorID'){
					$('#VendorID').combobox('selected',e);
				}else if(i == 'CustomerID'){
					$('#CustomerID').combobox('selected',e);
				}else if (i == 'ChargeMonth'){
					$('#month').val(e);
				}else if (i == 'ChargeYear'){
					$('#year').val(e);
				}else if(i == 'isfixedrate'){
					if(e =='1'){ 
						$('#isfixedrate').val("Yes");
						$('#fixratevalue').attr("readonly", false); 
						$('#costBankCharges').attr("readonly", true); 
						$('#sellBankCharges').attr("readonly", true); 
					}else{
						$('#isfixedrate').val("No");
						$('#fixratevalue').attr("readonly", true); 
						$('#costBankCharges').attr("readonly", false); 
						$('#sellBankCharges').attr("readonly", false); 						
					} 
				}else {
					$("#"+i).val(e);
				}
			});			
			
			$('#save-btn').val('Save Changes').attr('data',data.ID);		
			//$('#SONumber').addClass('readonly').attr('readonly','true');						
		}
	}
	
	fn_GetSODetails = function(){
			ProcessRequest('entry/BankCharges/getSODetail',{'ID':$('#ID').val()},'fn_GetSODetailCallBack');
	}	

	fn_GetSODetailCallBack=function(data){
		if(data){
			var obj = jQuery.parseJSON(data);
			$('#FromCurrency').val(obj[0].Currency);
			$('#FromValue').val(obj[0].UnitPrice);
		}	
	}
	
	//Delete Callback
	deleteCountryCallback = function(data){
		var data = explode("|",data);
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
			tbl_BankCharge.fnDraw();
	}
	

});
</script>
