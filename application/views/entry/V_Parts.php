<h2 class="title">Entry &raquo; Parts</h2>
<div class="top-button">
	<button id="add-new"><span class="ui-icon ui-icon-plusthick"></span>Add New Parts</button>
	<button id="back-to-list" style="display:none"><span class="ui-icon ui-icon-arrowthick-1-w"></span>Back to List</button>
</div>
<div class="clear"></div>
<div class="list">
	<table class="display" id="tbl_Parts">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>
<div class="newentry" style="display:none">
	<h3 class="form-title">Add Parts</h3>
	<form class="globalform" id="newParts-frm" name="newParts">
	<em style="font-size: 13px; color:#888"><span class="req">*</span> Required field</em>
			<ol>
				<div class="lfpane" >
					<li>
						<label>Part Number</label>
						<input type="text" id="PartNumber" name="PartNumber" value="" />
						<span class="req">*</span>
					</li>
					<li>
						<label>Customer</label>
						<select id="CustomerID" name="CustomerID" ></select>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="req">*</span>
					</li>
					<li>
						<label>Description</label>
						<input type="text" id="Description" name="Description" value="" />
						<span class="req">*</span>
					</li>
					<li>
						<label>Description2</label>
						<input type="text" id="Description2" name="Description2" value="" />
					</li>
					<li>
						<label>UOM</label>
						<select id="UOM" name="UOM" ></select>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="req">*</span>
					</li>
					<li>
						<label>Reference Number</label>
						<input type="text" id="ReferenceNumber" name="ReferenceNumber" value="" />
						<span class="req">*</span>
					</li>					
					<li>
						<label>MOQ</label>
						<input type="text" id="MOQ" name="MOQ" value="" class="text"/>
						<span class="req">*</span>
					</li>
					
					<li>
						<label>Status</label>
						<input type="radio" id="Active" name="Status" value="1" class="retain" checked>Active
						<input type="radio" id="In-Active" name="Status" value="0" class="retain">In-Active
					</li>
				</div>
				<div class="rfpane" >	
					
					<li>
						<label>Lead Time</label>
						<input type="text" id="LeadTime" name="LeadTime" value="" />
					</li>
					<li>
						<label>Currency (Selling)</label>
						<input type="text" id="SellingCurrency" name="SellingCurrency" value="" maxlength="3"/>
						<span class="req">*</span>
					</li>
					<li>
						<label>Unit Selling Price</label>
						<input type="text" id="UnitPrice" name="UnitPrice" value="" class="text decimal"/>
						<span class="req">*</span>
					</li>
					<li>
						<label>Currency (Base)</label>
						<input type="text"  id="BaseCurrency" name="BaseCurrency" value="" maxlength="3"/>
						<span class="req">*</span>
					</li>
					<li>
						<label>Base Price</label>
						<input type="text" id="BasePrice" name="BasePrice" value="" class="text decimal"/>
						<span class="req">*</span>
					</li>
					<!--<li>
						<label>Loading Price</label>
						<input type="text" id="LoadingPrice" name="LoadingPrice" value="" class="text decimal"/>
						<span class="req">*</span>
					</li>
					<li>
						<label>Date of Base Price Set</label>
						<input type="text" id="BasePriceDate" name="BasePriceDate" value="" class="datetime" readonly="readonly"/>
						<span class="req">*</span>
					</li>-->
					<li>
						<label>Credit Note</label>
						<input type="text" id="CreditNote" name="CreditNote" value="" class="text decimal"/>
						<span class="req">*</span>
					</li>					
				</div>
				<li class="buttons" style="margin-left: 5%;">
					<label>&nbsp;</label>
					<input type="button" id="save-btn" value="Save" data="" />
					<input type="button" id="viewlist-btn" value="View List" />
					<input type="button" id="delete-btn" value="Delete" />
				</li>
			</ol>
		
		<div class="clear"></div>
	</form>
</div>
<script type="text/javascript">
var tbl_Parts;
$(document).ready(function() {
	
	$("input:submit, input:button, button, .button").button();
	
	
// Field to fetch from DB {'DB Column':'Table Header'}
	var Parts_fields = {'PartNumber':'Part Number',
						  'Description':'Description',
						  'UOM':'UOM',
						  'SellingCurrency':'S. Cur',
						  'UnitPrice':'U-Price',
						  'BaseCurrency':'B. Cur',
						  'BasePrice':'B-Price',
						  'CreditNote':'CN',
						  'LeadTime':'LeadTime',
						  'Status':'Status'};
	
	ProcessRequest('services/DropDownLists/getComboBoxdata',{'AllCustomer':'AllCustomer','UOM':'UOM'},'fn_ComboBoxload');
	fn_ComboBoxload = function(data){
		var obj = jQuery.parseJSON(data);
		loadComboBoxData("CustomerID",obj.Customer,true);
		loadComboBoxData("UOM",obj.UOM,true);
	}
	
	$.each(Parts_fields,function(i,e){
		$('#tbl_Parts thead tr').append('<th>'+e+'</th>');
		$('#tbl_Parts tfoot tr').append('<th>'+e+'</th>');
	});
	$('#tbl_Parts thead tr').append('<th>Action</th>');
	$('#tbl_Parts tfoot tr').append('<th>Action</th>');	


// Table Initialization
	tbl_Parts = $('#tbl_Parts').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true,
		sAjaxSource: "entry/Parts/view",
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "WawiID", "value": "" } );
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(Parts_fields)) } );
			aoData.push( { "name": "Status", "value": $("#radiotype input:radio:checked").val() } ); 
			$.ajax( {
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	} );
	
		
	setTableHeaderHTML('tbl_Parts',' <div id="radiotype" style="width:300px;float:left;margin-left: 30px;"> Status : '+
						' <input type="radio" id="All" name="radiotype" checked="checked" value=""/><label for="All">All</label> '+
						' <input type="radio" id="1" name="radiotype" value="1" /><label for="1">Active</label> '+
						' <input type="radio" id="0" name="radiotype" value="0"/><label for="0">InActive</label> </div>');
	
	$( "#radiotype" ).buttonset();
		
  $("#radiotype input:radio").change(function(){
		//console.log( $("#radiotype input:radio:checked").val());
		tbl_Parts.fnDraw();
		
		// Do something interesting here
	});
	
	
    // Buttons
	$('#add-new').click(function(){
		$(this).fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#PartNumber').focus().select();});});
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Add New Parts');});
		$('#save-btn').val('Save').attr('data','');
		$('#newParts-frm').clearForm();
		// $('#CustomerID').removeAttr('readonly');
		// $('#UOM').removeAttr('readonly');
		$('#delete-btn').hide();
		$('#viewlist-btn').show();
	});
		
	$('#viewlist-btn').click(function(){
		$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
	});
	
	$('#back-to-list').click(function(){
		tbl_Parts.fnDraw();
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#viewlist-btn').click(function(){
		tbl_Parts.fnDraw();
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#cancel-btn').click(function(){
		tbl_Parts.fnDraw();
		$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#save-btn').click(function(){
		var rules =	[
				"required,PartNumber,Please enter Parts ID.",
				"required,CustomerID,Please enter Customer ID.",
				"required,Description,Please enter Parts Description.",
				"required,UOM,Please enter UOM.",
				//"required,ReferenceNumber,Please enter Reference Number.",
				"required,MOQ,Please enter MOQ.",
				"required,SellingCurrency,Please enter Selling Currency.",
				"required,UnitPrice,Please enter Unit Price.",
				"reg_exp,UnitPrice,^\\d*(\\.\\d{1\\,})?$,Please enter valid Unit Price.",
				"required,BaseCurrency,Please enter Parts Base Currency.",
				"required,BasePrice,Please enter Parts Base Price.",
				"reg_exp,BasePrice,^\\d*(\\.\\d{1\\,})?$,Please enter valid Parts Base Price.",
				"required,Status,Please Select Status.",
				"required,CreditNote,Please enter Credit Note"
				];
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = formSubmit;
		rsv.validate(document.forms['newParts'],rules); 
	});
	
	// Form Submission (add/update)
	function formSubmit(theForm){
		var formValues = $(theForm).serializeArray();
			formValues.push({ name: "ID", value: $('#save-btn').attr('data') });
		var action = $('#save-btn').attr('data') == '' ? 'save' : 'update';
		ProcessRequest('entry/Parts/'+action,formValues,'submitCallback');
	}
	
	// Form Submission Callback
	submitCallback = function(data){
		var data = explode("|",data);
		if(data[0] == 'success'){
			tbl_Parts.fnDraw();
			$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
			$('.newentry').slideToggle(function(){$('.list').slideToggle();});
		}
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
	}
	
	// Todo Function
	todo = function(action,id){
		switch(action){
			case 'edit' : ProcessRequest('entry/Parts/search',{ID:id},'searchCountryCallback'); break;	
			case 'delete' : 
				  var confirm  = ["deleteItem",id];
				  msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirm);				
				break
		}
	}
	
	deleteItem = function(id){
		ProcessRequest('entry/Parts/delete',{ID:id},'deleteCountryCallback');
	}  
	
	$('#delete-btn').click(function(){
		var confirm  = ["deleteItem",$('#save-btn').attr('data')];
		msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirm);				
	});
// Search Record Callback	
	searchCountryCallback = function(data){
		$('#add-new').fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#Description').focus().select();});});
		$('#delete-btn').show();
		$('#viewlist-btn').show();
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Update Parts');});
		
		if(data.length <= 1){
			var data = explode("|",data);
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');		
		}else{
			
			var data = json_decode(data);
			$.each(data,function(i,e){
				if(i == 'Status'){
					if(e =='1') $('#Active').attr("checked", "checked");
					else $('#In-Active').attr("checked", "checked");
				}else if(i == 'CustomerID'){
					$('#CustomerID').combobox('selected',e);
				}else if(i == 'UOM'){
					$('#UOM').combobox('selected',e);
				}else{
					$("#"+i).val(e);
				}
			});
			
			$('#save-btn').val('Save Changes').attr('data',data.PartNumber);			
			$('#PartNumber').addClass('readonly').attr('readonly','true');			
		}
	}
	
	//Delete Callback
	deleteCountryCallback = function(data){
		var data = explode("|",data);
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
			tbl_Parts.fnDraw();
	}
	
	$( ".datetime" ).datepicker({ 
		//minDate: -0, maxDate: "+2M",
		dateFormat: "mm-dd-yy"
	});
	
	$(".decimal").keydown(function(e) {
		var isEnable=true;

		// isEnable = (e.which != 110) ? false : true;
        if( ((e.which == 9) || (e.which == 46) || (e.which == 8) || (e.which == 110) || (e.which >= 48 && e.which <= 57) || (e.which >= 96 && e.which <= 105))){
            if(isEnable ==false && e.which ==110){return false;}
        }else{
			return false;
        }  
		if (isEnable == true) {            
			isEnable=(e.which ==110)?false:true;
        }
    });
	
});
</script>
