<h2 class="title">Entry &raquo; Payment Terms</h2>
<div class="top-button">
	<button id="add-new"><span class="ui-icon ui-icon-plusthick"></span>Add New Payment Terms</button>
	<button id="back-to-list" style="display:none"><span class="ui-icon ui-icon-arrowthick-1-w"></span>Back to List</button>
</div>
<div class="clear"></div>
<div class="list">
	<table class="display" id="tbl_paymentTerms">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>
<div class="newentry" style="display:none">
	<h3 class="form-title">Add Payment Terms</h3>
	<form class="globalform" id="newproduct-frm" name="newproduct">
	<em style="font-size: 13px; color:#888"><span class="req">*</span> Required field</em>
		<ol>
			<li>
				<label>Payment Terms</label>
				<input type="text" id="PaymentTerms" name="PaymentTerms" value="" />
					<span class="req">*</span>
			</li>
			<li class="buttons">
				<label></label>
				<input type="button" id="save-btn" value="Save" data="" />
				<input type="button" id="viewlist-btn" value="View List" />
				<input type="button" id="delete-btn" value="Delete" />
			</li>
		</ol>
	</form>
</div>
<script type="text/javascript">
var tbl_paymentTerms;
$(document).ready(function() {
	
	$("input:submit, input:button, button, .button").button();
	
// Field to fetch from DB {'DB Column':'Table Header'}
	var paymentterms_fields = {'PaymentTerms':'Description',
							  'EntryDate':'Entry Date',
							  'ID':'Action'
							};
	$.each(paymentterms_fields,function(i,e){
		$('#tbl_paymentTerms thead tr').append('<th>'+e+'</th>');
		$('#tbl_paymentTerms tfoot tr').append('<th>'+e+'</th>');
	});


// Table Initialization
	tbl_paymentTerms = $('#tbl_paymentTerms').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true,
		sAjaxSource: "entry/paymentterms/view",
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "WawiID", "value": "" } );
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(paymentterms_fields)) } );
			$.ajax( {
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					//showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	} );
	
 // Buttons
	$('#add-new').click(function(){
		$(this).fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#PaymentTerms').focus().select();});});
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Add New Payment Terms');});
		$('#save-btn').val('Save').attr('data','');
		$('#newproduct-frm').clearForm();
		$('#delete-btn').hide();
		$('#viewlist-btn').show();
	});
		
	$('#viewlist-btn').click(function(){
		$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
	});
	
	$('#back-to-list').click(function(){
		tbl_paymentTerms.fnDraw();
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#viewlist-btn').click(function(){
		tbl_paymentTerms.fnDraw();
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#cancel-btn').click(function(){
		tbl_paymentTerms.fnDraw();
		$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#save-btn').click(function(){
		var rules =	[
				"required,PaymentTerms,Please enter Payment Terms."
				];
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = formSubmit;
		rsv.validate(document.forms['newproduct'],rules); 
	});
	
	// Form Submission (add/update)
	function formSubmit(theForm){
		var formValues = $(theForm).serializeArray();
			formValues.push({ name: "ID", value: $('#save-btn').attr('data') });
		var action = $('#save-btn').attr('data') == '' ? 'save' : 'update';
		ProcessRequest('entry/paymentterms/'+action,formValues,'submitCallback');
	}
	
	// Form Submission Callback
	submitCallback = function(data){
		var data = explode("|",data);
		if(data[0] == 'success'){
			tbl_paymentTerms.fnDraw();
			$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
			$('.newentry').slideToggle(function(){$('.list').slideToggle();});
		}
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
	}
	
	// Todo Function
	todo = function(action,id){
		switch(action){
			case 'edit' : ProcessRequest('entry/paymentterms/search',{ID:id},'searchCountryCallback'); break;	
			case 'delete' : 
				  var confirm  = ["deleteItem",id];
				  msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirm);				
				break
		}
			
	}
	
	deleteItem = function(id){
		ProcessRequest('entry/paymentterms/delete',{ID:id},'deleteCountryCallback');
	}
	
	
	$('#delete-btn').click(function(){
		var confirm  = ["deleteItem",$('#save-btn').attr('data')];
		msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirm);				
	});
// Search Record Callback	
	searchCountryCallback = function(data){
		$('#add-new').fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#PaymentTerms').focus().select();});});
		$('#delete-btn').show();
		$('#viewlist-btn').show();
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Update Payment Terms');});
		if(data.length <= 1){
			var data = explode("|",data);
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');		
		}else{
			var data = json_decode(data);
			$('#PaymentTerms').val(data.PaymentTerms);
			$('#save-btn').val('Save Changes').attr('data',data.ID);			
		}
	}
	
	//Delete Callback
	deleteCountryCallback = function(data){
		var data = explode("|",data);
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
			tbl_paymentTerms.fnDraw();
	}
	
	$('#PaymentTerms').keydown(function(event){
		if ( event.which == 13 ) {
			$('#save-btn').click();
		}
	});
	
	// $(window).bind('resize', function () {
		// tbl_paymentTerms.fnAdjustColumnSizing();
	// });
	
});
</script>
