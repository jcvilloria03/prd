<h2 class="title">Purchase &raquo; Requisition</h2>
<div class="newentry">
	<form class="globalform" id="newcustomer-frm" name="newcustomer">
		<ol>
			<div class="lfpane" >
				<li>
					<label>PR NO</label>
					<input type="text" id="PRNumber" name="PRNumber" value="" class="readonly"/>
				</li>
				<li>
					<label>Department</label>
					<select name="Department" id="Department">
					</select>
				</li>
				<li>
					<label>Date Required</label>
					<input type="text" id="DateRequired" name="DateRequired" value="" />
				</li>
				
				<li>
					<label>Purchased By</label>
					<input type="text" id="PurchasedBy" name="PurchasedBy" value="" />
				</li>
			</div>
			<div class="rfpane" >
				<li>
					<label>PR Type</label>
					<select name="PRType" id="PRType">
						<option value="NORMAL">NORMAL</option>
						<option value="JSIEXPENSE">JSI EXPENSE</option>
						<option value="JSICOST">JSI COST</option>
					</select>
				</li>
				<li>
					<label></label>
					&nbsp;
				</li>
				<li>
					<label>Requested By</label>
					<input type="text" id="RequestedBy" name="RequestedBy" value="" />
				</li>
				<li>
					<label>Approved By</label>
					<input type="text" id="ApprovedBy" name="ApprovedBy" value="" />
				</li>
			</div>
			<div style="width: 100%;float:left;">
				<li  style="margin-left: 5%; padding-left: 15px;width:90%">
					<label>Special Instruction</label>
					<textarea name="SpecialInstruction" ID="SpecialInstruction" style="width:90%"> </textarea>
				</li>
				<li  style="margin-left: 5%; padding-left: 15px;width:90%">
					<label>Remarks</label>
					<textarea id="Remarks" name="Remarks" style="width:90%"> </textarea>
				</li>
			</div>
			<div style="width: 100%;">
				<li style="width:100%;" style="margin-left: 5%;">
					<label> </label>
					<input type="button" id="save-btn" name="save-btn" value="Save" />
					<input type="button" id="addpritems-btn" name="addpritems-btn" value="Add PR Items" />
				</li>
			</div>
			<div style="width:100%">
				<li style="width:100%;">
					<div class="list">
					<table class="display" id="tbl_PRDetail">
						<thead><tr></tr></thead>
						<tbody></tbody>
						<tfoot><tr></tr></tfoot>
					</table> 
					</div>
				</li>
			</div>
			<li class="buttons" style="margin-left: 5%;">
				<label></label>
				<input type="button" id="new-btn" value="New" data="" />
				<input type="button" id="seek-btn" value="Seek" />
				<input type="button" id="viewlist-btn" value="View List" />
				<input type="button" id="delete-btn" value="Delete" />
			</li>
		</ol>
		<div class="clear"></div>		
	</form>
</div>

<script type="text/javascript">
var tbl_PRDetail;

$(document).ready(function() {
	$('#PRNumber').attr('readonly','true');
	
		
	$( "#Department" ).combobox();
	// $( "#RequestedBy" ).combobox();
	// $( "#PurchasedBy" ).combobox();
	// $( "#ApprovedBy" ).combobox();

	
	ProcessRequest('services/DropDownLists/department',{ID:'0'},'Department');
	Department = function(data){
		$('#Department')
					.empty()
					.append('<option selected="selected" value=""></option>');
					
			var obj = jQuery.parseJSON(data);
			if (obj.length){
				$.each(obj, function(index,item) {
					 $('#Department')
						 .append($("<option></option>")
						 .attr("value",item.ID)
						 .text(item.Department + " - " + item.ID )); 
				});
			}		
	}
	
	
	var PRDetail_fields = {'PartNumber':'Part Number',
						  'VendorID':'Vendor ID',
						  'RequestedQty':'Request Qty',
						  'UnitPrice':'Unit Price',
						  'UOM':'UOM'};
	
	$.each(PRDetail_fields,function(i,e){
		$('#tbl_PRDetail thead tr').append('<th>'+e+'</th>');
		$('#tbl_PRDetail tfoot tr').append('<th>'+e+'</th>');
	});
	
	$("input:submit, input:button, button, .button").button();
	
	tbl_PRDetail = $('#tbl_PRDetail').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true,
		sAjaxSource: "entry/PurchaseRequisition/GetPRDetail",
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "WawiID", "value": "" } );
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(PRDetail_fields)) } );
			$.ajax( {
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					//showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	} );
	
	$('#save-btn').click(function(){
		var rules =	[
				"required,PRNumber,Please enter PRNumber.",
				"required,Department,Please enter Department ID.",
				"required,DateRequired,Please enter Date Required.",
				"required,PurchasedBy,Please enter Purchased By.",
				"required,RequestedBy,Please enter Requested By.",
				"required,ApprovedBy,Please enter Approved By."
				];
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = formSubmit;
		rsv.validate(document.forms['newParts'],rules); 
	});
	
	// Form Submission (add/update)
	function formSubmit(theForm){
		var formValues = $(theForm).serializeArray();
			formValues.push({ name: "ID", value: $('#save-btn').attr('data') });
		var action = $('#save-btn').attr('data') == '' ? 'save' : 'update';
		ProcessRequest('entry/PurchaseRequisition/'+action,formValues,'submitCallback');
	}
	
	submitCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
	}
	
	$('#PRType').change(function() {
		var prtype = $('#PRType').val();
		ProcessRequest('entry/PurchaseRequisition/PRNumber',{PRType:prtype},'GetPRNumber');
	});
	
	GetPRNumber = function(data){
		$('#PRNumber').val(data);
	}
	
	$('#PRType').change();
	
});
</script>

