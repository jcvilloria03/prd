<h2 class="title">Entry &raquo; Vendor</h2>
<div class="top-button">
	<button id="add-new"><span class="ui-icon ui-icon-plusthick"></span>Add New Vendor</button>
	<button id="back-to-list" style="display:none"><span class="ui-icon ui-icon-arrowthick-1-w"></span>Back to List</button>
</div>
<div class="clear"></div>
<div class="list">
	<table class="display" id="tbl_Vendor">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>
<div class="newentry" style="display:none">
	<h3 class="form-title">Add Vendor</h3>
	<form class="globalform" id="newVendor-frm" name="newVendor">
	<em style="font-size: 13px; color:#888"><span class="req">*</span> Required field</em>
		<ol>
			<div class="lfpane" >	
				<li>
					<label>ID</label>
					<input type="text" id="VendorID" name="VendorID" value="" />
					<span class="req">*</span>	
				</li>
				<li>
					<label>Vendor Name</label>
					<input type="text" id="VendorName" name="VendorName" value="" />
					<span class="req">*</span>	
				</li>
				<li>
					<label>Address 1</label>
					<input type="text" id="VendorAddress1" name="VendorAddress1" value="" />
					<span class="req">*</span>	
				</li>
				<li>
					<label>Address 2</label>
					<input type="text" id="VendorAddress2" name="VendorAddress2" value="" />
				</li>
				<li>
					<label>Address 3</label>
					<input type="text" id="VendorAddress3" name="VendorAddress3" value="" />
				</li>
				<li>
					<label>Vendor Status</label>
					<input type="radio" id="Active" name="Status" value="1" class="retain" checked>Active
					<input type="radio" id="In-Active" name="Status" value="0">In-Active
				</li>
			</div>
			<div class="rfpane" >
				<li>
					<label>Contact No</label>
					<input type="text" id="TelNo" name="TelNo" value="" />
					<span class="req">*</span>	
				</li>
				<li>
					<label>Fax</label>
					<input type="text" id="FaxNo" name="FaxNo" value="" />
					<span class="req">*</span>	
				</li>
				<li>
					<label>Contact Person</label>
					<input type="text" id="ContactPerson" name="ContactPerson" value="" />
					<span class="req">*</span>	
				</li>
				<li>
					<label>Payment Terms</label>
					<select id="PaymentTerms" name="PaymentTerms"></select>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="req">*</span>	
				</li>
				<li>
					<label>Delivery Terms</label>
					<select id="DeliveryTerms" name="DeliveryTerms"></select>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="req">*</span>	
				</li>
				<li>
					<label>GST Vendor</label>
					<Select id="GST" name="GST">
						<Option Value="1"> Yes </option>
						<Option Value="0"> No </option>
					</Select>
				</li>
			</div>
			<li class="buttons" style="margin-left: 5%;">
				<label></label>
				<input type="button" id="save-btn" value="Save" data="" />
				<input type="button" id="viewlist-btn" value="View List" />
				<input type="button" id="delete-btn" value="Delete" />
			</li>
		</ol>
		<div class="clear"></div>
	</form>
</div>
<script type="text/javascript">
var tbl_Vendor;
$(document).ready(function() {
	
	ProcessRequest('services/DropDownLists/getComboBoxdata',{'DeliveryTerm':'DeliveryTerm',
					'PaymentTerm':'PaymentTerm'},'fn_ComboBoxload');

	fn_ComboBoxload = function(data){
		var obj = jQuery.parseJSON(data);
		loadComboBoxData("DeliveryTerms",obj.DeliveryTerm,true);
		loadComboBoxData("PaymentTerms",obj.PaymentTerm,true);
	}
	
	$("input:submit, input:button, button, .button").button();
	
// Field to fetch from DB {'DB Column':'Table Header'}
	var Vendor_fields = {'VendorID':'ID',
						  'VendorName':'Name',
						  'TelNo':'TelNo',
						  'FaxNo':'FaxNo',
						  'ContactPerson':'ContactPerson',
						  'GST':'GST',
						  'Status':'Status'};
							
	
	$.each(Vendor_fields,function(i,e){
		$('#tbl_Vendor thead tr').append('<th>'+e+'</th>');
		$('#tbl_Vendor tfoot tr').append('<th>'+e+'</th>');
	});
	$('#tbl_Vendor thead tr').append('<th>Action</th>');
	$('#tbl_Vendor tfoot tr').append('<th>Action</th>');	


// Table Initialization
	tbl_Vendor = $('#tbl_Vendor').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true,
		sAjaxSource: "entry/Vendor/view",
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "WawiID", "value": "" } );
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(Vendor_fields)) } );
			aoData.push( { "name": "Status", "value": $("#radiotype input:radio:checked").val() } ); 
			$.ajax( {
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					//showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	} );
	
	setTableHeaderHTML('tbl_Vendor',' <div id="radiotype" style="width:300px;float:left;margin-left: 30px;"> Status : '+
					' <input type="radio" id="All" name="radiotype" checked="checked" value=""/><label for="All">All</label> '+
					' <input type="radio" id="1" name="radiotype" value="1" /><label for="1">Active</label> '+
					' <input type="radio" id="0" name="radiotype" value="0"/><label for="0">InActive</label> </div>');
	
	$( "#radiotype" ).buttonset();
		
  $("#radiotype input:radio").change(function(){
		//console.log( $("#radiotype input:radio:checked").val());
		tbl_Vendor.fnDraw();
		
		// Do something interesting here
	});
	
    // Buttons
	$('#add-new').click(function(){
		$(this).fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#VendorID').focus().select();});});
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Add New Vendor');});
		$('#save-btn').val('Save').attr('data','');
		$('#newVendor-frm').clearForm();
		$('#CustomerID').removeAttr('readonly');
		$('#delete-btn').hide();
		$('#viewlist-btn').show();
	});
		
	$('#viewlist-btn').click(function(){
		$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
	});
	
	$('#back-to-list').click(function(){
		tbl_Vendor.fnDraw();
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#viewlist-btn').click(function(){
		tbl_Vendor.fnDraw();
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#cancel-btn').click(function(){
		tbl_Vendor.fnDraw();
		$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#save-btn').click(function(){
		var rules =	[
				"required,VendorID,Please enter Vendor ID.",
				"required,VendorName,Please enter Vendor Name.",
				"required,VendorAddress1,Please enter Vendor Address 1.",
				"required,TelNo,Please enter Vendor Tel No.",
				"required,FaxNo,Please enter Vendor Fax.",
				"required,ContactPerson,Please enter Vendor Contact Person.",
				"required,PaymentTerms,Please select Payment Terms.",
				"required,DeliveryTerms,Please select Delivery Terms."				
				];
		rsv.VendorrorHandler = formError;	
		rsv.onCompleteHandler = formSubmit;
		rsv.validate(document.forms['newVendor'],rules); 
	});
	
	// Form Submission (add/update)
	function formSubmit(theForm){
		var formValues = $(theForm).serializeArray();
			formValues.push({ name: "ID", value: $('#save-btn').attr('data') });
		var action = $('#save-btn').attr('data') == '' ? 'save' : 'update';
		ProcessRequest('entry/Vendor/'+action,formValues,'submitCallback');
	}
	
	// Form Submission Callback
	submitCallback = function(data){
		var data = explode("|",data);
		if(data[0] == 'success'){
			tbl_Vendor.fnDraw();
			$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
			$('.newentry').slideToggle(function(){$('.list').slideToggle();});
		}
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
	}
	
	// Todo Function
	todo = function(action,id){
		switch(action){
			case 'edit' : ProcessRequest('entry/Vendor/search',{ID:id},'searchCountryCallback'); break;	
			case 'delete' : 
				  var confirm  = ["deleteItem",id];
				  msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirm);				
				break
		}
	}
	
	deleteItem = function(id){
		ProcessRequest('entry/Vendor/delete',{ID:id},'deleteCountryCallback');
	}
	
	
	$('#delete-btn').click(function(){
		var confirm  = ["deleteItem",$('#save-btn').attr('data')];
		msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirm);				
	});
// Search Record Callback	
	searchCountryCallback = function(data){
		$('#add-new').fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#VendorName').focus().select();});});
		$('#delete-btn').show();
		$('#viewlist-btn').show();
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Update Vendor');});
		
		if(data.length <= 1){
			var data = explode("|",data);
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');		
		}else{
			var data = json_decode(data);
			$.each(data,function(i,e){
				if(i == 'Status'){
					if(e =='1') $('#Active').attr("checked", "checked");
					else $('#In-Active').attr("checked", "checked");
				}else if(i == 'PaymentTerms'){
					$('#PaymentTerms').combobox('selected',e);
				}else if(i == 'DeliveryTerms'){
					$('#DeliveryTerms').combobox('selected',e);
				}
				else {
					$("#"+i).val(e);
				}
			});
			
			
			$('#save-btn').val('Save Changes').attr('data',data.VendorID);		
			$('#VendorID').addClass('readonly').attr('readonly','true');						
		}
	}
	
	$('#GST').keydown(function(event){
		if ( event.which == 13 ) {
			$('#save-btn').click();
		}
	});
	
	
	//Delete Callback
	deleteCountryCallback = function(data){
		var data = explode("|",data);
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
			tbl_Vendor.fnDraw();
	}
	
	// $(window).bind('resize', function () {
		// tbl_Vendor.fnAdjustColumnSizing();
	// });

});
</script>
