<h2>
	Purchase &raquo;Inventory Count
</h2>
<div class="clear"></div>

<div class="newentry"  >
	<h3 class="form-title">Inventory Count	</h3>
	<form class="globalform" id="inventoryCount-frm" name="inventoryCount">
		<ol>
			<div class="lfpane" >
			<li>
				<label>
					Customer
				</label>
				<select id="CustomerID" name="CustomerID">
				</select>
			</li>
			</div>
			<div class="rfpane" >	
			<li>	
				<label>
					Inventory Count Date :
				</label>
				<label id="InvCountDate"> 
				</label>
			</li>
			</div>
			<li style="float: right; margin-bottom: 15px; margin-right: 110px;">
				<label>&nbsp;</label>
				<input type="button" id="saveall-btn" name="save-btn" value="Save ALL" data=""/>
				<input type="button" id="Inventory-btn" name="save-btn" value="Go To Count" data=""/>
			</li>
			
		</ol>
		<div class="clear"></div>	
	</form>
	
	
</div>
<div class="list" id="tbl_InventoryCount-div"> 
	<table class="display" id="tbl_InventoryCount" >
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table>
</div>
<script type="text/javascript">
var tbl_InventoryCount;
$(document).ready(function(){
	$('input:submit, input:button, button, .button').button();
	ProcessRequest('services/DropDownLists/getComboBoxData',{'Customer':'Customer'},'fn_GetComboBoxdata');
	fn_GetComboBoxdata = function(data){
		 var obj = jQuery.parseJSON(data);
		 loadComboBoxData("CustomerID",obj.Customer,true);
		 $('#CustomerID').combobox({
			selected: function(event, ui) {
				tbl_InventoryCount.fnClearTable();
				ProcessRequest('inventory/InventoryCount/viewInventoryCountList',{'CustomerID':$('#CustomerID').val()},'fn_InventoryCountDatabind');
			}
		});
	}
	
	var InventoryCount_fields  = { 'No':'No',
						'PartNumber':'PartNumber',
					   'PartDesc':'PartDesc',
					   'Balance':'Balance',
					   'UOM':'UOM'};
					   
	$.each(InventoryCount_fields,function(i,e){
		$('#tbl_InventoryCount thead tr').append('<th>'+e+'</th>');
		$('#tbl_InventoryCount tfoot tr').append('<th>'+e+'</th>');
	});
	
	tbl_InventoryCount = $('#tbl_InventoryCount').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[-1], [ 'All']],
		iDisplayLength: 1000,
		sPaginationType: 'full_numbers',
		bProcessing: true
	});
	
	fn_InventoryCountDatabind = function(data){
		if(data && data != '1'){
			var no = 0;
			var status='',action='',partnumber='',partdesc='',balance='',uom='',inventoryid='',bal='';
			var obj = jQuery.parseJSON(data);
			var newArray = [];
			for (var i=0; i<obj.length; i++){
				no = no + 1;	
				partnumber = obj[i]['PartNumber'];
				partdesc = obj[i]['Description'];
				uom = obj[i]['UOM'];
				inventoryid = obj[i]['InventoryID'];
				bal = obj[i]['Balance'];
				balance ='<input type="text" id="balance_'+i+'" name="balance_'+i+'"  value="'+ bal +'"  class="int" />';
				balance +='<input type="hidden" id="hidpartnumber_'+i+'" name="hidpartnumber_'+i+'"  value="'+partnumber+'"   />';
				balance += '<input type="hidden" id="hidinventoryid_'+i+'" name="hidinventoryid_'+i+'"  value="'+inventoryid+'"   />';
				action='';
				newArray.push([no,
					   partnumber,
					   partdesc,
					   balance,
					   uom]);
			}
			tbl_InventoryCount.fnAddData(newArray);
			tbl_InventoryCount.fnAdjustColumnSizing();
			var currentTime = new Date();
			var month = currentTime.getMonth() +1;
			var day = currentTime.getDate();
			var year = currentTime.getFullYear();
			$('#InvCountDate').text(day +'  '+month+'  '+year);
		}else{
			$('#InvCountDate').text('');
		}
		$(".int").keydown(function(event) {
			// Allow only backspace, delete, enter, tab
			if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 ) {

			}else {
				// Ensure that it is a number and stop the keypress
				if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
					event.preventDefault(); 
				}   
			}
		});
	}
	
	$('#Inventory-btn').click(function(){
		loadContent('inventory/InventoryCountAdd',{'':''});
	});
	
	$('#saveall-btn').click(function(){
		var formValues =[];
		var partnumbers ='';
		var balances ='';
		var inventoryids='';
		var valid = true;
		var data = tbl_InventoryCount.fnGetData();
		if(data.length > 0){
			$.each(data,function(i,e){
				partnumbers  += $('#hidpartnumber_'+i).val() + "|";
				balances   += $('#balance_'+i).val() + "|";
				inventoryids += $('#hidinventoryid_'+i).val() + "|";
				if(balances ==''){
					valid = false;
				}
			});
			if(valid){
				formValues.push({ name: "partnumbers", value: partnumbers });
				formValues.push({ name: "balances", value: balances });
				formValues.push({ name: "inventoryids", value: inventoryids });
				ProcessRequest('inventory/InventoryCount/saveInventoryCountMulti',formValues,'fn_SavePOReceivingMultiData');
			}
		}
		
	});
	
	fn_SavePOReceivingMultiData = function (data){
		var data = explode("|",data);
		if(data[0] == 'success'){
			fn_InventoryCountDatabind();
		}
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','CustomerID-combox');	 
	}
	
	
	
	
});
</script>