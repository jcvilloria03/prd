<h2>
	Purchase &raquo;Inventory Count
</h2>
<div class="clear"></div>

<div class="newentry"  >
	<h3 class="form-title">Inventory Count	</h3>
	<form class="globalform" id="inventoryCount-frm" name="inventoryCount">
		<em style="font-size: 13px; color:#888"><span class="req">*</span> Required field</em>
		<ol>
			<div class="lfpane" >
				<li>
					<label>
						Customer
					</label>
					<select id="CustomerID" name="CustomerID">
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="req">*</span>
				</li>
				<li>
					<label>
						PartNumber
					</label>
					<select id="PartNumber" name="PartNumber">
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="req">*</span>
				</li>
				<li>
					<label>
						Balance
					</label>
					<input type="text" id='Balance' name='Balance' class='int'/>
					<span class="req">*</span>
				</li>
				<li>
					<label>&nbsp;</label>
					<button id="save-btn" name="save-btn" data="" ><span class="ui-icon ui-icon-disk"></span>Save</button>
					<button id="viewcustomerlist-btn" name="save-btn" data="" ><span class="ui-icon ui-icon-extlink"></span>Customer List</button>
				</li>
			</div>
			<div class="rfpane" >	
				
			</div>
			
		</ol>
		<div class="clear"></div>	
	</form>
	
<div class="list" id="tbl_InventoryCount-div"> 
	<table class="display" id="tbl_InventoryCount" >
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table>
</div>
</div>

<div id="tbl_Customerlist-div" style='width:740px;'>
	<table class="display" id="tbl_Customerlist">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>  

<script type="text/javascript">
var tbl_InventoryCount,tbl_Customerlist;
$(document).ready(function(){
	$("input:submit, input:button, button, .button").button().click(function(event){
		event.preventDefault();
	});

	ProcessRequest('services/DropDownLists/getComboBoxData',{'CustomerByCountDay':'CustomerByCountDay'},'fn_GetComboBoxdata');
	$('#PartNumber').combobox();
	
	var InventoryCount_fields  = { 'ID':'No',
									'PartNumber':'Part Number',
									'Description':'Description',
									'Description2':'Description2',
									'Balance':'Balance',
									'UOM':'UOM'};
								   
	$.each(InventoryCount_fields,function(i,e){
		$('#tbl_InventoryCount thead tr').append('<th>'+e+'</th>');
		$('#tbl_InventoryCount tfoot tr').append('<th>'+e+'</th>');
	});
	
	tbl_InventoryCount = $('#tbl_InventoryCount').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,
		bProcessing: true,
		bServerSide: true,
		sAjaxSource: "inventory/InventoryCountAdd/viewInventoryCountList",
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(InventoryCount_fields)) } );
			aoData.push( { "name": "CustomerID", "value": $('#CustomerID').val() } );  
			$.ajax( {
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
		
	var CustomerList_fields  = { 'CustomerID':'CustomerID',
									'CustomerName':'Customer Name',
									'DayOfCount':'Day Of Count'};
								   
	$.each(CustomerList_fields,function(i,e){
		$('#tbl_Customerlist thead tr').append('<th>'+e+'</th>');
		$('#tbl_Customerlist tfoot tr').append('<th>'+e+'</th>');
	});
	
	tbl_Customerlist = $('#tbl_Customerlist').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,
		bProcessing: true,
		bServerSide: true,
		sAjaxSource: "inventory/InventoryCountAdd/viewCustomerList4Inventory",
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(CustomerList_fields)) } );
			$.ajax( {
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	
	$('#viewcustomerlist-btn').click(function(){
		/* Create Dialog for IRDetail */
		$( "#tbl_Customerlist-div" ).dialog({
			autoOpen: true,
			resizable: false,
			height:590,
			width:790,
			modal: true,
			closeOnEscape: true,
			buttons: {
				Close: function() {
					tbl_Customerlist.fnDraw();
					$( this ).dialog( "destroy" );
				}
			},close: function(){
				tbl_Customerlist.fnDraw();
				$(this).dialog("destroy");  
			}
		});
	});
	
	$('#save-btn').click(function(){
		var rules =	[
					"required,CustomerID,Please enter CustomerID.",
					"required,PartNumber,Please enter PartNumber.",
					"required,Balance,Please enter Balance.",
					"digits_only,Balance,Please enter valid Balance."
				];
				rsv.customErrorHandler = formError;	
				rsv.onCompleteHandler = fn_formSubmitInventoryCount;
				rsv.displayType= "display-html";
				rsv.validate(document.forms['inventoryCount'],rules); 
	});
	
	fn_formSubmitInventoryCount = function(theform){
		var formValues = $(theform).serializeArray();
		//console.log(formValues);
		var action = 'saveInventoryCount'; 
		ProcessRequest('inventory/InventoryCountAdd/'+action,formValues,'fn_formSubmitInventoryCountCallback');
	}

	fn_formSubmitInventoryCountCallback = function(data){
		var data = explode("|",data);
		if(data[0]=='success'){
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','PartNumber-combox');	
			tbl_InventoryCount.fnDraw();
			
			$('#PartNumber').combobox('selected','');
			
			$('#Balance').val('');
		}else{
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','PartNumber-combox');	
		}
	}
	fn_GetComboBoxdata = function(data){
		 var obj = jQuery.parseJSON(data);
		 loadComboBoxData("CustomerID",obj.Customer,true);
		 $('#CustomerID').combobox({
			selected: function(event, ui) {
				tbl_InventoryCount.fnDraw();
				fn_PartNumberDataBind();
			}
		});
	}
	
	function fn_PartNumberDataBind(){
		ProcessRequest('services/DropDownLists/partnumberByCustomer',{'CustomerID':$('#CustomerID').val()},'fn_loadPartNumber');
	}
	
	fn_loadPartNumber = function(data){
		if(data != '1'){
			var obj = jQuery.parseJSON(data);
			$('#PartNumber')
					.empty()
					.append('<option selected="selected" value=""></option>');;
			$.each(obj, function(index,item) {
				$('#PartNumber')
				 .append($("<option ></option>")
				 .attr("value",item.ID)
				 .text(item.ID +' - '+ item.Value));	
			});
			$('#PartNumber').combobox();
			
			 $('#PartNumber').combobox({
				selected: function(event, ui) {
					$('#Balance').focus();
				}
			});
			
			$('#PartNumber-combox').focus();
		}
	}
	
	$('#tbl_Customerlist-div').hide();
	$(".int").keydown(function(event) {
		// Allow only backspace, delete, enter, tab
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 ) {

		}else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
	
});
</script>