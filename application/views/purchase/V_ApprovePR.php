<h2 class="title">Purchase &raquo; Approve Purchase Requisition</h2>
<br class="clear" />
<form class="globalform" style="min-height: 40px; border: none;">
	<ol>
		<li>
			<label>Year Requested</label>
			<select name="YearRequested" id="YearRequested" style="width: 100px;">
				<?php
				for($i = date('Y');$i >= 2012;$i--){
					echo '<option value="'.$i.'"';
					echo $i == date('Y') ? 'selected="selected"': '';
					echo '>'.$i.'</option>';
				}
				?>
			</select>
		</li>
	</ol>
</form>
<div class="list">
	<table class="display" id="tbl_PR" >
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table>  
</div>

<div id="PRRejectReason-div" name="PRRejectReason-div" title="Reject Reason" style="display: none;">
	<form class="globalform" id="PRRejectReason-frm" name="PRRejectReason" style="width:400px;">
		Reason
		<textarea id="ApprovedReason" name="ApprovedReason" style="width:370px"></textarea>
	</form>
</div>

<div  style="display: none;">
	<input type="hidden" id="hid-ismanager" name="hid-ismanager" value="<?php echo($is_manager);?>"/>
	<input type="hidden" id="hid-isbm" name="hid-isbm" value="<?php echo($is_BM);?>"/>
	<input type="hidden" id="hid-isgm" name="hid-isgm" value="<?php echo($is_GM);?>"/>
	<input type="hidden" id="hid-isPRDManager" name="hid-isPRDManager" value="<?php echo($is_PRDManager);?>"/>
	<input type="hidden" id="hid-username" name="hid-username" value="<?php echo($username);?>"/>
</div>
<script type="text/javascript">
var tbl_PR,fn_todoPR,fn_ApprovePRData,fn_RejectPRData;  
var isFiltered = false;
$(document).ready(function() {
	//$('#PRNumber').attr('readonly',true);
	$("input:submit, input:button, button, .button").button().click(function(event){
		event.preventDefault();
	});
	
	/* Create Table For PR */
	var PR_fields = { 'PRNumber':'PR Number',
					  'PRType':'PRType',
					  'Department':'Department',
					  'PartNumber':'Part Desc',
					  'UnitPrice':'UnitPrice',
					  'RequestedQty':'Qty',
					  'TotalQty':'Amount',
					  'PRDStatus':'Status'}; 
					   
	$.each(PR_fields,function(i,e){
		$('#tbl_PR thead tr').append('<th>'+e+'</th>');
		$('#tbl_PR tfoot tr').append('<th>'+e+'</th>');
	});
	
	$('#tbl_PR thead tr').append('<th>&nbsp;&nbsp;&nbsp;Action&nbsp;&nbsp;&nbsp;</th>');
	$('#tbl_PR tfoot tr').append('<th>Action</th>');	
	$('#tbl_PR,.display').css("white-space", "nowrap");
	
	tbl_PR = $('#tbl_PR').dataTable({
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50], [10, 25, 50]],
		sPaginationType: 'full_numbers',
		bDestroy: true,
		bProcessing: true,
		bServerSide: true, 
		aaSorting: [[0,'desc']],
		aoColumnDefs : [  {"aTargets" : [ 3 ],
						 "bSearchable": false,
						 "bSortable" : false
						},{"aTargets" : [ 4 ],
						 "sClass" : "alignRight",
						 "bSearchable": false,
						 "bSortable" : false
						}, {"aTargets" : [5 ],
						 "sClass" : "alignRight",
						 "bSearchable": false,
						 "bSortable" : false
						}, {"aTargets" : [ 6 ],
						 "sClass" : "alignRight",
						 "bSearchable": false,
						 "bSortable" : false
						}, {"aTargets" : [ 7 ],
						 "bSearchable": false,
						 "bSortable" : false
						}, {"aTargets" : [ 8 ],
						 "bSearchable": false,
						 "bSortable" : false
						}],
		sAjaxSource: "purchase/ApprovePR/viewPRData", 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(PR_fields)) } );
			aoData.push( { "name": "PRStatus", "value": $('#PRStatus').val() } );  
			aoData.push( { "name": "PRType", "value": $("#radiotype input:radio:checked").val() } ); 
			aoData.push( { "name": "YearRequested", "value": $("#YearRequested").val() } ); 
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				beforeSend : function(){
					if(!isFiltered){
						return false;
					}
				},
				success: fnCallback,
				error: function(request){
					//showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	
	setTableHeaderHTML('tbl_PR',' <div id="radiotype" style="width:300px;float:left;margin-left: 30px;"> Type : '+
						' <input type="radio" id="All" name="radiotype" checked="checked" value=""/><label for="All">All</label> '+
						' <input type="radio" id="NORMAL" name="radiotype" value="NORMAL" /><label for="NORMAL">Normal</label> '+
						' <input type="radio" id="JSIEXPENSE" name="radiotype" value="JSIEXPENSE"/><label for="JSIEXPENSE">JSI Expense</label>'+
						' <input type="radio" id="JSICOST" name="radiotype" value="JSICOST"/><label for="JSICOST">JSI Cost</label> </div>'+
						'<div  style="margin-left: 10px;width:300px;float:left"> Status :  <select name="PRStatus" id="PRStatus"> <option value="">All</option> '+
						' <option value="12">Draft</option>'+
						'<option value="11">Requested to PRD Manager</option><option value="9">Approved By PRD Manager</option>'+
						'<option value="10">Rejected by PRD Manager</option><option value="7">Approved By Dept Manager</option>'+
						'<option value="8">Rejected By Dept Manager</option>'+
						'<option value="5">Approved By Branch Manager</option><option value="6">Approved By Branch Manager</option>'+
						'<option value="1">Completed PR</option><option value="2">Deleted</option></select></div>'+
						'<input type="button" id="MultiApprove-btn" value="Approve Selected" />');   
	
	$( "#radiotype" ).buttonset();
	 
	$('#PRStatus').change(function(){
		tbl_PR.fnDraw(); 
	});	
	
	if($('#hid-isPRDManager').val() ==1){
		$('#PRStatus').val('11');
		isFiltered = true;
		tbl_PR.fnDraw(); 
	}else if($('#hid-isbm').val() ==1){
		$('#PRStatus').val('7');
		isFiltered = true;
		tbl_PR.fnDraw(); 
	}else if($('#hid-isgm').val() ==1){
		$('#PRStatus').val('5');
		isFiltered = true;
		tbl_PR.fnDraw(); 
	}else if($('#hid-ismanager').val() ==1){
		$('#PRStatus').val('9');
		isFiltered = true;
		tbl_PR.fnDraw(); 
	}else{
		isFiltered =  false;
	}
	
	$("#radiotype input:radio").change(function(){
		tbl_PR.fnDraw();
	});
	
	$('#MultiApprove-btn').click(function(){
		var data = tbl_PR.fnGetData();
		var valid = false;
		var count = 0;
		$.each(data,function(i,e){
			if($(e[0]).is("input")){
				if($('#chkID_'+$(e[0]).val()).is(":checked")){
					count++;
					valid = true;
				}
			}
		});
		console.log(count);
		if(valid){		
		 	var confirmIR = ["fn_ConfirmApproveMultiData",count];
			msgbox('Confirm','<p class="warning">Are you sure you want to Approve?</p>',0,confirmIR);		
		}else{
			msgbox("Error",'<p class="warning">Please Select Checkbox first before you click Approve.</p>','addpritems-btn');		
		}
	});
	
	
	fn_ConfirmApproveMultiData = function(pcount){
		var _count = 0;
		var formValues;
		var data = tbl_PR.fnGetData();
		var valid = false;
		var type ='';
		if($('#hid-isPRDManager').val()== 1){
			type='PRDManager';
		}else if($('#hid-isgm').val() == 1){
			type='GM';
		}else if($('#hid-isbm').val() == 1){
			type='BM';
		}else if($('#hid-ismanager').val() == 1){
			type='Manager';
		}else{
			type ='';
		}
		
		$.each(data,function(i,e){
			if($(e[0]).is("input")){
				formValues =  '';
				
				if($('#chkID_'+$(e[0]).val()).is(":checked")){
					_count++;
					 formValues =  {"PRNumber": $(e[0]).attr('data'),"Type":type,"Approved":1,'ApprovedReason': '' };
	
					ProcessRequest('purchase/PurchaseRequisition/submitPRData',formValues,
						function(data){ 
							if(_count==pcount){
								var data = explode("|",data);
								msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
								if(data[0]=='success'){
									tbl_PR.fnDraw();
								}
							}
						});
				}
			}
		});
	}
	
	fn_todoPR = function(action,pthis,prnumber){
		switch(action){
			case 'Checked' : 
				var nTr = $(pthis).parents('tr')[0];
				$(nTr).toggleClass('tropen');
			break;	
			case 'PrintPR' : 
					fn_PrintPR(prnumber);
				break;
			case 'PrintPRWithoutIMEI' : 
					fn_PrintPrWithoutPrice(prnumber);
				break;
			case 'print' : 
					fn_PrintPR(prnumber);
			break;
			case 'detail' : 
				var nTr = $(pthis).parents('tr')[0];
				if ( tbl_PR.fnIsOpen(nTr) )
				{
					/* This row is already open - close it */
					$(pthis).removeClass('details-close').addClass('details-open');
					tbl_PR.fnClose( nTr );
					$(nTr).children('td').toggleClass('tropen');
					$(nTr).children('td:eq(0)').toggleClass('tropen-bg');
				}
				else
				{
					/* Open this row */
					$(pthis).removeClass('details-open').addClass('details-close');
					ProcessRequest('purchase/PurchaseRequisition/getPRApprovalData',
							{'PRNumber':prnumber},
							function(data){ 
								tbl_PR.fnOpen( nTr,  fnFormatDetails ( data ), 'table-row-details' ); 
								$(nTr).children('td').toggleClass('tropen');
								$(nTr).children('td:eq(0)').toggleClass('tropen-bg');
							})
					
				}
			break;	
		}
	}
	
	function fnFormatDetails ( data )
	{
		return data; 
	}
	
	fn_PrintPrWithoutPrice = function(PRNumber){
		window.open("report/PrintPDF/PRWithoutPriceREPORT/"+PRNumber,'_blank',false);
	}
	
	fn_PrintPR = function(PRNumber){
		window.open("report/PrintPDF/PRREPORT/"+PRNumber,'_blank',false);
	}
	
	fn_ApprovePRData=function(PRNumber,Type,Approved,ApprovedReason){
		 var msg;
		 if(Type =='PRD'){
			msg = 'Are you sure you want to Submit PR to Manager?';
		 }else if(Type =='PRDManager'){ 
			if(Approved == 1){
				msg = 'Are you sure you want to Approve?';
			}else{
				msg = 'Are you sure you want to Reject?';
			}
		 }else if(Type =='Manager'){
			if(Approved == 1){
				msg = 'Are you sure you want to Approve?';
			}else{
				msg = 'Are you sure you want to Reject?';
			}
		 }else if(Type =='BM'){
			if(Approved == 1){
				msg = 'Are you sure you want to Approve?';
			}else{
				msg = 'Are you sure you want to Reject?';
			}
		 }else if(Type =='GM'){ 
		 	if(Approved == 1){
				msg = 'Are you sure you want to Approve?';
			}else{
				msg = 'Are you sure you want to Reject?';
			}
		 }

		 var formValues;
		 formValues =  {"PRNumber": PRNumber,"Type":Type,"Approved":Approved,'ApprovedReason': ApprovedReason };
			
		var confirmpr = ["fn_ConfirmSubmitPRData",formValues];
		msgbox('Confirm Complete','<p class="warning">'+msg+'</p>',0,confirmpr);
	}
	
		
	fn_ConfirmSubmitPRData = function(formValues){
		ProcessRequest('purchase/PurchaseRequisition/submitPRData',
						formValues,'fn_ConfirmSubmitPRCallBack');
	}
	
	fn_ConfirmSubmitPRCallBack=function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		if(data[0] == 'success'){
			tbl_PR.fnDraw();
		} 
	}
	
		fn_RejectPRData = function(PRNumber){
		$( "#PRRejectReason-div" ).dialog({
			resizable: false,
			height:300,
			width:490,
			modal: true,
			closeOnEscape: true,
			buttons: {
				"Reject": function() {
					if($('#ApprovedReason').val().length == 0){
						msgbox('Error Message','<p class="error"> Please entry Reason</p>','ApprovedReason');	
					}else{
						var type ='';
						if($('#hid-isPRDManager').val()== 1){
							type='PRDManager';
						}else if($('#hid-isgm').val() == 1){
							type='GM';
						}else if($('#hid-isbm').val() == 1){
							type='BM';
						}else if($('#hid-ismanager').val() == 1){
							type='Manager';
						}
						fn_ApprovePRData(PRNumber,type,2,$('#ApprovedReason').val());
						$(this).dialog("destroy"); 
					}
				},
				Close: function() {
					$(this).dialog("destroy");  
				}
			},
			close: function(){
				$(this).dialog("destroy");  
			}
		});
	}
	
});
</script>

