<h2 class="title">Purchase &raquo; Internal Request</h2>
<div class="top-button">
	<button id="add-new"><span class="ui-icon ui-icon-plusthick"></span>Add New Internal Request</button>
	<button id="back-to-list" style="display:none"><span class="ui-icon ui-icon-arrowthick-1-w"></span>Back to List</button>
</div>
<div class="clear"></div>
<div class="list">
	<table class="display" id="tbl_InternalRequest">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>
<div class="newentry" style="display:none"> 
	<h3 class="form-title">Add New Internal Purchase Request</h3>
	<form class="globalform" id="newInternalRequest-frm" name="newInternalRequest">
		<em style="font-size: 13px; color:#888"><span class="req">*</span> Required field</em>
		<ol>
			<div class="lfpane" >
				<!-- <li>
					<label>ASAP</label>
					<input id="ASAP" type="checkbox" value="" name="ASAP">
				</li>-->
				<li>
					<label>IR Type</label>
					<select name="IRType" id="IRType">
						<Option Value="NORMAL"> Packing materials</option>
						<Option Value="JSI"> JSI </option>
					</select>  
					<span class="req">*</span>
					<button id="ChangeIRType-btn" name="ChangeIRType-btn" data="" ><span class="ui-icon ui-icon-document"></span>Change</button>
				</li> 
				<li>
					<label>Date Required</label>
					<input type="text" id="DateRequired" name="DateRequired" value="" width="180px" autocomplete="off"/>
					<span class="req">*</span>
				</li> 
				<li>
					<label>Remarks</label>
					<textarea id="Remarks" name="Remarks" style="width:200px"> </textarea>
				</li>
			</div>
			<div class="rfpane" >	
				<li style="width:100%!important">
					<div id="IRStatus-span" ></div>
					<!--<input type="text" id="ApprovedBy" name="ApprovedBy" class="readonly"/>
					<label id="lbl-ApprovedReason" style="color:red"></label>-->
				</li>
				<li>
					<label>Department</label>
					<input type="text" id="Department" name="Department" class="readonly"/>
					<input type="hidden" id="hid-Department" name="hid-Department" value="<?php echo($dep_name);?>" />
				</li>
				<li>
					<label>Request By</label>
					<input type="text" id="RequestedBy" name="RequestedBy" class="readonly"/>
					<input type="hidden" id="hid-RequestedBy" name="hid-RequestedBy" value="<?php echo($name);?>" />
					<input type="hidden" id="hid-username" name="hid-username" value="<?php echo($username);?>" />
					<input type="hidden" id="hid-isPRDManager" name="hid-isPRDManager" value="<?php echo($is_PRDManager);?>"/>
				
				</li>
				<li> 
					<!--<input type="button" id="save-btn" name="save-btn" value="Save" data=""/> 
					<input type="button" id="add-btn" value="Add Items" data="" />	
					<input type="button" id="cancel-btn" value="Cancel Request" data="" />	
					<input type="button" id="CompleteIR-btn" value="Complete" data="" /> -->
					<button id="save-btn" name="save-btn" data="" ><span class="ui-icon ui-icon-disk"></span>Save</button>
					<button  id="add-btn" name="add-btn" data="" ><span class="ui-icon ui-icon-circle-plus"></span>Add Items</button>
					<button  id="cancel-btn" name="cancel-btn" data="" ><span class="ui-icon ui-icon-circle-close"></span>Cancel Request</button>
					<button id="CompleteIR-btn" name="CompleteIR-btn" data="" ><span class="ui-icon ui-icon-circle-check"></span>Complete</button>
				</li>
			</div>
			<div style="width: 100%;">
				<li style="float: right; margin-bottom: 15px; margin-right: 110px;">
					<!--<input type="button" id="new-btn" value="New" data="" />
					<input type="button" id="approve-btn" value="Approve" data="" />	
					<input type="button" id="reject-btn" value="Reject" data="" />	
					<input type="button" id="CreatedPR-btn" value="CreatedPR" data="" />	
					-->
					<button id="new-btn" name="new-btn" data="" ><span class="ui-icon ui-icon-plusthick"></span>New</button>
					<button id="approve-btn" name="approve-btn" data="" ><span class="ui-icon ui-icon-check"></span>Approve</button>
					<button id="reject-btn" name="reject-btn" data="" ><span class="ui-icon ui-icon-closethick"></span>Reject</button>
					<button id="CreatedPR-btn" name="CreatedPR-btn" data="" ><span class="ui-icon ui-icon-document"></span>Create New PR</button>
					<input type="hidden" id="hid-manager" name="hid-manager" value="<?php echo($is_manager);?>" />
				</li>
			</div>			
		</ol> 
		<div class="clear"></div>	
	</form>

</div>
<div id="tblIRDetail-div">
	<table class="display" id="tbl_IRDetail">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>  

<div id="IRDetail-div" style="display:none" title="Add Item"> 
	<form class="globalform" id="IRdetail-frm" name="IRdetail">
		<em style="font-size: 13px; color:#888"><span class="req">*</span> Required field</em>
		<ol>
			<li>
				<label>Customer</label>
				<select name="CustomerID" id="CustomerID">
				</select>
			</li>
			<li>
				<label>PartNumber / Description</label>
				<select name="PartNumber" id="PartNumber">
				</select>  
				<input type="text" id="PartNumber-txt" name="PartNumber-txt" value="" style="display: none;"/>
				<input type="hidden" id="hid-PartNumber" name="hid-PartNumber" />
				<input type="hidden" id="hid-IRDetailID" name="hid-IRDetailID" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
				&nbsp;&nbsp;&nbsp;				
				<span class="req">*</span>	 
			</li>
			<li>
				<label>Description1</label>
				<input type="text" id="PartDesc1" name="PartDesc1" value="" />
				
			</li>
			<li>
				<label>Qty</label>
				<input type="text" id="RequestedQty" name="RequestedQty" value="" class="int" />
				<span class="req">*</span>
			</li>
			<li>
				<label>UOM</label>
				<select name="UOM" id="UOM">
				</select>
				<input type="hidden" id="hid-UOM" name="hid-UOM" />
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;	
				&nbsp;&nbsp;&nbsp;
				<span class="req">*</span>
			</li>
		</ol>
		<div class="clear"></div>	
	</form>
</div>  
<div id="InternalRequestReason-div" name="InternalRequestReason-div" title="Reject Reason" style="display: none;">
	<form class="globalform" id="InternalRequestReason-frm" name="InternalRequestReason" style="width:400px;">
		Reason
		<textarea id="ApprovedReason" name="ApprovedReason" style="width:370px"></textarea>
	</form>
</div>
<div id="PRType-div" name="PRType-div" title="Create PR" style="display: none;">
	<form id="PRType-frm" name="PRType" style="width:300px;border: 1px solid #DAECF4;margin: 0;padding: 10px;">
		<label style=" color: #666666;    display: block;    float: left;    margin: 0;    padding: 4px 0;    width: 100px;">PR Type</label>
		<select name="PRType" id="PRType">
		</select>
	</form>
</div>
<script type="text/javascript">
var tbl_InternalRequest,tbl_IRDetail,fn_CheckPartNumber,fn_ActionApprove,fn_RejectIRData;
$(document).ready(function(){
	$("input:submit, input:button, button, .button").button().click(function(event){
		event.preventDefault();
	});

	// $('button').click(function(event){
		// event.preventDefault();
	// });
	
	var InternalRequest_fields = {'ID':'ID',
							   'IRType':'Type',
							   'DateRequired':'DateRequired',
							    'Department':'Department',
								'RequestedByName':'Requested By',
								'ManagerName':'Manager',
								'PRNumber':'PRNumber',
								'Approved':'Status'  
							};
	$.each(InternalRequest_fields,function(i,e){
		$('#tbl_InternalRequest thead tr').append('<th>'+e+'</th>');
		$('#tbl_InternalRequest tfoot tr').append('<th>'+e+'</th>');
	});
	$('#tbl_InternalRequest thead tr').append('<th>Action</th>');
	$('#tbl_InternalRequest tfoot tr').append('<th>Action</th>');	
	

	
	// Table Initialization
	tbl_InternalRequest = $('#tbl_InternalRequest').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50], [10, 25, 50]],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true,
		sAjaxSource: "purchase/InternalRequest/view",
		aaSorting: [[0,'desc']],
		aoColumnDefs : [  {"aTargets" : [ 7 ],
						 "bSearchable": false,
						 "bSortable" : false
						},{"aTargets" : [ 8 ],
						 "bSearchable": false,
						 "bSortable" : false,
						"sWidth": "95px"
						}],
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "WawiID", "value": "" } );
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(InternalRequest_fields)) } );
			aoData.push( { "name": "IRStatus", "value": $('#IRStatus').val() } );  
			aoData.push( { "name": "IRType", "value": $("#radiotype input:radio:checked").val() } );			
			$.ajax( {
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	} );
	
	$('#tbl_InternalRequest,.display').css("white-space", "nowrap");
	
	setTableHeaderHTML('tbl_InternalRequest',' <div id="radiotype" style="width:300px;float:left;margin-left: 30px;"> Type : '+
						' <input type="radio" id="All" name="radiotype" checked="checked" value=""/><label for="All">All</label> '+
						' <input type="radio" id="NORMAL" name="radiotype" value="NORMAL"/ /><label for="NORMAL">Packing materials</label> '+
						' <input type="radio" id="JSI" name="radiotype" value="JSI"/><label for="JSI">JSI</label> </div>'+
						'<div  style="margin-left: 10px;width:200px;float:left"> Status : <select name="IRStatus" id="IRStatus"> <option value="All">All</option>'+
						'<option value="Draft">Draft</option><option value="Pending">Pending</option>'+
						'<option value="Approved">Approved</option>'+
						'<option value="Rejected">Rejected</option><option value="ActivatedPR">Activated PR</option>'+
						'<option value="NonActivatedPR">Non-Activated PR</option><option value="CancelIR">Cancel IR</option></select></div>'+
						' <input type="button" id="MultiApprove-btn" value="Approve All" />');
	 
	 $( "#radiotype" ).buttonset();
	 
	 $("#radiotype input:radio").change(function(){
		//console.log( $("#radiotype input:radio:checked").val());
		tbl_InternalRequest.fnDraw();
		
		// Do something interesting here
	});

	
	if($('#hid-manager').val() ==1){
		$('#IRStatus').val('Pending');
		tbl_InternalRequest.fnDraw(); 
	}
	
	$('#MultiApprove-btn').click(function(){
		var data = tbl_InternalRequest.fnGetData();
		var valid = false;
		var count = 0;
		$.each(data,function(i,e){
			if($(e[0]).is("input")){
				//console.log($('#chkID_'+$(e[0]).val()).is(":checked"));
				if($('#chkID_'+$(e[0]).val()).is(":checked")){
					count++;
					valid = true;
				}
			}
		});
		if(valid){		
		 	var confirmIR = ["fn_ConfirmApproveMultiData",count];
			msgbox('Confirm','<p class="warning">Are you sure you want to Approve?</p>',0,confirmIR);		
		}else{
			msgbox("Error",'<p class="warning">Please Select Checkbox first before you click Approve.</p>','addpritems-btn');		
		}
	});
	
	fn_ConfirmApproveMultiData = function(pcount){
		var _count = 0;
		var formValues;
		var data = tbl_InternalRequest.fnGetData();
		var valid = false;
		$.each(data,function(i,e){
			if($(e[0]).is("input")){
				//console.log($('#chkID_'+$(e[0]).val()).is(":checked"));
				formValues =  '';
				if($('#chkID_'+$(e[0]).val()).is(":checked")){
					_count++;
					formValues =  {"ID": $(e[0]).val(),"manager":$('#hid-manager').val(),"type":'1','reason': '' };
					ProcessRequest('purchase/InternalRequest/actionInternalRequest',formValues,
						function(data){ 
							if(_count==pcount){
								var data = explode("|",data);
								msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
								if(data[0]=='success'){
									tbl_InternalRequest.fnDraw();
								}
							}
						});
				}
			}
		});
	}
	
	$('#IRStatus').change(function(){
		tbl_InternalRequest.fnDraw();
	});
	
	$('#IRType').change(function(){
		if($('#IRType').val() =='NORMAL'){
			$('#CustomerID').combobox('selected','');
			$('#CustomerID').combobox('enable');
			
			$('#PartNumber').combobox('selected','');
			$('#PartNumber').combobox('enable');
			$('#UOM').combobox('disable');
		}else{
			$('#CustomerID').combobox('disable');
			$('#PartNumber').combobox('disable');
			$('#UOM').combobox('enable');
			//$('#CustomerID-combox').focus();
			$('#DateRequired').focus();
		}
	});
	
	
	var IR_fields = {'ID':'ID',
				   'PartNumber':'PartNumber',
				   'PartDesc':'Part Desc',
				   'PartDesc1':'Part Desc1',
					'RequestedQty':'Requested Qty',
					'UOM':'UOM'}; 
	$.each(IR_fields,function(i,e){
		$('#tbl_IRDetail thead tr').append('<th>'+e+'</th>');
		$('#tbl_IRDetail tfoot tr').append('<th>'+e+'</th>');
	});
	$('#tbl_IRDetail thead tr').append('<th>Action</th>');
	$('#tbl_IRDetail tfoot tr').append('<th>Action</th>');	

	// Table Initialization
	tbl_IRDetail = $('#tbl_IRDetail').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		sAjaxSource: "purchase/InternalRequest/viewDetail", 
		aoColumnDefs : [  {"aTargets" : [ 6 ],
						 "bSearchable": false,
						 "bSortable" : false
						}],
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "WawiID", "value": "" } );
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(IR_fields)) } );
			aoData.push( { "name": "IRID", "value": $('#save-btn').attr('data') } ); 
		
			// aodata.push( { "name": "isortcol_0", "value": 0 } );
			// aodata.push( { "name": "isortingcols", "value": 1 } );
			// aodata.push( { "name": "ssortdir_0", "value": 'asc' } );
			// aodata.push( { "name": "bsortable_0", "value": true } );
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	} );
	
	$('#tblIRDetail-div').hide(); 
	function fn_AddInternalRequest(pButtonLabel){
		/* Create Dialog for IRDetail */
		$( "#IRDetail-div" ).dialog({
			autoOpen: true,
			resizable: false,
			height:430,
			width:490,
			modal: true,
			closeOnEscape: true,
			buttons: [{
				text: pButtonLabel,
				click: function() {
					var rules =	[
						"function,fn_CheckPartNumber", 
						"required,RequestedQty,Please enter Quantity.",
						"digits_only,RequestedQty,Please enter valid Quantity.",
						"required,UOM,Please enter UOM."
					];
					rsv.customErrorHandler = formError;	
					rsv.onCompleteHandler = fn_formSubmitIRDetail;
					rsv.displayType= "display-html";
					rsv.validate(document.forms['IRdetail'],rules);  }
				},{
				text: "Close",
				click: function() {
					tbl_IRDetail.fnDraw();
					$( this ).dialog( "destroy" );
				}
			}],close: function(){
				tbl_IRDetail.fnDraw();
				$(this).dialog("destroy");  
			}
		});
	}
	
	fn_CheckPartNumber = function(){
		var valid = true;
		var field;
		var msg ='';
		if($('#IRType').val() == 'NORMAL'){
			if($('#PartNumber').val().length == 0){
				field = $('#PartNumber-combox');
				msg = "Please enter PartNumber!";
				valid = false;
			}
		}else{
			if($('#PartNumber-txt').val().length == 0){
				field = $('#PartNumber-txt');
				msg = "Please enter Description!";
				valid = false;
			}
		}
	
		if(!valid)
			return [[field,msg]];
		
		return valid;
	}
	$('#add-btn').click(function(){
		fn_ClearFormDetail();
		fn_AddInternalRequest("Add"); 
	});
	
	fn_formSubmitIRDetail = function(theform){
		var formValues = $(theform).serializeArray();
		formValues.push({ name: "IRID", value:  $('#save-btn').attr('data') }); 
		formValues.push({ name: "IRDetailID", value: $('#hid-IRDetailID').val() });
		formValues.push({ name: "IRType", value: $('#IRType').val() });
		var action = 'saveIRDetail'; 
		ProcessRequest('purchase/InternalRequest/'+action,formValues,'fn_submitIRDetailCallback');
	}
	
	fn_submitIRDetailCallback = function(data){
		var data = explode("|",data);
		var field ='PartNumber-txt';
		if($('#IRType').val() =='NORMAL'){
			field = 'CustomerID-combox';
		}
		if(data[0]=='success'){
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>',field);	
			if($('#hid-IRDetailID').val().length ==0){
				fn_ClearFormDetail();
			}else{
				$("#IRDetail-div").dialog( "destroy" );
			}
		}else{
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>',field);	
		}
	}
	
	$("#CompleteIR-btn").click(function(){
		if(tbl_IRDetail.fnGetData().length == 0){
			msgbox("error",'<p class="error">No record found in IR Detail List</p>','addpritems-btn');		
		}else{
			var confirmIR = ["fn_ConfirmCompleteIRData",$('#save-btn').attr('data')];
			msgbox('Confirm Complete','<p class="warning">Are you sure you want to Complete IR?</p>',0,confirmIR);
		}
	});
	
	fn_ConfirmCompleteIRData= function(pid){
		ProcessRequest('purchase/InternalRequest/completeIRData',{'ID':pid},'fn_CompleteIRCallback');
	}
	
	fn_CompleteIRCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','PartNumber');	
		if(data[0]=='success'){
			fn_SearchIR($('#save-btn').attr('data'));
		}else{	
		}
	}
	
	$( "#DateRequired" ).datepicker({
		 minDate: new Date()
	 });
	
	// $('#ASAP').click(function(){
		// $( "#DateRequired" ).datepicker("destroy");
		 // if ($(this).is(':checked')){
			// $('#DateRequired').val('ASAP').removeClass().attr('class', 'readonly');
			// $('#Remarks').focus();
		 // }else{
			// $( "#DateRequired" ).datepicker();
			// $('#DateRequired').val('').removeClass().attr('class', 'datetime hasDatepicker').focus();
		 // }
	// });
	

	// Todo Function
	todo = function(action,id,type){
		switch(action){
			case 'cancelir':
				
				break;
			case 'view' : 
				fn_SearchIR(id);
				break; 
			case 'gotopr' : 
				fn_GOTOPR(id,type);  
				break;	
			case 'editIRDetail' : 
				ProcessRequest('purchase/InternalRequest/searchIRDetail',
					{ID:id},function(data){
						if(data.length <= 1){
							var data = explode("|",data);
							msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');		
						}else{
							var data = json_decode(data);
							fn_AddInternalRequest("Update");
							$('#CustomerID').combobox('selected',data['CustomerID']);
							fn_PartNumberDataBind();
							$('#hid-PartNumber').val(data['PartNumber']);
							$('#hid-UOM').val(data['UOM']);
							$('#hid-IRDetailID').val(data['ID']);
							if($('#IRType').val() =='NORMAL'){
								$('#partnumber').combobox('selected',data['PartNumber']);
							}else{
								$('#PartNumber-combox').val('');
								$('#PartNumber').combobox('disable');
								$('#UOM').combobox('selected',data['UOM']);
							}
							$('#PartNumber-txt').val(data['PartNumber']);
							$( "#PartDesc1" ).val(data['PartDesc1']);	
							$( "#RequestedQty" ).val(data['RequestedQty']);
							// $.each(data,function(i,e){
								// $("#"+i).val(e);
							// });
						}
					}); 
				break;	
			case 'detail' : 
				$('#save-btn').attr('data') 
				var nTr = $(id).parents('tr')[0];
				if ( tbl_InternalRequest.fnIsOpen(nTr) )
				{
					/* This row is already open - close it */
					//this.src = "assets/images/details_open.png";
					$(id).removeClass('details-close').addClass('details-open');
					tbl_InternalRequest.fnClose( nTr );
					$(nTr).children('td').toggleClass('tropen');
					$(nTr).children('td:eq(0)').toggleClass('tropen-bg');
				}
				else
				{
					/* Open this row */
					//this.src = "assets/images/details_close.png";
					$(id).removeClass('details-open').addClass('details-close');
					ProcessRequest('purchase/InternalRequest/getIRApprovalData',
							{'ID':type},
							function(data){ 
								tbl_InternalRequest.fnOpen( nTr,  fnFormatDetails ( data ), 'table-row-details' ); 
								$(nTr).children('td').toggleClass('tropen');
								$(nTr).children('td:eq(0)').toggleClass('tropen-bg');
							})
					
				}
				break;	
			case 'deleteIRDetail' : 
				var confirm  = ["deleteIRDetail",id];
				  msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirm);				
				break; 
			
		}
	} 
	 
	function fnFormatDetails ( data )
	{
		return data; 
	}
	
	$('#ChangeIRType-btn').click(function(){
		var confirm  = ["fn_ChangeIRType",$('#save-btn').attr('data')];
		msgbox('Confirm Deletion','<p class="warning">Are you sure you want to change IR Type?</p>',0,confirm);				
	});
	
	fn_ChangeIRType = function(id){
		ProcessRequest('purchase/InternalRequest/changeIRType',{ID:id},'fn_changeIRTypeCallBack'); 
	}
	
	fn_changeIRTypeCallBack = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
		if(data[0]=='success'){
			$('#back-to-list').click();
		}else{
			
		}
	}
	
	$('#CreatedPR-btn').click(function(){
		fn_GOTOPR($('#save-btn').attr('data'),$('#IRType').val()); 
	});

	fn_SearchIR = function(id){
		ProcessRequest('purchase/InternalRequest/search',{ID:id},'searchInternalRequestCallback'); 
	}
	
	deleteIRDetail = function(id){
		ProcessRequest('purchase/InternalRequest/deleteIRDetail',{ID:id},'deleteIRDetailCallBack');
	}
	
	deleteIRDetailCallBack = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
		if(data[0]=='success'){
			tbl_IRDetail.fnDraw();
		}else{
			
		}
	}
	
	todoIRDetailEdit = function (action,id,customerid,partnumber,partdesc1,qty,uom){
		switch(action){
			case 'editIRDetail' : 
				fn_AddInternalRequest("Update");
				$('#CustomerID').combobox('selected',customerid);
				fn_PartNumberDataBind();
				$('#hid-PartNumber').val(partnumber);
				$('#hid-UOM').val(uom);
				$('#hid-IRDetailID').val(id);
				if($('#IRType').val() =='NORMAL'){
					$('#partnumber').combobox('selected',partnumber);
				}else{
					$('#PartNumber-combox').val('');
					$('#PartNumber').combobox('disable');
				}
				$('#PartNumber-txt').val(partnumber);
				$( "#PartDesc1" ).val(partdesc1);	
				$( "#RequestedQty" ).val(qty);
				
				break; 	
		}
	}
	
	fn_GOTOPR = function(id,type){
		/* Create Dialog for PRDetail */
		if(type =='NORMAL'){
			$('#PRType')
				.empty()
				.append('<option selected="selected" value="NORMAL">NORMAL</option>');
		}else{
			$('#PRType')
				.empty()
				.append('<option selected="selected" value="JSICOST">JSICOST</option>')
				.append('<option value="JSIEXPENSE">JSIEXPENSE</option>');
		}
		$( "#PRType-div" ).dialog({
			resizable: false,
			height:250,
			width:350,
			modal: true,
			closeOnEscape: true,
			buttons: {
				"Create": function() {
					ProcessRequest('purchase/InternalRequest/createPRFromInterRequest',
									{'ID':id,'PRType':$('#PRType').val()},'createPRCallBack');  
					//loadContent('purchase/PurchaseRequisition');
				},
				Close: function() {
					$(this).dialog("destroy");
				}
			},close: function(){
				$(this).dialog("destroy");  
			}
		});
	}
	
	createPRCallBack = function (data){
		if(data != 'error'){
			$( "#PRType-div" ).dialog("destroy");
			loadContent('purchase/PurchaseRequisition',{'PRNumber':data});
		}else{
			msgbox("Error Message",'<p class="error">Please Try Again</p>','country_name');	
		}
	}
	
	searchInternalRequestCallback = function(data){
		//$( "#DateRequired" ).datepicker("destroy");
		if($('#add-new').is(":visible")){
			$('#add-new').fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('#IRType').focus();});});
			$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Internal Request');});
		} 
		if(data.length <= 1){
			var data = explode("|",data);
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');		
		}else{
			var data = json_decode(data);
			$.each(data,function(i,e){
				$("#"+i).val(e);
			});
			
			if($('#IRType').val() =='NORMAL'){
				$('#PartNumber-txt').hide();
				$('#PartNumber').combobox('show');
				
				//$('#CustomerID').combobox('selected',data.CustomerID);
				$('#PartNumber').combobox('enable');
				$('#CustomerID').combobox('enable');
				$('#UOM').combobox('disable');
				fn_PartNumberDataBind();
			}else{
				$('#PartNumber-txt').show();
				$('#PartNumber').combobox('hide');
				
				$('#PartNumber-combox').val('');
				$('#CustomerID').combobox('disable');
				$('#PartNumber').combobox('disable');
				$('#UOM').combobox('enable');
			}
			$( "#hid-ID" ).val(data.ID);
			
			
			// if(data.DateRequired == 'ASAP'){
				// $('#DateRequired').removeClass().attr('class', 'readonly');
				// $('#ASAP').attr('checked',true);
			// }else{
				// $('#ASAP').attr('checked',false);
				// $("#DateRequired" ).datepicker();
				// $('#DateRequired').val('').removeClass().attr('class', 'datetime hasDatepicker');
			// }
			
			//$('#lbl-ApprovedReason').text('');
			
			if(data['Status'] == '0'){
				fn_FormDisiable(false);
			}else{
				fn_FormDisiable(true);
			}
			
			$('#IRStatus-span').text(''); 
				
			if(data['Deleted']==1){
				$('#IRStatus-span').append('<span class="status red">Cancel</span>');
			}else {
				if(data['Approved']==2){
					$('#IRStatus-span').append('<span class="status red">' + data['ApprovedBy']+'</span>' );
					$('#IRStatus-span').append('<span class="status red">Reason :' + data['ApprovedReason'] +'</span>'); 
				}else if(data['Status']==0){
					$('#IRStatus-span').append('<span class="status grey">Draft</span>');
				}else if(data['Approved']==1){
					$('#IRStatus-span').append('<span class="status green">Approved by '+ data['ApprovedBy'] +'</span>'  );
				}else if(data['Status']==1){
					$('#IRStatus-span').append('<span class="status orange">Pending</span>' );
				}
				
				if(data['CreatedPR']==1){
					$('#IRStatus-span').append('<span class="status green">Activated PR</span>' );
				}
			}
			
			if($('#hid-manager').val() =='1' && data.Deleted =='0'
				&&  data.Status =='1' && data.CreatedPR =='0') {
				if($('#hid-Department').val() == $('#Department').val()){
					$( "#approve-btn" ).button({ disabled: false });
					$( "#reject-btn" ).button({ disabled: false });
				}else if($('#hid-Department').val() =='WH' && $('#Department').val() =='QA/ESD'){
					if(data['ApprovedBy'] == 'Gan Boon Kee'){
						$( "#approve-btn" ).button({ disabled: false });
						$( "#reject-btn" ).button({ disabled: false });
					} else {
						$( "#approve-btn" ).button({ disabled: true });
						$( "#reject-btn" ).button({ disabled: true });
					}
				}else if($('#Department').val() =='PRD' && $('#hid-isPRDManager').val() =='1'){
					$( "#approve-btn" ).button({ disabled: false });
					$( "#reject-btn" ).button({ disabled: false });
				}else{
					$( "#approve-btn" ).button({ disabled: true });
					$( "#reject-btn" ).button({ disabled: true });
				}
				console.log('0');
				
			}else{
			console.log('1');
				$( "#approve-btn" ).button({ disabled: true });
				$( "#reject-btn" ).button({ disabled: true });
			}
			if(data['RequestedByOrg']== $('#hid-username').val() 
				&& data['Status'] == '0' && data['Deleted'] == '0'){
				$( "#save-btn" ).button({ disabled: false });
				$( "#add-btn" ).button({ disabled: false });
				$( "#cancel-btn" ).button({ disabled: false });
				$( "#CompleteIR-btn" ).button({ disabled: false });
			}else{
				$( "#save-btn" ).button({ disabled: true });
				$( "#add-btn" ).button({ disabled: true });
				$( "#cancel-btn" ).button({ disabled: true });
				$( "#CompleteIR-btn" ).button({ disabled: true }); 
			}
			if(data['RequestedByOrg']== $('#hid-username').val() && data['Status'] == '1'
				&& data['Approved'] == '0' && data['Deleted'] == '0'){
				$( "#cancel-btn" ).button({ disabled: false });
			}
			if($('#hid-Department').val()=='PRD' && data['Approved'] == 1 
				&& data['Status'] == 1 && data['CreatedPR'] == 0 ){
				$( "#ChangeIRType-btn" ).button({ disabled: false });
				$( "#CreatedPR-btn" ).button({ disabled: false }); 
			}else{
				$( "#ChangeIRType-btn" ).button({ disabled: true });
				$( "#CreatedPR-btn" ).button({ disabled: true });
			}
			$("#tblIRDetail-div").show();
			
			//$('#save-btn').attr('data',data.ID).find('span.iu').span('Save Changes');	
			$('#save-btn').attr('data',data.ID);	
			$( "#new-btn" ).button({ disabled: false });
			tbl_IRDetail.fnDraw();
			
			$('#IRType').addClass('readonly').attr("disabled", true);
			//$('#CustomerID').combobox('disable');
		}
	}
	
	$('#reject-btn').click(function(){
		/* Create Dialog for PRDetail */
		fn_RejectIRData($('#save-btn').attr('data'));
	});
	
	fn_RejectIRData = function(id){
		$( "#InternalRequestReason-div" ).dialog({
			resizable: false,
			height:300,
			width:490,
			modal: true,
			closeOnEscape: true,
			buttons: {
				"Reject": function() {
					if($('#ApprovedReason').val().length == 0){
						msgbox('Error Message','<p class="error"> Please entry Reason</p>','ApprovedReason');	
						$(this).dialog("destroy"); 
					}else{
						fn_ActionApprove(id,$('#hid-manager').val(),'2',$('#ApprovedReason').val());
						$(this).dialog("destroy");  
					}
				},
				Close: function() {
					$(this).dialog("destroy");  
				}
			},close: function(){
				$(this).dialog("destroy");  
			}
		});
	}
	
	$('#approve-btn').click(function(){
		fn_ActionApprove($('#save-btn').attr('data'),$('#hid-manager').val(),'1','');
	});
	
	fn_ActionApprove = function (ID,manager,type,reason){
		 var formValues,msg;
		 formValues =  {"ID": ID,"manager":manager,"type":type,'reason': reason };
		 
		if(type =='1'){
			msg = 'Are you sure you want to Approve?';
		}else if(type =='2'){
			msg = 'Are you sure you want to Reject?';
		}else if(type =='0'){ 
			msg = 'Are you sure you want to Cancel?';
		}
		var confirmIR = ["fn_ConfirmSubmitIRData",formValues];
		msgbox('Confirm','<p class="warning">'+msg+'</p>',0,confirmIR);	
	}
	
	fn_ConfirmSubmitIRData = function(formValues){
		 ProcessRequest('purchase/InternalRequest/actionInternalRequest',formValues,'fn_ActionApproveCallBack');
	}
	
	fn_ActionApproveCallBack = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
		if(data[0]=='success'){
			if($('#save-btn').attr('data') != ''){
				fn_SearchIR($('#save-btn').attr('data')); 
			 }else{
				tbl_InternalRequest.fnDraw();
			 }
			//$( "#InternalRequestReason-div" ).dialog("destroy"); 
		}
	}
	
	$('#back-to-list').click(function(){
		$('#tblIRDetail-div').hide();
		tbl_InternalRequest.fnDraw();
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#add-new').click(function(){ 
		$(this).fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#IRType').focus();});});
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Add New Internal Request');});
		
		fn_ClearForm();
		fn_FormDisiable(false);
		//$('#ASAP').attr('checked',false);
		$('input#Description').focus().select();
	});
	
	
	$(".int").keydown(function(event) {
		// Allow only backspace, delete, enter, tab
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 ) {

		}else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
	
	$( "#cancel-btn" ).click(function(){
		/* Create Dialog for PRDetail */
		$( "#InternalRequestReason-div" ).dialog({
			resizable: false,
			height:300,
			width:490,
			modal: true,
			closeOnEscape: true,
			buttons: {
				"Reject": function() {
					if($('#ApprovedReason').val().length == 0){
						msgbox('Error Message','<p class="error"> Please entry Reason</p>','ApprovedReason');	
						$(this).dialog("destroy"); 
					}else{
						fn_ActionApprove($('#save-btn').attr('data'),'0','0',$('#ApprovedReason').val()); 
						$(this).dialog("destroy");  
					}
				},
				Close: function() {
					$(this).dialog("destroy");  
				}
			},close: function(){
				$(this).dialog("destroy");  
			}
		});
	});

	$('#new-btn').click(function(){ 
		console.log('Click New');
		fn_ClearForm();
		fn_FormDisiable(false);
		//$('#ASAP').attr('checked',false);
		$('input#Description').focus().select();
		tbl_IRDetail.fnDraw();  
	});
	
	$('#save-btn').click(function(){
		var rules =	[			
			"required,DateRequired,Please enter Date Required."
			];
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = fn_formSubmit;
		rsv.validate(document.forms['newInternalRequest'],rules); 
		//event.preventDefault();
	});
	
	// Form Submission (add/update)
	function fn_formSubmit(theForm){
		var formValues = $(theForm).serializeArray();
			formValues.push({ name: "ID", value: $('#save-btn').attr('data') });
		var action =  'saveInternalRequest';
		ProcessRequest('purchase/InternalRequest/'+action,formValues,'fn_submitCallback');
	}
	
	fn_submitCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','Description');	 
		if(data[0]=='success'){ 
			$("#tblIRDetail-div").show(); 
			tbl_IRDetail.fnDraw(); 
			 $('#save-btn').val('Save Changes').attr('data',data[2]);
			 fn_SearchIR(data[2]);
		}
	}
	
	
	function fn_FormDisiable(status){
		$('#newInternalRequest-frm :input').each(function(index) {
	
			if($(this).attr('type') !='button' && $(this).attr('type') != 'hidden' && 
				$(this).attr('id') !='Department' && $(this).attr('id') !='RequestedBy' && 
				$(this).attr('id') !='ApprovedBy' ){  
				if(status){
					//$("#ASAP").attr("disabled", true);
					$(this).addClass('readonly').attr("disabled", status);
				}else{
					//$("#ASAP").attr("disabled", false);
					$(this).removeClass('readonly').attr("disabled", status);
				}
				
			}
		});
		
	}
	
	fn_ClearForm = function(){
		$('#newInternalRequest-frm :input').each(function(index) {
			if($(this).attr('type') !='button' && $(this).attr('type') != 'hidden'){
				$(this).val('');
			}
		});
		$('#IRStatus-span').text('');
		$('#Department').val($('#hid-Department').val());
		$('#RequestedBy').val($('#hid-RequestedBy').val());	
		$( "#add-btn" ).button({ disabled: true });
		$( "#cancel-btn" ).button({ disabled: true });
		$( "#CompleteIR-btn" ).button({ disabled: true }); 
	

		$( "#ChangeIRType-btn" ).button({ disabled: true });
		$( "#CreatedPR-btn" ).button({ disabled: true }); 
		
		$( "#approve-btn" ).button({ disabled: true });
		$( "#reject-btn" ).button({ disabled: true });
		$( "#save-btn" ).button({ disabled: false });
		
		$('#save-btn').val('Save').attr('data','');
		
		$('#CustomerID').combobox('enable');
		//$('#DateRequired').attr('readonly',true);
		//$( "#DateRequired" ).datepicker();
		//$('#DateRequired').removeClass().attr('class', 'datetime hasDatepicker');

		//$('#li-approve').hide();
	}
	
	fn_ClearFormDetail = function(){
		$('#IRdetail-frm :input').each(function(index) {
			if($(this).attr('type') !='button' && $(this).attr('type') != 'hidden'){
				$(this).val(''); 
			}
			$('#hid-IRDetailID').val('');
			$('#PartNumber').empty();
			$('#hid-UOM').val('');
			$('#hid-PartNumber').val(''); 
		});
		
	}
	/* Create Combobox for Department, PartNumber, Vendor */
	fn_ComboBoxload = function(data){
		var obj = jQuery.parseJSON(data);
		if (obj.PRType.length){
			$.each(obj.PRType, function(index,item) {
				$('#PRType')
				 .append($("<option ></option>")
				 .attr("value",item.ID)
				 .text(item.Value));
			});
		}
		
		loadComboBoxData("UOM",obj.UOM,true); 
		loadComboBoxData("CustomerID",obj.Customer,true); 
		
		$('#PartNumber').combobox();
		$('#UOM').combobox('disable');
		
		$('#CustomerID').combobox({				
			selected: function(event, ui) {
				fn_PartNumberDataBind();
			}
		});
	}
	
	function fn_PartNumberDataBind(){
		ProcessRequest('services/DropDownLists/partnumberByCustomer',{'CustomerID':$('#CustomerID').val()},'fn_loadPartNumber');
	}
	
	/* End ComboBox */
	fn_loadPartNumber = function(data){
	
		if(data != '1'){
			var obj = jQuery.parseJSON(data);
			$('#PartNumber')
					.empty()
					.append('<option selected="selected" value=""></option>');;
			$.each(obj, function(index,item) {
				$('#PartNumber')
				 .append($("<option ></option>")
				 .attr("value",item.ID)
				 .text(item.ID +' - '+ item.Value));	
			});
			$('#PartNumber').combobox();
			
			$('#PartNumber').combobox({				
				selected: function(event, ui) {
					ProcessRequest('purchase/PurchaseRequisition/getPartsPrice',{PartNumber:$('#PartNumber').val()},'fn_GetPartUOM');
				}
			});
			$('#UOM').combobox('selected','');
			if($('#hid-PartNumber').val() ==''){
				$('#PartNumber').combobox('selected','');
				$('#UOM').combobox('selected','');
			}else{
				$('#PartNumber').combobox('selected',$('#hid-PartNumber').val());
				$('#UOM').combobox('selected',$('#hid-UOM').val());
			}
		}
	}
	
	fn_GetPartUOM = function(data){
		if(data.length <= 1){
			msgbox("error",'<p class="error">No record found</p>','PRNumber');		
		}else{
			var data = json_decode(data);
			$('#UOM').combobox('selected',data['UOM']);
		}
	}

	/* Start Loading Data*/
	ProcessRequest('services/DropDownLists/getComboBoxdata',{'PRType':'PRType','UOM':'UOM','Customer':'Customer'},'fn_ComboBoxload');
	
	
});
</script>	