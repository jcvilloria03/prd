<h2 class="title">Purchase &raquo; PO Receiving</h2>
<div class="clear"></div>

<div class="newentry"  >
	<h3 class="form-title">PO Receiving</h3>
	
	<div style="float: left;  width: 40%;" >
		<form  id="newPOReceiving-frm" name="newPOReceiving" class="globalform">
			<em style="font-size: 13px; color:#888"><span class="req">*</span> Required field</em>
			<ol>
				<li>
					<label>PO #</label>
					<input type="text" id="PONumber" name="PONumber" value="" autocomplete="off"/>
					<span class="req">*</span>
				</li>
				<li>
					<label>Part Number</label>
					<select name="PartNumber" id="PartNumber">
					</select>  
					&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;
					<span class="req">*</span>
				</li>
				<li>
					<label>Invoice Number</label>
					<input type="text" id="Invoice" name="Invoice" value=""/>
					<span class="req">*</span>
				</li>
				<li>
					<label>DONumber</label>
					<input type="text" id="DONumber" name="DONumber" value=""/>
					<span class="req">*</span>
				</li>
				<li>
					<label>Delivery Date</label>
					<input type="text" id="DeliveryDate" name="DeliveryDate" value="" class="datetime" readonly="readonly" />
					<span class="req">*</span>
				</li>
				<li>
					<label>Delivery Qty</label>
					<input type="text" id="Qty" name="Qty" value="" class="int"  />
					<span class="req">*</span>
				</li>
				<li>
					<label>Credit Note Invoice</label>
					<input type="text" id="CNInvoice" name="CNInvoice" value="" />					
				</li>				
				<li style="float: right; margin-bottom: 15px; margin-right: 10px;">
					<!--<input type="button" id="seek-btn" value="Seek" />
					<input type="button" id="saveall-btn" value="Save All" />-->
					<button id="save-btn"  name="save-btn" data="" ><span class="ui-icon ui-icon-disk"></span>Save</button>	
					<button id="saveforall-btn" name="saveforall-btn" data="" ><span class="ui-icon ui-icon-search"></span>Save All</button>
					<button id="seek-btn" name="seek-btn" data="" ><span class="ui-icon ui-icon-search"></span>Seek</button>
				</li>
			</ol>
			<div class="clear"></div>
		</form>
	</div>
	<div style="float: right;  width: 60%;" >
		<table class="display" id="tbl_POReceivingDetail" >
			<thead><tr></tr></thead>
			<tbody></tbody>
			<tfoot><tr></tr></tfoot>
		</table> 
	</div>
	<div class="clear"></div>
	<div id="poReceiving" name="podiv"  width="100%"> 
		<table class="display" id="tbl_POReceiving" >
			<thead><tr></tr></thead>
			<tbody></tbody>
			<tfoot><tr></tr></tfoot>
		</table> 
	</div>
</div>
<script type="text/javascript">

var tbl_POReceivingDetail, tbl_POReceiving;
$(document).ready(function(){
	$("input:submit, input:button, button, .button").button().click(function(event){
		event.preventDefault();
	});

	var POReceivingDetail_fields = { 'POReceivingDetailID':'No',
					   'PartNumber':'PartNumber',
					  'Invoice':'Invoice',
					  'DONumber':'DONumber',
					  'DeliveryDate':'DeliveryDate',
					  'SONumber':'SONumber',
					  'Qty':'Quantity',
					  'CNInvoice':'CNInvoice'};
	
	$.each(POReceivingDetail_fields,function(i,e){
		$('#tbl_POReceivingDetail thead tr').append('<th>'+e+'</th>');
		$('#tbl_POReceivingDetail tfoot tr').append('<th>'+e+'</th>');
	});
	

	
	tbl_POReceivingDetail = $('#tbl_POReceivingDetail').dataTable({
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aaSorting: [[0,'desc']],
		sAjaxSource: 'purchase/POReceiving/viewPOReceivingDetailData', 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(POReceivingDetail_fields)) } );
			aoData.push( { "name": "PONumber", "value": $('#PONumber').val() } );	
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		},
		fnDrawCallback:function(){
			$('#tbl_POReceivingDetail tbody td .edit').editable( 'purchase/POReceiving/updatePOReceivingDetail', {
				submit  : 'Save',
				style   : 'display: inline',
                "callback": function( data, y ) {
                    /* Redraw the table from the new data on the server */
						var data = explode("|",data);
						if(data[0]=='success'){
							
						}else{
						}						
						fn_searchPONumber();
							msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','Qty');	
						//console.log(y);
					//tbl_POReceivingDetail.fnDraw();
                },"submitdata": function ( value, settings ) {
					var fieldname='';
					if($(this).parent().index() =='2'){
						fieldname ='Invoice';
					}else if($(this).parent().index() =='3'){
						fieldname ='DONumber';
					}else if($(this).parent().index() =='5'){
						fieldname ='SONumber';
					}else if($(this).parent().index() =='6'){
						fieldname ='Qty';
					}else if($(this).parent().index() =='7'){
						fieldname ='CNInvoice';
					}
					return {fieldname: fieldname};
				},
                "height": "14px"
            } );
		}
	});

 	setTableHeader('tbl_POReceivingDetail','PO Receiving Detail');
	
	/* Create Table For SO */
	var POReceiving_fields = { 'PONumber':'PONumber',
					  'PartNumber':'PartNumber',
					  'Description':'Description',
					  'RequestQty':'Requested Qty',
					  'ReceivedQty':'Received Qty',
					  'RemainQty':'Remain Qty'
					  };
	
	$.each(POReceiving_fields,function(i,e){
		$('#tbl_POReceiving thead tr').append('<th>'+e+'</th>');
		$('#tbl_POReceiving tfoot tr').append('<th>'+e+'</th>');
	});
	
	tbl_POReceiving = $('#tbl_POReceiving').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aaSorting: [[0,'desc']],
		sAjaxSource: 'purchase/POReceiving/viewPOReceivingData', 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(POReceiving_fields)) } );
			aoData.push( { "name": "PONumber", "value": $('#PONumber').val() } );		
			aoData.push( { "name": "Type", "value": $("#radiotype input:radio:checked").val() } ); 	
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	//Set Title for PRDetail
	//setTableHeader('tbl_POReceiving','PO Receiving Summary');
		
	setTableHeaderHTML('tbl_POReceiving',' <div id="radiotype" style="width:400px;float:left;margin-left: 30px;">Type : '+
						' <input type="radio" id="All" name="radiotype" checked="checked" value=""/><label for="All">All</label> '+
						' <input type="radio" id="RemainQty" name="radiotype" value="RemainQty" /><label for="RemainQty">Remaining PartNumber</label>'+
						' <input type="radio" id="RemainQty" name="radiotype" value="CompleteQty" /><label for="CompleteQty">Complete Qty</label></div>');
	
		
	 $("#radiotype input:radio").change(function(){
		tbl_POReceiving.fnDraw();
	});
 
 
	$("#PONumber").keypress(function(event) {
		if ( event.which == 13 ) {
			fn_searchPONumber();
		}
    });
	
	fn_searchPONumber = function(){
		ProcessRequest('purchase/POReceiving/checkPONumber',
						{'PONumber':$('#PONumber').val()},'fn_checkPONumberCallBack');
	}
	
	fn_checkPONumberCallBack = function(data){
		var data = explode("|",data);
		if(data[0] == '1'){
			tbl_POReceivingDetail.fnDraw();
			tbl_POReceiving.fnDraw();

			if(jQuery.trim($('#PONumber').val()).length > 6 && tbl_POReceivingDetail.fnGetData().length ==0){
				$('#saveforall-btn').button("enable");
			}else{
				$('#saveforall-btn').button("disable");
			}
			
			fn_PartNumberDataBind();
			
			$('#PartNumber-combox').focus();
			if(data[1] ==0){
				fn_FormDisiable(true);
			}else{
				fn_FormDisiable(false);
			}
			
		}else{
			msgbox('warning','<p class="warning">PONumber does not exist.</p>','PONumber');	
		}
	}
	
	$('#saveforall-btn').click(function(){
		var rules =	[
			"required,PONumber,Please enter PONumber.",
			"required,Invoice,Please enter Invoice.",
			"required,DONumber,Please enter DONumber.",
			"required,DeliveryDate,Please enter Delivery Date."
		];
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = fn_formSubmitPOSaveAll;
		rsv.displayType= "display-html";
		rsv.validate(document.forms['newPOReceiving'],rules);  
	});
	

	fn_formSubmitPOSaveAll = function(theform){
		var formValues = $(theform).serializeArray();
		formValues.push({ name: "PONumber", value: $('#PONumber').val() });
		var action = 'savePOReceivingSaveAll'; 
		ProcessRequest('purchase/POReceiving/'+action,formValues,'fn_savePOReceivingSaveAllCallback');
	}
	
	fn_savePOReceivingSaveAllCallback = function(data){
		var data = explode("|",data);
		if(data[0]=='success'){
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','PartNumber-combox');	
			fn_ClearForm();
			tbl_POReceivingDetail.fnDraw();
			tbl_POReceiving.fnDraw();
		}else{
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','Qty');	
		}
	}
	
	$('#save-btn').click(function(){
		var rules =	[
			"required,PartNumber,Please enter PartNumber.",
			"required,PONumber,Please enter PONumber.",
			"required,Invoice,Please enter Invoice.",
			"required,DONumber,Please enter DONumber.",
			"required,DeliveryDate,Please enter Delivery Date.",
			"required,Qty,Please enter Qty.",
			"digits_only,Qty,Please enter valid Quantity."
			
		];
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = fn_formSubmitPOReceivingDetail;
		rsv.displayType= "display-html";
		rsv.validate(document.forms['newPOReceiving'],rules);  
	});
	
	fn_formSubmitPOReceivingDetail = function(theform){
		var formValues = $(theform).serializeArray();
		formValues.push({ name: "PONumber", value: $('#PONumber').val() });
		var action = 'savePOReceivingDetail'; 
		ProcessRequest('purchase/POReceiving/'+action,formValues,'fn_formSubmitPOReceivingDetailCallback');
	}
	
	fn_formSubmitPOReceivingDetailCallback = function(data){
		var data = explode("|",data);
		if(data[0]=='success'){
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','PartNumber-combox');	
			fn_ClearForm();
			tbl_POReceivingDetail.fnDraw();
			tbl_POReceiving.fnDraw();
		}else{
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','Qty');	
		}
	}
	
	$('#seek-btn').click(function(){
		fn_FormDisiable(true);
		fn_ClearForm();
		$('#PONumber').val('').focus();
		tbl_POReceivingDetail.fnDraw();
		tbl_POReceiving.fnDraw();
	});
	
	fn_FormDisiable = function(status){	
		$('#newPOReceiving-frm :input').each(function(index) {
		if($(this).attr('type') !='button' && $(this).attr('type') != 'hidden' 
			&& $(this).attr('id') != 'PONumber' && $(this).attr('id') != 'PartNumber'){  
				if(status){
					//$("#ASAP").attr("disabled", true);
					$(this).addClass('readonly').attr("disabled", status);
				}else{
					//$("#ASAP").attr("disabled", false);
					$(this).removeClass('readonly').attr("disabled", status);
				}
			}
		});
		if(status){
			$('#PartNumber').combobox('disable');
			$('#PONumber').removeClass('readonly').attr("disabled", false);
		}else{
			$('#PartNumber').combobox('enable');
			$('#PONumber').addClass('readonly').attr("disabled", true);
		}
	}
	
	fn_ClearForm = function(){
		$('#newPOReceiving-frm :input').each(function(index) {
			if($(this).attr('type') !='button' 
				&& $(this).attr('type') != 'hidden' &&  $(this).attr('id') != 'PONumber'){
				$(this).val(''); 
			}
		});
		
	}
	
	function fn_PartNumberDataBind(){
		ProcessRequest('services/DropDownLists/partnumberByPONumber',{'PONumber':$('#PONumber').val()},'fn_loadPartNumber');
	}
	
	/* End ComboBox */
	fn_loadPartNumber = function(data){
	
		if(data != '1'){
			var obj = jQuery.parseJSON(data);
			$('#PartNumber')
					.empty()
					.append('<option selected="selected" value=""></option>');
			
			$.each(obj, function(index,item) {
				$('#PartNumber')
				 .append($("<option ></option>")
				 .attr("value",item.ID)
				 .text(item.ID +' - '+ item.Value));	
			});
			if(obj.length==1){
				$('#PartNumber').combobox('selected',obj[0].ID);
				$('#Invoice').focus();
			}
			$('#PartNumber').combobox();
		}
	}
	

	$('#PartNumber').combobox();
	fn_FormDisiable(true);
	$('#PONumber').focus();
	
	$(".int").keydown(function(event) {
			// Allow only backspace, delete, enter, tab
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 ) {

		}else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
	
	$( ".datetime" ).datepicker({ 
		maxDate: new Date(),
		//minDate: -0, maxDate: "+2M",
		dateFormat: "mm-dd-yy"
	});
	
});

	
</script>
