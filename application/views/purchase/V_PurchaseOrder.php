<h2 class="title">Purchase &raquo; Purchase Order</h2>

<div class="clear"></div>
<div class="list" id="tbl_pr-div"> 
	<table class="display" id="tbl_PR">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>
<div class="newentry"  style="display:none">
	<h3 class="form-title">Purchase Order</h3>
	<form class="globalform" id="newPO-frm" name="newPO">
	<em style="font-size: 13px; color:#888"><span class="req">*</span> Required field</em>
		<ol>
			<div class="lfpane" >
				<li>
					<label>PR NO</label>
					<input type="text" id="PRNumber" name="PurchasedBy" value="" class="readonly"/>
				</li>
				<li>
					<label>PO No</label>
					<input type="text" id="PONumber" name="PurchasedBy" value=""  class="readonly"/>
				</li>
				<!--<li>
					<label> Supplier</label>
				</li>-->
				<li>
					<label>Supplier</label>
					<select id="VendorID" name="VendorID">
					</select>
					<span class="req">*</span>	
					<textarea id="VendorAddress" name="VendorAddress" style="width:85%" rows=3> 
					</textarea>
				</li>
				<li>
					<label> Ship To</label>
					<select id="ShipTo" name="ShipTo">
					</select>
					<span class="req">*</span>	
					<textarea id="ShipToAddress" name="ShipToAddress" style="width:85%" rows=3> 
					</textarea>
				</li>
				<li>
					<label> Billing To</label>
					<select id="BillingTo" name="BillingTo">
					</select>
					<span class="req">*</span>	
					<textarea id="BillingAddress" name="BillingAddress" style="width:85%" rows=3> 
					</textarea>
				</li>
			</div>
			<div class="rfpane" >	
				<li style="width:100%!important">
					<div id="POStatus-span" ></div>
				</li>
				<li>
					<input type="button" id="unlockpo-btn" value="Unlock PO" />
					<input type="hidden" id="hidstatusPO" />
				</li>
				<li>
					<label>Payment Terms</label>
					<select id="PaymentTerms" name="PaymentTerms">
					</select>
					<span class="req">*</span>	
				</li>
				<li>
					<label>Delivery Terms</label>
					<select id="DeliveryTerms" name="DeliveryTerms">
					</select>
					<span class="req">*</span>	
				</li>
				<li>
					<label> Blanket Order </label>
					<input type="checkbox" id="BlanketOrder" name="BlanketOrder" value="" />
				</li>
				<li>
					<label> Required Delivery Date</label>
					<input type="text" id="ReqDeliveryDate" name="ReqDeliveryDate" value="" class="datetime" readonly="readonly"/>
					<span class="req">*</span>	
				</li>
				<li>
					<label>Attention</label>
					<input type="text" id="Attention" name="Attention" value="" />
					<span class="req">*</span>	
				</li>
				<li>
					<label>Ext</label>
					<input type="text" id="Ext" name="Ext" value="" />
					<span class="req">*</span>	
				</li>
				<li>
					<label>Buyer</label>
					<input type="text" id="Buyer" name="Buyer" value="" />
				</li>
				<li>
					<label>Buyer Ext</label>
					<input type="text" id="BuyerExt" name="BuyerExt" value="" />
					<span class="req">*</span>	
				</li>
				<li >
					<label> Special Requirements</label>
					<textarea id="Remarks" name="Remarks" style="width:110%"> </textarea>
					<span class="req">*</span>	
				</li>
			</div>
			<div style="width: 100%;">
				<li style="float: right; margin-bottom: 15px; margin-right: 110px;">
				
				<!--	<input type="button" id="new-btn" name="new-btn" value="New" data=""/>
					<input type="button" id="save-btn" name="save-btn" value="Save" data=""/>
					<input type="button" id="complete-btn" name="complete-btn" value="Complete PO" data=""/>
					<input type="button" id="delete-btn" name="delete-btn" value="Delete" data=""/>
					<input type="button" id="printpo-btn" value="Print PO" />-->
					
					<button id="new-btn" name="new-btn" data="" ><span class="ui-icon ui-icon-plusthick"></span>New</button>
					<button id="save-btn" name="save-btn" data="" ><span class="ui-icon ui-icon-disk"></span>Save</button>
					<button id="complete-btn" name="complete-btn" data=""  ><span class="uiui-icon ui-icon-circle-check"></span>Complete PO</button>
					<button  id="delete-btn" name="delete-btn" data="" ><span class="ui-icon ui-icon-circle-close"></span>Delete</button>
					<button id="printpo-btn" name="printpo-btn" data="" ><span class="ui-icon ui-icon-print"></span>Print PO</button>
					
				
				</li>
			</div>
		</ol>
		<div class="clear"></div>		
	</form>
	<div class="clear"></div>		
	<div class="list" > 
		<table class="display" id="tbl_PODetail">
			<thead><tr></tr></thead>
			<tbody></tbody>
			<tfoot><tr></tr></tfoot>
		</table>
	</div>	
</div>

<div id="PRDDeleteReason-div" name="PRDDeleteReason-div" title="Deleted Reason" style="display: none;">
	<form class="globalform" id="PRDDeletedReason-frm" name="PRDDeletedReason" style="width:400px;">
		Reason
		<textarea id="DeletedReason" name="DeletedReason" style="width:370px"></textarea>
	</form>
</div>
<script type="text/javascript">
var tbl_PR,tbl_PODetail,fn_disiableAll;
$(document).ready(function(){
	$("input:submit, input:button, button, .button").button().click(function(event){
		event.preventDefault();
	});
	$('#unlockpo-btn').hide();
	
	fn_GetComboBoxdata = function(data){
		var obj = jQuery.parseJSON(data);
		loadComboBoxData("VendorID",obj.Vendor,false);
		loadComboBoxData("ShipTo",obj.Addresses,false);
		loadComboBoxData("BillingTo",obj.Addresses,false);
		loadComboBoxData("PaymentTerms",obj.PaymentTerm,true);
		loadComboBoxData("DeliveryTerms",obj.DeliveryTerm,true);
		//Addresses
		$('#ShipTo').combobox({
			selected: function(event, ui) {
				$("textarea#ShipToAddress").val($('#ShipTo').val());
			}
		});
		$('#BillingTo').combobox({
			selected: function(event, ui) {
				$("textarea#BillingAddress").val($('#BillingTo').val());
			}
		});
		$('#VendorID').combobox({
			selected: function(event, ui) {
				ProcessRequest('purchase/PurchaseOrder/getVendorAddress',{VendorID:$('#VendorID').val()},'fn_GetVendorAddress');
			}
		});
			
	}
	
	var PR_fields = { 'ID':'ID',
					  'PRNumber':'PR No',
					  'PRType':'PRType',
					  'Department':'Department',
					  'DateRequired':'DateRequired',
					  'RequestedBy':'RequestedBy',
					  'PurchasedBy':'PurchasedBy',
					  'ApprovedBy':'ApprovedBy',
					  'Status':'Status'};
	
	$.each(PR_fields,function(i,e){
		$('#tbl_PR thead tr').append('<th>'+e+'</th>');
		$('#tbl_PR tfoot tr').append('<th>'+e+'</th>');
	});
	
	$('#tbl_PR thead tr').append('<th>Action</th>');
	$('#tbl_PR tfoot tr').append('<th>Action</th>');
	
	tbl_PR = $('#tbl_PR').dataTable({
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50], [10, 25, 50]],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aaSorting: [[0,'desc']],
		sAjaxSource: 'purchase/PurchaseOrder/viewPendingPurchaseData', 
		aoColumnDefs: [ { "bSortable": false, "aTargets": [ 8 ], "bSearchable": false } ],
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(PR_fields)) } );
			aoData.push( { "name": "PRStatus", "value": $('#PRStatus').val() } );	
			aoData.push( { "name": "PRType", "value": $("#radiotype input:radio:checked").val() } ); 		
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	//setTableHeader('tbl_PR','Purchase Requisition List');
	setTableHeaderHTML('tbl_PR',' <div id="radiotype" style="width:300px;float:left;margin-left: 30px;"> Type : '+
						' <input type="radio" id="All" name="radiotype" checked="checked" value=""/><label for="All">All</label> '+
						' <input type="radio" id="NORMAL" name="radiotype" value="NORMAL" /><label for="NORMAL">Normal</label> '+
						' <input type="radio" id="JSIEXPENSE" name="radiotype" value="JSIEXPENSE"/><label for="JSIEXPENSE">JSI Expense</label>'+
						' <input type="radio" id="JSICOST" name="radiotype" value="JSICOST"/><label for="JSICOST">JSI Cost</label> </div>'+
						'<select name="PRStatus" id="PRStatus"> <option value="">All</option>'+
					  '<option value="Pending">Pending</option><option value="Draft">Draft</option>'+
					  '<option value="Completed">Completed</option><option value="Deleted">Deleted</option></select>'); 
	
	$( "#radiotype" ).buttonset();
	 
	 
	$('#PRStatus').change(function(){
		tbl_PR.fnDraw(); 
	});
	
	 $("#radiotype input:radio").change(function(){
		//console.log( $("#radiotype input:radio:checked").val());
		tbl_PR.fnDraw(); 
		
		// Do something interesting here
	});

	
	
	/* Create Table For PRDetail */
	var PODetail_fields = {'No':'No',
						  'PartNumber':'PartNumber',
						  'PartDesc1':'Part Desc',
						  'Qty':'Qty',
						  'UnitPrice':'Unit Price',
						  'UOM':'UOM',
						  'ExtAmount':'Ext Amount'};
	
	$.each(PODetail_fields,function(i,e){
		$('#tbl_PODetail thead tr').append('<th>'+e+'</th>');
		$('#tbl_PODetail tfoot tr').append('<th>'+e+'</th>');
	});
	
	tbl_PODetail = $('#tbl_PODetail').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bProcessing: true
	});
	
	//Set Title for PRDetail
	setTableHeader('tbl_PODetail','Purchase Order Item List');
	
	function fn_PRDetailDatabind(){
		tbl_PODetail.fnClearTable();
		ProcessRequest('purchase/PurchaseOrder/viewAllPRAndPRDetailData',{PONumber:$('#PRNumber').val()},'fn_viewPRDetailData');
	}
	
	fn_viewPRDetailData = function(data){
		var obj = jQuery.parseJSON(data);
		
		if(obj.PurchaseOrder[0]){
			$.each(obj.PurchaseOrder[0],function(i,e){
				$("#newPO-frm #"+i).val(e);
			});
			
			
			$('#unlockpo-btn').hide();
			
			// if($('#ReqDeliveryDate').val() =='01-01-2099'){
				// $('#BlanketOrder').attr('checked',true);
				// $('#ReqDeliveryDate').removeClass().attr('class','readonly');
			// }else{
				// $('#BlanketOrder').attr('checked',false); 
				// $('#ReqDeliveryDate').removeClass().attr('class', 'datetime hasDatepicker');
			// }
			
			if(obj.PurchaseOrder[0].Deleted =='1'){
				$('#POStatus-span').append('<span class="status red">Deleted</span>');
				$('#hidstatusPO').val('Deleted');
				loadbtnAction(fn_btnList(false,true,true,true,true));
				fn_disiableAll(true);
			}else if(obj.PurchaseOrder[0].Status == '1'){
				$('#POStatus-span').append('<span class="status green" > Completed PO</font>');
				$('#hidstatusPO').val('Completed');
				$('#unlockpo-btn').show();
				loadbtnAction(fn_btnList(false,true,true,true,false));
				fn_disiableAll(true);
			}else{
				$('#POStatus-span').append('');
				$('#hidstatusPO').val('');
				fn_disiableAll(false);
				$('#PRNumber').addClass('readonly').attr('readonly',true);
				$('#PONumber').addClass('readonly').attr('readonly',true);
				if(obj.PurchaseOrder[0].VendorID =='' || obj.PurchaseOrder[0].VendorID ==null ){
					loadbtnAction(fn_btnList(false,false,true,true,true));
				}else{
					loadbtnAction(fn_btnList(false,false,false,false,true));
				}
			}
			
		
			
			$('#VendorID').combobox('selected',obj.PurchaseOrder[0].VendorID);
			$('#PaymentTerms').combobox('selected',obj.PurchaseOrder[0].PaymentTerms);
			$('#DeliveryTerms').combobox('selected',obj.PurchaseOrder[0].DeliveryTerms);
			$('#VendorID-combox').focus();
		}
			
		if(obj.PODetail){
			var no = 0;
			var partnumber,partdesc,qty,unitprice,uom;
			for (var i=0; i<obj.PODetail.length; i++){
				partnumber = obj.PODetail[i]['PartNumber'];
				partdesc = obj.PODetail[i]['PartDesc1'];
				qty = obj.PODetail[i]['Quantity'];
				unitprice = obj.PODetail[i]['UnitPrice'];
				uom = obj.PODetail[i]['UOM'];
				no = no + 1;
				
				tbl_PODetail.fnAddData( [no,partnumber,partdesc,qty,unitprice,uom,parseInt(qty) * parseFloat(unitprice)]);
			}
			tbl_PODetail.fnAdjustColumnSizing();
		}
		
	}
	
	fn_disiableAll = function(status){

		$('#newPO-frm').find('input,select,textarea').each(function() {
			if($(this).attr('type') !='button' ){
				if($(this).attr('id') != 'ReqDeliveryDate'){
					if(status){
						$(this).addClass('readonly').attr('readonly',true);
					}else{
						$(this).removeClass('readonly').removeAttr('readonly');
					}
				}else{
					if(status) $(this).removeClass().attr('class', 'readonly');
					else $(this).removeClass().attr('class', 'datetime hasDatepicker');
				}
			}
		});
		if($('#ReqDeliveryDate').val() =='01-01-2099'){
			$('#BlanketOrder').attr('checked',true);
		}
		
		if(status){
			$("#BlanketOrder").attr("disabled", true);
			$('#VendorID').combobox("disable");
			$('#PaymentTerms').combobox("disable");
			$('#DeliveryTerms').combobox("disable");
			$('#ShipTo').combobox("disable");
			$('#BillingTo').combobox("disable");
		}else{
			
			$("#BlanketOrder").removeAttr("disabled");
			$('#VendorID').combobox("enable");
			$('#PaymentTerms').combobox("enable");
			$('#DeliveryTerms').combobox("enable");
			$('#ShipTo').combobox("enable");
			$('#BillingTo').combobox("enable");
		}
	}
	// function fn_PRDatabind(){
		// tbl_PR.fnClearTable();
		// ProcessRequest('purchase/PurchaseOrder/viewPendingPurchaseData',{PRNumber:''},'fn_viewPRData');
	// }
	
	
	fn_GetVendorAddress = function(data){
		
		var data = json_decode(data);
		var shiptoaddress ='';
		$.each(data[0],function(i,e){
			shiptoaddress = shiptoaddress + e + '\n'			
		});
		$("textarea#VendorAddress").val(shiptoaddress);
		
	}
		
	$('#BlanketOrder').click(function(){
		 if ($(this).is(':checked')){
			$('#ReqDeliveryDate').val('01-01-2099').removeClass().attr('class', 'readonly');
		 }else{
			$('#ReqDeliveryDate').val('').removeClass().attr('class', 'datetime hasDatepicker');
		 }
	});
	
	$('#unlockpo-btn').click(function(){
		ponumber = $('#PONumber').val();
		var confirmpo = ["fn_confirmUnlockPOData",ponumber];
		msgbox('Confirm Unlock','<p class="warning">Are you sure you want to Unlock PO?</p>',0,confirmpo);
	});
	
	fn_confirmUnlockPOData = function(ponumber){
		ProcessRequest('purchase/PurchaseOrder/unlockPOData',{PONumber:ponumber},'fn_unlockPOCallback');
	}
	
	fn_unlockPOCallback= function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		if(data[0] == 'success'){
			//fn_PODetail($('#PONumber').val());
			$('#new-btn').click();
		}
	}
	
	$('#complete-btn').click(function(){
		fn_callCompletePOData($('#PONumber').val());
	});

	fn_callCompletePOData = function(ponumber){
		var confirmpo = ["fn_ConfirmCompletePOData",ponumber];
		msgbox('Confirm Complete','<p class="warning">Are you sure you want to Complete PO?</p>',0,confirmpo);
	}
	
	fn_ConfirmCompletePOData = function(ponumber){
		ProcessRequest('purchase/PurchaseOrder/completePOData',{PONumber:ponumber},'fn_CompletePOCallback');
	}
	
	fn_CompletePOCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		if(data[0] == 'success'){
			$('#new-btn').click();
			//fn_PODetail($('#PONumber').val());
		}
	}
	
	$('#printpo-btn').click(function(){
		fn_PrintPO($('#PONumber').val());
	});
	
	fn_PrintPO = function(ponumber){
		window.open("report/PrintPDF/POREPORT/"+ponumber,'_blank',false);
	}
	
	
	$('#new-btn').click(function(){
		$('#tbl_pr-div').slideToggle(function(){$('.newentry').slideToggle();});
		$('#POStatus-span').text('');
		$('#hidstatusPO').val('');
		//fn_PRDatabind
		tbl_PR.fnDraw(); 
	});

	$('#save-btn').click(function(){
		var rules =	[
				"required,VendorID,Please enter Supplior.",
				"required,VendorAddress,Please enter Vendor To Address.",
				"required,ShipToAddress,Please enter Ship To Address.",
				"required,BillingAddress,Please enter Billing Address.",
				"required,PaymentTerms,Please enter Date Payment Terms.",
				"required,DeliveryTerms,Please enter Delivery Terms.",
				"required,ReqDeliveryDate,Please enter Require Delivery Date.",
				"required,Attention,Please enter Attention.",
				"required,Ext,Please enter Ext.",
				"required,Buyer,Please enter Ext.",
				"required,BuyerExt,Please enter Ext."
				];
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = fn_formSubmit;
		rsv.validate(document.forms['newPO'],rules); 
	});
	
	$('#delete-btn').click(function(){
		$( "#PRDDeleteReason-div" ).dialog({
			resizable: false,
			height:300,
			width:490,
			modal: true,
			buttons: {
				"Deleted": function() {
					if($('#DeletedReason').val().length == 0){
						msgbox('Error Message','<p class="error"> Please entry Reason for delete</p>','DeletedReason');	
					}else{
						fn_callDeletePOData($('#PONumber').val());
						$(this).dialog("destroy"); 
					}
				},
				Close: function() {
					$(this).dialog("destroy");  
				}
			}
		});
		
	});
	
	
	fn_callDeletePOData = function(ponumber){
		var confirmpo = ["fn_ConfirmDeletePOData",ponumber];
		msgbox('Confirm Deletion','<p class="warning">Are you sure you want to delete?</p>',0,confirmpo);
	}
	
	fn_ConfirmDeletePOData = function(ponumber){
		ProcessRequest('purchase/PurchaseOrder/deletePOData',{'PONumber':ponumber,'DeletedReason':$('#DeletedReason').val()},'fn_deletePOCallback');
	}
	
	fn_deletePOCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		if(data[0] == 'success'){
			$('#new-btn').click();
		}
	}
	
	// Form Submission (add/update)
	function fn_formSubmit(theForm){
		var formValues = $(theForm).serializeArray();
			formValues.push({ name: "PONumber", value: $('#PONumber').val() });
		var action = 'updatePO';
		ProcessRequest('purchase/PurchaseOrder/'+action,formValues,'fn_submitCallback');
	}
	
	fn_submitCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
		if(data[0]=='success'){
			loadbtnAction(fn_btnList(false,false,false,false,true));
			//fn_PRDatabind();
			tbl_PR.fnDraw(); 
		}
	}
	
	
	
	fn_todoPR = function(action,prnumber){
		switch(action){
			case 'PODetail' : 
					$('#PRNumber').val(prnumber);
					$('#PONumber').val($('#PRNumber').val());
					fn_PODetail(prnumber);
				break;	
			case 'detail' : 
				var nTr = $(prnumber).parents('tr')[0];
				if ( tbl_PR.fnIsOpen(nTr) )
				{
					/* This row is already open - close it */
					//this.src = "assets/images/details_open.png";
					$(prnumber).removeClass('details-close').addClass('details-open');
					tbl_PR.fnClose( nTr );
					$(nTr).children('td').toggleClass('tropen');
					$(nTr).children('td:eq(0)').toggleClass('tropen-bg');
				}
				else
				{
					/* Open this row */
					//this.src = "assets/images/details_close.png";
					$(prnumber).removeClass('details-open').addClass('details-close');
					ProcessRequest('purchase/PurchaseOrder/getPRDetail',
							{'PRNumber':tbl_PR.fnGetData( nTr )[1]},
							function(data){ 
								tbl_PR.fnOpen( nTr,  fnFormatDetails ( data ), 'table-row-details' ); 
								$(nTr).children('td').toggleClass('tropen');
								$(nTr).children('td:eq(0)').toggleClass('tropen-bg');
							})
					
				}
				//fn_PRDatabind(); 
			break;	
			case 'PrintPo' : 
				console.log('print');
				fn_PrintPO(prnumber);
				break;			
		}
	}
	
	
	function fnFormatDetails ( data )
	{
		return data; 
	}

	
	fn_PODetail=function(prnumber){
		ProcessRequest('purchase/PurchaseOrder/createPO',{'PRNumber':prnumber},'fn_CreatePO');
	}
	
	fn_CreatePO = function(data){
		$('.newentry').slideToggle(function(){$('#tbl_pr-div').slideToggle();});
		fn_PRDetailDatabind();
	}

	fn_deletePRCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		if(data[0] == 'success'){
			fn_cleanPRDetailfrm();
			fn_cleanPRfrm();
			fn_formStatus(false);
			//fn_btnEnable(false);
			$('#PRType').change();
		}
	}
	
	function fn_btnList(_new,_save,_complete,_delete,_printpo){
		btn_list = {'new-btn':_new,
				'save-btn':_save,
				'complete-btn':_complete,
				'delete-btn':_delete,
				'printpo-btn':_printpo};
		return(btn_list);
	}
	
	//fn_PRDatabind();
	
	ProcessRequest('services/DropDownLists/getComboBoxdata',
					{'Vendor':'Vendor','PaymentTerm':'PaymentTerm','DeliveryTerm':'DeliveryTerm','Addresses':'Addresses'},'fn_GetComboBoxdata');
	
	$( ".datetime" ).datepicker({ 
		//minDate: -0, maxDate: "+2M",
		dateFormat: "mm-dd-yy"
	});
	
	$(".int").keydown(function(event) {
				// Allow only backspace, delete, enter, tab
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 ) {

		}else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
	
	
	// fn_viewPRData  = function(data){
		// tbl_PR.fnClearTable(); 
		// if(data){
			// var obj = jQuery.parseJSON(data);
			// var no = 0;
			// var prnumber;
			// var newArray = [];
			// var action = '';
			// var status ='';
			// for (var i=0; i<obj.length; i++){
				// prnumber = obj[i]['PRNumber'];
				// status='';
				// action = '';
				// if(obj[i]['PRStatus'] =='1'){
					// if(obj[i]['POStatus'] =='1'){
						// action = '<a class="detail-btn" href="#" onclick="fn_todoPR(\'PODetail\',\''+prnumber+'\');" >PO Detail</a>'
						// action += '&nbsp;&nbsp;&nbsp;<a class="print-btn" href="#" onclick="fn_todoPR(\'PrintPo\',\''+prnumber+'\');" >Print PO</a>';
						// status = '<font style="color:green">Completed</font>';
					// }else if(obj[i]['POStatus'] =='0'){
						// action = '<a class="create-btn" href="#" onclick="fn_todoPR(\'PODetail\',\''+prnumber+'\');" >Create PO</a>'
						// status ='<font style="color:#cecece">Pending</font>';
					// }
					// if(obj[i]['PODeleted'] =='1'){
						// status = '<font style="color:red">Deleted</font>'
					// }
				// }
				// // else{
					// // action = '<a class="PODetail-btn" href="#" onclick="fn_todoPR(\'PODetail\',\''+prnumber+'\');" >Create PO</a>';
				// // }
				
				
				// no = no + 1;
				// newArray.push([no,
					// prnumber,
					// obj[i]['PRType'],
					// obj[i]['Department'],
					// obj[i]['DateRequired'],
					// obj[i]['RequestedBy'],
					// obj[i]['PurchasedBy'],
					// obj[i]['ApprovedBy'],
					// status,
					// action
				// ]);
			// }
			// tbl_PR.fnAddData(newArray); 
			// tbl_PR.fnAdjustColumnSizing();
		// }
	// }
});
</script>
