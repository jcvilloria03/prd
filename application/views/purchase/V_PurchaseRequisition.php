<h2 class="title">Purchase &raquo; Purchase Requisition</h2>
<div class="top-button">
	<button id="add-new"><span class="ui-icon ui-icon-plusthick"></span>Add New PR</button>
	<button id="back-to-list" style="display:none"><span class="ui-icon ui-icon-arrowthick-1-w"></span>Back to List</button>
</div>
<div class="clear"></div>
<div class="list">
	<table class="display" id="tbl_PR" >
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table>  
</div>
<div class="newentry"  style="display:none">
	<h3 class="form-title">Add New Requisition</h3>
	<form class="globalform" id="newPR-frm" name="newPR">	
		<em style="font-size: 13px; color:#888"><span class="req">*</span> Required field</em>
		<ol>
			<div class="lfpane" >
				<li>
					<label>PR Type</label>
					<select name="PRType" id="PRType">
					</select>
				</li>
				<li>
					<label>PR NO</label>
					<input type="text" id="PRNumber" name="PRNumber" value="" class="readonly"/>
					<input type="hidden" id="PRNumber-hid" name="PRNumber-hid" value=""/>
					<input type="hidden" id="post-hid" name="post-hid" value="<?php echo($PRNumber);?>"/>
					<input type="hidden" id="hid-deptname" name="hid-deptname" value="<?php echo($dep_name);?>"/>
					<input type="hidden" id="hid-internalrequest" name="post-hid" />
					<input type="hidden" id="hid-ismanager" name="hid-ismanager" value="<?php echo($is_manager);?>"/>
					<input type="hidden" id="hid-isbm" name="hid-isbm" value="<?php echo($is_BM);?>"/>
					<input type="hidden" id="hid-isgm" name="hid-isgm" value="<?php echo($is_GM);?>"/>
					<input type="hidden" id="hid-isPRDManager" name="hid-isPRDManager" value="<?php echo($is_PRDManager);?>"/>
					<input type="hidden" id="hid-username" name="hid-username" value="<?php echo($username);?>"/>
					<input type="hidden" id="hid-PRDSubmitted" name="hid-PRDSubmitted" />
					<span class="req">*</span>	
				</li>
				<li>
					<label>Department</label>
					<select name="Department" id="Department">
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="req">*</span>	
				</li>
				<li>
					<label>Supplier/Vendor</label>
					<select name="VendorID" id="VendorID">
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="req">*</span>	
				</li>
				<!--<li>
					<label>ASAP</label>
					<input id="ASAP" type="checkbox" value="" name="ASAP">
				</li>-->
				<li>
					<label>Date Required</label>
					<input type="text" id="DateRequired" name="DateRequired" value="" />
					<span class="req">*</span>	
				</li>				
				<li>
					<label>Special Instruction</label>
					<textarea name="Comment" ID="Comment" style="width:200px"></textarea>
				</li>
				<li>
					<label>GST</label>
					<Select id="GST" name="GST">
						<Option Value="1"> Yes </option>
						<Option Value="0"> No </option>
					</Select>
					<span class="req">*</span>	
				</li>
				<li>
					<label>Required GM Approved</label>
					<Select id="RequiredGMApproved" name="RequiredGMApproved">
						<Option Value="1"> Yes </option>
						<Option Value="0" selected> No </option> 
					</Select>
					<span class="req">*</span>	
				</li>
			</div>
			<div class="rfpane" >				
				<li style="width:100%!important">
					<div id="PRStatus-span" ></div>
					<input type="button" id="unlockpr-btn" value="Unlock PR" />
					<input type="hidden" id="hidstatusPR" />
					
				</li>
				<li>
					<label>Requested By</label>
					<select name="RequestedBy" id="RequestedBy">
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="req">*</span>	
				</li>
				<li>
					<label>Purchased By</label>
					<select name="PurchasedBy" id="PurchasedBy">
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="req">*</span>	
				</li> 
				<li>
					<label>Approved By</label>
					<select name="ApprovedBy" id="ApprovedBy">
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="req">*</span>	
				</li>
				<li>
					<label>Discount</label>
					<input type="text" id="Discount" name="Discount" value="" />
				</li>
				<li>
					<label>Remarks</label>
					<textarea id="Remarks" name="Remarks" style="width:350px; height: 100px; "maxlength ="5000"></textarea>
				</li>
				<li>
					<!--<input type="button" id="save-btn" name="save-btn" value="Save" data=""/>
					<input type="button" id="addpritems-btn" name="addpritems-btn" value="Add PR Items" />
					<input type="button" id="delete-btn" value="Delete" />
					<input type="button" id="submit-btn" name="submit-btn" value="Submit PR" />-->
					<button id="save-btn" name="save-btn" data="" ><span class="ui-icon ui-icon-disk"></span>Save</button>
					<button  id="addpritems-btn"  name="addpritems-btn" data="" ><span class="ui-icon ui-icon-circle-plus"></span>Add PR Items</button>
					<button  id="delete-btn" name="delete-btn" data="" ><span class="ui-icon ui-icon-circle-close"></span>Delete</button>
					<button id="submit-btn" name="submit-btn" data="" ><span class="ui-icon ui-icon-circle-check"></span>Submit PR</button>

				</li>
			</div>
			<div style="width: 100%;">
				<li style="float: right; margin-bottom: 15px; margin-right: 110px;">
					<!--<input type="button" id="viewlist-btn" value="View List" style="display:none"/>
					<input type="button" id="seek-btn" value="Seek" />
					<input type="button" id="new-btn" value="New" data="" />
					<input type="button" id="printpr-btn" value="Print PR" />
					<input type="button" id="printprwithoutprice-btn" value="Print PR Without Price" />
					<input type="button" id="completepr-btn" name="completepr-btn" value="Complete PR" style="display:none" />
					<input type="button" id="Approved-btn" value="Approved" />
					<input type="button" id="Reject-btn" value="Reject" />-->
					
					<button id="viewlist-btn" name="viewlist-btn" data="" style="display:none"><span class="ui-icon ui-icon-disk"></span>View List</button>
					<button id="seek-btn" name="seek-btn" data="" ><span class="ui-icon ui-icon-search"></span>Seek</button>
					<button id="new-btn" name="new-btn" data="" ><span class="ui-icon ui-icon-plusthick"></span>New</button>
					<button id="printpr-btn" name="printpr-btn" data="" ><span class="ui-icon ui-icon-print"></span>Print PR</button>
					<button id="printprwithoutprice-btn" name="printprwithoutprice-btn" data="" ><span class="ui-icon ui-icon-print"></span>Print PR Without Price</button>
					<button id="completepr-btn" name="completepr-btn" data=""  style="display:none" ><span class="uiui-icon ui-icon-circle-check"></span>Complete PR</button>
					<button id="Approved-btn" name="Approved-btn" data="" ><span class="ui-icon ui-icon-check"></span>Approved</button>
					<button id="Reject-btn" name="Reject-btn" data="" ><span class="ui-icon ui-icon-closethick"></span>Reject</button>
					
				</li>
			</div>			
						
		</ol>
		<div class="clear"></div>		
	</form>	
	<div class="clear"></div>
	
</div>
<div  id="tbl_PRDetail-div" > 
	<table class="display" id="tbl_PRDetail">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>

<div id="prdetail-div" name="prdetail-div" title="PR Detail" style="display: none;">
	<form class="globalform" id="newPRDetail-frm" name="newPRDetail" style="width:450px;">
		<ol>
			<li id="department-detail-li">
				<label>Department</label>
				<input type="text" name="Department-Detail" id="Department-Detail" value="" style="width: 270px;"/>
				<input type="hidden" id="id-hid" />
				<span class="req">*</span>	
			</li>
			<li id="customer-detail-li" style='display:none'>
				<label>Customer</label>
				<select name="CustomerID" id="CustomerID">
				</select>
				
			</li>
			<li>
				<label>Part Number</label>
				<input type="text" id="PartNumber" name="PartNumber" value="" style="display: none;"/>
				<hidden type="text" id="PartNumber-hid" name="PartNumber-hid" value="" />
				<select name="PartNumber-cbx" id="PartNumber-cbx">
				</select>
			</li>
			<li id="partdesc-li">
				<label>Part Desc</label>
				<input type="text" id="PartDesc1" name="PartDesc1" value=""/>
				<span class="req">*</span>	 
			</li>
			<li>
				<label>Quantity</label>
				<input type="text" id="RequestedQty" name="RequestedQty" value="" class="int"/>
				<span class="req">*</span>	
			</li>
			<li>
				<label>UOM</label>
				<select id="UOM" name="UOM" ></select>
				&nbsp;&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;&nbsp;
				<span class="req">*</span>	
			</li>
			<li>
				<label>Unit Price</label>
				<input type="text" id="UnitPrice" name="UnitPrice" value="" />
				<span class="req">*</span>	
			</li>
			<li>
				<label>Currency</label>
				<input type="text" id="Currency" name="Currency" value="" maxlength="3"/>
				<span class="req">*</span>	
			</li>
		</ol>
		<div class="clear"></div>		
	</form>
</div>

<!--<div id="prdiv" name="prdiv"  title="Purchase Requisition List"  style="display: none;"> 
	<table class="display" id="tbl_PR" >
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>-->

<div id="internalRequestdiv" name="internalRequestdiv" style="display: none;">
	<table class="display" id="tbl_internalRequest">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table>
</div>

<div id="PRRejectReason-div" name="PRRejectReason-div" title="Reject Reason" style="display: none;">
	<form class="globalform" id="PRRejectReason-frm" name="PRRejectReason" style="width:400px;">
		Reason
		<textarea id="ApprovedReason" name="ApprovedReason" style="width:370px"></textarea>
	</form>
</div>
<div id="PRDDeleteReason-div" name="PRDDeleteReason-div" title="Deleted Reason" style="display: none;">
	<form class="globalform" id="PRDDeletedReason-frm" name="PRDDeletedReason" style="width:400px;">
		Reason
		<textarea id="DeletedReason" name="DeletedReason" style="width:370px"></textarea>
	</form>
</div>
<script type="text/javascript">
var tbl_PRDetail,tbl_PR,fn_checkPartNumber,tbl_internalRequest,fn_todoPRDetail,fn_SubmitPRData,fn_RejectPRData;  
$(document).ready(function() {
	//$('#PRNumber').attr('readonly',true);
	$("input:submit, input:button, button, .button").button().click(function(event){
		event.preventDefault();
	});
	/* Create Table For PR */
	var PR_fields = { 'ID':'ID',
					  'PRNumber':'PR No',
					  'PRType':'PRType',
					  'Department':'Department',
					  'DateRequired':'DateRequired',
					  'RequestedBy':'RequestedByName',
					  'ApprovedBy':'ApprovedBy',
					  'PRDStatus':'Status'}; 
	
	$.each(PR_fields,function(i,e){
		$('#tbl_PR thead tr').append('<th>'+e+'</th>');
		$('#tbl_PR tfoot tr').append('<th>'+e+'</th>');
	});
	$('#tbl_PR thead tr').append('<th>Action</th>');
	$('#tbl_PR tfoot tr').append('<th>Action</th>');	
	$('#tbl_PR,.display').css("white-space", "nowrap");
	tbl_PR = $('#tbl_PR').dataTable({
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50], [10, 25, 50]],
		sPaginationType: 'full_numbers',
		bDestroy: true,
		bProcessing: true,
		bServerSide: true, 
		aaSorting: [[0,'desc']],
		aoColumnDefs : [  {"aTargets" : [ 7 ],
					 "bSearchable": false
					},{"aTargets" : [ 8 ],
					 "bSearchable": false,
					 "bSortable" : false
					}],
		sAjaxSource: "purchase/PurchaseRequisition/viewPRDataAllData", 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(PR_fields)) } );
			aoData.push( { "name": "PRStatus", "value": $('#PRStatus').val() } );  
			aoData.push( { "name": "PRType", "value": $("#radiotype input:radio:checked").val() } ); 
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					//showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
  $('#tbl_PR tbody td img').die().live( 'click', function () { 
        var nTr = $(this).parents('tr')[0];
        if ( tbl_PR.fnIsOpen(nTr) )
        {
            /* This row is already open - close it */
            this.src = "assets/images/details_open.png";
            tbl_PR.fnClose( nTr );
			$(nTr).children('td').toggleClass('tropen');
			$(nTr).children('td:eq(0)').toggleClass('tropen-bg');
        }
        else
        {
            /* Open this row */
            this.src = "assets/images/details_close.png";
			ProcessRequest('purchase/PurchaseRequisition/getPRApprovalData',
					{'PRNumber':tbl_PR.fnGetData( nTr )[1]},
					function(data){ 
						tbl_PR.fnOpen( nTr,  fnFormatDetails ( data ), 'table-row-details' ); 
						$(nTr).children('td').toggleClass('tropen');
						$(nTr).children('td:eq(0)').toggleClass('tropen-bg');
					})
            
        }
    } );
	
	function fnFormatDetails ( data )
	{
		//console.log(aData[1]); 
		//$.ajaxSetup({async:false});
		//console.log(sOut);
		return data; 
	}

	// //set DataTable Header for tbl_PR
	//setTableHeader('tbl_PR','Purchase Requisition List'); 
	setTableHeaderHTML('tbl_PR',' <div id="radiotype" style="width:300px;float:left;margin-left: 30px;"> Type : '+
						' <input type="radio" id="All" name="radiotype" checked="checked" value=""/><label for="All">All</label> '+
						' <input type="radio" id="NORMAL" name="radiotype" value="NORMAL" /><label for="NORMAL">Normal</label> '+
						' <input type="radio" id="JSIEXPENSE" name="radiotype" value="JSIEXPENSE"/><label for="JSIEXPENSE">JSI Expense</label>'+
						' <input type="radio" id="JSICOST" name="radiotype" value="JSICOST"/><label for="JSICOST">JSI Cost</label> </div>'+
						'<div  style="margin-left: 10px;width:300px;float:left"> Status :  <select name="PRStatus" id="PRStatus"> <option value="">All</option> '+
						' <option value="12">Draft</option>'+
						'<option value="11">Requested to PRD Manager</option><option value="9">Approved By PRD Manager</option>'+
						'<option value="10">Rejected by PRD Manager</option><option value="7">Approved By Dept Manager</option>'+
						'<option value="8">Rejected By Dept Manager</option>'+
						'<option value="5">Approved By Branch Manager</option><option value="6">Rejected By Branch Manager</option>'+
						'<option value="1">Completed PR</option><option value="2">Deleted</option></select></div>'+
						'<input type="button" id="MultiApprove-btn" value="Approve Selected" />');   
	
	$( "#radiotype" ).buttonset();
	 
	$('#PRStatus').change(function(){
		tbl_PR.fnDraw(); 
	});
	
	if($('#hid-isPRDManager').val() ==1){
		$('#PRStatus').val('11');
		tbl_PR.fnDraw(); 
	}else if($('#hid-isbm').val() ==1){
		$('#PRStatus').val('7');
		tbl_PR.fnDraw(); 
	}else if($('#hid-isgm').val() ==1){
		$('#PRStatus').val('5');
		tbl_PR.fnDraw(); 
	}else if($('#hid-ismanager').val() ==1){
		$('#PRStatus').val('9');
		tbl_PR.fnDraw(); 
	}
	
	$('#MultiApprove-btn').click(function(){
		var data = tbl_PR.fnGetData();
		var valid = false;
		var count = 0;
		$.each(data,function(i,e){
			if($(e[0]).is("input")){
				//console.log($('#chkID_'+$(e[0]).val()).is(":checked"));
				if($('#chkID_'+$(e[0]).val()).is(":checked")){
					count++;
					valid = true;
				}
			}
		});
		if(valid){		
		 	var confirmIR = ["fn_ConfirmApproveMultiData",count];
			msgbox('Confirm','<p class="warning">Are you sure you want to Approve?</p>',0,confirmIR);		
		}else{
			msgbox("Error",'<p class="warning">Please Select Checkbox first before you click Approve.</p>','addpritems-btn');		
		}
	});
	
	
	fn_ConfirmApproveMultiData = function(pcount){
		var _count = 0;
		var formValues;
		var data = tbl_PR.fnGetData();
		var valid = false;
		var type ='';
		if($('#hid-isPRDManager').val()== 1){
			type='PRDManager';
		}else if($('#hid-isbm').val() == 1){
			type='BM';
		}else if($('#hid-ismanager').val() == 1){
			type='Manager';
		}else if($('#hid-isgm').val() == 1){
			type='GM';
		}else{
			type ='';
		}
		
		$.each(data,function(i,e){
			if($(e[0]).is("input")){
				//console.log($('#chkID_'+$(e[0]).val()).is(":checked"));
				formValues =  '';
				if($('#chkID_'+$(e[0]).val()).is(":checked")){
					_count++;
					 formValues =  {"PRNumber": e[1],"Type":type,"Approved":1,'ApprovedReason': '' };
	
					ProcessRequest('purchase/PurchaseRequisition/submitPRData',formValues,
						function(data){ 
							if(_count==pcount){
								var data = explode("|",data);
								msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
								if(data[0]=='success'){
									tbl_PR.fnDraw();
								}
							}
						});
				}
			}
		});
	}
	
	
	 $("#radiotype input:radio").change(function(){
		//console.log( $("#radiotype input:radio:checked").val());
		tbl_PR.fnDraw();
		
		// Do something interesting here
	});

	
	/* Create Table For PRDetail */
	var InternalRequest_fields = {'ID':'ID',
						 'PartNumber':'PartNumber',
						  'PartDesc1':'PartDesc1',
						  'RequestedQty':'RequestedQty',
						  'UOM':'UOM',
						  'Status':'Status'
						  };
	
	$.each(InternalRequest_fields,function(i,e){
		$('#tbl_internalRequest thead tr').append('<th>'+e+'</th>');
		// if(i=='Select'){
			// $('#tbl_internalRequest tfoot tr').append('<th>&nbsp;</th>');
		// }else{
			// $('#tbl_internalRequest tfoot tr').append('<th>'+e+'</th>');
		// }
	});
	
	tbl_internalRequest = $('#tbl_internalRequest').dataTable( { 
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aaSorting: [[0,'desc']],
		sAjaxSource: "purchase/PurchaseRequisition/viewInternalRequest", 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(InternalRequest_fields)) } );
			aoData.push( { "name": "PRNumber", "value": $('#PRNumber').val() } );  
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					//showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	//Set Title for PRDetail
	setTableHeader('tbl_internalRequest','Internal Request Item List');

	fn_todoPR = function(action,prnumber){
		switch(action){
			case 'edit' : 
					fn_searchPRData(prnumber);
					//$( "#prdiv" ).dialog( "destroy" );	
				break;	
			case 'delete' : 
					fn_callDeletePRData(prnumber);
					tbl_PR.fnDraw();
					//fn_PRDatabind(); 
				break;
			case 'detail' : 
					var nTr = $(prnumber).parents('tr')[0];
					if ( tbl_PR.fnIsOpen(nTr) )
					{
						/* This row is already open - close it */
						//this.src = "assets/images/details_open.png";
						$(prnumber).removeClass('details-close').addClass('details-open');
						tbl_PR.fnClose( nTr );
						$(nTr).children('td').toggleClass('tropen');
						$(nTr).children('td:eq(0)').toggleClass('tropen-bg');
					}
					else
					{
						/* Open this row */
						//this.src = "assets/images/details_close.png";
						$(prnumber).removeClass('details-open').addClass('details-close');
						ProcessRequest('purchase/PurchaseRequisition/getPRApprovalData',
								{'PRNumber':tbl_PR.fnGetData( nTr )[1]},
								function(data){ 
									tbl_PR.fnOpen( nTr,  fnFormatDetails ( data ), 'table-row-details' ); 
									$(nTr).children('td').toggleClass('tropen');
									$(nTr).children('td:eq(0)').toggleClass('tropen-bg');
								})
						
					}
					//fn_PRDatabind(); 
				break;	
			case 'PrintPR' : 
					fn_PrintPR(prnumber);
				break;
			case 'PrintPRWithoutIMEI' : 
					fn_PrintPrWithoutPrice(prnumber);
				break;
			case 'print' : 
					fn_PrintPR(prnumber);
				break;
		}
	}
	
	/* End create table for PR */
	
	/* Create Table For PRDetail */
	var PRDetail_fields = {'ID':'No',
						  'Department':'Department',
						  'PartNumber':'Part Number ',
						  'PartDesc1':'PartDesc1',
						  'RequestedQty':'Request Qty',
						  'UnitPrice':'Unit Price',
						  'UOM':'UOM',
						  'RequestedByName':'Request By'};
	
	$.each(PRDetail_fields,function(i,e){
		$('#tbl_PRDetail thead tr').append('<th>'+e+'</th>');
		$('#tbl_PRDetail tfoot tr').append('<th>'+e+'</th>');
	}); 
	$('#tbl_PRDetail thead tr').append('<th>Action</th>');
	$('#tbl_PRDetail tfoot tr').append('<th>Action</th>');	
	tbl_PRDetail = $('#tbl_PRDetail').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aoColumnDefs: [ { "bSortable": false, "aTargets": [ 8 ], "bSearchable": false } ],
		sAjaxSource: "purchase/PurchaseRequisition/viewPRDetailData",  
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(PRDetail_fields)) } ); 
			aoData.push( { "name": "PRNumber", "value": $('#PRNumber').val() } );
			aoData.push( { "name": "PRDStatus", "value": $('#hidstatusPR').val() } );
			
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					//showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	//Set Title for PRDetail
	setTableHeader('tbl_PRDetail','Purchase Item List');
	
	fn_todoPRDetail = function(action,id){
		var prdnumber = $('#PRNumber').val();
		switch(action){
			case 'edit' : 
					prdetailDiaglog("Update");
					ProcessRequest('purchase/PurchaseRequisition/getPRDetailData',{'ID':id},'fn_getPRDetailCallback');
				break;	
			case 'delete' : 
				  var confirmprdetail  = ["deletePRDetailData",id];
				  msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirmprdetail);				
				break;
		}
	}
	
	
	fn_getPRDetailCallback = function(data){
		if(data.length <= 1){
			var data = explode("|",data);
			msgbox("error",'<p class="error">No record found</p>','PRNumber');		
		}else{
			var data = json_decode(data);
			//Add Currency
			$('#newPRDetail-frm #Currency').removeClass('readonly').removeAttr('readonly');
			
			$('#newPRDetail-frm #UnitPrice').removeClass('readonly').removeAttr('readonly');
			$('#newPRDetail-frm #UOM').combobox('enable');
			$.each(data,function(i,e){
				$("#newPRDetail-frm #"+i).val(e);
			});
			$('#newPRDetail-frm #UOM').combobox('selected',data.UOM);
			$('#newPRDetail-frm #Department-Detail').val(data.Department);
			$('#newPRDetail-frm #id-hid').val(data.ID);
			$('#newPRDetail-frm #PartNumber-hid').val(data.PartNumber);
			fn_CheckPRType();
			fn_PartNumberDataBind();
			if($('#newPR-frm #PRType').val() == 'NORMAL'){
				$('#newPRDetail-frm #CustomerID').combobox('selected',data.CustomerID);
				//$('#newPRDetail-frm #PartNumber-cbx').combobox('selected',data.PartNumber);
			}else{ 
				if($('#hid-internalrequest').val() =='1'){
					//$('#newPRDetail-frm #Department-Detail').addClass('readonly').attr('readonly',true); 
					if($('#PRType').val() =='JSICOST'){
					
					}else{
						$('#newPRDetail-frm #RequestedQty').addClass('readonly').attr('readonly',true);
					}
					 
				}else{
					$('#newPRDetail-frm #Department-Detail').removeClass('readonly').removeAttr('readonly');
					$('#newPRDetail-frm #RequestedQty').removeClass('readonly').removeAttr('readonly');
				}
			}
			
		}
	}
	
	deletePRDetailData = function(id){
		ProcessRequest('purchase/PurchaseRequisition/deletePRDetailData',{'ID':id},'deletePRDetailCallback');
	}
	
	deletePRDetailCallback = function(data){
		var data = explode("|",data);
		if(data[0] == 'success'){
			//fn_PRDetailDatabind();
			tbl_PRDetail.fnDraw();
		}
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	 
	}
	/* End Create Table for PRDetail */
	
	
	/* Create Combobox for Department, PartNumber, Vendor */
	fn_ComboBoxload = function(data){
		var obj = jQuery.parseJSON(data);
		var dept = [];
		dept = obj.Department;
		loadComboBoxData("newPR-frm #Department",dept,true);
		dept.splice(dept[0],1);
		//loadComboBoxData("newPRDetail-frm #PartNumber-cbx",obj.Part,false);
		
		loadComboBoxData("newPR-frm #VendorID",obj.Vendor,true);
		loadComboBoxData("newPRDetail-frm #UOM",obj.UOM,true);
		
		loadComboBoxData("newPR-frm #RequestedBy",obj.RequestedBy,true);
		loadComboBoxData("newPR-frm #PurchasedBy",obj.PurchasedBy,true);
		loadComboBoxData("newPR-frm #ApprovedBy",obj.ApprovedBy,true);
		
		loadComboBoxData("newPRDetail-frm #CustomerID",obj.Customer,true);
	
		$('#newPRDetail-frm #CustomerID').combobox({		
			selected: function(event, ui) {
				
				fn_PartNumberDataBind();
			}
		});
		
		if (obj.PRType.length){
			$.each(obj.PRType, function(index,item) {
				$('#PRType')
				 .append($("<option ></option>")
				 .attr("value",item.ID)
				 .text(item.Value));
			});
		}
		// $('#newPRDetail-frm #PartNumber-cbx').combobox({				
			// selected: function(event, ui) {
				// ProcessRequest('purchase/PurchaseRequisition/getPartsPrice',{PartNumber:$('#PartNumber-cbx').val()},'fn_GetPartPrices');
			// }
		// });
			
		$('#Department-combox').focus();
		if($('#post-hid').val().length > 0){
			fn_searchPRData($('#post-hid').val());
		}

	}
	/* End ComboBox */
	
	$('#back-to-list').click(function(){
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
		$('#tbl_PRDetail-div').hide();
		//fn_PRDatabind();
		tbl_PR.fnDraw();
	});
	
	if($('#hid-deptname').val() != 'PRD' && $('#hid-isPRDManager').val() != '1'){
		$( "#add-new" ).button({ disabled: true }); 
	}else{
	}
	$('#add-new').click(function(){
		$(this).fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#PRType').focus().select();});});
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Purchase Requisition');});
		$('#tbl_PRDetail-div').show();
		fn_NewPR();
	});
	
	function prdetailDiaglog(pButtonLabel){
	/* Create Dialog for PRDetail */
		$( "#prdetail-div" ).dialog({
			autoOpen: true,
			resizable: false,
			height:500,
			width:490,
			modal: true,
			closeOnEscape: true,
			buttons: [{
				text: pButtonLabel,
				click: function() {
					var rules =	[
						"function,fn_checkPartNumber",
						"function,fn_department",
						"required,RequestedQty,Please enter Quantity.",
						"digits_only,RequestedQty,Please enter valid Quantity.",
						"required,UnitPrice,Please enter Unit Price.",
						"reg_exp,UnitPrice,^\\d*(\\.\\d{1\\,})?$,Please enter valid Unit Price.",
						"required,Currency,Please enter Currency.",
						"required,UOM,Please enter UOM."
						];
					
					rsv.customErrorHandler = formError;	
					rsv.onCompleteHandler = fn_formSubmitPRDetail;
					rsv.displayType= "display-html";
					rsv.validate(document.forms['newPRDetail'],rules); }
				},{
				text: "Close",
				click: function() {
					//fn_PRDetailDatabind();
					tbl_PRDetail.fnDraw();
					$( this ).dialog( "destroy" );

				}
			}],
			close: function(){
				tbl_PRDetail.fnDraw();
				$(this).dialog("destroy");  
			}
		});
	
	}
	function fn_formSubmitPRDetail(theForm){
		var formValues = $(theForm).serializeArray();
			formValues.push({ name: "PRNumber", value: $('#PRNumber').val() });
			formValues.push({ name: "PRType", value: $('#PRType').val() });
			formValues.push({ name: "PartNumber-hid", value:$('#PartNumber-hid').val() });
			formValues.push({ name: "ID", value:$('#id-hid').val() });
		var action = $('#id-hid').val() == '' ? 'savePRDetail' : 'updatePRDetail';
		ProcessRequest('purchase/PurchaseRequisition/'+action,formValues,'fn_submitPRDetailCallback');
	}

	
   fn_searchPRData = function(prnumber){
		if($('#add-new').is(":visible")){
			$('#add-new').fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('#PRNumber').focus();});});
			$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Purchase Requisition');});
		} 
		
		ProcessRequest('purchase/PurchaseRequisition/GetPRData',{PRNumber:prnumber},'fn_getPRCallback');
   }
    
	fn_getPRCallback = function(data){
		if(data.length <= 1){
			var data = explode("|",data);
			msgbox("error",'<p class="error">No record found</p>','PRNumber');		
		}else{
			var data = json_decode(data);
			$.each(data.PR,function(i,e){
				if(i =='Department'){
					$('#Department').combobox('selected',e);
				}else{
					$("#newPR-frm #"+i).val(e);
				}				
			});
			$('#VendorID').combobox('selected',data.PR.VendorID);
			
			$('#RequestedBy').combobox('selected',data.PR.RequestedBy);
			$('#PurchasedBy').combobox('selected',data.PR.PurchasedBy);
			$('#ApprovedBy').combobox('selected',data.PR.ApprovedBy);
			
			//$('#save-btn').val('Save Changes').attr('data',data.PR.PRNumber);
			$('#save-btn').attr('data',data.PR.PRNumber);
			$('#PRNumber').addClass('readonly').attr('readonly',true);
			$('#PRNumber-hid').val($('#PRNumber').val());
			
			fn_formStatus(false);
			$('#unlockpr-btn').hide();
			
			// if(data.DateRequired == 'ASAP'){
				// $('#DateRequired').removeClass().attr('class', 'readonly');
				// $('#ASAP').attr('checked',true);
			// }else{
				// $('#ASAP').attr('checked',false);
				// $( "#DateRequired" ).datepicker();
				// $('#DateRequired').val('').removeClass().attr('class', 'datetime hasDatepicker');
			// }
			$('#PRStatus-span').text(''); 
			
			if(data.PR.Deleted == '1'){
				$('#PRStatus-span').append('<span class="status red">Deleted</span>');
				$('#PRStatus-span').append('<span class="status red">Reason : '+ data.PR.DeletedReason+'</span>');
				$('#hidstatusPR').val('Deleted');
				fn_disiableAll(true);
				$( "#save-btn").button({ disabled: true });
				$( "#addpritems-btn").button({ disabled: true });
				$( "#delete-btn").button({ disabled: true });
				$( "#submit-btn").button({ disabled: true });
			}else{
				 if(data.PR.Status == '1'){
					$('#hidstatusPR').val('Completed');
				}else if(data.PR.GMApproved =='1'){
					$('#hidstatusPR').val('Completed');
				}else if(data.PR.BMApproved =='1'){
					$('#hidstatusPR').val('Completed');
				}else if(data.PR.ManagerApproved =='1'){
					 $('#hidstatusPR').val('Completed');
				}else if(data.PR.PRDApproved =='1'){
					$('#hidstatusPR').val('Completed'); 
				}else if(data.PR.PRDSubmitted =='1'){
					 $('#hidstatusPR').val('Completed');
				}else{
					 $('#hidstatusPR').val('');
				}
				
				if(data.PR.PRDSubmitted =='1'){
					 fn_disiableAll(true);
					 $('#PRStatus-span').append('<span class="status orange">Submitted to PRD Manager By PRD User</span>');
					$( "#save-btn").button({ disabled: true });
					$( "#addpritems-btn").button({ disabled: true });
					$( "#delete-btn").button({ disabled: true });
					$( "#submit-btn").button({ disabled: true });
				}else{
					$('#PRStatus-span').append('<span class="status grey" >Draft</font>');
					$( "#save-btn").button({ disabled: true });
					if($('#hid-deptname').val() =='PRD' || $('#hid-isPRDManager').val()== 1 ){
						$( "#save-btn").button({ disabled: false });
						$( "#addpritems-btn").button({ disabled: false });
						$( "#delete-btn").button({ disabled: false });
						$( "#submit-btn").button({ disabled: false });
					}else{
						$( "#save-btn").button({ disabled: true });
					}
				}
				
				if(data.PR.PRDApproved =='1' || data.PR.PRDApproved == '2'){
					if(data.PR.PRDApproved =='1'){
						$( "#save-btn").button({ disabled: true });
						$( "#addpritems-btn").button({ disabled: true });
						$( "#delete-btn").button({ disabled: true });
						$( "#submit-btn").button({ disabled: true });
						$('#PRStatus-span').append('<span class="status orange"> Approved by PRD Manager ('+data.PR.PRDApprovedByName+')</span>');
					}if(data.PR.PRDApproved =='2'){
						$('#PRStatus-span').append('<span class="status orange"> Rejected by PRD Manager ('+data.PR.PRDApprovedByName+')</span>'); 
						$('#PRStatus-span').append('<span class="status orange"> Reason :'+ data.PR.PRDApprovedReason +'</span>');
					}
					if($('#hid-ismanager').val() == 1){
						$( "#printpr-btn").button({ disabled: false });
						if(data.PR.MultiManagerApproval == 1){
							$.each(data.PRDetail,function(i,e){
								if(e['ApprovedBy'] == $('#hid-username').val()){
									$( "#Approved-btn").button({ disabled: false });
									$( "#Reject-btn").button({ disabled: false });
								}
							});
						}else{
							$( "#Approved-btn").button({ disabled: false });
							$( "#Reject-btn").button({ disabled: false });
						}
					}
				}else{
					if($('#hid-isPRDManager').val()== 1 ){
						$( "#Approved-btn").button({ disabled: false });
						$( "#Reject-btn").button({ disabled: false });
					}else{
						
					}
				}
					
				if(data.PR.ManagerApproved =='1' || data.PR.ManagerApproved == '2'){
					$( "#printpr-btn").button({ disabled: false });
					if(data.PR.MultiManagerApproval == '1'){
						$.each(data.PRDetail,function(i,e){
							if(e['Approved'] == '1'){
								$('#PRStatus-span').append('<span class="status orange"> Approved By Dept. Manager ('+ e['ApprovedByName'] +')</span>');
							}else if(e['Approved'] == '2'){
								$('#PRStatus-span').append('<span class="status orange">  Rejected By Dept. Manager ('+ e['ApprovedByName'] +')</span>');
								$('#PRStatus-span').append('<span class="status orange">  Reason : '+ e['ApprovedReason'] +'</span>');
							}else { }
						});
						if(data.PR.ManagerApproved =='1' && $('#hid-isbm').val() == 1){
							$( "#Approved-btn").button({ disabled: false });
							$( "#Reject-btn").button({ disabled: false });
						}
					}else{
						if(data.PR.ManagerApproved =='1'){
							$('#PRStatus-span').append('<span class="status orange"> Approved By Dept. Manager('+data.PR.ManagerApprovedByByName+')</span>');
							if($('#hid-isbm').val() == 1){
								$( "#Approved-btn").button({ disabled: false });
								$( "#Reject-btn").button({ disabled: false });
							}	
						}else{
							$('#PRStatus-span').append('<span class="status red" > Rejected By Dept. Manager('+data.PR.ManagerApprovedByByName+')</span>');
							$('#PRStatus-span').append('<span class="status red" > Reason :'+ data.PR.ManagerApprovedReason +'</span>');  
							if($('#hid-ismanager').val() == 1){
								$( "#Approved-btn").button({ disabled: false });
								$( "#Reject-btn").button({ disabled: false });
							}else{
								$( "#Approved-btn").button({ disabled: true });
								$( "#Reject-btn").button({ disabled: true });
							}
						}
					}
				
					
				}else{}
				
				if(data.PR.BMApproved =='1'){
					$('#PRStatus-span').append('<span class="status orange"> Approved By BM ('+data.PR.BMApprovedByName+')</span>'); 
					if(($('#hid-isbm').val() == '1') || ($('#hid-isgm').val() == '1' && data.PR.RequiredGMApproved =='1')){
						$( "#Approved-btn").button({ disabled: false });
						$( "#Reject-btn").button({ disabled: false });
					}else{
						$( "#Approved-btn").button({ disabled: true });
						$( "#Reject-btn").button({ disabled: true });					
					}
				}else if(data.PR.BMApproved == '2'){
					$('#PRStatus-span').append('<span class="status red" >  Rejected By BM ('+data.PR.BMApprovedByName+') </span>'); 
					$('#PRStatus-span').append('<span class="status red" >  Reason :'+ data.PR.BMApprovedReason +'</span>');  
				}else{}
				
				$('#GST').val(data.PR.GST);
				
				$('#RequiredGMApproved').val(data.PR.RequiredGMApproved);
				if(data.PR.GMApproved =='1' || data.PR.GMApproved == '2'){
					if(data.PR.GMApproved =='1'){
						$('#PRStatus-span').append('<span class="status orange"> Approved By GM ('+data.PR.GMApprovedByName+')</span>'); 
					}else if(data.PR.GMApproved =='2'){
						$('#PRStatus-span').append('<span class="status red" >  Rejected By GM ('+data.PR.GMApprovedByName+')</span>'); 
						$('#PRStatus-span').append('<span class="status red" >  Reason :'+ data.PR.GMApprovedReason +'</span>');  
					}
					
					if($('#hid-isgm').val() == '1' && data.PR.RequiredGMApproved =='1'){
						$( "#Approved-btn").button({ disabled: false });
						$( "#Reject-btn").button({ disabled: false });
					}else{
						$( "#Approved-btn").button({ disabled: true });
						$( "#Reject-btn").button({ disabled: true });					
					}
				}else{}
				
				if(data.PR.Status=='1'){
					$('#PRStatus-span').append('<span class="status green" > Completed PO</font>');
					$( "#Approved-btn").button({ disabled: true });
					$( "#Reject-btn").button({ disabled: true });	
				}
			}
			
			if($('#hid-deptname').val() =='PRD' ||  $('#hid-isPRDManager').val()== 1){
				$( "#new-btn").button({ disabled: false });
				$( "#seek-btn").button({ disabled: false });
			}else{
				$( "#new-btn").button({ disabled: true });
				$( "#seek-btn").button({ disabled: true });
			}
			 $('#hid-internalrequest').val(data.PR.InternalRequest);
			if(data.PR.InternalRequest == '1' ){
				$('#RequestedBy').combobox("disable");
				$('#ApprovedBy').combobox("disable");
			}
			
			$('#tbl_PRDetail-div').show();
			$('#PRType').addClass('readonly').attr("disabled", true);
			//$("#Department").combobox('disable');
			fn_CheckPRType();
			
			//fn_PRDetailDatabind();
			tbl_PRDetail.fnDraw();
		}
	}
	
	function fn_PRDatabind(){
		//ProcessRequest('purchase/PurchaseRequisition/viewPRDataAllData',{PRNumber:''},'fn_viewPRData');
	}
	
	// function fn_PRDetailDatabind(){
		// //tbl_PRDetail.fnClearTable();
		// ProcessRequest('purchase/PurchaseRequisition/viewPRDetailData',{PRNumber:$('#PRNumber').val()},'fn_viewPRDetailData');
	// }
	
	
	
	$('#submit-btn').click(function(){
		if(tbl_PRDetail.fnGetData().length == 0){
			msgbox("error",'<p class="error">No record found in PR Detail List</p>','addpritems-btn');		
		}else{
			fn_SubmitPRData($('#PRNumber').val(),'PRD',1,'');
		}
	});
	$('#Approved-btn').click(function(){
		var type ='';
		if($('#hid-isPRDManager').val()== 1){
			type='PRDManager';
		}else if($('#hid-ismanager').val() == 1){
			type='Manager';
		}else if($('#hid-isgm').val() == 1){
			type='GM';
		}else if($('#hid-isbm').val() == 1){
			type='BM';
		}

		fn_SubmitPRData($('#PRNumber').val(),type,1,'');
	});
	
	fn_SubmitPRData=function(PRNumber,Type,Approved,ApprovedReason){
		 var msg;
		 if(Type =='PRD'){
			msg = 'Are you sure you want to Submit PR to Manager?';
		 }else if(Type =='PRDManager'){ 
			if(Approved == 1){
				msg = 'Are you sure you want to Approve?';
			}else{
				msg = 'Are you sure you want to Reject?';
			}
		 }else if(Type =='Manager'){
			if(Approved == 1){
				msg = 'Are you sure you want to Approve?';
			}else{
				msg = 'Are you sure you want to Reject?';
			}
		 }else if(Type =='BM'){
			if(Approved == 1){
				msg = 'Are you sure you want to Approve?';
			}else{
				msg = 'Are you sure you want to Reject?';
			}
		 }else if(Type =='GM'){ 
		 	if(Approved == 1){
				msg = 'Are you sure you want to Approve?';
			}else{
				msg = 'Are you sure you want to Reject?';
			}
		 }

		 var formValues;
		 formValues =  {"PRNumber": PRNumber,"Type":Type,"Approved":Approved,'ApprovedReason': ApprovedReason };
			
		var confirmpr = ["fn_ConfirmSubmitPRData",formValues];
		msgbox('Confirm Complete','<p class="warning">'+msg+'</p>',0,confirmpr);
	}
	
	fn_ConfirmSubmitPRData = function(formValues){
		ProcessRequest('purchase/PurchaseRequisition/submitPRData',
						formValues,'fn_ConfirmSubmitPRCallBack');
	}
	
	fn_ConfirmSubmitPRCallBack=function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		if(data[0] == 'success'){
			if($('#save-btn').attr('data') != '' && $('#add-new').is(":visible") == false){
				fn_searchPRData($('#PRNumber').val());
			 }else{
				tbl_PR.fnDraw();
			 }
			
		} 
	}
	
	$('#Reject-btn').click(function(){
		fn_RejectPRData($('#PRNumber').val());
	});
	
	fn_RejectPRData = function(PRNumber){
		$( "#PRRejectReason-div" ).dialog({
			resizable: false,
			height:300,
			width:490,
			modal: true,
			closeOnEscape: true,
			buttons: {
				"Reject": function() {
					if($('#ApprovedReason').val().length == 0){
						msgbox('Error Message','<p class="error"> Please entry Reason</p>','ApprovedReason');	
					}else{
						var type ='';
						if($('#hid-isPRDManager').val()== 1){
							type='PRDManager';
						}else if($('#hid-isbm').val() == 1){
							type='BM';
						}else if($('#hid-ismanager').val() == 1){
							type='Manager';
						}else if($('#hid-isgm').val() == 1){
							type='GM';
						}
						fn_SubmitPRData(PRNumber,type,2,$('#ApprovedReason').val());
						$(this).dialog("destroy"); 
					}
				},
				Close: function() {
					$(this).dialog("destroy");  
				}
			},
			close: function(){
				$(this).dialog("destroy");  
			}
		});
	}
	// function fn_ActionApprove(Type,Approved,ApprovedReason){
		 // ProcessRequest('purchase/InternalRequest/actionInternalRequest',formValues,'fn_ActionApproveCallBack');
	// }
	
	
	
	
	
	$('#completepr-btn').click(function(){
		if(tbl_PRDetail.fnGetData().length == 0){
			msgbox("error",'<p class="error">No record found in PR Detail List</p>','addpritems-btn');		
		}else{
			fn_callCompletePRData($('#PRNumber').val());
		}
	});
	
	fn_callCompletePRData = function(prnumber){
		var confirmpr = ["fn_ConfirmCompletePRData",prnumber];
		msgbox('Confirm Complete','<p class="warning">Are you sure you want to Complete PR?</p>',0,confirmpr);
	}
	
	fn_ConfirmCompletePRData = function(prnumber){
		ProcessRequest('purchase/PurchaseRequisition/completePRData',{PRNumber:prnumber},'fn_CompletePRCallback');
	}
	
	fn_CompletePRCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		if(data[0] == 'success'){
			fn_searchPRData($('#PRNumber').val());
		}
	}
	
	$('#delete-btn').click(function(){
		$( "#PRDDeleteReason-div" ).dialog({
			resizable: false,
			height:300,
			width:490,
			modal: true,
			closeOnEscape: true,
			buttons: {
				"Deleted": function() {
					if($('#DeletedReason').val().length == 0){
						msgbox('Error Message','<p class="error"> Please entry Reason for delete</p>','DeletedReason');	
					}else{
						fn_callDeletePRData($('#PRNumber').val()); 
						$(this).dialog("destroy"); 
					}
				},
				Close: function() {
					$(this).dialog("destroy");  
				}
			},
			close: function(){
				$(this).dialog("destroy");  
			}
		});
	
	});
	
	fn_callDeletePRData = function(prnumber){
		var confirmpr = ["fn_ConfirmDeletePRData",prnumber];
		msgbox('Confirm Deletion','<p class="warning">Are you sure you want to delete?</p>',0,confirmpr);
	}
	
	fn_ConfirmDeletePRData = function(prnumber){
		ProcessRequest('purchase/PurchaseRequisition/deletePRData',{'PRNumber':prnumber,'DeletedReason':$('#DeletedReason').val()},'fn_deletePRCallback');
	}
	
	fn_deletePRCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		if(data[0] == 'success'){
			$('#PRNumber').val($('#PRNumber').val()+'-Deleted');
			fn_searchPRData($('#PRNumber').val());
			// fn_cleanPRDetailfrm();
			// fn_cleanPRfrm();
			// fn_formStatus(false);
			// loadbtnAction(fn_btnList(false,false,false,false,true,true,true,true,true,true,true,true));
			// $('#PRType').change();
		}
	}
	
	$('#unlockpr-btn').click(function(){
		prnumber = $('#PRNumber').val();
		var confirmpr = ["fn_confirmUnlockPRData",prnumber];
		msgbox('Confirm Unlock','<p class="warning">Are you sure you want to Unlock PR?</p>',0,confirmpr);
	});
	
	fn_confirmUnlockPRData = function(prnumber){
		ProcessRequest('purchase/PurchaseRequisition/unlockPRData',{PRNumber:prnumber},'fn_unlockPRCallback');
	}
	
	fn_unlockPRCallback= function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		if(data[0] == 'success'){
			fn_searchPRData($('#PRNumber').val());
		}
	}
	
	$('#seek-btn').click(function(){
		fn_formStatus(true);
		loadbtnAction(fn_btnList(false,false,false,true,true,true,true,true,true,true,true,true));
		tbl_PRDetail.fnClearTable();
		$('$PRStatus-span').text('');
	});
	
	$('#addpritems-btn').click(function(){
		if($('#hid-internalrequest').val() =='1'){
			var prnumber =$('#PRNumber').val();
			
			$('#internalRequestdiv').dialog({
				resizable: false,
				height:525,
				width:950,
				modal: true,
				closeOnEscape: true,
				buttons: {
					"Add": function() {
						var data = tbl_internalRequest.fnGetData();
						var ID ='';
						var unitprice ='';
						var valid = false;
						$.each(data,function(i,e){
							
							if($('#chkID_'+data[i][0]).is(':checked')){
								
								ID += $('#chkID_'+data[i][0]).val() + "|";
								unitprice += $('#UnitPrice_'+data[i][0]).val()  + "|";
								valid = true;
							}
						});
						if(valid){
							ProcessRequest('purchase/PurchaseRequisition/addInternalRequest',
									{'PRNumber':$('#PRNumber').val(),'ID':ID,'UnitPrice':unitprice},
									'fn_addInternalRequestCallBack'); 
						}else{
							msgbox("error",'<p class="error">Please select</p>','PRNumber');		
						}
					
					},
					Close: function() {
									
						$(this).dialog("destroy");
					}
				},
				close: function(){
					$(this).dialog("destroy");  
				},
				open : function(){
					tbl_internalRequest.fnDraw();
					tbl_internalRequest.fnAdjustColumnSizing();		
				}
			});	
			
			//ProcessRequest('purchase/PurchaseRequisition/viewInternalRequest',{'PRNumber':prnumber},'fn_viewInternalRequestCallBack');
		}else{
			fn_cleanPRDetailfrm();
			prdetailDiaglog("Add");
		}		
	});
	
	
	
	fn_addInternalRequestCallBack = function(data){
		var data = explode("|",data);
		if(data[0] == 'success'){
			//fn_PRDetailDatabind(); 
			tbl_PRDetail.fnDraw();
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','save-btn');
			tbl_internalRequest.fnDraw();
		}
	}
	
	fn_GetPartPrices = function(data){
		if(data.length <= 1){
			msgbox("error",'<p class="error">No record found</p>','PRNumber');		
		}else{
			var data = json_decode(data);
			$('#UOM').combobox('selected',data['UOM']);
			$('#UnitPrice').val(data['BasePrice']);
			$('#Currency').val(data['Currency']);
		}
	}
	
	
	 fn_submitPRDetailCallback =function(data){
		var data = explode("|",data);
		var focus ='';
		if(data[0] == 'success'){
			fn_cleanPRDetailfrm();
			if($('#hid-internalrequest').val() =='1'){
				$( "#prdetail-div" ).dialog("destroy");  
				//fn_PRDetailDatabind();
				tbl_PRDetail.fnDraw();
			}
		}
		if($("#PRType").val() == 'NORMAL'){
			focus='PartNumber-cbx-combox';
		}else{
			if($("#Department").val() == '1'){
				focus='Department-Detail';
			}else{
				focus='PartNumber';	
			}
		}
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>',focus);	 
	 };
	 
	fn_department= function(){
		var valid = true;
		var field;
		field = $('#Department-Detail');
		if($('#PRType').val() == 'NORMAL'){
			return true;
		}else if($('#Department').val() =='1'){
			if(field.val() == ""){
				valid = false;
			}		
		}
		
		if(!valid)
			return [[field,"Please enter Department"]];
		
		return true;
	}
	 
	fn_checkPartNumber = function(){
		var valid = true;
		var field;
		if($('#PRType').val() == 'NORMAL'){
			field = $('#PartNumber-cbx-combox');
			if(field.val() == ""){
				valid = false;
			}		
		}else{
			field = $('#PartNumber');
			if(field.val() == ""){
				valid = false;
			}
		}
		if(!valid)
			return [[field,"Please enter Partnumber"]];
		
		return true;
	}
	
			
	  $("#DateRequired").datepicker({
			constrainInput:false,
            changeMonth: true,
			changeYear: true,
			showOtherMonths: true,
			showOn: "button",
            buttonImage: "assets/images/calendar.png",
            buttonImageOnly: true
        });
	// $( "#DateRequired" ).datepicker();
	
	// $('#ASAP').click(function(){
		// $( "#DateRequired" ).datepicker("destroy");
		 // if ($(this).is(':checked')){
			// $('#DateRequired').val('ASAP').removeClass().attr('class', 'readonly');
		 // }else{
			// $( "#DateRequired" ).datepicker();
			// $('#DateRequired').val('').removeClass().attr('class', 'datetime hasDatepicker');
		 // }
	// });
	
	$('#printprwithoutprice-btn').click(function(){
			fn_PrintPrWithoutPrice($('#PRNumber').val());
	});
	fn_PrintPrWithoutPrice = function(PRNumber){
		window.open("report/PrintPDF/PRWithoutPriceREPORT/"+PRNumber,'_blank',false);
	}
	$('#printpr-btn').click(function(){
			fn_PrintPR($('#PRNumber').val());
	});
	 
	 fn_PrintPR = function(PRNumber){
		window.open("report/PrintPDF/PRREPORT/"+PRNumber,'_blank',false);
	 }
	$('#new-btn').click(function(){
		fn_NewPR();
	});
	
	fn_NewPR = function(){
		fn_formStatus(false);
		fn_cleanPRfrm();
		$('#PRType').change();
		
		if($('#hid-deptname').val() == 'PRD' || $('#hid-isPRDManager').val() =='1'){
			loadbtnAction(fn_btnList(false,false,false,false,true,true,true,true,true,true,true,true));
		}else{
			loadbtnAction(fn_btnList(true,true,true,true,true,true,true,true,true,true,true,true));
		}
		$('#tbl_PRDetail-div').hide();
		$('#PRStatus-span').text('');
		$('#hidstatusPR').val('');
		$('#save-btn').val('Save').attr('data','');
		 
		// $("#ASAP").attr("disabled", false);
		// $('#ASAP').attr('checked',false);
		$('input#Description').focus().select();
	}
	
	$('#save-btn').click(function(){
		var rules =	[
				"required,PRNumber,Please enter PRNumber.",
				"required,Department,Please enter Department.",
				"function,fn_checkdepartment",
				"required,DateRequired,Please enter Date Required.",
				"required,Discount,Please enter Discount.",
				"reg_exp,Discount,^\\d*(\\.\\d{1\\,})?$,Please enter valid Discount.",
				"required,PurchasedBy,Please enter Purchased By.",
				"required,RequestedBy,Please enter Requested By.",
				"required,ApprovedBy,Please enter Approved By."
				];
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = fn_formSubmit;
		rsv.validate(document.forms['newPR'],rules); 
	});
	
	fn_checkdepartment = function(){
		var valid = true;
		var field;
		if($('#PRType').val() == 'NORMAL'){
			field = $('#Department');
			if(field.val() == '1'){
				valid = false;
			}		
		}
		if(!valid)
			return [[field,"Please enter department!"]];
		
		return valid;
	}
	
	// Form Submission (add/update)
	function fn_formSubmit(theForm){
		var formValues = $(theForm).serializeArray();
			formValues.push({ name: "ID", value: $('#save-btn').attr('data') });
		var action = $('#PRNumber-hid').val() == '' ? 'savePR' : 'updatePRD';
		ProcessRequest('purchase/PurchaseRequisition/'+action,formValues,'fn_submitCallback');
	}

	//Save Feedback
	fn_submitCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
		if(data[0]=='success'){
			loadbtnAction(fn_btnList(false,false,false,false,false,false,false,true,true,false,true,true)); 
			fn_CheckPRType();
			$('#PRType').addClass('readonly').attr("disabled", true);
			$('#save-btn').val('Save Changes').attr('data',$('#PRNumber').val());
			$('#PRNumber-hid').val($('#PRNumber').val());
			$('#tbl_PRDetail-div').show();
		}
	}
	
	$('#PRType').change(function() {
		fn_NewPRNumber();
		fn_CheckPRType();
	});
	
	fn_NewPRNumber = function(){
		var prtype = $('#PRType').val();
		
		if(!prtype){
			prtype= 'NORMAL';
		}
		ProcessRequest('purchase/PurchaseRequisition/PRNumber',{PRType:prtype},'fn_GetPRNumber');
	}
	
	function fn_CheckPRType(){
		 var prtype = $('#PRType').val();
		if(prtype == 'NORMAL'){
			$('#newPRDetail-frm #Department-Detail').addClass('readonly').attr('readonly',true);
			$('#department-detail-li').hide();
			$('#customer-detail-li').show();
			$('#newPRDetail-frm #PartNumber').hide();
			$('#newPRDetail-frm #VendorID').hide();
			
			$('#newPRDetail-frm #RequestedBy').hide();
			$('#newPRDetail-frm #PurchasedBy').hide();
			$('#newPRDetail-frm #ApprovedBy').hide();
			
			//AddCurrency
			$('#newPRDetail-frm #Currency').addClass('readonly').attr('readonly',true);
			
			$('#newPRDetail-frm #UnitPrice').addClass('readonly').attr('readonly',true);
			$('#newPRDetail-frm #UOM').combobox('disable');
			
			
			//$('#newPRDetail-frm #CustomerID').combobox('enable');
			
			$('#newPRDetail-frm .PartNumber-cbx-combox').show();
			$('#partdesc-li').hide();

		}else{
			$('#customer-detail-li').hide();
			// if($("#Department").val() == 1){
				// $('#newPRDetail-frm #Department-Detail').removeClass('readonly').removeAttr('readonly');
				// $('#department-detail-li').show();
			// }else{
				// $('#newPRDetail-frm #Department-Detail').addClass('readonly').attr('readonly',true);
				// $('#department-detail-li').hide();
			// }
			//Currency
			$('#newPRDetail-frm #Currency').removeClass('readonly').removeAttr('readonly');
			
			$('#newPRDetail-frm #UnitPrice').removeClass('readonly').removeAttr('readonly');
			$('#newPRDetail-frm #UOM').combobox('enable');			
			$('#newPRDetail-frm .PartNumber-cbx-combox').hide();
			//$('#newPRDetail-frm #CustomerID').combobox('disable');			
			$('#newPRDetail-frm #PartNumber').show();
			$('#newPRDetail-frm #VendorID').show();
			
			$('#newPRDetail-frm #RequestedBy').show();
			$('#newPRDetail-frm #PurchasedBy').show();
			$('#newPRDetail-frm #ApprovedBy').show();
			
			$('#partdesc-li').show();

		}
	}
	
	fn_GetPRNumber = function(data){
		$('#PRNumber').val(data);
	}
	
	
	function fn_formStatus(pstatus){
		if(pstatus){
			$('#PRNumber').removeClass('readonly').removeAttr('readonly');
			$('#PRType').addClass('readonly').attr("disabled", true);
			$("#Department").combobox('disable');
			$("#VendorID").combobox('disable');
		
			$("#RequestedBy").combobox('disable');
			$("#PurchasedBy").combobox('disable');
			$("#ApprovedBy").combobox('disable');
			
			$("#GST").prop("disabled", true).addClass('readonly');
			$("#RequiredGMApproved").prop("disabled", true).addClass('readonly');
			
			$('#Discount').val('').addClass('readonly').attr('readonly',true);
			
			$('#DateRequired').val('').addClass('readonly').attr('readonly',true);
			$('#PurchasedBy').val('').addClass('readonly').attr('readonly',true);
			$('#RequestedBy').val('').addClass('readonly').attr('readonly',true);
			$('#ApprovedBy').val('').addClass('readonly').attr('readonly',true);
			$('#Comment').val('').addClass('readonly').attr('readonly',true);
			$('#Remarks').val('').addClass('readonly').attr('readonly',true);
			$('#PRNumber').val('').focus();
		}else{
			$('#PRNumber').addClass('readonly').attr('readonly',true);
			$('#PRType').removeClass('readonly').removeAttr('disabled');
			$("#VendorID").combobox('enable');
			
			$("#RequestedBy").combobox('enable');
			$("#PurchasedBy").combobox('enable');
			$("#ApprovedBy").combobox('enable');
			
			$("#GST").prop("disabled", false).removeClass('readonly'); 
			$("#RequiredGMApproved").prop("disabled", false).removeClass('readonly'); 
			
			$('#Discount').removeClass('readonly').removeAttr('readonly');
			$('#DateRequired').removeClass('readonly').removeAttr('readonly');
			$('#PurchasedBy').removeClass('readonly').removeAttr('readonly');
			$('#RequestedBy').removeClass('readonly').removeAttr('readonly');
			$('#ApprovedBy').removeClass('readonly').removeAttr('readonly');
			$('#Comment').removeClass('readonly').removeAttr('readonly');
			$('#Remarks').removeClass('readonly').removeAttr('readonly');
			$("#Department").combobox('enable');
			$('#Department-combox').focus();
		}
	}

	function fn_disiableAll(status){
		$('#newPR-frm').find('input,select,textarea').each(function() {
			if($(this).attr('type') !='button'){
				if(status){
					//$("#ASAP").attr("disabled", true);
					$(this).addClass('readonly').attr('readonly',true);
				}else{
					//$("#ASAP").attr("disabled", false);
					$(this).removeClass('readonly').removeAttr('readonly');
				}
			}
		});
		if(status){
			$('#Department').combobox("disable");
			$('#VendorID').combobox("disable");
			
			$('#RequestedBy').combobox("disable");
			$('#PurchasedBy').combobox("disable");
			$('#ApprovedBy').combobox("disable");
			
			$("#GST").attr("disabled", "disabled").addClass('readonly');
			$("#RequiredGMApproved").attr("disabled", "disabled").addClass('readonly');
		}else{
			$('#Department').combobox("enable");
			$('#VendorID').combobox("enable");
			
			$('#RequestedBy').combobox("enable");
			$('#PurchasedBy').combobox("enable");
			$('#ApprovedBy').combobox("enable");
			
			$("#GST").attr("disabled", "").removeClass('readonly');
			$("#RequiredGMApproved").attr("disabled", "").removeClass('readonly'); 
		}
	}
	
	function fn_cleanPRDetailfrm(){
		$('#newPRDetail-frm :input').each(function(index) {
			if($(this).attr('type') !='button'){
				$(this).val(''); 
			}
			$('#PartNumber-cbx').empty();
		});
	}
	
	function fn_cleanPRfrm(){
		$('#newPR-frm :input').each(function(index) {
			if($(this).attr('type') !='button' && $(this).attr('type') != 'hidden'){
				$(this).val('');
			}
		});
		tbl_PRDetail.fnClearTable();
		
		$('#PRNumber-hid').val('');
		$('#hid-internalrequest').val('');
		// $('#DateRequired').attr('readonly',true);
		// $( "#DateRequired" ).datepicker();
		// $('#DateRequired').removeClass().attr('class', 'datetime hasDatepicker');
	}
		
	function fn_btnList(_viewlist,_seek,_new,_save,_addpritems,_completepr,
						_delete,_printpr,_printprwithoutpricebtn,_submitbtn,_approvedbtn,_rejectbtn){
		btn_list = {'viewlist-btn':_viewlist,
			   'seek-btn':_seek,
				'new-btn':_new,
				'save-btn':_save,
				'addpritems-btn':_addpritems,
				'completepr-btn':_completepr,
				'delete-btn':_delete,
				'printpr-btn':_printpr,
				'printprwithoutprice-btn':_printprwithoutpricebtn,
				'submit-btn':_submitbtn,
				'Approved-btn':_approvedbtn,
				'Reject-btn':_rejectbtn
				};
		return(btn_list);
	}


	
	$("#newPRDetail-frm #PartNumber").keypress(function(event) {
		if ( event.which == 13 ) {
			$('#newPRDetail-frm #PartDesc1').focus();
		}
    });
	
	$("#PRNumber").keydown(function(event) {
		if ( event.which == 13 ) {
			var prnumber = $('#PRNumber').val();
			fn_searchPRData(prnumber);
		}
    });
   
   
	$('#newPRDetail-frm #RequestedQty').keydown(function (event) {
		if ( event.which == 13 ) {
			if($("#PRType").val() == 'NORMAL'){
				$('.ui-dialog').find('button:first').focus();
			}
		}
	});
	
	$(".int").keydown(function(event) {
		// Allow only backspace, delete, enter, tab
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 ) {

		}else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
	
	$(".decimal").keydown(function(event) {
		// Allow only backspace, delete, enter, tab
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 
		|| event.keyCode == 190 || event.keyCode == 110 ) {
			//

			if(($(this).val().split(".")[0]).indexOf("00")>-1){
				$(this).val($(this).val().replace("00","0"));
			}
		}else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
	
	
	function fn_PartNumberDataBind(){
		ProcessRequest('services/DropDownLists/partnumberByCustomer',{'CustomerID':$('#CustomerID').val()},'fn_loadPartNumber');
	}
	
	fn_loadPartNumber = function(data){

		if(data != '1'){
			var obj = jQuery.parseJSON(data);
			$('#PartNumber-cbx')
				.empty()
				.append('<option selected="selected" value=""></option>');;
			$.each(obj, function(index,item) {
				$('#PartNumber-cbx')
				 .append($("<option ></option>")
				 .attr("value",item.ID)
				 .text(item.ID +' - '+ item.Value));	
			});
			
			$('#PartNumber-cbx').combobox();
			
			$('#newPRDetail-frm #PartNumber-cbx').combobox({				
				selected: function(event, ui) {
					ProcessRequest('purchase/PurchaseRequisition/getPartsPrice',{PartNumber:$('#PartNumber-cbx').val()},'fn_GetPartPrices');
				}
			});
			if($('#newPRDetail-frm #PartNumber-hid').val() == ''){
				$('#PartNumber-cbx').combobox('selected','');
			}else{
				$('#PartNumber-cbx').combobox('selected',$('#newPRDetail-frm #PartNumber-hid').val());
			}
		}
	}
	
	/* Start Loading Data*/
	ProcessRequest('services/DropDownLists/getComboBoxdata',{
					'Department':'Department','Vendor':'Vendor','UOM':'UOM',
					'PRType':'PRType','RequestedBy':'RequestedBy','PurchasedBy':'PurchasedBy',
					'ApprovedBy':'ApprovedBy','Customer':'Customer'
					},'fn_ComboBoxload');
	loadbtnAction(fn_btnList(false,false,false,false,true,true,true,true,true,true,true,true));
	
	$('#PartNumber-cbx').combobox();
	$('#tbl_PRDetail-div').hide();
	fn_NewPRNumber();
	$('#tbl_PRDetail-div').hide();
	//fn_PRDatabind(); 
	//tbl_PR.fnDraw();
	
	$('#unlockpr-btn').hide();
	/*End FormLoad*/
	

		// $('#newPRDetail-frm #UnitPrice').keydown(function (event) {
		// if ( event.which == 13 ) {
			// //$('.ui-dialog').find('button:first').trigger('click');
			// $('.ui-dialog').find('button:first').focus();
		// }
	// });
});
</script>

