<h2 class="title">Purchase &raquo; Sale Order</h2>
<div class="top-button">
	<button id="add-new"><span class="ui-icon ui-icon-plusthick"></span>Add New Sale Order</button>
	<button id="back-to-list" style="display:none"><span class="ui-icon ui-icon-arrowthick-1-w"></span>Back to List</button>
</div>
<div class="clear"></div>
<div class="list" id="sodiv" name="sodiv" >
	<table class="display" id="tbl_SO">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table>
</div>
<div class="newentry"  style="display:none">
	<h3 class="form-title">Sale Order</h3>
	<form class="globalform" id="newSO-frm" name="newSO">
		<ol>
			<div class="lfpane" >
				<li>
					<label>SO Type</label>
					<select name="SOType" id="SOType">
						<option value="Single">Single</option>
						<option value="Multi">Multi</option>
					</select>
				</li>
				<li>
					<label>PO No</label>
					<select id="PONumber" name="PONumber">
					</select>
					<input type="text"  id="PONumber-txt" name="PONumber-txt" class="readonly" style="display:none">

				</li>
				<li>
					<label>SO NO</label>
					<input type="text" id="SONumber" style="width:100px;" name="SONumber" value="" class="readonly"/>
					<span class="req">*</span>
				</li>
				<li>
					<label>CustomerID</label>
					<select id="CustomerID" name="CustomerID">
					</select>
					&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;
					<span class="req">*</span>
				</li>
				<li>
					<label>Mould Cost</label>
					<input type="text" id="OneTimeCharge" name="OneTimeCharge" class="decimal"/>
					<span class="req">*</span>
				</li>
				<li>
					<label>TT Charges</label>
					<input type="text" id="TTCharges" name="TTCharges" class="decimal"/>
					<span class="req">*</span>
				</li>
				<li>
					<label>Permit Charges</label>
					<input type="text" id="FreightCharges" name="FreightCharges" class="decimal"/>
					<span class="req">*</span>
				</li>
				
			</div>
			<div class="rfpane" >	
				<li style="width:100%!important">
					<div id="SOStatus-span" ></div>
				</li>
				<li>
					<input type="button" id="unlockso-btn" value="Unlock SO" />
					<input type="hidden" id="hidstatusSO" />
				</li>
				<li>
					<label>SO Date</label>
					<input type="text" id="SODate" name="SODate" class="readonly" readonly="true"/>
					<input type="hidden" id="SODate-hid" name="SODate-hid" />
				</li>
				<li>
					<label>Name</label>
					<input type="text" id="Name" name="Name" value="<?php echo $name ?>" class="readonly" readonly="true"/>
					<input type="hidden" id="Name-hid" name="Name-hid" value="<?php echo $name ?>"/>
				</li>
				<li >
					<label> Remarks</label>
					<textarea id="Remarks" name="Remarks" style="width:110%"> </textarea>
				</li>
				<li>
					<!--<input type="button" id="save-btn" name="save-btn" value="Save" data=""/>
					<input type="button" id="add-btn" name="add-btn" value="Add SO Item" data=""/>
					<input type="button" id="delete-btn" name="delete-btn" value="Delete"/>-->
					<button id="save-btn" name="save-btn" data="" ><span class="ui-icon ui-icon-disk"></span>Save</button>
					<button  id="add-btn"  name="add-btn" data="" ><span class="ui-icon ui-icon-circle-plus"></span>Add SO Item</button>
					<button  id="delete-btn" name="delete-btn" data="" ><span class="ui-icon ui-icon-circle-close"></span>Delete</button>
				
				</li>
			</div>
			<div style="width: 100%;">
				<li style="float: right; margin-bottom: 15px; margin-right: 110px;">
					<!--<input type="button" id="viewlist-btn" name="viewlist-btn" value="View List" data="" style="display:none"/>
					<input type="button" id="seek-btn" name="seek-btn" value="Seek" data=""/>
					<input type="button" id="new-btn" name="new-btn" value="New" data=""/>
					<input type="button" id="completeso-btn" name="completeso-btn" value="Complete So"/>
					<input type="button" id="printso-btn" name="printso-btn" value="Print SO"/>-->
					<button id="viewlist-btn" name="viewlist-btn" data="" style="display:none"><span class="ui-icon ui-icon-disk"></span>View List</button>
					<button id="seek-btn" name="seek-btn" data="" ><span class="ui-icon ui-icon-search"></span>Seek</button>
					<button id="new-btn" name="new-btn" data="" ><span class="ui-icon ui-icon-plusthick"></span>New</button>
					<button id="completeso-btn" name="completeso-btn" data=""  ><span class="uiui-icon ui-icon-circle-check"></span>Complete SO</button>
					<button  id="printso-btn" name="printso-btn" data="" ><span class="ui-icon ui-icon-print"></span>Print SO</button>
		
				</li>
			</div>
		</ol>
		<div class="clear"></div>		
	</form>
	<div class="clear"></div>	

</div>
<div id="tbl_SODetail-div" >  
	<table class="display" id="tbl_SODetail" >
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table>
</div>

<div id="sodetail-div" name="sodetail-div" style="display: none;">
	<table class="display" id="tbl_PODetail">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table>
</div>

<div id="edit-sodetail" name="edit-sodetail" style="display: none;">
	<form  id="edit-sodetail-frm" name="edit-sodetail" style="border: 1px solid #DAECF4;margin: 0;min-height: 60px;padding: 10px;">
		<li style="list-style: none outside none;">
			<label>Quantity</label>
			<input type="text" id="newQty" name="newQty" class="int" value="" />
			<input type="hidden" id="hidRemainQty" name="hidRemainQty" value="" />
			<input type="hidden" id="hidOldQty" name="hidOldQty" value="" />
			<input type="hidden" id="hidSOID" name="hidSOID" value="" />
		</li>
	</form>
</div>

<div id="SODeleteReason-div" name="SODeleteReason-div" style="display: none;">
	<form class="globalform" id="SODeleteReason-frm" name="SODeleteReason" style="width:400px;">
		Reason
		<textarea id="DeletedReason" name="DeletedReason" style="width:370px"></textarea>
	</form>
</div>
<script type="text/javascript">

var tbl_SO,tbl_SODetail,tbl_PODetail;
$(document).ready(function(){
	$("input:submit, input:button, button, .button").button().click(function(event){
		event.preventDefault();
	});
	
	$('#SOType').focus();
	$('#unlockso-btn').hide();
	fn_GetComboBoxdata = function(data){
		 var obj = jQuery.parseJSON(data);
		 loadComboBoxData("PONumber",obj.PurchaseOrder,false);
		 loadComboBoxData("CustomerID",obj.Customer,true);
		 $('#PONumber').combobox({
			selected: function(event, ui) {
				//auto SONumber
				//ProcessRequest('purchase/SaleOrder/getSONumber',{'PONumber':$('#PONumber').val()},'fn_getSONumber');
				fn_CheckSOType();
				
			}
		});
	}
	
	$('#SOType').change(function(){
		fn_CheckSOType();
	});
	fn_CheckSOType = function(){
		$('#SONumber').val($('#PONumber').val());
		if($('#SOType').val() == 'Single'){
			$('#SONumber').addClass('readonly').attr('readonly',true);
		}else{
			$('#SONumber').removeClass('readonly').attr('readonly',false);
		}
	}
	
	/* Create Table For SO */
	var SO_fields = { 'ID':'ID',
					  'SONumber':'SONumber',
					  'PONumber':'PONumber',
					  'SOType':'SOType',
					  'CustomerName':'Customer',
					  'SODate':'SODate',
					  'TTCharges':'TTCharges',
					  'FreightCharges':'FreightCharges',
					  'SOStatus':'SOStatus'
					  };
	$.each(SO_fields,function(i,e){
		$('#tbl_SO thead tr').append('<th>'+e+'</th>');
		$('#tbl_SO tfoot tr').append('<th>'+e+'</th>');
	});
	$('#tbl_SO thead tr').append('<th>Action</th>');
	$('#tbl_SO tfoot tr').append('<th>Action</th>');	
	tbl_SO = $('#tbl_SO').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aaSorting: [[0,'desc']],
		sAjaxSource: "purchase/SaleOrder/viewSOallData", 
		aoColumnDefs: [ { "bSortable": false, "aTargets": [ 8 ], "bSearchable": false } ],
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(SO_fields)) } );
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					//showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	/* Create Table For PRDetail */
	var SODetail_fields = {'No':'No',
						  'PartDesc':'Part Desc',
						  'Qty':'Qty',
						  'UnitPrice':'Unit Price',
						  'UOM':'UOM',
						  'ExtAmount':'Ext Amount',
						  'Action':'Action'};
	
	$.each(SODetail_fields,function(i,e){
		$('#tbl_SODetail thead tr').append('<th>'+e+'</th>');
		$('#tbl_SODetail tfoot tr').append('<th>'+e+'</th>');
	});
	
	tbl_SODetail = $('#tbl_SODetail').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bProcessing: true
	  });
	
	//Set Title for PRDetail
	setTableHeader('tbl_SODetail','Sale Order Item List');
	
	/* Create Table For PRDetail */
	var PODetail_fields = {'No':'No',
						  'Part Desc':'Part Desc',
						  'Quantity':'Quantity',
						  'UnitPrice':'Unit Price',
						  'Select':'<input type="checkbox" id="checkall" name="checkall" /></th>'
						  };
	
	$.each(PODetail_fields,function(i,e){
		$('#tbl_PODetail thead tr').append('<th>'+e+'</th>');
		if(i=='Select'){
			$('#tbl_PODetail tfoot tr').append('<th>&nbsp;</th>');
		}else{
			$('#tbl_PODetail tfoot tr').append('<th>'+e+'</th>');
		}
		
	});
	
	tbl_PODetail = $('#tbl_PODetail').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bProcessing: true
	});
	
	//Set Title for PRDetail
	setTableHeader('tbl_PODetail','Purchase Order Item List');

	fn_GetVendorAddress = function(data){
		
		var data = json_decode(data);
		var shiptoaddress ='';
		$.each(data[0],function(i,e){
			shiptoaddress = shiptoaddress + e + '\n'			
		});
		$("textarea#ShipToAddress").val(shiptoaddress);
		
	}
	
	$('#back-to-list').click(function(){
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
		$('#tbl_SODetail-div').hide();
		//fn_PRDatabind();
		tbl_SO.fnDraw();
	});
	
	$('#add-new').click(function(){
		$(this).fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#SOType').focus().select();});});
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Add New Sale Order');});
		$('#tbl_SODetail-div').show();
		$('#new-btn').click();
		//fn_NewPR();
	});
	
	// $('#viewlist-btn').click(function(){
		// $('#sodiv').dialog({
			// resizable: false,
			// height:525,
			// width:950,
			// modal: true,
			// buttons: {
				// Close: function() {
					// //fn_PRDetailDatabind();				
					// $(this).dialog("destroy"); 
				// }
			// },
			// close: function(){
				// $(this).dialog("destroy");  
			// },
			// open : function(){
				// tbl_SO.fnDraw();				
			// }
		// });	
	// });
	
	// function fn_SODatabind(){
		// ProcessRequest('purchase/SaleOrder/viewSOallData',{SONumber:''},'fn_viewSOData');
	// }
	
	
	
	fn_todoSO = function(action,sonumber){
		switch(action){
			case 'edit' : 
				fn_searchSOData(sonumber);
				//$( "#sodiv" ).dialog("destroy"); 	
				break;	
			case 'delete' : 
				fn_callDeleteSOData(sonumber);
				break;
			case 'print' : 
				fn_PrintSO(sonumber);
				break;
			case 'detail' : 
				var nTr = $(sonumber).parents('tr')[0];
				if ( tbl_SO.fnIsOpen(nTr) )
				{
					/* This row is already open - close it */
					//this.src = "assets/images/details_open.png";
					$(sonumber).removeClass('details-close').addClass('details-open');
					tbl_SO.fnClose( nTr );
					$(nTr).children('td').toggleClass('tropen');
					$(nTr).children('td:eq(0)').toggleClass('tropen-bg');
				}
				else
				{
					/* Open this row */
					//this.src = "assets/images/details_close.png";
					$(sonumber).removeClass('details-open').addClass('details-close');
					//console.log(tbl_SO.fnGetData( nTr )[1]);
					ProcessRequest('purchase/SaleOrder/getSODetail',
							{'SONumber':tbl_SO.fnGetData( nTr )[1]},
							function(data){ 
								tbl_SO.fnOpen( nTr,  fnFormatDetails ( data ), 'table-row-details' ); 
								$(nTr).children('td').toggleClass('tropen');
								$(nTr).children('td:eq(0)').toggleClass('tropen-bg');
							})
					
				}
				//fn_PRDatabind(); 
			break;	
		}
	}
	
	
	
	function fnFormatDetails ( data )
	{
		return data; 
	}
	
	$('#printso-btn').click(function(){
		fn_PrintSO($('#SONumber').val());
	});
	
	fn_PrintSO = function(sonumber){	
		window.open("report/PrintPDF/SOREPORT/"+sonumber,'_blank',false);
	}
	
	$('#add-btn').click(function(){
		fn_PODetailDataBind();
	});
	
	fn_PODetailDataBind = function(){
		ProcessRequest('purchase/SaleOrder/viewPODetail',{'SONumber':$('#SONumber').val(),'PONumber':$('#PONumber-txt').val()},'fn_viewPODetailData');
	}
	
	fn_viewPODetailData=function(data){
		if(data){
			var obj = jQuery.parseJSON(data);
			var no = 0;
			var partnumber,Qty;
			Qty = '0';
			tbl_PODetail.fnClearTable();
			readonly = ($('#SOType').val()=='Single')?'class="readonly" readonly ="true"':'class="int"';
			for (var i=0; i<obj.length; i++){
				partnumber = obj[i]['PartNumber'];
				Qty = obj[i]['Quantity']
				no = no + 1;
				tbl_PODetail.fnAddData( [no,
										obj[i]['PartDesc'],
										'<input type="text" id="Quantity_'+i+'" value="'+Qty+'" '+readonly+'/>',
										obj[i]['UnitPrice'],
										'<input type="checkbox" id="chkPartNumber_'+i+'" value="'+partnumber+'" />'	
										]);
			}
			
			$('#sodetail-div').dialog({
				resizable: false,
				height:525,
				width:950,
				modal: true,
				buttons: {
					"Add": function() {
						var data = tbl_PODetail.fnGetData();
						var partnumber ='';
						var quantity ='';
						var valid =false;
						var validqty =false;
						if(data.length > 0){
							$.each(data,function(i,e){
								if($('#chkPartNumber_'+i).is(':checked')){
									
									partnumber += $('#chkPartNumber_'+i).val() + "|";
									quantity += $('#Quantity_'+i).val()  + "|";
									if(parseFloat(data[i][3]) > 0){
										validqty = true;		
									}
									valid = true;
									

								}
								
							});
							if(valid){
								if(validqty){
										ProcessRequest('purchase/SaleOrder/saveSODetail',
										{'SONumber':$('#SONumber').val(),'PONumber':$('#PONumber-txt').val(),
										'PartNumber':partnumber,'Quantity':quantity},
										'fn_SaveSODetail');	
								}else{
										msgbox('Error Message','<p class="error"> Please update selling price before you add into Sale Order Detail.</p>','DeletedReason');	
								}
							}else{
								msgbox('Error Message','<p class="error">Please select first.</p>','DeletedReason');	
							}
							
						}
					},
					Close: function() {
						fn_SODetailDataBind();			
						$(this).dialog("destroy");
					}
				},
				close: function(){
					$(this).dialog("destroy");  
				},
				open : function(){
					tbl_PODetail.fnAdjustColumnSizing();		
				}
			});	
			
		}
	}
	
	fn_SaveSODetail = function(data){
		var data = explode("|",data);
		var focus ='';
		if(data[0] == 'success'){
			fn_PODetailDataBind();
		}
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','');	 
		//fn_ClearForm();
	}
	

	fn_searchSOData = function(sonumber){
		if($('#add-new').is(":visible")){
			$('#add-new').fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('#PRNumber').focus();});});
			$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Sale Order');});
		} 
		ProcessRequest('purchase/SaleOrder/GetSOData',{'SONumber':sonumber},'fn_GetSOData');
	}
	
	fn_GetSOData = function(data){
		if(data.length <= 1){
			var data = explode("|",data);
			msgbox("error",'<p class="error">No record found</p>','SONumber');		
		}else{
			var data = json_decode(data);

			if (data.length){
				$.each(data[0],function(i,e){
					$("#newSO-frm #"+i).val(e);
				});
				
				$('#unlockso-btn').hide();
				
				$('#PONumber-txt').val(data[0].PONumber);
				$('#CustomerID').combobox('selected',data[0].CustomerID);
			}
			$('#SOStatus-span').text(''); 
				
			if(data[0].Status == '1' || data[0].Deleted == '1' ){
				if(data[0].Status == '1'){
					$('#SOStatus-span').append('<span class="status green" > Completed SO</font>');
					$('#hidstatusSO').val('Completed');
					$('#unlockso-btn').show();
					loadbtnAction(fn_btnList(false,false,false,true,true,true,true,false));
				}else{
					$('#SOStatus-span').append('<span class="status red">Deleted</span>');
					$('#hidstatusSO').val('Deleted');
					loadbtnAction(fn_btnList(false,false,false,true,true,true,true,true));
				}
				fn_disiableAll(true);
				$('#CustomerID').combobox("disable");
				//console.log('test');
			}else{
				$('#SOStatus-span').text('');
				$('#hidstatusSO').val('');
				loadbtnAction(fn_btnList(false,false,false,false,false,false,false,true));
				fn_disiableAll(false);
				$('#PONumber-txt').addClass('readonly').attr('disabled',true);
			}

			//$('#save-btn').val('Save Changes'); 	
			 $('#SONumber').addClass('readonly').attr('readonly',true);

			$('#PONumber').combobox("hide");
			
			$('#PONumber-txt').show();
			
			
			fn_SODetailDataBind();
			
			$('#SODate').addClass('readonly').attr('disabled',true);
			
			$('#SOType').addClass('readonly').attr('disabled',true);
			$('#CustomerID-combox').focus();

		}
	}
	
	$('#unlockso-btn').click(function(){
		sonumber = $('#SONumber').val();
		var confirmso = ["fn_confirmUnlockSOData",sonumber];
		msgbox('Confirm Unlock','<p class="warning">Are you sure you want to Unlock SO?</p>',0,confirmso);
	});
	
	fn_confirmUnlockSOData = function(sonumber){
		ProcessRequest('purchase/SaleOrder/unlockSOData',{SONumber:sonumber},'fn_unlockSOCallback');
	}
	
	fn_unlockSOCallback= function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		if(data[0] == 'success'){
			//fn_PODetail($('#PONumber').val());
			//$('#new-btn').click();
			
			
			$('#SODate').addClass('readonly').attr('disabled',true);
			fn_searchSOData($('#SONumber').val());
		}
	}
	
	$('#completeso-btn').click(function(){
		if(tbl_SODetail.fnGetData().length == 0){
			msgbox("error",'<p class="error">No record found in PR Detail List</p>','addpritems-btn');		
		}else{
			fn_callCompleteSOData($('#SONumber').val());
		}
	});
	
	fn_callCompleteSOData = function(sonumber){
		var confirmso = ["fn_ConfirmCompleteSOData",sonumber];
		msgbox('Confirm Complete','<p class="warning">Are you sure you want to Complete SO?</p>',0,confirmso);
	}
	
	fn_ConfirmCompleteSOData = function(sonumber){
		ProcessRequest('purchase/SaleOrder/completeSOData',{SONumber:sonumber},'fn_CompleteSOCallback');
	}
	
	fn_CompleteSOCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		if(data[0] == 'success'){
			//$('#new-btn').click();
			fn_searchSOData($('#SONumber').val());
		}
	}
	
	$('#seek-btn').click(function(){
		$('#SODate').addClass('readonly').attr('disabled',true);
		$('#SOType').addClass('readonly').attr('disabled',true);
		$('#PONumber').combobox("hide");
		$('#PONumber-txt').show();
		$('#CustomerID').combobox("disable");
		//Add One Time Charge
		$('#OneTimeCharge').addClass('readonly').attr('readonly',true);
		
		$('#TTCharges').addClass('readonly').attr('readonly',true);
		$('#FreightCharges').addClass('readonly').attr('readonly',true);
		$('#Remarks').addClass('readonly').attr('readonly',true);
		$('#SONumber').val('').removeClass('readonly').removeAttr('readonly').focus();
		$( "#save-btn" ).button({ disabled: true });
		$( "#printso-btn" ).button({ disabled: true });
		$( "#unlockso-btn" ).hide();
	});
	

	$('#checkall').click( function() {
		if ($('#checkall').attr('checked')) {
			$('input', tbl_PODetail.fnGetNodes()).attr('checked','checked');
		}else{
			$('input', tbl_PODetail.fnGetNodes()).removeAttr('checked');
		}
	});
	
	$('#new-btn').click(function(){
		 $('#PONumber-combox').attr('readonly',false);
		$('#PONumber').combobox("show");
		$('#PONumber-txt').hide();
		$('#CustomerID').combobox('enable');
		$('#SONumber').val('').addClass('readonly').attr('readonly',true);
		$('#SOType').removeClass('readonly').attr('disabled',false);
		loadbtnAction(fn_btnList(false,false,false,false,true,true,true,true));
		
		//Add One time Charge
		$('#OneTimeCharge').val('').removeClass('readonly').attr('readonly',false);
		
		$('#TTCharges').val('').removeClass('readonly').attr('readonly',false);
		$('#FreightCharges').val('').removeClass('readonly').attr('readonly',false);
		$('#Remarks').val('').removeClass('readonly').attr('readonly',false);
		$('#SOStatus-span').text('');
		$('#hidstatusSO').val('');
		$('#save-btn').attr('data',''); //val('Save').
		fn_ClearForm();
		$('#PONumber-combox').attr('readonly',false);
	});

	$('#save-btn').click(function(){
		var rules =	[
				"function,fn_CheckPONumber",
				"required,SONumber,Please enter SONumber.",
				"function,fn_checkSONumber",
				"required,CustomerID,Please enter Customer.",
				"required,OneTimeCharge,Please enter TT Charges.",
				"reg_exp,OneTimeCharge,^\\d*(\\.\\d{1\\,})?$,Please enter valid One Time Charge.",
				"required,TTCharges,Please enter TT Charges.",
				"reg_exp,TTCharges,^\\d*(\\.\\d{1\\,})?$,Please enter valid TT Charges.",
				"required,FreightCharges,Please enter Frieght Charges.",
				"reg_exp,FreightCharges,^\\d*(\\.\\d{1\\,})?$,Please enter valid Freight Charges."
				];
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = fn_formSubmit;
		rsv.validate(document.forms['newSO'],rules); 
	});
	fn_CheckPONumber = function(){
		var valid = true;
		var field;
		
		if($('#PONumber-txt').val().length == 0){
			field = $('#PONumber');
			if(field.val().length == 0 ){
				valid = false;
			}		
		}
		if(!valid)
			return [[field,"Please enter PONumber!"]];
		
		return valid;
	}
	fn_checkSONumber = function(){
		var valid = true;
		var field;
		if($('#SOType').val() != 'Single'){
			field = $('#SONumber');
			if($('#PONumber').val() == field.val()){
				console.log('1');
				valid = false;
			}else if(field.val().substring(0,6) != $('#PONumber').val()){
				console.log('2');
				valid = false;
			}else if(field.val().substring(6,field.val().length) == ""){
				console.log('3');
				valid = false;
			}
		}
		if(!valid)
			return [[field,"Please enter correct SONumber!"]];
		
		return true;
	}
	
	// Form Submission (add/update)
	function fn_formSubmit(theForm){
		var formValues = $(theForm).serializeArray();
			//formValues.push({ name: "SONumber", value: $('#SONumber').val() });
			//formValues.push({ name: "PONumber", value: $('#PONumber').val() });
			formValues.push({ name: "SODate", value: $('#SODate').val() });
			formValues.push({ name: "SOType", value: $('#SOType').val() });
		var action = 'saveSO';
		ProcessRequest('purchase/SaleOrder/'+action,formValues,'fn_submitCallback');
	
	
	}
	
	fn_submitCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
		if(data[0]=='success'){
			loadbtnAction(fn_btnList(false,false,false,false,false,false,false,true));
			$('#SOType').addClass('readonly').attr('disabled',true);
			$('#SONumber').addClass('readonly').attr('readonly',true);
			//$('#PONumber-txt').val($('#PONumber').val());
			$('#PONumber').combobox("hide");
			$('#PONumber-txt').val($('#PONumber').val()).show();
			//$('#CustomerID').combobox("disable");
		}
	}
	
	$('#delete-btn').click(function(){
		$( "#SODeleteReason-div" ).dialog({
			resizable: false,
			height:300,
			width:490,
			modal: true,
			buttons: {
				"Deleted": function() {
					if($('#DeletedReason').val().length == 0){
						msgbox('Error Message','<p class="error"> Please entry Reason for delete</p>','DeletedReason');	
					}else{
						fn_callDeleteSOData($('#SONumber').val()); 
						$(this).dialog("destroy"); 
					}
				},
				Close: function() {
					$(this).dialog("destroy");  
				}
			}
		});
	
		
	});
	
	fn_callDeleteSOData = function(ponumber){
		var confirmso = ["fn_ConfirmDeleteSOData",ponumber];
		msgbox('Confirm Deletion','<p class="warning">Are you sure you want to delete?</p>',0,confirmso);
	}
	
	fn_ConfirmDeleteSOData = function(sonumber){
		ProcessRequest('purchase/SaleOrder/deleteSOData',{'SONumber':sonumber,'DeletedReason':$('#DeletedReason').val()},'fn_deleteSOCallback');
	}
	
	fn_deleteSOCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		if(data[0] == 'success'){
			$('#new-btn').click();
				tbl_SO.fnDraw();
		}
	}
	
	// fn_todoSO = function(action,ponumber){
		// switch(action){
			// case 'SODetail' : 
					// $('#PONumber').val(ponumber);
					// ProcessRequest('purchase/SaleOrder/GetSONumber',{'PONumber':ponumber},'fn_GetSONumber');
				// break;	

		// }
	// }
	fn_GetSONumber = function(data){
		$('#SONumber').val(data);
	}
	
	function fn_SODetailDataBind(){
		ProcessRequest('purchase/SaleOrder/viewSODetail',{'SONumber':$('#SONumber').val()},'fn_viewSODetail');
	}
	
	fn_viewSODetail = function(data){
		if(data){
			var obj = jQuery.parseJSON(data);
			var no = 0;
			var partnumber,Qty,UnitPrice,action,sodetailid,remainqty,uom;
			Qty =0;
			UnitPrice = 0;
			remainqty =0;
			tbl_SODetail.fnClearTable();
			for (var i=0; i<obj.length; i++){
				partnumber = obj[i]['PartNumber'];
				Qty = obj[i]['Quantity'];
				UnitPrice = obj[i]['UnitPrice'];
				sodetailid = obj[i]['SODetailID'];
				remainqty = obj[i]['RemainQty'];
				uom = obj[i]['UOM'];
				no = no + 1;
				if($('#hidstatusSO').val()== 'Deleted' || $('#hidstatusSO').val()== 'Completed'){
					action ='';
				}else{  
				action = '<a class="edit-btn" href="#" onclick="fn_todoSODetail(\'edit\',\''+sodetailid+'\',\''+Qty+'\',\''+remainqty+'\');" > Edit'+ 
						 '</a><a class="delete-btn" href="#" onclick="fn_todoSODetail(\'delete\',\''+sodetailid+'\',\''+Qty+'\',\''+remainqty+'\');" >Delete</a>';
				 }
				tbl_SODetail.fnAddData( [no,
										obj[i]['PartNumber'],
										Qty,
										UnitPrice,
										uom,
										parseInt(Qty)*parseFloat(UnitPrice),
										action
										]);
			}
			$('#tbl_SODetail-div').show();
			tbl_SODetail.fnAdjustColumnSizing();			
		}
	}

	fn_todoSODetail = function(action,sodetailid,qty,remainqty){
		switch(action){
			case 'edit' : 
				$('#hidSOID').val(sodetailid);
				$('#hidRemainQty').val(remainqty);
				$('#newQty').val(qty).focus();	
				$('#edit-sodetail').dialog({
					resizable: false,
					height:150,
					width:310,
					modal: true,
					buttons: {
						"Update": function() {
							ProcessRequest('purchase/SaleOrder/updateSODetailQty',
												{'SODetailID':$('#hidSOID').val(),
												'Quantity':$('#newQty').val()},'fn_updateSODetail');
						},
						Close: function() {
							$(this).dialog("destroy"); 
						}
					},
					close: function(){
						$(this).dialog("destroy");  
					},
					open : function(){
						//tbl_SO.fnDraw();				
					}
				});	
				break;	
			case 'delete' : 
					fn_callDeleteSODetailData(sodetailid);
				break;
		}

	}
	
	fn_updateSODetail = function(data){
		var data = explode("|",data);
		var focus ='';
		if(data[0] == 'success'){
			$('#edit-sodetail').dialog("destroy"); 
			fn_SODetailDataBind();
		}
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','');	 
	}
	fn_callDeleteSODetailData = function(sodetailid){
		var confirmsodetail = ["fn_ConfirmDeleteSODetailData",sodetailid];
		msgbox('Confirm Deletion','<p class="warning">Are you sure you want to delete?</p>',0,confirmsodetail);
	}
	
	fn_ConfirmDeleteSODetailData = function(sodetailid){
		ProcessRequest('purchase/SaleOrder/deleteSODetailData',{'SODetailID':sodetailid},'fn_deleteSODetailCallback');
	}
	
	fn_deleteSODetailCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		if(data[0] == 'success'){
			fn_SODetailDataBind();
		}
	}
	
	
	fn_getSONumber = function(data){
		$('#SONumber').val(data);
	}
	fn_LastDateOfCurrentMonth = function(){
		var now = new Date();
		//lastDayOfTheMonth = new Date(1900+now.getYear(), now.getMonth()+1, 0);
		//var month = lastDayOfTheMonth.getMonth() + 1;
		var month = now.getMonth() + 1;
		var day = now.getDate();
		var year = now.getFullYear();
		$('#SODate-hid').val(month+'/'+day+'/'+year);
		$('#SODate').val($('#SODate-hid').val());
	}
	fn_LastDateOfCurrentMonth();
	
	function fn_btnList(_viewlist,_seek,_new,_save,_addbn,_completeso,_delete,_printso){
		btn_list = {'viewlist-btn':_viewlist,
			   'seek-btn':_seek,
				'new-btn':_new,
				'save-btn':_save,
				'add-btn':_addbn,
				'completeso-btn':_completeso,
				'delete-btn':_delete
				,'printso-btn':_printso};
		return(btn_list);
	}
	
	
	
	ProcessRequest('services/DropDownLists/getComboBoxData',{'Customer':'Customer','PurchaseOrder':'PONumber4SO'},'fn_GetComboBoxdata');
	loadbtnAction(fn_btnList(false,false,false,false,true,true,true,true));
	$('#tbl_SODetail-div').hide();
			
	fn_ClearForm = function(){
		$('#newSO-frm :input').each(function(index) {
			if($(this).attr('type') !='button' && $(this).attr('id') !='SODate-hid' && $(this).attr('id') !='Name-hid'){
				$(this).val('');
			}
		});
		$('#SODate').val($('#SODate-hid').val());
		$('#Name').val($('#Name-hid').val());
		
		$( "#unlockso-btn" ).hide();
		tbl_SODetail.fnClearTable();
	}
	

	$("input[name=SONumber]").keydown(function(event) {
		if (event.which == 13 ) {
			if($('#SOType').is('[disabled]')){
				var sonumber = $('#SONumber').val().replace(/\s+/, "");
				$('#SONumber').val(sonumber);
				fn_searchSOData(sonumber);
			}
		}
   });
   
	
	function fn_disiableAll(status){
		$('#newSO-frm').find('input,select,textarea').each(function() {
			if($(this).attr('type') !='button' && $(this).attr('id') !='CustomerID-combox'){
				if(status){
					$(this).addClass('readonly').attr('readonly',true);
				}else{
					$(this).removeClass('readonly').removeAttr('readonly');
				}
			}
		});
		if(status){
			$('#CustomerID').combobox("disable");
		}else{
			$('#CustomerID').combobox("enable");
		}

	}
	
	$( ".datetime" ).datepicker({ 
		//minDate: -0, maxDate: "+2M",
		dateFormat: "mm-dd-yy"
	});
	
	$(".int").keydown(function(event) {
			// Allow only backspace, delete, enter, tab
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 ) {

		}else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
	
	$(".decimal").keydown(function(event) {
		// Allow only backspace, delete, enter, tab
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 
		|| event.keyCode == 190 || event.keyCode == 110 ) {
			//

			if(($(this).val().split(".")[0]).indexOf("00")>-1){
				$(this).val($(this).val().replace("00","0"));
			}
		}else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
			
// fn_viewSOData = function(data){
		// tbl_SO.fnClearTable();
		// if(data){
			// var no = 0;
			// var status='',action='',SONumber ='';
			// var obj = jQuery.parseJSON(data);
			// var newArray = [];			
			// for (var i=0; i<obj.length; i++){
				// no = no + 1;	
				// SONumber=obj[i]['SONumber'];
				// if(obj[i]['Status'] ==1 || obj[i]['Deleted'] ==1){
					// if(obj[i]['Deleted'] == 1){
						// status='<font style="color:red">Deleted</font>'
						// action='';
					// }else if(obj[i]['Status'] ==1){
						// status='<font style="color:green">Completed</font>';
						// action = '<a class="print-btn" href="#" onclick="fn_todoSO(\'print\',\''+SONumber+'\');" > print</a>';
					// }
				// }else{
					 // status='<font style="color:#cecece">Pending</font>';
					 // action = '<a class="edit-btn" href="#" onclick="fn_todoSO(\'edit\',\''+SONumber+'\');" > Edit</a>'+ 
					 // '<a class="delete-btn" href="#" onclick="fn_todoSO(\'delete\',\''+SONumber+'\');" >Delete</a>';
				// }
				 // newArray.push([no,
					   // SONumber,
					   // obj[i]['PONumber'],
					   // obj[i]['SOType'],
					   // obj[i]['CustomerName'],
					   // obj[i]['SODate'],
					   // obj[i]['TTCharges'],
					   // obj[i]['FreightCharges'],
					   // status,action]);
			
			// }
			
			// tbl_SO.fnAddData(newArray);
			// tbl_SO.fnAdjustColumnSizing();
		// }
	// }
	
});
</script>
