<h2 class="title">Purchase &raquo; Skip PR Approval</h2>

<div class="newentry" >
	<h3 class="form-title">Search Purchase Requisition</h3>
	<form class="globalform" id="newPR-frm" name="newPR">
		<ol>
			<div class="lfpane" >
				<li>
					<label>PR Type</label>
					<select id="PRType">
						<option value="JSIEXPENSE">JSIEXPENSE</option>
					</select>
				</li>
			</div>
			<div class="rfpane" >
				<li>
					<label>PRNumber</label>
					<select id="PRNumber">
					</select>
				</li>
				<li>
					<label>&nbsp;</label>
					<button id="clear-btn" name="clear-btn" data="" >
						<span class="ui-icon ui-icon-search"></span>Clear
					</button>
				</li>
			</div>			
			
		</ol>
	</form>
</div>
<div id="PRSkipApproval-div" >
	<h3 class="form-title">Purchase Requisition</h3>
	<form class="globalform" id="newPR-frm" name="newPR">	
		<ol>
			<div class="lfpane" >
				<li>
					<label>Supplier/Vendor</label>
					<label name="VendorName" style="border:1px solid #cecece;">&nbsp;</label>
				</li>

				<li>
					<label>Date Required</label>
					<label name="DateRequired" style="border:1px solid #cecece;">&nbsp;</label>

				</li>				
				<li>
					<label>Special Instruction</label>
					<label name="Comment" style="border:1px solid #cecece;">&nbsp;</label>
				</li>
				<li>
					<label>GST</label>
					<label name="GST" style="border:1px solid #cecece;">&nbsp;</label>
				</li>
				<li>
					<label>Required GM Approved</label>
					<label name="RequiredGMApproved" style="border:1px solid #cecece;">&nbsp;</label>
				</li>
			</div>
			<div class="rfpane" >				
				<li>
					<label>Requested By</label>
					<label name="RequestedBy" style="border:1px solid #cecece;">&nbsp;</label>
				</li>
				<li>
					<label>Purchased By</label>
					<label name="PurchasedBy" style="border:1px solid #cecece;">&nbsp;</label>
				</li> 
				<li>
					<label>Approved By</label>
					<label name="ApprovedBy" style="border:1px solid #cecece;">&nbsp;</label>
				</li>
				<li>
					<label>Discount</label>
					<label name="Discount" style="border:1px solid #cecece;">&nbsp;</label>
				</li>
				<li>
					<label>Remarks</label>
					<label name="Remarks" style="border:1px solid #cecece;">&nbsp;</label>
				</li>
				<li>
					<label>&nbsp;</label>
					<button id="skipApproval-btn" name="skipApproval-btn" data="" >
						<span class="ui-icon ui-icon-search"></span>Skip Approval
					</button>
				</li>
			</div>
		
		</ol>
		<div class="clear"></div>		
	</form>	
	<div class="clear"></div>


	<div  id="tbl_PRDetail-div" > 
		<table class="display" id="tbl_PRDetail">
			<thead><tr></tr></thead>
			<tbody></tbody>
			<tfoot><tr></tr></tfoot>
		</table> 
	</div>
</div>
<script type="text/javascript">
var tbl_PRDetail;
$(document).ready(function() {
	$("input:submit, input:button, button, .button").button().click(function(event){
		event.preventDefault();
	});
	
	/* Start Loading Data*/
	function fn_loadPRCombobox()
	{
		ProcessRequest('services/DropDownLists/PRNumberOnlyApprovalPRDMgr',{
					'PRType':$('#PRType').val()
					},'fn_PRNumberLoad');
	}
	fn_loadPRCombobox();		
	fn_PRNumberLoad = function(data){
	
		if(data ==1){
			$('#PRNumber').empty()
					.append('<option selected="selected" value=""></option>');
			$('#PRNumber').combobox();
		}else{
		
			var obj = jQuery.parseJSON(data);
			if (obj.length){
				$('#PRNumber').empty()
					.append('<option selected="selected" value=""></option>');
				$.each(obj, function(index,item) {
					$('#PRNumber')
					 .append($("<option ></option>")
					 .attr("value",item.ID)
					 .text(item.Value));
				});
			}

				
			$('#PRNumber').combobox({		
				selected: function(event, ui) {
					ProcessRequest('purchase/SkipPRApproval/viewPRData',{
							'PRNumber':$('#PRNumber').val()
							},'fn_PRDataLoadCallBack');
					tbl_PRDetail.fnDraw();
				}
			});
		}
	}
	
	
	/* Create Table For PRDetail */
	var PRDetail_fields = {'ID':'No',
						  'Department':'Department',
						  'PartNumber':'Part Number ',
						  'PartDesc1':'PartDesc1',
						  'RequestedQty':'Request Qty',
						  'UnitPrice':'Unit Price',
						  'UOM':'UOM',
						  'RequestedByName':'Request By'};
	
	
	
	
	$.each(PRDetail_fields,function(i,e){
		$('#tbl_PRDetail thead tr').append('<th>'+e+'</th>');
		$('#tbl_PRDetail tfoot tr').append('<th>'+e+'</th>');
	}); 

	tbl_PRDetail = $('#tbl_PRDetail').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		sAjaxSource: "purchase/SkipPRApproval/viewPRDetailData",  
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(PRDetail_fields)) } ); 
			aoData.push( { "name": "PRNumber", "value": $('#PRNumber').val() } );
			
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	fn_PRDataLoadCallBack = function(data){
		if(data ==1){
			$('#PRSkipApproval-div').hide();
		}else{
			var data = json_decode(data);
			$.each(data,function(i,e){
					$('[name='+i+']').text(e);
			});
			
			$('#PRSkipApproval-div').show();
			$('#PRNumber').combobox("disable");
			$("#PRType").attr("disabled", "disabled").addClass('readonly');
		}
	}
	
	$('#skipApproval-btn').click(function(){
		var confirmIR = ["fn_confirmSkipApproval",$('#PRNumber').val()];
		msgbox('Confirm','<p class="warning">Are you sure you want to Skip Manager Approve PR #'+$('#PRNumber').val()+'</p>',0,confirmIR);	
	
	});
	
	fn_confirmSkipApproval = function(prnumber){
		ProcessRequest('purchase/SkipPRApproval/actionSkipPRApproval',{
							'PRNumber':prnumber
							},'fn_actionSkipPRApprovalCallBack');
	}
	
	fn_actionSkipPRApprovalCallBack = function(data){
		if(data ==0){
			msgbox('success','<p class="success">Successfully Complete.</p>');	
			fn_Clearform();
		}else{
			msgbox('Error Message','<p class="error">Could not success.<br>Please contact support.</p>');				
		}
	}
	
	fn_Clearform = function(){
		$("#PRType").attr("disabled", "").removeClass('readonly');
		$('#PRSkipApproval-div').hide();
		$('#PRNumber').combobox("destroy");
		fn_loadPRCombobox();	
		
	}
	$('#clear-btn').click(function(){
		fn_Clearform();
	});
	$('#PRSkipApproval-div').hide();
});

</script>

