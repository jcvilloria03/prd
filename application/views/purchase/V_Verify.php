<h2 class="title">Purchase &raquo; Verify</h2>
<div class="newentry">
<h3 class="form-title">PRD Summary Report</h3>
	<form class="globalform" method="post" action="#">
		<ol>
			<div class="lfpane" >
				<li>
					<label>Date : </label> 
					<input type="text" id="from" name="from" readonly="readonly" />
				</li>
				<li>
					<label>Supplier: </label> 
					<select id="VendorID" name="VendorID">
					</select>
				</li>
			</div>
			<div class="rfpane" >
				<li>
					<label>To : </label> 
					<input type="text" id="to" name="to" readonly="readonly" />
				</li>
				<li>
					<label>Customer : </label> 
					<select id="CustomerID" name="CustomerID">
					</select>
				</li>
			</div>
			<div  class="lfpane" >
				<li>
					<label></label> 
					<button id="Search-btn" name="Search-btn" data="" ><span class="ui-icon ui-icon-search"></span>Search</button>
						<button id="ExportToPdf-btn" name="ExportToPdf-btn" data="" ><span class="ui-icon ui-icon-search">
					</span>Export to Excel</button>
				</li>
			</div>
		</ol>
		<div class="clear"></div>		
    </form>
</div>

<div id="tabs">
	<ul>
		<li><a href="#tabs-2">Sale Order Summary</a></li>
		<li><a href="#tabs-3">Received Summary Report( JSI Cost / Expense)</a></li>
	</ul>
	<div id="tabs-2">
		<div id="SaleOrderSummary-div" name="SaleOrderSummary-div" >
			<table class="display" id="tbl_SaleOrderSummary">
				<thead><tr></tr></thead>
				<tbody></tbody>
				<tfoot><tr></tr></tfoot>
			</table>
		</div>
	</div>
	<div id="tabs-3">
		<div id="ReceivedSummary-div" name="ReceivedSummary-div" >
			<table class="display" id="tbl_ReceivedSummary">
				<thead><tr></tr></thead>
				<tbody></tbody>
				<tfoot><tr></tr></tfoot>
			</table>
		</div>
		
	</div>
</div>

<script type="text/javascript">
var tbl_SaleOrderSummary,tbl_ReceivedSummary;
$(document).ready(function() {
	$("input:submit, input:button, button, .button").button().click(function(event){
		event.preventDefault();
	});
	
	
	$( "#from" ).datepicker({
		minDate: "07/01/2013", 
		maxDate: new Date(),
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
			$( "#to" ).datepicker( "option", "minDate", selectedDate ).focus();
		},onSelect : function() {
	
		}
		
	});
	
	$( "#to" ).datepicker({
		minDate: "07/01/2013", 
		maxDate: new Date(),
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
			$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		}
	});

	var d = new Date();
	var curr_date = d.getDate();
	var curr_month = d.getMonth() + 1;
	var curr_year = d.getFullYear();
	$( "#from" ).datepicker( "setDate", curr_month+'/01/'+curr_year );
	$( "#to" ).datepicker(  "setDate",curr_month+'/'+curr_date+'/'+curr_year );
		
	
	
	var SaleOrderSummary_fields = { 'QAConfirm':'QAConfirm',
								  'SONumber':'SONumber',
								  'PONumber':'PONumber',
								  'CustomerName':'CustomerName',
								  'DeliveryDate':'Date',
								  'VendorName':'VendorName',
								  'Invoice':'Invoice',
								  'PartNumber':'PartNumber',
								  'Description':'PartDesc1',
								  'SOQty':'Qty',
								  'BPrice':'BPrice',
								  'BCurrency':'BCurrency',
								  'BuyGST':'B GST',
								  'SPrice':'SPrice',
								  'SCurrency':'SCurrency',
								  'TTCharges':'TTCharges',
								  'FreightCharges':'FreightCharges',
								  'AdminCost':'AdminCost',
								  'GST':'S GST',
								  'TotalAmount':'TotalAmount'};
  
	$.each(SaleOrderSummary_fields,function(i,e){
		$('#tbl_SaleOrderSummary thead tr').append('<th>'+e+'</th>');
		$('#tbl_SaleOrderSummary tfoot tr').append('<th>'+e+'</th>');
	});
	
	$('#tbl_SaleOrderSummary,.display').css("white-space", "nowrap");
	tbl_SaleOrderSummary = $('#tbl_SaleOrderSummary').dataTable({
		//"sScrollY": "300px",
		bJQueryUI: true,
		sScrollX: "96%",
		aLengthMenu: [[10, 25, 50], [10, 25, 50]],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aaSorting: [[0,'desc']],
		aoColumnDefs : [{"aTargets" : [ 9 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 10 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 11 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 12 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 13 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 14 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 15 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 16 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 17 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 18 ],
			 sClass: "alignRight" 
			}],
		sAjaxSource: 'purchase/Verify/view_SOSummary', 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(SaleOrderSummary_fields)) } );	
			aoData.push( { "name": "from", "value": $('#from').val() } );	
			aoData.push( { "name": "to", "value":  $('#to').val() } );	
			aoData.push( { "name": "CustomerID", "value":  $('#CustomerID').val() } );	
			aoData.push( { "name": "VendorID", "value":  $('#VendorID').val() } );	
			aoData.push( { "name": "soConfirmType", "value":   $("#soConfirmType input:radio:checked").val()  } );	
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	 setTableHeaderHTML('tbl_SaleOrderSummary',' <div id="soConfirmType" style="width:400px;float:left;margin-left: 30px;"> Type: '+
						' <input type="radio" id="All" name="soConfirmType" checked="checked" value=""/><label for="All">All</label> '+
						' <input type="radio" id="Confirm" name="soConfirmType" value="Confirm"/><label for="Confirm">Confirm</label> '+
						' <input type="radio" id="NotConfirm" name="soConfirmType" value="NotConfirm"/><label for="Confirm">Have Not Confirmed Yet</label>'+
						' <button id="btn-confirm-so" name="btn-confirm-so" >Confirm</button></div>');

	
	 $("#soConfirmType input:radio").change(function(){
		tbl_SaleOrderSummary.fnDraw();
	});
 
	 // new FixedColumns( tbl_SaleOrderSummary, {
		 // "iLeftWidth": 50,
		 // "sLeftWidth": 'relative'
	 // } );
	
	fn_todoPR = function(action,pthis,prnumber){
		switch(action){
			case 'Checked' : 
				var nTr = $(pthis).parents('tr')[0];
				$(nTr).toggleClass('tropen');
			break;	
			case 'PrintPR' : 
				fn_PrintPR(pthis);
				break;
			case 'PrintPO' : 
				fn_PrintPO(pthis);
				break;
			case 'PrintSO':
				fn_PrintSO(pthis);
				break;
			case 'SOSummary' : 
				fn_RemoveSOSummary(pthis);
				break;
			case 'ReceivingSummary' : 
				fn_RemoveReceivingSummary(pthis);
				break;
		}
	}
	
	fn_RemoveSOSummary = function(data){
		var confirmso = ["fn_ConfirmRemoveSOSummary",data];
		msgbox('Confirm','<p class="warning">Are you sure you want to Confirm?</p>',0,confirmso);		
	}
	fn_ConfirmRemoveSOSummary = function(data){
		ProcessRequest('purchase/Verify/removeQASaleOrder',{'ID':data},'fn_ConfirmRemoveSOSummaryCallBack');
		console.log(data);
	}
	
	fn_ConfirmRemoveSOSummaryCallBack = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		tbl_SaleOrderSummary.fnDraw();
	}
	fn_RemoveReceivingSummary = function(data){
		var confirmso = ["fn_ConfirmRemoveReceivingSummary",data];
		msgbox('Confirm','<p class="warning">Are you sure you want to Confirm?</p>',0,confirmso);		
	}
	fn_ConfirmRemoveReceivingSummary = function(data){
		ProcessRequest('purchase/Verify/removeQAReceiving',{'ID':data},'fn_ConfirmRemoveReceivingSummaryCallBack');
		
	}
	fn_ConfirmRemoveReceivingSummaryCallBack = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
		tbl_ReceivedSummary.fnDraw();
	}
	 fn_PrintPO = function(PONumber){
		window.open("report/PrintPDF/POREPORT/"+PONumber,'_blank',false);
	 }
	 fn_PrintPR = function(PRNumber){
		window.open("report/PrintPDF/PRREPORT/"+PRNumber,'_blank',false);
	 }
	 fn_PrintSO = function(sonumber){	
		window.open("report/PrintPDF/SOREPORT/"+sonumber,'_blank',false);
	}
	
	$('#btn-confirm-so').click(function(){
		var data = tbl_SaleOrderSummary.fnGetData();
		var valid = false;
		var count = 0;
		$.each(data,function(i,e){
			if($(e[0]).is("input")){
				if($('#chkID_'+$(e[0]).val()).is(":checked")){
					count++;
					valid = true;
				}
			}
		});
		if(count ==0 ){
			msgbox('Error Message','<p class="error">Kindly Select the checkbox first before clicking confirm button.</p>');
		}else{
			var confirmso = ["fn_confirmVerifySO",count];
			msgbox('Confirm','<p class="warning">Are you sure you want to Confirm?</p>',0,confirmso);		
		}
		
	});
	
	fn_confirmVerifySO = function(){
		var data = tbl_SaleOrderSummary.fnGetData();
		var sodetailid ='';
		$.each(data,function(i,e){
			if($(e[0]).is("input")){
				if($('#chkID_'+$(e[0]).val()).is(":checked")){
					sodetailid = $(e[0]).attr('data') + '|' + sodetailid;
				}
			}
		});
		console.log(sodetailid);
		ProcessRequest('purchase/Verify/saveQASaleOrder',{'SODetailIDs':sodetailid},'fn_confirmVerifySOCallBack');
	}
	
	fn_confirmVerifySOCallBack= function(data){
		tbl_SaleOrderSummary.fnDraw();
		if(data == '0'){
			msgbox('Confirm','<p class="warning">Success.</p>');
		}else{
			msgbox('Confirm','<p class="warning">Kindly Try Again.</p>');
		}
	}
	
	
		
	var ReceivingSummaryReport_fields = { 'QAConfirm':'QAConfirm',
									'PONumber':'PONumber',
									'PRNumber':'PRNumber',
									'Department':'Department',
									'VendorName':'VendorName',
									'Invoice':'Invoice',
									'DeliveryDate':'DeliveryDate',
									'PartNumber':'PartNumber',
									'PartDesc1':'PartDesc1',
									'Qty':'Qty',
									'UnitPrice':'UnitPrice',
									'GST':'GST',
									'TotalAmount':'TotalAmount',
									'Currency':'Currency',};
	  
	$.each(ReceivingSummaryReport_fields,function(i,e){
		$('#tbl_ReceivedSummary thead tr').append('<th>'+e+'</th>');
		$('#tbl_ReceivedSummary tfoot tr').append('<th>'+e+'</th>');
	});
	
	$('#tbl_ReceivedSummary,.display').css("white-space", "nowrap");
	tbl_ReceivedSummary = $('#tbl_ReceivedSummary').dataTable({
		bJQueryUI: true,
		sScrollX: "96%",
		aLengthMenu: [[10, 25, 50], [10, 25, 50]],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aaSorting: [[0,'desc']],
		aoColumnDefs : [{"aTargets" : [ 8 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 9 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 10 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 11 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 12 ],
			 sClass: "alignRight" 
			}],
		sAjaxSource: 'purchase/Verify/view_ReceivingSummary', 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(ReceivingSummaryReport_fields)) } );
			aoData.push( { "name": "PRType", "value": $("#radiotype1 input:radio:checked").val() } ); 			
			aoData.push( { "name": "from", "value": $('#from').val() } );	
			aoData.push( { "name": "to", "value":  $('#to').val() } );	
			aoData.push( { "name": "VendorID", "value":  $('#VendorID').val() } );	
			aoData.push( { "name": "ReceivingConfirmType", "value":   $("#ReceivingConfirmType input:radio:checked").val()  } );	
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
		
	 setTableHeaderHTML('tbl_ReceivedSummary',' <div id="ReceivingConfirmType" style="width:400px;float:left;margin-left: 30px;"> Type: '+
						' <input type="radio" id="All" name="ReceivingConfirmType" checked="checked" value=""/><label for="All">All</label> '+
						' <input type="radio" id="Confirm" name="ReceivingConfirmType" value="Confirm"/><label for="Confirm">Confirm</label> '+
						' <input type="radio" id="NotConfirm" name="ReceivingConfirmType" value="NotConfirm"/><label for="Confirm">Have Not Confirmed Yet</label>'+
						' <button id="btn-confirm-received" name="btn-confirm-received" >Confirm</button></div>');

	 $("#ReceivingConfirmType input:radio").change(function(){
		tbl_ReceivedSummary.fnDraw();
	});
 
						
	$('#btn-confirm-received').click(function(){
		console.log('Click Receiving Button');
		var data = tbl_ReceivedSummary.fnGetData();
		var valid = false;
		var count = 0;
		$.each(data,function(i,e){
			if($(e[0]).is("input")){
				if($('#chkID_'+$(e[0]).val()).is(":checked")){
					count++;
					valid = true;
				}
			}
		});
		if(count ==0 ){
			msgbox('Error Message','<p class="error">Kindly Select the checkbox first before clicking confirm button.</p>');
		}else{
			var confirmso = ["fn_confirmVerifyReceiving",count];
			msgbox('Confirm','<p class="warning">Are you sure you want to Confirm?</p>',0,confirmso);		
		}
	});
	
	fn_confirmVerifyReceiving = function(data){
		console.log(data);
		var data = tbl_ReceivedSummary.fnGetData();
		var poreceivingdetailid ='';
		$.each(data,function(i,e){
			if($(e[0]).is("input")){
				if($('#chkID_'+$(e[0]).val()).is(":checked")){
					poreceivingdetailid = $(e[0]).attr('data') + '|' + poreceivingdetailid;
				}
			}
		});
		console.log(poreceivingdetailid);
		ProcessRequest('purchase/Verify/saveQAReceiving',{'POReceivingDetailIDs':poreceivingdetailid},'fn_confirmVerifyReceivingCallBack');
	}
	
	fn_confirmVerifyReceivingCallBack = function(data){
		tbl_ReceivedSummary.fnDraw();
		if(data == '0'){
			msgbox('Confirm','<p class="warning">Success.</p>');
		}else{
			msgbox('Confirm','<p class="warning">Kindly Try Again.</p>');
		}
	}
	
	$('#Search-btn').click(function(){
		var active = $( "#tabs" ).tabs( "option", "active" );
		if(active ==0){
			tbl_SaleOrderSummary.fnDraw();
		}else if(active ==1){
			tbl_ReceivedSummary.fnDraw();
		}
	});
	
	
	 $( "#tabs" ).tabs({
		  activate: function(event ,ui){
				if(ui.newTab.index() ==0){
					$('#CustomerID').combobox("enable");
					tbl_SaleOrderSummary.fnDraw();
				}else if(ui.newTab.index() ==1){
					$('#CustomerID').combobox("disable");
					tbl_ReceivedSummary.fnDraw();
				}
			}
	 });
	 
	/* Create Combobox for Department, PartNumber, Vendor */
	fn_ComboBoxload = function(data){
		var obj = jQuery.parseJSON(data);
		loadComboBoxData("VendorID",obj.Vendor,true);
		loadComboBoxData("CustomerID",obj.Customer,true);
		
		$('#CustomerID').combobox("disable");
	}
	/* End ComboBox */
	
	/* Start Loading Data*/
	ProcessRequest('services/DropDownLists/getComboBoxdata',{
					'Vendor':'Vendor','Customer':'Customer'
					},'fn_ComboBoxload');
	
		$('#ExportToPdf-btn').click(function(){
	
		console.log('Export');
		if($('#from').val() ==''){
			msgbox('Error Message','<p class="error">Please select from date.</p>');		
			return;
		}else if($('#to').val() ==''){
			msgbox('Error Message','<p class="error">Please select to date.</p>');		
			return;
		}
		
		var active = $( "#tabs" ).tabs( "option", "active" );
		var reporttype ='',PRType='';
		if(active ==0){
			reporttype = 'SOSummary';
		}else if(active ==1){
			reporttype = 'POOpeningSummary';
			//PRType= $("#ReceivingConfirmType input:radio:checked").val();
			PRType= '';
		}

	  var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "report/PrintExcel/exportSummaryReport";
		mapForm.style.display='none';
		
		var CustomerID = document.createElement("input");
		CustomerID.type = "text";
		CustomerID.name = "CustomerID";
		CustomerID.value = $('#CustomerID').val();		
		var VendorID = document.createElement("input");
		VendorID.type = "text";
		VendorID.name = "VendorID";
		VendorID.value = $('#VendorID').val();		
		var Type = document.createElement("input");
		Type.type = "text";
		Type.name = "reporttype";
		Type.value = reporttype;
		var PR_Type = document.createElement("input");
		PR_Type.type = "text";
		PR_Type.name = "PRType";
		PR_Type.value = PRType;
		var From = document.createElement("input");
		From.type = "text";
		From.name = "from";
		From.value = $('#from').val();
		var To = document.createElement("input");
		To.type = "text";
		To.name = "to";
		To.value = $('#to').val();
		
		mapForm.appendChild(CustomerID);
		mapForm.appendChild(VendorID);
		mapForm.appendChild(Type);
		mapForm.appendChild(PR_Type);
		mapForm.appendChild(From);
		mapForm.appendChild(To);

		document.body.appendChild(mapForm);

		mapForm.submit();

		
	});
	
	
});

</script>

