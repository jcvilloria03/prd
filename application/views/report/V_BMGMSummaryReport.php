
<div class="newentry">
<h3 class="form-title">Report</h3>
	<form class="globalform" method="post" action="#">
		<ol>
		<!--	<li style="width:100%">
				<label style="margin-left: 5%;">Report Type </label> 
					<div id="div-ReportType">
						<input type="checkbox" id="chkSummary" checked /><label for="chkSummary" >Summary</label>
						<input type="checkbox" id="chkPurchaseFromSummary" checked /><label for="chkPurchaseFromSummary" style=" width: 180px;">Purchase From Supplier</label>
						<input type="checkbox" id="chhkBillingToCustomer" checked /><label for="chhkBillingToCustomer" >Billing to customer</label>
						<input type="checkbox" id="chkJSICost" checked /><label for="chkJSICost"> JSI COST</label>
					</div>
			</li>-->
			<div class="lfpane" >
				<li>
					<label>From </label> 
					<input type="text" id="from" name="from" />
				</li>
			</div>
			<div class="rfpane" >
				<li>
					<label>To : </label> 
					<input type="text" id="to" name="to" />
				</li>
			</div>
			<div  class="lfpane" >
				<li>
					<label></label> 
					<button id="Search-btn" name="Search-btn" data="" ><span class="ui-icon ui-icon-search"></span>Search</button>
				</li>
			</div>
		</ol>
		<div class="clear"></div>		
    </form>
</div>

<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Summary</a></li>
		<li><a href="#tabs-2">Purchase From Supplier</a></li>
		<li><a href="#tabs-3">Billing To Customer</a></li>
		<li><a href="#tabs-4">JSI Cost For Each Department</a></li>
	</ul>
	<div id="tabs-1">
	<div id='div-Accomplishment' style="float:left; width: 40%;"  >
		<a style="cursor: hand;"  >
			<h3>Accomplishment</h3>
		</a> 
		<table class="dashboardtbl" id="Internal Request" style="min-width:350px;width:100%;">
			<thead>
				<tr>
					<th align="left"></th> 
					<th align="right" class="completed">SGD</th>
					<th align="right" class="completed">USD</th>
				</tr>
			</thead><tbody>
				<tr>
					<td>Billing TO Customer</td>
					<td align="right">
						<label id="Billing_SGD"></label>
					</td>
					<td align="right">
						<label id="Billing_USD"></label>
					</td>
				</tr>
				<!--<tr>
					<td>PM Profit Margin</td>
					<td>
						0
					</td>
					<td>
						0
					</td>
				</tr>-->
				<tr>
					<td>JSI Cost & EXPENSE</td>
					<td align="right">
						<label id="JSI_SGD"></label>
					</td>
					<td align="right">
						<label id="JSI_USD"></label>
					</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div id='div-POSOQty' style="float:right; width: 40%;" >
		<a style="cursor: hand;"  >
			<h3>PO & SO QTY</h3>
		</a> 
		<table class="dashboardtbl" id="Internal Request" style="min-width:350px;width:100%;">
			<thead>
				<tr>
					<th align="left"></th> 
					<th align="right" class="completed">Total Qty</th>
				</tr>
			</thead><tbody>
				<tr>
					<td>Normal PO</td>
					<td align="right"><label id="lblNormalPO"></label></td>
				</tr>
				<tr>
					<td>JSI COST</td>
					<td align="right"><label id="lblJSICost"></label></td>
				</tr>
				<tr>
					<td>JSI EXPENSE</td>
					<td align="right"><label id="lblJSIExpense"></label></td>
				</tr>
				<tr>
					<td>Sale Order</td>
					<td align="right"><label id="lblSaleOrder"></label></td>
				</tr>
			</tbody>
		</table>
			
	</div>
	<div class="clear"></div>	
	</div>
	<div id="tabs-2">
		<div  id="div-PurchaseFromSupplier" title="Purchase From Supplier" name="div-PurchaseFromSupplier"  >
			<table class="display" id="tbl_PurchaseFromSupplier">
				<thead><tr></tr></thead>
				<tbody></tbody>
				<tfoot><tr></tr></tfoot>
			</table>
		</div>
	</div>
	<div id="tabs-3">
		<div  id="div-BillingToCustomer"  title="Billing To Customer"  name="div-BillingToCustomer"  >
			<table class="display" id="tbl_BillingToCustomer">
				<thead><tr></tr></thead>
				<tbody></tbody>
				<tfoot><tr></tr></tfoot>
			</table>
		</div>
	</div>
	<div id="tabs-4">
		<div id="div-JsiCostEachDept"  title="Jsi Cost For Each Department" name="div-JsiCostEachDept" >
			<table class="display" id="tbl_JsiCostEachDept">
				<thead><tr></tr></thead>
				<tbody></tbody>
				<tfoot><tr></tr></tfoot>
			</table>
		</div>
	</div>
</div>

<script type="text/javascript">
var tbl_PurchaseFromSupplier,tbl_BillingToCustomer,tbl_JsiCostEachDept;
$(document).ready(function() {
	$("input:submit, input:button, button, .button").button().click(function(event){
		event.preventDefault();
	});
  
	var PurchaseFromSupplier_fields = { 'VendorName':'SupplierName',
									'SGD':'SGD',
									'USD':'USD'};
							  
	$.each(PurchaseFromSupplier_fields,function(i,e){
		$('#tbl_PurchaseFromSupplier thead tr').append('<th>'+e+'</th>');
		$('#tbl_PurchaseFromSupplier tfoot tr').append('<th>'+e+'</th>');
	});
	
	tbl_PurchaseFromSupplier = $('#tbl_PurchaseFromSupplier').dataTable({
		bJQueryUI: true,
		sScrollX: "96%",
		aLengthMenu: [[10, 25, 50], [10, 25, 50]],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aoColumnDefs : [  {"aTargets" : [ 1 ],
			 "bSearchable": false,
			  "bSortable" : false,
			   sClass: "alignRight" 
			},{"aTargets" : [ 2 ],
			 "bSearchable": false,
			 "bSortable" : false,
			  sClass: "alignRight" 
			}],
		sAjaxSource: 'report/BMGMSummaryReport/view_PurchaseFromSupplier', 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(PurchaseFromSupplier_fields)) } );	
			aoData.push( { "name": "from", "value": $('#from').val() } );	
			aoData.push( { "name": "to", "value":  $('#to').val() } );	
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	setTableHeader('tbl_PurchaseFromSupplier','Purchase From Supplier');
	
	var BillingToCustomer_fields = { 'CustomerName':'CustomerName',
									'SGD':'SGD',
									'USD':'USD'};
							  
	$.each(BillingToCustomer_fields,function(i,e){
		$('#tbl_BillingToCustomer thead tr').append('<th>'+e+'</th>');
		$('#tbl_BillingToCustomer tfoot tr').append('<th>'+e+'</th>');
	});
	
	tbl_BillingToCustomer = $('#tbl_BillingToCustomer').dataTable({
		bJQueryUI: true,
		sScrollX: "96%",
		aLengthMenu: [[10, 25, 50], [10, 25, 50]],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aoColumnDefs : [  {"aTargets" : [ 1 ],
			 "bSearchable": false,
			  "bSortable" : false,
			   sClass: "alignRight" 
			},{"aTargets" : [ 2 ],
			 "bSearchable": false,
			 "bSortable" : false,
			  sClass: "alignRight" 
			}],
		sAjaxSource: 'report/BMGMSummaryReport/view_BillingToCustomer', 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(BillingToCustomer_fields)) } );	
			aoData.push( { "name": "from", "value": $('#from').val() } );	
			aoData.push( { "name": "to", "value":  $('#to').val() } );	
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	setTableHeader('tbl_BillingToCustomer','Billing To Customer');
	
	var JsiCostEachDept_fields = { 'Department':'Department',
									'SGD':'SGD',
									'USD':'USD'};
							  
	$.each(JsiCostEachDept_fields,function(i,e){
		$('#tbl_JsiCostEachDept thead tr').append('<th>'+e+'</th>');
		$('#tbl_JsiCostEachDept tfoot tr').append('<th>'+e+'</th>');
	});
	
	tbl_JsiCostEachDept = $('#tbl_JsiCostEachDept').dataTable({
		bJQueryUI: true,
		sScrollX: "96%",
		aLengthMenu: [[10, 25, 50], [10, 25, 50]],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aoColumnDefs : [  {"aTargets" : [ 1 ],
			 "bSearchable": false,
			  "bSortable" : false,
			   sClass: "alignRight" 
			},{"aTargets" : [ 2 ],
			 "bSearchable": false,
			 "bSortable" : false,
			  sClass: "alignRight" 
			}],
		sAjaxSource: 'report/BMGMSummaryReport/view_JSICost4Depat', 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(JsiCostEachDept_fields)) } );	
			aoData.push( { "name": "from", "value": $('#from').val() } );	
			aoData.push( { "name": "to", "value":  $('#to').val() } );	
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	setTableHeader('tbl_JsiCostEachDept','JSI Cost For Each Department');
		
	$( "#from" ).datepicker({
		minDate: "07/01/2013", 
		maxDate: new Date(),
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
			$( "#to" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	
	$( "#to" ).datepicker({
		minDate: "07/01/2013", 
		maxDate: new Date(),
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
			$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		}
	});
	
	$('#Search-btn').click(function(){
	
		var active = $( "#tabs" ).tabs( "option", "active" );
		if(active ==0){
			fn_GetSummaryReport();
		}else if(active ==1){
			tbl_PurchaseFromSupplier.fnDraw();
		}else if(active ==2){
			tbl_BillingToCustomer.fnDraw();
		}else if(active ==3){
			tbl_JsiCostEachDept.fnDraw();
		}
		
		
	});

	  $( "#tabs" ).tabs({
		  activate: function(event ,ui){
				if(ui.newTab.index() ==0){
					fn_GetSummaryReport();
				}else if(ui.newTab.index() ==1){
					tbl_PurchaseFromSupplier.fnDraw();
				}else if(ui.newTab.index() ==2){
					tbl_BillingToCustomer.fnDraw();
				}else if(ui.newTab.index() ==3){
					tbl_JsiCostEachDept.fnDraw();
				}
				
			}
	 });
	 
	 fn_GetSummaryReport = function(){
		 ProcessRequest('report/BMGMSummaryReport/getSummaryData',{'from':$('#from').val(),'to':$('#to').val()},function(data){
			var obj = jQuery.parseJSON(data);
			$('#lblNormalPO').html(obj.PONORMAL);
			$('#lblJSICost').html(obj.JSICOST);
			$('#lblJSIExpense').html(obj.POJSIEXPENSE);
			$('#lblSaleOrder').html(obj.SOCount);
			
			
			$('#Billing_SGD').html(obj.Billing_SGD);
			$('#Billing_USD').html(obj.Billing_USD);
			$('#JSI_SGD').html(obj.JSI_SGD);
			$('#JSI_USD').html(obj.JSI_USD);
			
		});
	}
});

</script>