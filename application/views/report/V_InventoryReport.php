<h2 class="title">Reports &raquo; Inventory Report</h2>
<div class="clear"></div>
<div>
	<h3>Inventory Report</h3>
	<form class="globalform" id="inventoryreport-frm" name="inventoryreport-frm">
		<ol>
			<li>
				<label>Type</label>
				<select name="Type" id="Type">
					<option value="1">Week Usage</option>
					<option value="2">Week Usage & Count</option>
				</select>
			</li>
			<li>
				<label>Customer</label>
				<select name="Customer" id="Customer"></select>
			</li>
			<li>
				<label>From</label>
				<input  type="text" name="From" id="From" class="datetime" readonly="readonly"/>
				<label style="text-align:center">To</label>
				<input  type="text" name="To" id="To" class="datetime" readonly="readonly"/>
			</li>
			<div style="width: 100%;">
				<li style="float: left; margin-bottom: 15px; margin-right: 110px;">
					<label>&nbsp;</label>
					<input type="button" id="Submit-btn" name="Submit-btn" value="Submit"/>
					<input type="button" id="Export-btn" name="Export-btn" value="Export"/>
				</li>
			</div>
		</ol>
		<br class="clear">		
	</form>
</div>
<div id="divInventory">
</div>

<script type="text/javascript">

$(document).ready(function(){
	$('input:submit, input:button, button, .button').button();
	
	ProcessRequest('services/DropDownLists/getComboBoxdata',{'Customer':'Customer'},'fn_ComboBoxload');

	fn_ComboBoxload = function(data){
		var obj = jQuery.parseJSON(data);
		loadComboBoxData("Customer",obj.Customer,true);
	}

	$('#Submit-btn').click(function(){
		var rules =	[
				"required,Type,Please select Report Type.",
				"required,Customer,Please select customer.",
				"required,From,Please select From Date.",
				"required,To,Please select To Date.",
				];
				
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = fn_SubmitFunction;
		rsv.displayType= "display-html";
		rsv.validate(document.forms['inventoryreport-frm'],rules); 

	});
	
	fn_SubmitFunction =function(){
		var formValues =[];
		formValues.push({ name: "Type", value: $('#Type').val() });
		formValues.push({ name: "Customer", value: $('#Customer').val() });
		formValues.push({ name: "From", value: $('#From').val() });
		formValues.push({ name: "To", value: $('#To').val() });
		ProcessRequest('report/InventoryReport/viewInventoryReport',formValues,'fn_submitCallBack');
	}	
	
	fn_submitCallBack = function(data){
		if(data==2){
			msgbox("Error",'<p class="error">No record found</p>','Customer');	
		}else{
			var obj = jQuery.parseJSON(data);
			var count=1;
			var table = "<table class='dataTable'  border='1' width='100%' style='border: 1px solid #DAECF4;'> <tr bgcolor='#2779AA'><td rowspan='2'>SN</td><td rowspan='2'>PartNumber</td><td rowspan='2'>Desc </td><td rowspan='2'>Supplier</td><td rowspan='2'>LeadTime</td>";
			var sn,oldsn,partno,partdesc,supplier,leadtime,reportdate,orderqty,usuage,countqty,reporttype;
			var balance;
			reporttype = $('#Type').val();
			supplier = '&nbsp;';
			sn = 1;
			oldsn = 0;
			$.each(obj["Header"],function(a,b){
				count = count + 1;
				reportdate = b;
				if(reporttype== '1'){
					table = table + '<td align="center">' + b +'</td>';
				}else{
					table = table + '<td colspan="3" align="center">' + b +'</td>';
				}
			});
			table = table  +"</tr><tr  bgcolor='#2779AA'>";
			for(var i=1;i<count;i++){
				if(reporttype== '1'){
					table = table + '<td>Usuage</td>';
				}else{
					table = table + '<td>Order</td><td>Usuage</td><td>Count</td>';
				}
				
			}
			table = table  +'</tr>';
			$.each(obj["Body"],function(a,b){
				
				table = table  +"<tr bgcolor='#F2F2F2'>";
				
				$.each(b[0],function(c,d){
					partno = d[0];
					partdesc=d[1];
					leadtime = d[2];
					orderqty = d[4];
					usuage = d[5];
					countqty = d[6];
					supplier = d[7];
					balance =  d[8];
					if(sn!=oldsn){
						oldsn=sn;
						table = table  + '<td>' + sn +'</td><td>'+partno+'</td><td>'+partdesc+'</td><td>' + supplier +'</td><td>'+leadtime+'</td>';
					}
					if(reporttype== '1'){
						table = table  + '<td>'+usuage+'</td>';
					}else{
						table = table  + '<td>' + orderqty +'</td><td>'+usuage+'</td><td>' + countqty + '</td>';
					}
					
					
				});
				sn = sn + 1;
				table = table  +'</tr>';
			});
			table = table+'</table>'

			$('#divInventory').html(table);
		}
	}
	$('#Export-btn').click(function(){
	    var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "report/PrintExcel/Inventory4WeeklyUsuageReport";
		mapForm.style.display='none';
		
		var Type = document.createElement("input");
		Type.type = "text";
		Type.name = "Type";
		Type.value = $('#Type').val();
		var Customer = document.createElement("input");
		Customer.type = "text";
		Customer.name = "Customer";
		Customer.value = $('#Customer').val();
		var From = document.createElement("input");
		From.type = "text";
		From.name = "From";
		From.value = $('#From').val();
		var To = document.createElement("input");
		To.type = "text";
		To.name = "To";
		To.value = $('#To').val();
		mapForm.appendChild(Type);
		mapForm.appendChild(Customer);
		mapForm.appendChild(From);
		mapForm.appendChild(To);

		document.body.appendChild(mapForm);

		map = window.open("", "Map", "status=0,title=0,height=600,width=800,scrollbars=1");

		if (map) {
			mapForm.submit();
		} else {
			alert('You must allow popups for this map to work.');
		}
		
		
	});
	
	$( "#From" ).datepicker({
		dateFormat: "mm-dd-yy",
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
			$( "#To" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	
	 $( "#To" ).datepicker({
		dateFormat: "mm-dd-yy",
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
		$( "#From" ).datepicker( "option", "maxDate", selectedDate );
		}
	});

});
</script>
