<?php
	$CurrentName = $POData->CurrentName;
	$PRType = $POData->PRType;
	$PONumber = $POData->PONumber;
	$VendorName = $POData->VendorName;
	$VendorAddress1 = $POData->VendorAddress1;
	$VendorAddress2 = $POData->VendorAddress2;
	$VendorAddress3 = $POData->VendorAddress3;
	
	
	$VendorContactperson = $POData->VendorContactperson;
	$VendorTelNo = $POData->VendorTelNo;
	$VendorFaxNo = $POData->VendorFaxNo;
	$PaymentTermName = $POData->PaymentTermName;
	$DeliveryTermName = $POData->DeliveryTermName;
	
	$VendorAddress1 = $POData->VendorAddress1;
	$VendorAddress2 = $POData->VendorAddress2;
	$VendorAddress3 = $POData->VendorAddress3;
	$ShipToAddress = $POData->ShipToAddress;
	$BillingAddress = $POData->BillingAddress;
	$Attention = $POData->Attention;
	$Ext = $POData->Ext;
	$ReqDeliveryDate = $POData->ReqDeliveryDate;
	$Remarks = $POData->Remarks;
	$Buyer = $POData->Buyer;
	$BuyerExt = $POData->BuyerExt;
	$Discount = $POData->Discount;
	
	$GST = $POData->GST;
	$CompletedPODate =  $POData->CompletedPODate;
	$BaseCurrency = $PODetail[0]['BaseCurrency'];
	$Department = $PODetail[0]['Department'];
	
	
	if($BaseCurrency =="USD"){
		$symcurrency = 'US$';
	}else if($BaseCurrency =="SGD"){
		$symcurrency ='S$';
	}else{
		$symcurrency ='$';
	}

?>

<html>
<head>
<style>
@page {
  font-family: arial,sans-serif;

}
table{
	font-size: <?php echo((count($PODetail)<20)?'11':'9');?>px;
}
</style>
</head>
<body>
<table style="border:0;font-size: 9x;"  border="0" cellspacing="0" border="0" cellpadding="0" width="1100"><tr>
	<td align="right"> Print Date :<?php echo date("m/d/y G.i:s<br>", time());?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
</table>
<table border="0" cellspacing="0"  cellpadding="0" width="100%"><tr><td>
	<table style="border:.1px solid black" border="0" cellspacing="0"  cellpadding="2px" width="100%"><tr><td>
	<table  style="border:.1px solid black" border="0" cellspacing="0"  cellpadding="2px" width="100%">
	<tr>
		<td colspan="6" align="center"  valign="middle">
			&nbsp;<br/><h3> JSI LOGISTICS (S) PTE LTD</h3>&nbsp;
		</td>
	</tr>
	<tr>
		<td colspan="6" align="center" >
				&nbsp;	<br/>PURCHASE ORDER<br/>&nbsp;	
		</td>
	</tr>
	<tr>
		<td colspan="6" valign="top">	
<table   border="0" cellspacing="0"  cellpadding="3px" width="1000">
	<tr>
		<td width="60%" valign="top">
			<table width="100%" cellspacing="0">
				<tr>
					<td>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td>
						<b>NAME OF SUPPLIER : </b>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo($VendorName);?>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<b>ADDRESS :</b>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo($VendorAddress1."<br />".	$VendorAddress2 .'<br />'.$VendorAddress3 ); ?>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<b>CONTACTS:</b>
					</td>
				</tr>
				<tr>
					<td>
						<b>ATTN :</b> 
						<?php echo($VendorContactperson);?>
					</td>
				</tr>
				<tr>
					<td>
						<b>TEL : </b>
						<?php echo($VendorTelNo);?>
					</td>
				</tr>
				<tr>
					<td>
						<b>FAX :</b>
						<?php echo($VendorFaxNo);?>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<b>Payment Terms :</b>
						<?php echo($PaymentTermName);?>
					</td>
				</tr>
				<tr>
					<td>
						<b>Shipment Terms : </b>
						<?php echo($DeliveryTermName);?>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
		</td>
		<td width="40%" valign="top">
			<table width="100%" cellspacing="0">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<b>PURCHASE ORDER NO :</b>
						<?php echo($PONumber);?>
					</td>
				</tr>
				<tr>
					<td><b>DATE : </b>
						<?php echo($CompletedPODate);?>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td> 
						<b>BILL TO ADDRESS :</b>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo(str_replace(Chr(13),"<br>",$BillingAddress) ); ?>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<b>SHIP TO ADDRESS:</b>
					</td>
				</tr>
				<tr>
					<td>	
						<?php echo(str_replace(Chr(13),"<br>",$ShipToAddress)); ?>
					</td>
				</tr>
			
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<b>Attention :</b>
						<?php echo($Attention);?>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<b>Ext:</b>
						<?php echo($Ext);?>
					</td>
				</tr>
				<tr>
					<td>
						<b>Required Delivery Date :</b>
						<?php echo(($ReqDeliveryDate=='01 Jan 2099')?'Blanket Order':$ReqDeliveryDate);?>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
		
		</td>
	</tr>
	<tr>
		<td align="center" align="center"  style="border:.1px solid black">
			<b>S/N</b>
		</td>
		<td align="center" align="center"   style="border:.1px solid black">
			<b>QTY</b>
		</td>
		<td align="center" align="center"  style="border:.1px solid black">
			<b>UOM</b>
		</td>
		<td align="center" align="center"   width="7cm"  style="border:.1px solid black">
			<b>DESCRIPTION </b>
		</td>
		<td align="right" align="center"  style="border:.1px solid black">
			<b>UNIT PRICE(<?php echo($symcurrency);?>)</b>
		</td>
		<td align="right" align="center" style="border:.1px solid black">
			<b>AMOUNT(<?php echo($symcurrency);?>)</b>
		</td>
	</tr>

<?php 

$totalAmount=0;
if(isset($PODetail)){

if(trim($Department) != '0' && trim($Department) != '' &&  trim($Department) != 'NULL'){
	$DepName = $PODetail[0]['Department'];
	 echo('<tr><td style="border-right:.1px solid black;">&nbsp;</td>
			<td style="border-right:.1px solid black;">&nbsp;</td>
			 <td style="border-right:.1px solid black;">&nbsp;</td>
			 <td style="border-right:.1px solid black;"><b>Department : '.$DepName.'</b></td>
			 <td style="border-right:.1px solid black;">&nbsp;</td>
			 <td style="border-right:.1px solid black;">&nbsp;</td></tr>');
} 
$rowsCount=0;
$sn = 0;
foreach ($PODetail as $val)
{
$rowsCount +=1;
$sn +=1;
$Quantity = $val['Quantity'];
$PartNumber   =  $val['PartNumber'];
$PartName = $val['PartName'];
$PartName2   =  $val['PartName2'];
$UnitPrice    = $val['UnitPrice'];
$Amount    		= $val['Amount'];
$UOM =  $val['UOM'];

$Amount1 = explode('.', $Amount);

$totalAmount += $Amount;

if(trim($Department) != '0' && trim($Department) != '' &&  trim($Department) != 'NULL'){
	if($DepName != $val['Department']){
		$rowsCount +=1;
		$DepName = $val['Department'];
		 echo('<tr><td style="border-right:.1px solid black;">&nbsp;</td>
			 <td style="border-right:.1px solid black;">&nbsp;</td>
			 <td style="border-right:.1px solid black;">&nbsp;</td>
			 <td style="border-right:.1px solid black;"><b>Department : '.$DepName.'</b></td>
			 <td style="border-right:.1px solid black;">&nbsp;</td>
			 <td style="border-right:.1px solid black;">&nbsp;</td></tr>');
	
	}
}
?>
	<tr>
		<td align="center" style="border-right:.1px solid black;">
			<?php echo($sn);?>
		</td>
		<td align="center" style="border-right:.1px solid black;">
			<?php echo($Quantity);?>
		</td>
		<td align="center" style="border-right:.1px solid black;">
			<?php echo($UOM);?>
		</td>
		<td align="left" style="border-right:.1px solid black;">
			<?php echo($PartName);
				if($PartName2 <> ''){
					echo('<br />'.$PartName2);
					$rowsCount +=1;
					}
			if($PRType =='NORMAL'){
				echo("<br/>P/N:".$PartNumber);
				$rowsCount +=1;
			}
			?>
		</td>
		<td align="center" style="border-right:.1px solid black;">
			<?php echo(number_format($UnitPrice, numberFormatDecimal($UnitPrice), '.', ''));?>
		</td>
		<td align="center" >
			<?php echo(number_format($Amount, 2, '.', ''));?>
		</td>
	</tr>
<?php } 
}else{ 
	
?>
	<tr>
		<td colspan="6" align="center"> <b>No Data</b> </td>
	</tr>
<?php
}
?>

<?php
		if($rowsCount<20){
			$rowsCount = 20 - $rowsCount;
			for ($i=1; $i<=$rowsCount; $i++)
			{
				 echo('<tr><td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td></tr>');
			}
		}

$totalAmount = $totalAmount - $Discount;
$totalAmount = number_format($totalAmount , 2, '.', '');
if($GST ==1){
	$GSTAmt = number_format($totalAmount * .07, 2, '.', '');
	$GrandTotal = number_format($totalAmount + $GSTAmt, 2, '.', '');
}else{
	$GrandTotal = number_format($totalAmount , 2, '.', '');
}
?>

<?php 
if ($Discount > 0){
 ?>
	<tr>
		<td style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td style="border-right:.1px solid black;" align="center">
			Discount
		</td>
		<td align="center">
			<?php echo($symcurrency .' ('.  number_format( $Discount, 2, '.', '') . ')');?>
		</td>
	</tr>
 <?php
}
?>
	<tr>
		<td  style="border-right:.1px solid black;">&nbsp;
		</td>
		<td  style="border-right:.1px solid black;">&nbsp;
		</td>
		<td  style="border-right:.1px solid black;">&nbsp;
		</td>
		<td  style="border-right:.1px solid black;">&nbsp;
		</td>
		<td  style="border-right:.1px solid black;">&nbsp;
		</td>
		<td>
		</td>
	</tr>
	<tr>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td align="left" style="border-right:.1px solid black;">
			<b>1) Kindly acknowledge receipt via email.<br /><br />
			2) Please enclose Invoice(s) into an envelope<br />
			   and attach to the DO when delivery.<br /><br />
			<!--3) PO is subject to reschedule and <br />
			   cancellation.<br/>-->
		   </b>
	    </td>
		<td align="center" style="border-right:.1px solid black;">
			<?php 
				if($GST ==1){
					echo("7% GST");
				}
			?>
			
		</td>
		<td align="center">
			<?php 
				if($GST ==1){
					if ($PRData->PatchGST > 0){
						echo number_format($PRData->PatchGST, 2, '.', '');
					} else {
					 echo($GSTAmt);
					}
				}
			?>
		</td>
	</tr>
	<tr >
		<td align="right" colspan="5" style="border-top:.1px solid black;border-right:.1px solid black;">
			<b>TOTAL</b>
		</td>
		<td align="center" style="border-top:.1px solid black;">
			<?php 
			
				if ($PRData->PatchGSTAmount > 0){
					echo number_format($PRData->PatchGSTAmount, 2, '.', '');
				} else {
					echo($GrandTotal);
				}
			?>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="6"  style="border-top:.1px solid black;">
			<br />
			<b>Remarks :</b>
			<?php echo($Remarks);?>
			<br /><br />&nbsp;
		</td>
	</tr>
	<?php if ( strtotime($CompletedPODate) > strtotime('2016-11-24') ) :?>
	<tr>
		<td align="left" colspan="6"  style="border-top:.1px solid black;">
			<p>"Terms and Conditions applicable to AS9120 Aerospace Requirements"
			<br>
			1) The right of access for the customer and regulatory authorities are granted by the organization to the applicable areas of the facilities, at any level of the supplier chain, Involved in the order and to all applicable records.
			<br>
			2) Please provide certificate of compliance(COC)
			<br>
			3) Please ensure records retention of 7 years.
			<br>
			4) Please notify the organization of nonconforming product.
			<br>
			5) Please obtain organization approval for nonconfirming product of disposition.
			<br>
			6) Please notify the organization of changes in product and/or process definition, changes of suppliers <br> Change of manufacturing facility location and, where required, obtain organization approval.
			<br>
			7) Please flow down to the supply chain the applicable requirements including customer requirements.
			<?php if ( strtotime($CompletedPODate) > strtotime('2017-11-14') ) :?>
			<br>
			8) Please adhere to organizer code of conduct and ethics to supply & delivery of products.
			<?php endif;?>
			</p>
		</td>
	</tr>
	<?php endif;?>
	<tr>
		<td align="right" colspan="6" style="border-top:.1px solid black;">
			<TABLE width="100%">
				<TR>
					<TD width="10%">
						<b>BUYER:	</b>
					</TD>
					<TD>
						<?php echo($Buyer);?>
					</TD>
					<TD width="10%">
						<b>SIGNATURE:</b>
					</TD>
					<TD>
						<?php echo($Buyer);?>
					</TD>
					<TD width="10%">
						<b>DATE:	</b>
					</TD>
					<TD>
						<?php echo(date("d-M-y"));?>
					</TD>
				</TD>
				<TR>
					<TD>
						<b>EXT:	</b>
					</TD>
					<TD COLSPAN="5">
						<?php echo($BuyerExt);?>
					</TD>
					
				</TD>
			</TABLE>
		</td>
	</tr>
</table>
</tr></td></table>
	<table style="border:0;font-size: 9x;"  border="0" cellspacing="0" border="0" cellpadding="0" width="1100">
	<tr> <td colspan='3' align='right'>This is a Computer Generated Document.  No Signature Required.</td></tr>
	<tr>
		<?php if ( strtotime($CompletedPODate) > strtotime('2017-11-14') ) {?>
			<td width="33%" align="left">&nbsp;&nbsp;FM-PRD-742-02</td>
			<td width="33%" align="left">REV : 02</td>
			<td align="right" width="34%" > 15 Nov'17&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
		<?php } else if ( strtotime($CompletedPODate) > strtotime('2016-11-24') && strtotime($CompletedPODate) < strtotime('2017-11-15') ) {?>
			<td width="33%" align="left">&nbsp;&nbsp;FM-PRD-742-02</td>
			<td width="33%" align="left">REV : 01</td>
			<td align="right" width="34%" > 24 Nov'16&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
		<?php } else {?>
			<td width="33%" align="left">&nbsp;&nbsp;FM-PRD-742-01</td>
			<td width="33%" align="left">REV : 00</td>
			<td align="right" width="34%" > 12 Sept'12&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr></table>
		<?php }?>
		
</td></tr></table>
</body>
</html>