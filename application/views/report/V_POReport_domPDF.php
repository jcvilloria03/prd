<?php

	$PRType = $POData->PRType;
	$PONumber = $POData->PONumber;
	$VendorName = $POData->VendorName;
	$VendorAddress1 = $POData->VendorAddress1;
	$VendorAddress2 = $POData->VendorAddress2;
	$VendorAddress3 = $POData->VendorAddress3;
	
	$VendorContactperson = $POData->VendorContactperson;
	$VendorTelNo = $POData->VendorTelNo;
	$VendorFaxNo = $POData->VendorFaxNo;
	$PaymentTermName = $POData->PaymentTermName;
	$DeliveryTermName = $POData->DeliveryTermName;
	
	$VendorAddress1 = $POData->VendorAddress1;
	$VendorAddress2 = $POData->VendorAddress2;
	$VendorAddress3 = $POData->VendorAddress3;
	$ShipToAddress = $POData->ShipToAddress;
	$BillingAddress = $POData->BillingAddress;
	$Attention = $POData->Attention;
	$Ext = $POData->Ext;
	$ReqDeliveryDate = $POData->ReqDeliveryDate;
	$Remarks = $POData->Remarks;
	$Buyer = $POData->Buyer;
	$BuyerExt = $POData->BuyerExt;
	
	
	$BaseCurrency = $PODetail[0]['BaseCurrency'];

	if($BaseCurrency =="USD"){
		$symcurrency = 'US$';
	}else if($BaseCurrency =="SGD"){
		$symcurrency ='S$';
	}else{
		$symcurrency ='$';
	}

?>

<style>
table{
	font-family: arial,sans-serif;
	font-size: 12px;
}
td.b-border{
	border-bottom:.5px solid black;
}
td.br-border{
	border-right:.5px solid black;
	border-bottom:.5px solid black;
}
td.brlt-border{
	border-right:.5px solid black;
	border-bottom:.5px solid black;
	border-left:.5px solid black;
	border-top:.5px solid black;
}
td.blt-border{
	border-right:.5px solid black;
	border-bottom:.5px solid black;
	border-top:.5px solid black;
}
td.blt-border{
	border-bottom:.5px solid black;
	border-left:.5px solid black;
	border-top:.5px solid black;
}
</style>
<table style="border:.5px solid black" border="0" cellspacing="0"  cellpadding="1px" width="100%"><tr><td>
<table  style="border:.5px solid black" border="0" cellspacing="0" cellpadding="5px"  width="100%">
	<tr>
		<td colspan=5 align="center" class="b-border" valign="middle">
			<h3> JSI LOGISTICS (S) PTE LTD<br />
			No 3. CHANGI NORTH STREET 2 #01-02B/03, SINGAPORE 498827<br />
			Tel : 6545 4041 &nbsp; &nbsp;&nbsp; FAX:6545 7279
			</h3>
		</td>
	</tr>
	<tr>
		<td colspan=5 align="center" class="b-border">
			<h4>
			PURCHASE ORDER
			</h4>
		</td>
	</tr>
	<tr>
		<td colspan="5" valign="top">	
<table width="100%">
	<tr>
		<td width="50%" valign="top">
			<table width="100%" cellspacing="0">
				<tr>
					<td>
						&nbsp;
					</td>
				</tr>
				<tr>
					<td>
						<b>NAME OF SUPPLIER : </b>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo($VendorName);?>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<b>ADDRESS :</b>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo($VendorAddress1."<br />".	$VendorAddress2 .'<br />'.$VendorAddress3 ); ?>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<b>CONTACTS:</b>
					</td>
				</tr>
				<tr>
					<td>
						<b>ATTN :</b> 
						<?php echo($VendorContactperson);?>
					</td>
				</tr>
				<tr>
					<td>
						<b>TEL : </b>
						<?php echo($VendorTelNo);?>
					</td>
				</tr>
				<tr>
					<td>
						<b>FAX :</b>
						<?php echo($VendorFaxNo);?>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<b>Payment Terms :</b>
						<?php echo($PaymentTermName);?>
					</td>
				</tr>
				<tr>
					<td>
						<b>Shipment Terms : </b>
						<?php echo($DeliveryTermName);?>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
			</table>
		</td>
		<td width="50%" valign="top">
			<table width="100%" cellspacing="0">
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<b>PURCHASE ORDER NO :</b>
						<?php echo($PONumber);?>
					</td>
				</tr>
				<tr>
					<td><b>DATE : </b>
						<?php echo(date("d-M-y"));?>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td> 
						<b>BILL TO ADDRESS :</b>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo(str_replace(Chr(13),"<br>",$BillingAddress) ); ?>
					</td>
				</tr>
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<b>SHIP TO ADDRESS:</b>
					</td>
				</tr>
				<tr>
					<td>	
						<?php echo(str_replace(Chr(13),"<br>",$ShipToAddress)); ?>
					</td>
				</tr>
			
				<tr><td>&nbsp;</td></tr>
				<tr>
					<td>
						<b>Attention :</b>
						<?php echo($Attention);?>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<b>Ext:</b>
						<?php echo($Ext);?>
					</td>
				</tr>
				<tr>
					<td>
						<b>Required Delivery Date :</b>
						<?php echo($ReqDeliveryDate);?>
					</td>
				</tr>
				<tr>
					<td colspan="2">
						&nbsp;
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
		
		</td>
	</tr>
	<tr>
		<td align="center" align="center" class="brlt-border"  width="5%">
			<b>S/N</b>
		</td>
		<td align="center" align="center" class="brlt-border" width="7%">
			<b>QTY</b>
		</td>
		<td align="center" align="center" class="brlt-border" >
			<b>DESCRIPTION / PART NUMBER</b>
		</td>
		<td align="right" align="center" class="brlt-border"  width="7%">
			<b>UNIT PRICE(<?php echo($symcurrency);?>)</b>
		</td>
		<td align="right" align="center" class="brlt-border"  width="15%">
			<b>AMOUNT(<?php echo($symcurrency);?>)</b>
		</td>
	</tr>

<?php 

$totalAmount=0;
if(isset($PODetail)){
$sn = 0;
foreach ($PODetail as $val)
{
$sn +=1;
$Quantity = $val['Quantity'];
$PartNumber   = ($PRType =='NORMAL') ? $val['PartName'] :$val['PartNumber'];
$PartName2   =  $val['PartName2'];
$UnitPrice    = $val['UnitPrice'];
$Amount    		= $val['Amount'];
$UOM =  $val['UOM'];

$Amount1 = explode('.', $Amount);

$totalAmount += $Amount;

?>
	<tr>
		<td align="center" class="brlt-border" >
			<?php echo($sn);?>
		</td>
		<td align="center" class="brlt-border" >
			<?php echo($Quantity.' '. $UOM);?>
		</td>
		<td align="left" class="brlt-border" >
			<?php echo($PartNumber);?>
			<?php echo ($PartName2 <> '')?'<br />'.$PartName2.'<br/>':''; ?>
		</td>
		<td align="right" class="brlt-border" >
			<?php echo($UnitPrice);?>
		</td>
		<td align="right" class="brlt-border" >
			<?php echo($Amount);?>
		</td>
	</tr>
<?php } 
}else{ 
	
?>
	<tr>
		<td colspan="5" align="center"> <b>No Data</b> </td>
	</tr>
<?php
}
$totalAmount = number_format($totalAmount, 2, '.', '');
$GST = number_format($totalAmount * .07, 2, '.', '');
$GrandTotal = number_format($totalAmount + $GST, 2, '.', '');
?>
	<tr>
		<td align="center"  class="brlt-border" >
			&nbsp;
		</td>
		<td align="center"  class="brlt-border" >
			&nbsp;
		</td>
		<td align="left" class="brlt-border" >
			<b>1) Kindly acknowledge receipt via email.<br /><br />
			2) Please enclose Invoice(s) into an envelope<br />
			   and attach to the DO when delivery.<br /><br />
			<!--3) PO is subject to reschedule and <br />
			   cancellation.<br/>-->
		   </b>
		<td align="center"  class="brlt-border" >
			7% GST
		</td>
		<td align="right"  class="brlt-border" >
			<?php echo($GST); ?>
		</td>
	</tr>
	<tr>
		<td align="right" colspan="4"   class="brlt-border" >
			<b>TOTAL</b>
		</td>
		<td align="right"  class="brlt-border" >
			<?php echo($GrandTotal)?>
		</td>
	</tr>
	<tr>
		<td align="left" colspan=5 class="b-border" >
			<br />
			<b>Remarks :</b>
			<?php echo($Remarks);?>
			<br /><br />&nbsp;
		</td>
	</tr>
	<tr>
		<td align="right" colspan=5>
			<TABLE width="100%" >
				<TR>
					<TD width="10%">
						<b>BUYER:	</b>
					</TD>
					<TD>
						<?php echo($Buyer);?>
					</TD>
					<TD width="10%">
						<b>SIGNATURE:	</b>
					</TD>
					<TD>
						
					</TD>
					<TD width="10%">
						<b>DATE:	</b>
					</TD>
					<TD>
						<?php echo(date("d-M-y"));?>
					</TD>
				</TD>
				<TR>
					<TD>
						<b>EXT:	</b>
					</TD>
					<TD COLSPAN="5">
						<?php echo($BuyerExt);?>
					</TD>
					
				</TD>
			</TABLE>
				
		</td>
	</tr>
</table>
</tr></td></table>
<table style="border:0;font-size: 8px;" border="0" cellspacing="0"  cellpadding="0" width="100%">
<tr><td width="33%" align="left">&nbsp;&nbsp;FM-PRD-742-02</td><td width="33%" align="left">REV : 00</td>
<td align="right"> 29 Sept'12&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
</table>
