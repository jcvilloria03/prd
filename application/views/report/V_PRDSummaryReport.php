<div class="newentry">
<h3 class="form-title">PRD Summary Report</h3>
	<form class="globalform" method="post" action="#">
		<ol>
			<div class="lfpane" >
				<li>
					<label>Date : </label> 
					<input type="text" id="from" name="from" readonly="readonly" />
				</li>
				<li>
					<label>Supplier: </label> 
					<select id="VendorID" name="VendorID">
					</select>
				</li>
			</div>
			<div class="rfpane" >
				<li>
					<label>To : </label> 
					<input type="text" id="to" name="to" readonly="readonly" />
				</li>
				<li>
					<label>Customer : </label> 
					<select id="CustomerID" name="CustomerID">
					</select>
				</li>
			</div>
			<div  class="lfpane" >
				<li>
					<label></label> 
					<button id="Search-btn" name="Search-btn" data="" ><span class="ui-icon ui-icon-search"></span>Search</button>
					<button id="ExportToPdf-btn" name="ExportToPdf-btn" data="" ><span class="ui-icon ui-icon-search">
					</span>Export to Excel</button>
				</li>
			</div>
		</ol>
		<div class="clear"></div>		
    </form>
</div>

<div id="tabs">
	<ul>
		<li><a href="#tabs-1">Opening Order</a></li>
		<li><a href="#tabs-2">Sale Order Summary</a></li>
		<li><a href="#tabs-3">SO Summary (CN)</a></li>
		<li><a href="#tabs-4">Received Summary Report( JSI Cost / Expense)</a></li>
	</ul>
	<div id="tabs-1">
		<div id="POOpenOrderReport-div" name="POOpenOrderReport-div" >
			<table class="display" id="tbl_OpenOrderReport">
				<thead><tr></tr></thead>
				<tbody></tbody>
				<tfoot><tr></tr></tfoot>
			</table>
		</div>
	</div>
	<div id="tabs-2">
		<div id="SaleOrderSummaryReport-div" name="SaleOrderSummaryReport-div" >
			<table class="display" id="tbl_SOSummaryReport">
				<thead><tr></tr></thead>
				<tbody></tbody>
				<tfoot><tr></tr></tfoot>
			</table>
		</div>
	</div>
	<div id="tabs-3">
		<div id="SOSummaryCNReport-div" name="SOSummaryCNReport-div" >
			<table class="display" id="tbl_SOSummaryCNReport">
				<thead><tr></tr></thead>
				<tbody></tbody>
				<tfoot><tr></tr></tfoot>
			</table>
		</div>
	</div>	
	<div id="tabs-4">
		<div id="POReceivedSummaryReport-div" name="POReceivedSummaryReport-div" >
			<table class="display" id="tbl_ReceivedSummaryReport">
				<thead><tr></tr></thead>
				<tbody></tbody>
				<tfoot><tr></tr></tfoot>
			</table>
		</div>
	</div>
</div>


<script type="text/javascript">
var tbl_SOSummaryReport,tbl_OpenOrderReport,fn_todoPR,tbl_ReceivedSummaryReport,tbl_SOSummaryCNReport;
$(document).ready(function() {
	$("input:submit, input:button, button, .button").button().click(function(event){
		event.preventDefault();
	});
	


	$( "#from" ).datepicker({
		minDate: "07/01/2013", 
		maxDate: new Date(),
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
			$( "#to" ).datepicker( "option", "minDate", selectedDate ).focus();
		},onSelect : function() {
	
		}
		
	});
	
	$( "#to" ).datepicker({
		minDate: "07/01/2013", 
		maxDate: new Date(),
		defaultDate: "+1w",
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
			$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		}
	});

		var d = new Date();
	var curr_date = d.getDate();
	var curr_month = d.getMonth() + 1;
	var curr_year = d.getFullYear();
	// $('#from').val(curr_month+'/01/'+curr_year);
	// $('#to').val(curr_month+'/'+curr_date+'/'+curr_year);
	$( "#from" ).datepicker( "setDate", curr_month+'/01/'+curr_year );
	$( "#to" ).datepicker(  "setDate",curr_month+'/'+curr_date+'/'+curr_year );
	
	$( "#from" ).addClass('readonly');
	$( "#to" ).addClass('readonly');
	var OpenOrderReport_fields = { 'PONumber':'PONumber',
									'PRNumber':'PRNumber',
									'CompletedPODate':'Create PO Date',
									'VendorName':'VendorName',
									'POBalanceQty':'Oustanding Qty',
									'PartNumber':'PartNumber',
									'PartDesc1':'PartDesc1',
									'UnitPrice':'UnitPrice',
									'GST':'GST',
									'TotalAmount':'TotalAmount',
									'Currency':'Currency',};
	  
	$.each(OpenOrderReport_fields,function(i,e){
		$('#tbl_OpenOrderReport thead tr').append('<th>'+e+'</th>');
		$('#tbl_OpenOrderReport tfoot tr').append('<th>'+e+'</th>');
	});
	
	$('#tbl_OpenOrderReport,.display').css("white-space", "nowrap");
	tbl_OpenOrderReport = $('#tbl_OpenOrderReport').dataTable({
		bJQueryUI: true,
		sScrollX: "96%",
		aLengthMenu: [[10, 25, 50], [10, 25, 50]],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aaSorting: [[0,'desc']],
		aoColumnDefs : [{"aTargets" : [ 6 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 7 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 8 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 9 ],
			 sClass: "alignRight" 
			}],
		sAjaxSource: 'report/PRDSummaryReport/View_OpenOrderReport', 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(OpenOrderReport_fields)) } );	
			aoData.push( { "name": "PRType", "value": $("#radiotype input:radio:checked").val() } ); 
			aoData.push( { "name": "from", "value": $('#from').val() } );	
			aoData.push( { "name": "to", "value":  $('#to').val() } );	
			aoData.push( { "name": "VendorID", "value":  $('#VendorID').val() } );	
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	setTableHeaderHTML('tbl_OpenOrderReport',' <div id="radiotype" style="width:400px;float:left;margin-left: 30px;"> PR Type : '+
						' <input type="radio" id="All" name="radiotype" checked="checked" value=""/><label for="All">All</label> '+
						' <input type="radio" id="NORMAL" name="radiotype" value="NORMAL" /><label for="NORMAL">Normal</label> '+
						' <input type="radio" id="JSIEXPENSE" name="radiotype" value="JSIEXPENSE"/><label for="JSIEXPENSE">JSI Expense</label>'+
						' <input type="radio" id="JSICOST" name="radiotype" value="JSICOST"/><label for="JSICOST">JSI Cost</label> </div>');
	
	//$( "#radiotype" ).buttonset();
	
	 $("#radiotype input:radio").change(function(){
		tbl_OpenOrderReport.fnDraw();
	});
 
	var SOSummaryCNReport_fields = { 'SONumber':'SONumber',
							  'PONumber':'PONumber',
							  'CustomerName':'CustomerName',
							  'DeliveryDate':'Delivery Date',
							  'VendorName':'VendorName',
							  'PartNumber':'PartNumber',
							  'Description':'PartDesc1',
							  'Invoice':'Invoice',
							  'SOQty':'Qty',
							  'BPrice':'BPrice',
							  'BCurrency':'BCurrency',
							  'BuyGST':'B GST',
							  'SPrice':'SPrice',
							  'SCurrency':'SCurrency',
							  'TTCharges':'TTCharges',
							  'FreightCharges':'FreightCharges',
							  'AdminCost':'AdminCost',
							  'GST':'GST',
							  'GST':'S GST',
							  'TotalAmount':'TotalAmount'};
	$.each(SOSummaryCNReport_fields,function(i,e){
		$('#tbl_SOSummaryCNReport thead tr').append('<th>'+e+'</th>');
		$('#tbl_SOSummaryCNReport tfoot tr').append('<th>'+e+'</th>');
	});							  
							  
	var SOSummaryReport_fields = { 'SONumber':'SONumber',
							  'PONumber':'PONumber',
							  'CustomerName':'CustomerName',
							  'DeliveryDate':'Delivery Date',
							  'VendorName':'VendorName',
							  'PartNumber':'PartNumber',
							  'Description':'PartDesc1',
							  'Invoice':'Invoice',
							  'SOQty':'Qty',
							  'BPrice':'BPrice',
							  'BCurrency':'BCurrency',
							  'BuyGST':'B GST',
							  'SPrice':'SPrice',
							  'SCurrency':'SCurrency',
							  'TTCharges':'TTCharges',
							  'FreightCharges':'FreightCharges',
							  'AdminCost':'AdminCost',
							  'GST':'GST',
							  'GST':'S GST',
							  'TotalAmount':'TotalAmount'};
  
	$.each(SOSummaryReport_fields,function(i,e){
		$('#tbl_SOSummaryReport thead tr').append('<th>'+e+'</th>');
		$('#tbl_SOSummaryReport tfoot tr').append('<th>'+e+'</th>');
	});
	
	$('#tbl_OpenOrderReport,.display').css("white-space", "nowrap");

	tbl_SOSummaryCNReport = $('#tbl_SOSummaryCNReport').dataTable({
		bJQueryUI: true,
		sScrollX: "96%",
		aLengthMenu: [[10, 25, 50], [10, 25, 50]],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aaSorting: [[0,'desc']],
		aoColumnDefs : [{"aTargets" : [ 9 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 10 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 11 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 12 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 13 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 14 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 15 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 16 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 17 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 8 ],
			 sClass: "alignRight" 
			}],
		sAjaxSource: 'report/PRDSummaryReport/view_SOSummaryCNReport', 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(SOSummaryCNReport_fields)) } );	
			aoData.push( { "name": "from", "value": $('#from').val() } );	
			aoData.push( { "name": "to", "value":  $('#to').val() } );	
			aoData.push( { "name": "CustomerID", "value":  $('#CustomerID').val() } );	
			aoData.push( { "name": "VendorID", "value":  $('#VendorID').val() } );	
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	tbl_SOSummaryReport = $('#tbl_SOSummaryReport').dataTable({
		bJQueryUI: true,
		sScrollX: "96%",
		aLengthMenu: [[10, 25, 50], [10, 25, 50]],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aaSorting: [[0,'desc']],
		aoColumnDefs : [{"aTargets" : [ 9 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 10 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 11 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 12 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 13 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 14 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 15 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 16 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 17 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 8 ],
			 sClass: "alignRight" 
			}],
		sAjaxSource: 'report/PRDSummaryReport/view_SOSummaryReport', 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(SOSummaryReport_fields)) } );	
			aoData.push( { "name": "from", "value": $('#from').val() } );	
			aoData.push( { "name": "to", "value":  $('#to').val() } );	
			aoData.push( { "name": "CustomerID", "value":  $('#CustomerID').val() } );	
			aoData.push( { "name": "VendorID", "value":  $('#VendorID').val() } );	
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	/*PO Opening Summary Report*/
	
	var ReceivedSummaryReport_fields = { 'PONumber':'PONumber',
									'PRNumber':'PRNumber',
									'Department':'Department',
									'VendorName':'VendorName',
									'Invoice':'Invoice',
									'DeliveryDate':'DeliveryDate',
									'PartNumber':'PartNumber',
									'PartDesc1':'PartDesc1',
									'Qty':'Qty',
									'UnitPrice':'UnitPrice',
									'GST':'GST',
									'TotalAmount':'TotalAmount',
									'Currency':'Currency',};
	  
	$.each(ReceivedSummaryReport_fields,function(i,e){
		$('#tbl_ReceivedSummaryReport thead tr').append('<th>'+e+'</th>');
		$('#tbl_ReceivedSummaryReport tfoot tr').append('<th>'+e+'</th>');
	});
	
	$('#tbl_ReceivedSummaryReport,.display').css("white-space", "nowrap");
	tbl_ReceivedSummaryReport = $('#tbl_ReceivedSummaryReport').dataTable({
		bJQueryUI: true,
		sScrollX: "96%",
		aLengthMenu: [[10, 25, 50], [10, 25, 50]],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true, 
		aaSorting: [[0,'desc']],
		aoColumnDefs : [{"aTargets" : [ 8 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 9 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 10 ],
			 sClass: "alignRight" 
			},{"aTargets" : [ 11 ],
			 sClass: "alignRight" 
			}],
		sAjaxSource: 'report/PRDSummaryReport/view_ReceivedSummaryReport', 
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(ReceivedSummaryReport_fields)) } );
			aoData.push( { "name": "PRType", "value": $("#radiotype1 input:radio:checked").val() } ); 			
			aoData.push( { "name": "from", "value": $('#from').val() } );	
			aoData.push( { "name": "to", "value":  $('#to').val() } );	
			aoData.push( { "name": "VendorID", "value":  $('#VendorID').val() } );	
			$.ajax( { 
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	});
	
	setTableHeaderHTML('tbl_ReceivedSummaryReport',' <div id="radiotype1" style="width:400px;float:left;margin-left: 30px;"> Type : '+
					' <input type="radio" id="All" name="radiotype1" checked="checked" value=""/><label for="All">All</label> '+
					' <input type="radio" id="JSIEXPENSE" name="radiotype1" value="JSIEXPENSE"/><label for="JSIEXPENSE">JSI Expense</label>'+
					' <input type="radio" id="JSICOST" name="radiotype1" value="JSICOST"/><label for="JSICOST">JSI Cost</label> </div>');

	//$( "#radiotype1" ).buttonset();
	 $("#radiotype1 input:radio").change(function(){
		tbl_ReceivedSummaryReport.fnDraw();
	});
	/*PO Opening Summary Report*/
	
	$('#Search-btn').click(function(){
		var active = $( "#tabs" ).tabs( "option", "active" );
		if(active ==0){
			tbl_OpenOrderReport.fnDraw();
		}else if(active ==1){
			tbl_SOSummaryReport.fnDraw();
		}else if(active ==2){
			tbl_SOSummaryCNReport.fnDraw();
		}else if(active ==3){
			tbl_ReceivedSummaryReport.fnDraw();
		}
		
	});
	$('#ExportToPdf-btn').click(function(){
	
		console.log('Export');
		if($('#from').val() ==''){
			msgbox('Error Message','<p class="error">Please select from date.</p>');		
			return;
		}else if($('#to').val() ==''){
			msgbox('Error Message','<p class="error">Please select to date.</p>');		
			return;
		}
		
		var active = $( "#tabs" ).tabs( "option", "active" );
		var reporttype ='',PRType='';
		if(active ==0){
			reporttype = 'POSummary';
			PRType= $("#radiotype input:radio:checked").val();
		}else if(active ==1){
			reporttype = 'SOSummary';
		}else if(active ==2){
			reporttype = 'SOCNSummary';
		}else if(active ==3){
			reporttype = 'POOpeningSummary';
			PRType= $("#radiotype1 input:radio:checked").val();
		}
		
	  var mapForm = document.createElement("form");
		mapForm.target = "Map";
		mapForm.method = "POST"; // or "post" if appropriate
		mapForm.action = "report/PrintExcel/exportSummaryReport";
		mapForm.style.display='none';
		
		var CustomerID = document.createElement("input");
		CustomerID.type = "text";
		CustomerID.name = "CustomerID";
		CustomerID.value = $('#CustomerID').val();		
		var VendorID = document.createElement("input");
		VendorID.type = "text";
		VendorID.name = "VendorID";
		VendorID.value = $('#VendorID').val();		
		var Type = document.createElement("input");
		Type.type = "text";
		Type.name = "reporttype";
		Type.value = reporttype;
		var PR_Type = document.createElement("input");
		PR_Type.type = "text";
		PR_Type.name = "PRType";
		PR_Type.value = PRType;
		var From = document.createElement("input");
		From.type = "text";
		From.name = "from";
		From.value = $('#from').val();
		var To = document.createElement("input");
		To.type = "text";
		To.name = "to";
		To.value = $('#to').val();
		
		mapForm.appendChild(CustomerID);
		mapForm.appendChild(VendorID);
		mapForm.appendChild(Type);
		mapForm.appendChild(PR_Type);
		mapForm.appendChild(From);
		mapForm.appendChild(To);

		document.body.appendChild(mapForm);

		//map = window.open("", "Map", "status=0,title=0,height=600,width=800,scrollbars=1");
mapForm.submit();
		// if (map) {
			
		// } else {
			// alert('You must allow popups for this map to work.');
		// }
		
	});
	
	
	 $( "#tabs" ).tabs({
		  activate: function(event ,ui){
				if(ui.newTab.index() ==0){
					$( "#from" ).addClass('readonly');
					$( "#to" ).addClass('readonly');
					$('#CustomerID').combobox("disable");
					tbl_OpenOrderReport.fnDraw();
				}else if(ui.newTab.index() ==1 || ui.newTab.index() ==2){
					$( "#from" ).removeClass('readonly');
					$( "#to" ).removeClass('readonly');
					$('#CustomerID').combobox("enable");
					tbl_SOSummaryReport.fnDraw();
				}else if(ui.newTab.index() ==3){
					$( "#from" ).removeClass('readonly');
					$( "#to" ).removeClass('readonly');
					$('#CustomerID').combobox("disable");
					tbl_ReceivedSummaryReport.fnDraw();
				}
			}
	 });
	 
	
	fn_todoPR = function(action,prnumber){
		switch(action){
			case 'PrintPR' : 
				fn_PrintPR(prnumber);
				break;
			case 'PrintPO' : 
				fn_PrintPO(prnumber);
				break;
			case 'PrintSO':
				fn_PrintSO(prnumber);
				break;
			case 'PrintSOCN':
				fn_PrintSOCN(prnumber);
				break;				
		}
	}
	
	  
	 fn_PrintPO = function(PONumber){
		window.open("report/PrintPDF/POREPORT/"+PONumber,'_blank',false);
	 }
	 fn_PrintPR = function(PRNumber){
		window.open("report/PrintPDF/PRREPORT/"+PRNumber,'_blank',false);
	 }
	 fn_PrintSO = function(sonumber){	
		window.open("report/PrintPDF/SOREPORT/"+sonumber,'_blank',false);
	}
	 fn_PrintSOCN = function(sonumber){	
		window.open("report/PrintPDF/SOCNREPORT/"+sonumber,'_blank',false);
	}	
	
	
	/* Create Combobox for Department, PartNumber, Vendor */
	fn_ComboBoxload = function(data){
		var obj = jQuery.parseJSON(data);
		loadComboBoxData("VendorID",obj.Vendor,true);
		loadComboBoxData("CustomerID",obj.Customer,true);
		
		$('#CustomerID').combobox("disable");
	}
	/* End ComboBox */
	
	/* Start Loading Data*/
	ProcessRequest('services/DropDownLists/getComboBoxdata',{
					'Vendor':'Vendor','Customer':'Customer'
					},'fn_ComboBoxload');
	
	
});

</script>