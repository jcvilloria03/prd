<?php

	$TotalAmount = $PRData->TotalAmount;
	$PageNumber = $PRData->PageNumber;
	$PageCount = $PRData->PageCount;
	
	$FirstPage = $PRData->FirstPage;
	$LastPage = $PRData->LastPage;
	
	$PRNumber = $PRData->PRNumber;
	$PRType = $PRData->PRType;
	$Department = $PRData->Department;
	$DepartmentName = $PRData->DepartmentName;
	$DateRequired = $PRData->DateRequired;
	$RequestedDate = $PRData->RequestedDate;
	
	$Remarks = $PRData->Remarks;
	$Remarks2 = $PRData->Remarks2;
	
	$RequestedBy = $PRData->RequestedBy;
	$PurchasedBy = $PRData->PurchasedBy;
	$PRDApprovedBy = $PRData->PRDApprovedBy;
	$ApprovedBy = $PRData->ApprovedBy;
	$GMApprovedBy = $PRData->GMApprovedBy;
	$BMApprovedBy = $PRData->BMApprovedBy;
	
	$PRDApprovedDate = $PRData->PRDApprovedDate;
	$ApprovedDate = $PRData->ManagerApprovedDate;
	$GMApproveDate = $PRData->GMApprovedDate;
	$BMApprovedDate = $PRData->BMApprovedDate;
	
	
	$RequiredGMApproved = $PRData->RequiredGMApproved;
	$PRDApproved = $PRData->PRDApproved;
	$ManagerApproved = $PRData->ManagerApproved;
	$GMApproved = $PRData->GMApproved;
	$BMApproved = $PRData->BMApproved;
	
	$Comment = $PRData->Comment;
	$VendorName  = $PRData->VendorName;
	
	$GST = $PRData->GST;
	$InternalRequest = $PRData->InternalRequest;
	
	$MultiManagerApproval = $PRData->MultiManagerApproval;
	
	$Discount = $PRData->Discount;
	// if ($Discount > 0){
	// echo "dsfsdf";
	// }
	// die();
?>

<html>
<head>
<style>
@page {
 font-family: arial,sans-serif;
 font-size: 12px;
}
table{
	font-size: 11px;
}
</style>
</head>
<body>
<table style="border:0;font-size: 9x;"  border="0" cellspacing="0" border="0" cellpadding="0" width="1100"><tr>
	<td align="right"> Print Date :<?php echo date("m/d/y G.i:s<br>", time());?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
</table>
<table border="0" cellspacing="0" cellpadding="0" width="100%"><tr><td>
	<table style="border:.1px solid black;" border="0" cellspacing="0" cellpadding="2px" width="100%"><tr><td>
		<table style="border:.1px solid black;" border="0" cellspacing="0" cellpadding="5px" width="100%">
			<tr>
				<td colspan="7" align="center" valign="middle">
					&nbsp;<br><h3> JSI LOGISTICS (S) PTE LTD </h3> &nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="5" align="center" style="border-right:0px solid black;">
					&nbsp;<br>PURCHASE REQUISITION<br>&nbsp;
				</td>
				<td colspan="2" align="left">
					PR NO : <?php echo($PRNumber.'<br> Pg : '.$PageNumber.'  of  '.$PageCount ) ?>
				</td>
			</tr>
			<tr>
				<td colspan="4" align="left" style="border-right:0px;">
					<br>DEPARTMENT : <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo($DepartmentName)?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</u>
					<br>&nbsp;
				</td>
				<td colspan="3" align="center" style="border-left:0px;">
					DATE REQUIRED : <b><u>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo($DateRequired) ?>&nbsp;&nbsp;&nbsp;&nbsp;</u></b>
				</td>
			</tr>
			<tr>
				<td align="center"  width="3em">
					S/N
				</td>
				<td align="center" width="3em">
					QTY
				</td>
				<td align="center" width="5em">
					UOM
				</td>
				<td align="center" >
					DESCRIPTION 
				</td>
				<td align="center" width="15em">
					NAME OF SUPPLIER
				</td>
				<td align="center"  width="10em">
					UNIT <br>
					PRICE
				</td>
				<td align="center" width="12em">
					TOTAL AMOUNT <br>
					<small>(SELECTED SUPPLIER)</small>
				</td>
			</tr>
		<?php 
		$subtotal=0;
		$rowsCount=0;
		if(isset($PRDetail)){
		$sn = 0+(($PageNumber-1)*10);
		if($FirstPage =='1'){
			if(trim($Department) =='1' && $PRType!='NORMAL'){
				$DepName = $PRDetail[0]['Department'];
				 echo('<tr><td>&nbsp;</td>
					 <td>&nbsp;</td>
					 <td>&nbsp;</td>
					 <td><b>Department : '.$DepName.'</b></td>
					 <td align="center">'.$VendorName.'</td>
					 <td>&nbsp;</td>
					 <td>&nbsp;</td></tr>');
				$rowsCount +=1;
			}
		}else{
				$DepName = $PRDetail[0]['Department'];
		}
		foreach ($PRDetail as $val)
		{
			$rowsCount +=1;
			$sn +=1 ;
			$RequestedQty = $val['RequestedQty'];
			$partDesc  = ($PRType =='NORMAL') ? $val['partname'] :$val['PartNumber'];
			$PartNumber  = $val['PartNumber'];
			$PartName = $val['PartName'];
			$PartName2  = $val['PartName2'];
			//$VendorName  = $val['VendorName'];
			$UnitPrice  = $val['UnitPrice'];
			$Amount  		= $val['Amount'];
			$BaseCurrency = $val['BaseCurrency'];
			$UOM = $val['UOM'];
			$Amount1 = explode('.', $Amount);

			if($BaseCurrency =="USD"){
				$symcurrency = 'US$';
			}else if($BaseCurrency =="SGD"){
				$symcurrency = 'S$';
			}else{
				$symcurrency ='$';
			}
			
			$subtotal += $Amount;
			if(trim($Department) =='1' && $PRType!='NORMAL')
			{
				if($DepName != $val['Department']){
					 $DepName = $val['Department'];
						echo('<tr><td>&nbsp;</td>
						 <td>&nbsp;</td>
						 <td>&nbsp;</td>
						 <td><b>Department : '.$DepName.'</b></td>
						 <td>&nbsp;</td>
						 <td>&nbsp;</td>
					<td>&nbsp;</td></tr>');
					$rowsCount +=1;
				}
			}
			?>
				<tr>
					<td align="center">
						<?php echo($sn);?>
					</td>
					<td align="center">
						<?php echo($RequestedQty);?>
					</td>
					<td align="center">
						 <?php echo($UOM);?>
					</td>
					<td align="left">
					<?php echo($PartName);
						if($PartName2 <> ''){
							echo('<br>'.$PartName2);
							$rowsCount +=1;
							}
					if($PRType =='NORMAL'){
						echo("<br>P/N:".$PartNumber);
						$rowsCount +=1;
					}
					?>
					</td>
					<td align="center">
						<?php echo((trim($Department) !='1' && $sn==1)?$VendorName:'');?>&nbsp;
					</td>
					<td align="center">
						<?php echo($symcurrency .' '.number_format($UnitPrice, numberFormatDecimal($UnitPrice), '.', ''));?>
					</td>
					<td align="center">
						<?php echo($symcurrency .' '. number_format($Amount, 2, '.', ''));?>
					</td>
				</tr>
			<?php 
			} 
		}else{ 
			
		?>
			<tr>
				<td colspan="7" align="center"> No Data </td>
			</tr>
		<?php
		}
		if($rowsCount<15 && $PageCount == '1'){
			if ($Discount > 0){
				 
			 ?>
				<tr>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						Discount
					</td>
					<td align="center">
						<?php echo($symcurrency .' ('.  number_format( $Discount, 2, '.', '') . ')');?>
					</td>
				</tr>
			 <?php
				 }
			$rowsCount = 15 - $rowsCount;
			for ($i=1; $i<=$rowsCount; $i++)
			{
				echo('<tr><td>&nbsp;</td>
						 <td>&nbsp;</td>
						 <td>&nbsp;</td>
						 <td>&nbsp;</td>
						 <td>&nbsp;</td>
						 <td>&nbsp;</td>
					<td>&nbsp;</td></tr>');
			}
		}
		
		$subtotal = number_format($subtotal, 2, '.', '');
		$subtotal1 = explode('.', $subtotal);
		$subtotal2 = $subtotal1[0];
		$subtotal3 = $subtotal1[1];

		?>
		<?php 
			 if($LastPage=='1' && $PageCount != '1'){
				if ($Discount > 0){
				 
			 ?>
				<tr>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						Discount
					</td>
					<td align="center">
						<?php echo($symcurrency .' ('.  number_format( $Discount, 2, '.', '') . ')');?>
					</td>
				</tr>
			 <?php
				 }
			?>
			<tr>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					Sub Total
				</td>
				<td align="center">
					<?php echo($symcurrency .' '. $subtotal);?>
				</td>
			</tr>
			<?php
			if ($Discount > 0){
			 ?>
				<tr>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						&nbsp;
					</td>
					<td align="center">
						Discount
					</td>
					<td align="center">
						<?php echo($symcurrency .' ('.  number_format( $Discount, 2, '.', '') . ')');?>
					</td>
				</tr>
			 <?php
				 }
				 ?>
			<tr>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					Grand TTL
				</td>
				<td align="center">
					<?php echo($symcurrency .' '. number_format($TotalAmount, 2, '.', ''));?>
				</td>			
			</tr>
			<?php
			}
			if($LastPage=='1'){
				if($GST =='0'){
					$TotalAmount = $TotalAmount - $Discount;
				}
				$TotalAmount1 = explode('.', $TotalAmount);
				$TotalAmount2 = $TotalAmount1[0];
				$TotalAmount3 = $TotalAmount1[1];
				
				$GrandTotalAmount = number_format( $TotalAmount, 2, '.', '');
				$GrandTotalAmount1 = explode('.', $GrandTotalAmount);
				$GrandTotalAmount2 = $GrandTotalAmount1[0];
				$GrandTotalAmount3 = $GrandTotalAmount1[1];
			}
			 
			if($GST =='1'){
				$TotalAmount = $TotalAmount - $Discount;
				$GSTAmt = number_format( ($TotalAmount * .07), 2, '.', '');
				$GSTAmt1 = explode('.', $GSTAmt);
				$GSTAmt2 = $GSTAmt1[0];
				$GSTAmt3 = $GSTAmt1[1];

				$TotalAmount1 = explode('.', $TotalAmount);
				$TotalAmount2 = $TotalAmount1[0];
				$TotalAmount3 = $TotalAmount1[1];
				
				$GrandTotalAmount = number_format( ($GSTAmt + $TotalAmount), 2, '.', '');
				$GrandTotalAmount1 = explode('.', $GrandTotalAmount);
				$GrandTotalAmount2 = $GrandTotalAmount1[0];
				$GrandTotalAmount3 = $GrandTotalAmount1[1];
			?>
			
			<tr>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					&nbsp;
				</td>
				<td align="center">
					7% GST
				</td>
				<td align="center">
					<?php 
						if ($PRData->PatchGST > 0){
							echo number_format($PRData->PatchGST, 2, '.', '');
						} else {
							echo($symcurrency .' '.$GSTAmt);
						}
						?>
				</td>
			</tr>
			<?php } ?>
			<?php if($LastPage=='1' && trim($Comment) != ''){?>
			<tr>
				<td>
					&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
				<td align="left">
					<?php echo($Comment);?>
				</td>
				<td>
					&nbsp;
				</td>
				<td>
					&nbsp;
				</td>
				<td>
				&nbsp;
				</td>
			</tr>
			<?php }?>
			<tr>
				<td align="left" colspan="5">
					<p>REMARKS : <?php echo(str_replace(Chr(13),"<br>",$Remarks) ); ?></p>

					<?php if (isset($Remarks2) && strlen($Remarks2)>0):  ?>
					<p><?php echo(str_replace(Chr(13),"<br>",$Remarks2) ); ?></p>
					<?php endif;?>
										<br>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					
				</td>
				<td align="center" valign="top">
					<?php echo($LastPage<>'1')?'SUB':''; ?>TOTAL
				</td>
				<td align="center" valign="top">
					<?php 
					if ($PRData->PatchGSTAmount == 0){
						echo($symcurrency .' ');
						echo(($LastPage=='1' )?$GrandTotalAmount:$subtotal);
					} else {
						echo number_format($PRData->PatchGSTAmount, 2, '.', '');
					}
					?>
				</td>
			</tr>
		
			<tr>
				<td COLSPAN="7">
					<table width="1100" border="0">
						<tr>
							<td width="">&nbsp;
							</td>
							<td align="center">Authorized Personnel
							</td>
							<td colspan="4">&nbsp;
							</td>
							<td align="center"  width="12%">
								Manager
							</td>
							<td align="center"  width="12%">
							 / &nbsp; Head of Dept. 
							 
							</td>
							<td  align="center" width="12%">
							<?php echo(($RequiredGMApproved ==1)?' / &nbsp; General Manager':'');?>
							
							</td>
						</tr>	
						<tr>
							<td width="12%">REQUESTED BY : 
							</td>
							<td align="center">
								<?php 
									$Req = '';
									if($MultiManagerApproval==0){
										$Req = $RequestedBy;
									}else{
										$result = array();
										foreach($PRDetail as $tmpArray){
											$result[$tmpArray['RequestedByName']] = $tmpArray['Approved'];
										}
										
										foreach(array_keys($result) as $keysVal){
										
											if($Req != ''){
												$Req .=  '&nbsp;/&nbsp;';
											}
											$Req .= $keysVal;
										}
									}
									
									echo($Req);
								?>
							</td>
							<td width="12%">PURCHASED BY : 
							</td>
							<td align="center"> 
									<?php echo($PurchasedBy);?>
								
							</td>
							<td  align="center"> 
									<?php echo (($PRDApproved)?$PRDApprovedBy:''); ?>
							</td>
							<td width="12%"> APPROVED BY :
							</td>
							<td align="center"> 
							<?php 
								$App = '';
								if($MultiManagerApproval==0){
									if($ManagerApproved ==1){
										$App = $ApprovedBy;
									}
								}else{
									$result = array();
									foreach($PRDetail as $tmpArray){
										$result[$tmpArray['ApprovedByName']] = $tmpArray['Approved'];
									}
									
									foreach(array_keys($result) as $keysVal){
										if($result[$keysVal]==1){
											if($App != ''){
												$App .=  '&nbsp;/&nbsp;';
											}
											$App .= $keysVal;
										}
									}
								}
								
								echo($App);

								?>
							</td>
							<td align="center">
								<?php echo(($BMApproved  ==1)?$BMApprovedBy:'');?>
							</td >
							<td align="center">
								<?php echo(($GMApproved  ==1)?$GMApprovedBy:'');?>
							</td>
						</tr>
						
						<tr>
							<td align="right">DATE : 
							</td>
							<td align="center">
								<?php echo($RequestedDate);?>
							</td>
							<td align="right">DATE : 
							</td>
							<td align="center">
								<?php echo($PRDApprovedDate);?>
							</td>
							<td align="center">
								<?php echo($PRDApprovedDate);?>
							</td>
							<td align="right"> DATE :
							</td>
							<td align="center"> <?php echo($ApprovedDate);?>
							</td>
							<td align="center"> <?php echo($BMApprovedDate);?>
							</td>
							<td align="center"> <?php echo(($RequiredGMApproved)?$GMApproveDate:'');?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</tr></td></table>
	<table style="border:0;font-size: 9x;" border="0" cellspacing="0" border="0" cellpadding="0" width="1024">
	<tr> <td colspan='3' align='right'>This is a Computer Generated Document.  No Signature Required.</td>
</tr>
	<tr>
		<td width="33%" align="left">&nbsp;&nbsp;FM-PRD-742-01</td>
		<td width="33%" align="left">REV : 00</td>
		<td align="right" width="34%"> 29 Sep'12 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
	</tr></table>
</td></tr></table>
</body>
</html>