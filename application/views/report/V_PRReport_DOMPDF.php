<?php

	$PRNumber = $PRData->PRNumber;
	$PRType = $PRData->PRType;
	$Department = $PRData->Department;
	$DepartmentName = $PRData->DepartmentName;
	$DateRequired = $PRData->DateRequired;
	
	$Remarks = $PRData->Remarks;
	$RequestedBy = $PRData->RequestedBy;
	$PurchasedBy = $PRData->PurchasedBy;
	$ApprovedBy = $PRData->ApprovedBy;
	
	$Comment = $PRData->Comment;
	$VendorName   = $PRData->VendorName;
?>

<style>
table{
	font-family: arial,sans-serif;
	font-size: 12px;
}
td.b-border{
	border-bottom:.5px solid black;
}
td.br-border{
	border-right:.5px solid black;
	border-bottom:.5px solid black;
}
td.brlt-border{
	border-right:.5px solid black;
	border-bottom:.5px solid black;
	border-left:.5px solid black;
	border-top:.5px solid black;
}
td.blt-border{
	border-right:.5px solid black;
	border-bottom:.5px solid black;
	border-top:.5px solid black;
}
td.bt-border{
	border-bottom:.5px solid black;
	border-top:.5px solid black;
}
td.blt-border{
	border-bottom:.5px solid black;
	border-left:.5px solid black;
	border-top:.5px solid black;
}
</style>
<table style="border:.5px solid black" border="0" cellspacing="0"  cellpadding="1px" width="100%"><tr><td>
<table style="border:.5px solid black" border="0" cellspacing="0" cellpadding="5px"  width="100%">
	<tr>
		<td colspan=7 align="center" class="b-border" valign="middle">
			<h3> JSI LOGISTICS (S) PTE LTD </h3> 
		</td >
	</tr>
	<tr >
		<td colspan=5 align="center" class="br-border">
			<br/>PURCHASE REQUISITION<br/>&nbsp;
		</td>
		<td colspan=2 align="left" class="b-border">
			PR NO : <?php echo($PRNumber) ?>
		</td>
	</tr>
	<tr>
		<td colspan=3 align="left" class="b-border">
			<br/>DEPATMENT : <?php echo($DepartmentName)?> 
			<br/>&nbsp;
		</td>
		<td colspan=4 align="center" class="b-border">
			DATE REQUIRED : <b><?php echo($DateRequired) ?></b>
		</td>
	</tr>
	<tr>
		<td align="center" class="brlt-border" width="5%">
			S/N
		</td>
		<td align="center" class="brlt-border" width="7%">
			QTY
		</td>
		<td align="center" class="brlt-border">
			DESCRIPTION 
		</td>
		<td align="center" class="brlt-border">
			NAME OF SUPPLIER
		</td>
		<td align="center" class="brlt-border" width="7%">
			UNIT <br/>
			PRICE
		</td>
		<td align="center" colspan="2" class="brlt-border" width="15%">
			TOTAL AMOUNT <br/>
			(SELECTED SUPPLIER)
		</td>
	</tr>
<?php 
$totalAmount=0;

if(isset($PRDetail)){
$sn = 0;

if($Department ='1' && $PRType!='NORMAL'){
	$DepName = $PRDetail[0]['Department'];
	echo('<tr><td class="brlt-border">&nbsp;</td>
		<td class="brlt-border">&nbsp;</td>
		<td class="brlt-border"><b>'.$DepName.'</b></td>
		<td class="brlt-border">'.$VendorName.'</td>
		<td class="brlt-border">&nbsp;</td>
		<td class="brlt-border">&nbsp;</td>
		<td class="brlt-border">&nbsp;</td></tr>');
}

foreach ($PRDetail as $val)
{
	$sn +=1;
	$RequestedQty = $val['RequestedQty'];
	$partDesc   = ($PRType =='NORMAL') ? $val['partname'] :$val['PartNumber'];
	$PartNumber = $val['PartNumber'];
	//$VendorName   = $val['VendorName'];
	$UnitPrice    = $val['UnitPrice'];
	$Amount    		= $val['Amount'];
	$PartName2   =  $val['PartName2'];
	$BaseCurrency =  $val['BaseCurrency'];
	$UOM =  $val['UOM'];
	$Amount1 = explode('.', $Amount);

	if($BaseCurrency ="USD"){
		$symcurrency = 'US$';
	}else if($BaseCurrency ="USD"){
		$symcurrency = 'S$';
	}else{
		$symcurrency ='$';
	}
	
	$totalAmount += $Amount;
	if($Department ='1' && $PRType!='NORMAL')
	{
		if($DepName != $val['Department']){
			$DepName = $val['Department'];
			echo("<tr><td colspan=7 class='brlt-border' width='100%'><b>$DepName</b></td></tr>");
		}
	}
	?>
		<tr>
			<td align="center" class="brlt-border">
				<?php echo($sn);?>
			</td>
			<td align="center" class="brlt-border">
				<?php echo($RequestedQty.' '. $UOM);?>
			</td>
			<td align="left" class="brlt-border">
				<?php echo($partDesc);?>
<?php
	if($PRType =='NORMAL'){
		echo ($PartName2 <> '')?'<br/>'.$PartName2.'<br/>':'';
		echo("<br/>P/N:".$PartNumber);
		 
	}
?>
			</td>
			<td align="center" class="brlt-border">
				<?php echo($VendorName);?>
			</td>
			<td align="center" class="brlt-border">
				<?php echo($symcurrency .' '.$UnitPrice);?>
			</td>
			<td align="right" class="brlt-border">
				<?php echo($symcurrency .' '. $Amount1[0]);?>
			</td>
			<td align="right" class="brlt-border">
				<?php echo ( $Amount1[1])?>
			</td>
		</tr>
	<?php 
	} 
}else{ 
	
?>
	<tr>
		<td colspan="7" align="center"> No Data </td>
	</tr>
<?php
}

$GSTAmt = number_format( ($totalAmount  * .07), 2, '.', '');
$GSTAmt1 =  explode('.', $GSTAmt);
$GSTAmt2 = $GSTAmt1[0];
$GSTAmt3 = $GSTAmt1[1];

$totalAmount = number_format($GSTAmt + $totalAmount, 2, '.', '');
$totalAmount1 =  explode('.', $totalAmount);
$totalAmount2 = $totalAmount1[0];
$totalAmount3 = $totalAmount1[1];

?>
	<tr>
		<td align="center" class="td.bt-border">
			&nbsp;
		</td>
		<td align="center" class="brlt-border">
			&nbsp;
		</td>
		<td align="center" class="brlt-border">
			&nbsp;
		</td>
		<td align="center" class="brlt-border">
			&nbsp;
		</td>
		<td align="center" class="brlt-border">
			7% GST
		</td>
		<td align="right" class="brlt-border">
			<?php echo($symcurrency .' '.$GSTAmt2);?>
		</td>
		<td align="right"  class="brlt-border">
			<?php echo($GSTAmt3);?>
		</td>
	</tr>
	<tr>
		<td align="left" class="blt-border" colspan="4">
			REMARKS : <?php echo($Remarks)?>
			<br />
			<center><?php echo($Comment);?></center>
		</td>
		<td align="left" class="blt-border" valign="top">
			TOTAL
		</td>
		<td class="brlt-border" align="right"  valign="top">
			<?php echo($symcurrency .' '.$totalAmount2);?>
		</td >
		<td class="brlt-border" align="right"  valign="top">
			<?php echo($totalAmount3);?>
		</td>
	</tr>
	<tr>
		<td COLSPAN=7>
			<table width="100%">
				<tr>
					<td>&nbsp;
					</td>
					<td>Authorized Personnel
					</td>
					<td colspan="3">&nbsp;
					</td>
					<td>
						Head of Dept.  / General Manager
					</td>
				</tr>	
				<tr>
					<td width="12%">REQUESTED BY : 
					</td>
					<td>
						<?php echo($RequestedBy)?>
					</td>
					<td width="12%">PURCHASED BY : 
					</td>
					<td> 
						<?php echo($ApprovedBy)?>
					</td>
					<td width="12%"> APPROVED BY :
					</td>
					<td> 
						<?php echo($PurchasedBy)?>
					</td>
				</tr>
				<tr>
					<td>DATE : 
					</td>
					<td>10 May 12
					</td>
					<td>DATE : 
					</td>
					<td>10 May 12
					</td>
					<td> DATE :
					</td>
					<td> 10 May 12
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</tr></td></table>
<table style="border:0;font-size: 8px;" border="0" cellspacing="0"  cellpadding="0" width="100%">
<tr> <td colspan='3'>This is a Computer Generated Document.  No Signature Required.</td>
</tr>
<tr><td width="33%" align="left">&nbsp;&nbsp;FM-PRD-742-01</td><td width="33%" align="left">REV : 00</td>
<td align="right"> DATE : 29 Sep'12 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
</table>
<?php die();?>