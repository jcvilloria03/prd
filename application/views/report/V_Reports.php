<style>
	.datatableStyle{
		word-wrap:break-word;
	}
</style>

<div class="newentry">
<h3 class="form-title">Report</h3>
	<form class="globalform" method="post" action="#">
		<ol>
			<li>
			<label>Report List:</label> 
				<select class="select" id="report" name="report">
				</select>
				
			</li>
			<li>
			<label>Field (<em>filter</em>):</label> 
				<select class="select" id="field" name="field"></select>
				<select style="margin-left:5px" class="select" id="filtertype" name="filtertype">
					<option selected="selected" value="is">is</option>
					<option value="is not">is not</option>
					<option value="contain">contains</option>
					<option value="notcontain">doesn't contain</option>
					<option value="begin">begins with</option>
					<option value="end">ends with</option>
					<option value="greater than">is greater than</option>
					<option value="less than">is less than</option>
					<option value="greater than or equal to">is greater than or equal to</option>
					<option value="less than or equal to">is less than or equal to</option>
					<option value="range">between</option>
				</select>
				<input type="text" style="margin-left:5px" title="This is a filter field to seach" id="filter" name="filter" class="text" value="">
			</li>
			<li id='liRange'>
			<label>Range <em>(from</em>):</label> 
				<input type="text" id="range_from" name="range_from" class="text" value="">
				<label style="margin-left: 30px;">Range <em>(to</em>):</label> 
				<input type="text" id="range_to" name="range_to" class="text" value="">
			</li>
			<li>
				<label>Sort by:</label> 
				<select class="select" id="sort_by" name="sort_by"></select>
				<label style="margin-left:30px;">Sorting:</label> 
				<select class="select" id="sort_type" name="sort_type">
					<option selected="selected" value="ASC">Ascending</option>
					<option value="DESC">Descending</option>
				</select>
			</li>
			
			<li class="buttons">
				<input type="button" value="Submit" id="submit-btn" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
				<div class="clr"></div>
			</li>
		</ol>
		<div class="clear"></div>		
    </form>
</div>

<div id="result" style="width:100%; overflow:auto; display:none"></div>

<script type="text/javascript">
var ReportDetails = new Object();

$(document).ready(function() {
	
	$('#liRange').slideUp();
	
	fn_populate = function(obj){
		if (obj=='no data'){
			$('#report').empty().append('<option value="">'+ obj +'</option>');
		}else{
			if (obj.length){
				ReportDetails = obj;
				for (var i=0;i < obj[0].length;i++){
					$('#report').append('<option value="' + obj[0][i].ReportColumns + '">' + obj[0][i].ReportName + '</option>');
				}
				$('#report').change();
				
			}else{
				$('#report').empty().append('<option value="">no current data</option>');
			}
		}
	}
	
	fn_searchData = function(obj){
		if (obj[0] == 'nodata'){
			msgbox('PRD Online','No Available data. Please refresh your browser');
		} else {
			obj[1] = obj[1].split("[").join('');
			obj[1] = obj[1].split("]").join('');
			
			var table = '';
			var thead = '';
			var tbody = '';
			var tfoot = '';
		
			table = '<table cellpadding="0" cellspacing="0" border="0" class="display" id="reportresult">';
			
			thead = '<thead><tr>';
			tfoot = '<tfoot><tr>';
			$.each(obj[1].split(","),function(a,b){
				thead +='<th>'+b+'</th>';
				tfoot +='<th>'+b+'</th>';
			});
			thead += '</tr></thead>';
			tfoot += '</tr></tfoot>';
			
			tbody ='<tbody>';
			$.each(obj[0],function(a,b){
				tbody+='<tr>';
				$.each(b,function(c,d){
					// if ($.trim(d) != ''){
						// var StrTemp = d.toString();
						// var n=StrTemp.indexOf("AM"); 
						// var n1=StrTemp.indexOf("PM");
						
						// if ((n > 0) || (n1 > 0)){
							// var arrayDate = StrTemp.split(' ');
							// d = arrayDate[0] + ' ' + arrayDate[1] + ', ' + arrayDate[2];
						// }
					// }
					tbody +='<td align="left">'+d+'</td>';
				});
				tbody+='</tr>';
			});
			tbody+='</tbody>';
			
			table += thead+tbody+tfoot+'</table>';
			
			$('#result').html(table);
			$('#result').css("overflow-y", "hidden");
			$('#result').slideDown('slow');

			var reportresultTable;
			
			reportresultTable = $('#reportresult').dataTable({
				"sDom": '<"H"lTf>r<"F"tip>',
				"oTableTools": {
                        "aButtons": [
                        	 {
                                "sExtends": "xls",
                                "sFileName": "Report.xls",
                                "bFooter": false
                            }
                        ]
                    },
				'sPaginationType': 'full_numbers',
				'bFilter': true,
				'bPaginate': true,
				'bJQueryUI': true,
				'aLengthMenu': [[10, 25, 50, -1], [10, 25, 50, 'All']],
				"sScrollX": "100%",
				"aaSorting":[],
				"bScrollCollapse": true
			});
			$('.DTTT_container').css({'float':'left','margin-left':20});
			$('.DTTT_container a').css({'padding':'0 5px','margin-right':10}).button();
			reportresultTable.fnAdjustColumnSizing();
			$('#reportresult').css("white-space", "br");
		}
	}
	

	$('#report').change(function(){
		$('#range_from, #range_to, #filter').val('');
		
		$('#result').slideUp('slow');
		$('#result').html('');
		$('#field,#field2,#sort_by').empty()
		
		for (var i=0;i < ReportDetails[1][$(this).val()].length;i++){
			obj = ReportDetails[1][$(this).val()];
			$('#field,#field2').append('<option value="' + obj[i]['fieldname'] + '" fieldtype="' + obj[i]['fieldtype'] + '">' + obj[i]['fieldalias'] + '</option>');
			$('#sort_by').append('<option value="' + obj[i]['fieldname'] + '">' + obj[i]['fieldalias'] + '</option>');
		}
	});
	
	$('#filtertype').change(function(){
		if ($(this).val() == 'range'){
			$('#liRange').slideDown();
			$('#filter').slideUp();
			$('#range_from').focus();
		} else {
			$('#liRange').slideUp();
			$('#filter').slideDown();
			$('#filter').focus();
		}
	});
	
	$('#field').change(function(){
		var FieldType = $('option:selected', this).attr('fieldtype').toLowerCase();
		if (FieldType == 'date'){
			makeitCalendar('filter');
			makeitCalendar('range_from');
			makeitCalendar('range_to');
		} else {
			makeitText('filter');
			makeitText('range_from');
			makeitText('range_to');
		}
	});

	function makeitCalendar(objField){
			$('#' + objField).val('');
			$('#' + objField).attr('readonly', true); // makes it readonly
			$('#' + objField).datepicker({
				dateFormat: "yy-mm-dd"
			});
	}
	
	function makeitText(objField){
			$('#' + objField).removeAttr('readonly');
			$('#' + objField).datepicker("destroy");
	}

	$('#submit-btn').click(function(){
		$('#result').slideUp('slow');	
		$('#result').html('');
		
		if ($('#filtertype').val() == 'range'){
			if($.trim($('#range_from').val()) != ''){
				if($.trim($('#range_to').val()) == ''){
					msgbox('PRD Online','Please input something on the <strong>range to</strong> field , otherwise clear the <strong>range from</strong> field');
					return false;
				}
			}
		}
		
		ProcessRequest('report/reports/searchData',$('.globalform').serialize(),'fn_searchData');
	});

	ProcessRequest('report/reports/populate',{runfirst:'runfirst'},'fn_populate');
});

</script>