<?php
$SONumber = $SOData->SONumber;
$PONumber = $SOData->PONumber;
$CustomerName = $SOData->CustomerName;
$CustomerAddress1 = $SOData->CustomerAddress1;
$CustomerAddress2 = $SOData->CustomerAddress2;
$CustomerAddress3 = $SOData->CustomerAddress3;
$CustomerTelNo = $SOData->CustomerTelNo;
$CustomerFaxNo = $SOData->CustomerFaxNo;
$AdminCost = $SOData->AdminCost;
$CustomerContactPerson = $SOData->CustomerContactPerson;
$EmailAddress = $SOData->EmailAddress;
$Name = $SOData->Name;
$SODate = $SOData->SODate;
$TTCharges = $SOData->TTCharges;
$FreightCharges = $SOData->FreightCharges;
$AdminOneTimeCharge = $SOData->AdminOneTimeCharge;
$CAF = $SOData->CAF;
//Add One Time Charge
$OneTimeCharge = $SOData->OneTimeCharge;
$TransportationCharge=$SOData->TransportationCharge;

$Remarks = $SOData->Remarks;
$EntryDate  = $SOData->EntryDate;
$GST =  $SOData->GST;
$SellingCurrency = $SODetail[0]['SellingCurrency'];

if($SellingCurrency =="USD"){
	$symcurrency = 'US$';
}else if($SellingCurrency ="USD"){
	$symcurrency = 'S$';
}else{
	$SellingCurrency ='$';
}
$DeliveryDate =  $SODetail[0]['DeliveryDate'];
$DONumber =  $SODetail[0]['DONumber'];

?>
<html>
<head>
<style>
@page {
  font-family: arial,sans-serif;
  font-size: 12px;
}
table{
	font-size: 11px;
}
</style>
</head>
<body>
<table style="border:0;font-size: 9x;"  border="0" cellspacing="0" border="0" cellpadding="0" width="1100"><tr>
	<td align="right"> Print Date :<?php echo date("m/d/y G.i:s<br>", time());?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
</table>
<table border="0" cellspacing="0"  cellpadding="0" width="100%"><tr><td>
	<table style="border:.1px solid black" border="0" cellspacing="0"  cellpadding="2px" width="100%"><tr><td>
	<table style="border:.1px solid black" border="0" cellspacing="0" cellpadding="5px"  width="100%">
	<tr>
		<td colspan="6" align="center"  valign="middle">
			<h3>
			JSI LOGISTICS (S) PTE LTD<br />
			No 3. CHANGI NORTH STREET 2 #01-02B/03, SINGAPORE 498827<br />
			Tel : 6545 4041 &nbsp; &nbsp;&nbsp; FAX:6545 7279
			</h3>
		</td>
	</tr>
	<tr>
		<td colspan="6" align="center" >
			<h4>
			SALES ORDER
			</h4>
		</td>
	</tr>
	<tr>
		<td colspan="6">
			<table width="1000">
				<tr>
					<td width="50%">
<table width="100%">
		<tr>
			<td>
				<b>NAME OF COMPANY :</b>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo($CustomerName);?>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>
				<b>ADDRESS :</b>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo($CustomerAddress1);?>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo($CustomerAddress2);?>
			</td>
		</tr>
		<tr>
			<td><?php echo($CustomerAddress3);?>
			</td>
		</tr>
		<tr>
			<td>
				Tel : <?php echo($CustomerTelNo);?>
			</td>
		</tr>
		<tr>
			<td>
				Fax :<?php echo($CustomerFaxNo);?>
			</td>
		</tr>
			<tr><td>&nbsp;</td></tr>
		<tr>
			<td>
				<b>ATTENTION :</b>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo($CustomerContactPerson);?>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo($EmailAddress);?>
			</td>
		</tr>
	</table>
					</td>
					<td width="50%" valign="top">
<table width="100%">
	<tr>
		<td>
			<b>SALES ORDER NO :</b>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
			<?php echo($SONumber);?>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
			<b>DATE :</b>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
			<?php echo($SODate);//echo($SODate);?>
		</td>
	</tr>
</table>
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
	<tr>
		<td  align="center" style="border:.1px solid black">
			<b>S/N</b>
		</td>
		<td align="center"  style="border:.1px solid black">
			<b>QTY</b>
		</td>
		<td align="center"  style="border:.1px solid black">
			<b>UOM</b>
		</td>
		<td align="center" width="7cm"  style="border:.1px solid black">
			<b>DESCRIPTION</b>
		</td>
		<td align="center" style="border:.1px solid black">
			<b>UNIT PRICE(<?php echo($symcurrency);?>)</b>
		</td>
		<td align="center"   style="border:.1px solid black">
			<b>AMOUNT(<?php echo($symcurrency);?>)</b>
		</td>
	</tr>
<?php 
$rowsCount=0;
$totalAmount=0;
if(isset($SODetail)){
$sn = 0;
foreach ($SODetail as $val)
{
$rowsCount +=1;
$sn +=1;
$Quantity = $val['Quantity'];
$PartName   =  $val['PartName'];
$PartName2   =  $val['PartName2'];
$PartNumber   =  $val['PartNumber'];
$UnitPrice    = $val['UnitPrice'];
$Amount    		= $val['Amount'];
$UOM 	= $val['UOM'];
$Amount1 = explode('.', $Amount);

$totalAmount += $Amount;
?>
	<tr>
		<td align="center" style="border-right:.1px solid black;">
			<?php echo($sn);?>
		</td>
		<td align="center" style="border-right:.1px solid black;">
			<?php echo($Quantity);?>
		</td>
		<td align="center" style="border-right:.1px solid black;">
			<?php echo($UOM);?>
		</td>
		<td align="left" style="border-right:.1px solid black;">
			<?php echo($PartName);?><br />
			<?php echo ($PartName2 <> '')?$PartName2.'<br/>':''; ?>
			P/N : <?php echo($PartNumber);?>
		</td>
		<td align="center" style="border-right:.1px solid black;">
			<?php echo( number_format($UnitPrice, numberFormatDecimal($UnitPrice), '.', ''));?>
		</td>
		<td align="center">
			<?php echo(number_format($Amount, 2, '.', ''));?>
		</td>
	</tr>
<?php }
  /*Reggie added Admin OneTime Cost/Charge 2014.04.11*/
if($OneTimeCharge  <> 0) {
	$totalAmount += $OneTimeCharge;
	?>
	
		<tr>
			<td align="center" style="border-right:.1px solid black;">
				
			</td>
			<td align="center" style="border-right:.1px solid black;">
				1
			</td>
			<td align="center" style="border-right:.1px solid black;">
				-
			</td>
			<td align="left" style="border-right:.1px solid black;">
				Mould Cost one time charge
			<td align="center" style="border-right:.1px solid black;">
				<?php echo($OneTimeCharge);?>
			</td>
			<td align="center">
				<?php echo($OneTimeCharge);?>
			</td>
		</tr>
<?php}else{ 
	
?>
	<tr>
		<td colspan="6" align="center"> <b>No Data</b> </td>
	</tr>
<?php
}



		
		
	/*Ei Ei added  Transporation Cost/Charge 2014.08.14*/
if($TransportationCharge  <> 0) {
	$totalAmount += $TransportationCharge;
	?>
	
		<tr>
			<td align="center" style="border-right:.1px solid black;">
				
			</td>
			<td align="center" style="border-right:.1px solid black;">
				1
			</td>
			<td align="center" style="border-right:.1px solid black;">
				-
			</td>
			<td align="left" style="border-right:.1px solid black;">
				Transporation charges
			<td align="center" style="border-right:.1px solid black;">
				<?php echo($TransportationCharge);?>
			</td>
			<td align="center">
				<?php echo($TransportationCharge);?>
			</td>
		</tr>
	<?php
	}
	
	if($rowsCount<20){
			$rowsCount = 20 - $rowsCount;
			for ($i=1; $i<=$rowsCount; $i++)
			{
				 echo('<tr><td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td></tr>');
			}
		}
}

else{ 
	
?>
	<tr>
		<td colspan="6" align="center"> <b>No Data</b> </td>
	</tr>
<?php
}



		if($rowsCount<20){
			$rowsCount = 20 - $rowsCount;
			for ($i=1; $i<=$rowsCount; $i++)
			{
				 echo('<tr><td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td>
				 <td style="border-right:.1px solid black;">&nbsp;</td></tr>');
			}
		}


?>
<?php if($TTCharges <> 0){
$totalAmount += $TTCharges;
?>
	<tr>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td align="center" style="border-right:.1px solid black;">
			-
		</td>
		<td align="left" style="border-right:.1px solid black;">
			TT Charges 
		<td align="center" style="border-right:.1px solid black;">
			-
		</td>
		<td align="center">
			<?php echo($TTCharges);?>
		</td>
	</tr>
	<?php } 
	?>
	<?php if($FreightCharges <> 0){
	$totalAmount += $FreightCharges;
	?>
	<tr>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td align="center" style="border-right:.1px solid black;">
			-
		</td>
		<td align="left" style="border-right:.1px solid black;">
			Permit Charges
		<td align="center" style="border-right:.1px solid black;">
			-
		</td>
		<td align="center">
			<?php echo($FreightCharges);?>
		</td>
	</tr>
	<?php } ?>
	<?php 
	if ($AdminOneTimeCharge  <> 0){ ?>
		<tr>
			<td align="center" style="border-right:.1px solid black;">
				
			</td>
			<td align="center" style="border-right:.1px solid black;">
				1
			</td>
			<td align="center" style="border-right:.1px solid black;">
				-
			</td>
			<td align="left" style="border-right:.1px solid black;">
				Admin Cost one time charge
			<td align="center" style="border-right:.1px solid black;">
				-
			</td>
			<td align="center">
				<?php echo($AdminOneTimeCharge);?>
			</td>
		</tr>
	<?php }?>
	<?php 
	if ($CAF  <> 0){ ?>
		<tr>
			<td align="center" style="border-right:.1px solid black;">
				
			</td>
			<td align="center" style="border-right:.1px solid black;">
				1
			</td>
			<td align="center" style="border-right:.1px solid black;">
				-
			</td>
			<td align="left" style="border-right:.1px solid black;">
				CAF charge
			<td align="center" style="border-right:.1px solid black;">
				-
			</td>
			<td align="center">
				<?php echo($CAF);?>
			</td>
		</tr>
	<?php }
	
$AdminCost1 =  $totalAmount * ($AdminCost/100);
//$AdminCost1 =  ($totalAmount / 100)*$AdminCost;
//$AdminCost1 = number_format($AdminCost1, 2, '.', '');
//$AdminCost1 = floor($AdminCost1*100)/100;
$totalAmount  = $totalAmount + number_format($AdminCost1, 2, '.', '') + $AdminOneTimeCharge+$CAF; 
if($GST == 1){
	$GSTAmt = $totalAmount * .07;
	$totalAmount = $totalAmount + number_format($GSTAmt, 2, '.', '');
	//$totalAmount = $totalAmount+$GSTAmt+$TTCharges+$FreightCharges+$OneTimeCharge+TransportationCharge;
}else{
	$totalAmount = $totalAmount;
	//$totalAmount = $totalAmount+$TTCharges+$FreightCharges+$OneTimeCharge+TransportationCharge;
}

//$totalAmount = number_format($totalAmount, 2, '.', '');
//$totalAmount = floor($totalAmount*100)/100;
	
	
	
	if($AdminCost >0){?>
	<tr>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		<td align="center" style="border-right:.1px solid black;">
			<?php echo number_format($AdminCost, 0, '.', ''); ?>% Admin Cost
		</td>
		<td align="center">
			<?php echo(number_format($AdminCost1, 2, '.', ''));?>
		</td>
	</tr>
	<?php }?>
	<tr>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		</td>
		<td align="center" style="border-right:.1px solid black;">
			&nbsp;
		<td align="center" width="20%" style="border-right:.1px solid black;">
			<?php echo($GST ==1)?'7':'0'; ?>% GST
		</td>
		<td align="center">
			<?php echo(number_format($GSTAmt, 2, '.', ''));
				//echo(floor($GSTAmt*100)/100);
			?>
		</td>
	</tr>

	<tr>
		<td align="right" colspan=5 style="border:.1px solid black;">
			<b>TOTAL</b>
		</td>
		<td align="center" style="border:.1px solid black;">
			<?php 
				echo( $symcurrency .' '.$totalAmount);
			?>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="6" style="border:.1px solid black;">
			<table width="100%">
				<tr>
					<td valign="top">
						<b>REMAKRS :</b>
					</td>
					<td valign="top">
						<?php echo(str_replace(Chr(13),"<br>",$Remarks) ); ?>
					</td>
					<td>
						<table>
							<tr>
								<td>
									d/0:
								</td>
								<td>
									<?php echo($DONumber);?>
								</td>
							</tr>
							<tr>
								<td>
									dd:
								</td>
								<td>
									<?php echo($DeliveryDate);?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			<!--		<td>
						d/0:
					</td>
					<td>
						3-2222
					</td>
				</tr>
				<tr>
					<td colspan="3">
						&nbsp;
					</td>
					<td colspan="3">
						&nbsp;
					</td>
					<td>
						dd:
					</td>
					<td>
						18.03.10
					</td>
				</tr>-->
			</table>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="6" valign="middle">	
			<table width="1000">
				<tr>
					<td><b>Name :</b>
					</td>
					<td><?php echo($Name);?>
					</td>
					<td><b>SIGNATURE:</b>
					</td>
					<td>&nbsp;
					</td>
					<td><b>DATE:</b>
					</td>
					<td>
						 <?php echo($SODate);//echo(date("d M Y"));?>
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;
					</td>
				</tr>
			</table>
			
		<br />
		</td>
	</tr>
</table>
</tr></td></table>
<table style="border:0;font-size: 9x;"  border="0" cellspacing="0" border="0" cellpadding="0" width="1100"><tr>
	<td width="33%" align="left">&nbsp;&nbsp;FM-PRD-742-03</td>
	<td width="33%" align="left">REV : 00</td>
	<td align="right"> 29 Sep'12 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
</table>
</td></tr></table>
</body>
</html>