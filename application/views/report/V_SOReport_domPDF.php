<?php
$SONumber = $SOData->SONumber;
$PONumber = $SOData->PONumber;
$CustomerName = $SOData->CustomerName;
$CustomerAddress1 = $SOData->CustomerAddress1;
$CustomerAddress2 = $SOData->CustomerAddress2;
$CustomerAddress3 = $SOData->CustomerAddress3;
$CustomerTelNo = $SOData->CustomerTelNo;
$CustomerFaxNo = $SOData->CustomerFaxNo;
$AdminCost = $SOData->AdminCost;
$CustomerContactPerson = $SOData->CustomerContactPerson;
$EmailAddress = $SOData->EmailAddress;
$Name = $SOData->Name;
$SODate = $SOData->SODate;
$TTCharges = $SOData->TTCharges;
$FreightCharges = $SOData->FreightCharges;
$Remarks = $SOData->Remarks;
$EntryDate  = $SOData->EntryDate;
$GST =  $SOData->GST;
$SellingCurrency = $SODetail[0]['SellingCurrency'];

if($SellingCurrency =="USD"){
	$symcurrency = 'US$';
}else if($SellingCurrency ="USD"){
	$symcurrency = 'S$';
}else{
	$SellingCurrency ='$';
}
$DeliveryDate =  $SODetail[0]['DeliveryDate'];

?>
<style>
table{
	font-family: arial,sans-serif;
	font-size: 12px;
}
td.b-border{
	border-bottom:.5px solid black;
}
td.r-border{
	border-right:.5px solid black;
}
td.br-border{
	border-right:.5px solid black;
	border-bottom:.5px solid black;
}
td.brt-border{
	border-right:.5px solid black;
	border-bottom:.5px solid black;
	border-top:.5px solid black;
}
td.brlt-border{
	border-right:.5px solid black;
	border-bottom:.5px solid black;
	border-left:.5px solid black;
	border-top:.5px solid black;
}

</style>
<table style="border:.5px solid black" border="0" cellspacing="0"  cellpadding="2px" width="100%"><tr><td>
<table style="border:.5px solid black" border="0" cellspacing="0"  cellpadding="10px" width="100%">
	<tr>
		<td colspan="5" align="center" class="b-border" valign="middle">
			<h3>
			JSI LOGISTICS (S) PTE LTD<br />
			No 3. CHANGI NORTH STREET 2 #01-02B/03, SINGAPORE 498827<br />
			Tel : 6545 4041 &nbsp; &nbsp;&nbsp; FAX:6545 7279
			</h3>
		</td>
	</tr>
	<tr>
		<td colspan="5" align="center" class="b-border">
			<h4>
			SALES ORDER
			</h4>
		</td>
	</tr>
	<tr>
		<td colspan="5">
			<table width="100%">
				<tr>
					<td width="50%">
<table width="100%">
		<tr>
			<td>
				<b>NAME OF COMPANY :</b>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo($CustomerName);?>
			</td>
		</tr>
		<tr><td>&nbsp;</td></tr>
		<tr>
			<td>
				<b>ADDRESS :</b>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo($CustomerAddress1);?>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo($CustomerAddress2);?>
			</td>
		</tr>
		<tr>
			<td><?php echo($CustomerAddress3);?>
			</td>
		</tr>
		<tr>
			<td>
				Tel : <?php echo($CustomerTelNo);?>
			</td>
		</tr>
		<tr>
			<td>
				Fax :<?php echo($CustomerFaxNo);?>
			</td>
		</tr>
			<tr><td>&nbsp;</td></tr>
		<tr>
			<td>
				<b>ATTENTION :</b>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo($CustomerContactPerson);?>
			</td>
		</tr>
		<tr>
			<td>
				<?php echo($EmailAddress);?>
			</td>
		</tr>
	</table>
					</td>
					<td width="50%" valign="top">
<table width="100%">
	<tr>
		<td>
			<b>SALES ORDER NO :</b>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
			<?php echo($SONumber);?>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
			<b>DATE :</b>
		</td>
	</tr>
	<tr><td>&nbsp;</td></tr>
	<tr>
		<td>
			<?php echo($DeliveryDate);//echo($SODate);?>
		</td>
	</tr>
</table>
					</td>
				</tr>
			</table>
			
		</td>
	</tr>
	<tr>
		<td  align="center" class="brlt-border"  width="5%" >
			<b>S/N</b>
		</td>
		<td align="center" class="brlt-border" width="7%">
			<b>QTY</b>
		</td>
		<td align="center" class="brlt-border" >
			<b>DESCRIPTION</b>
		</td>
		<td align="center" class="brlt-border"  width="10%">
			<b>UNIT PRICE(<?php echo($symcurrency);?>)</b>
		</td>
		<td align="center"  class="brlt-border"  width="10%">
			<b>AMOUNT(<?php echo($symcurrency);?>)</b>
		</td>
	</tr>
<?php 

$totalAmount=0;
if(isset($SODetail)){
$sn = 0;
foreach ($SODetail as $val)
{
$sn +=1;
$Quantity = $val['Quantity'];
$PartName   =  $val['PartName'];
$PartName2   =  $val['PartName2'];
$PartNumber   =  $val['PartNumber'];
$UnitPrice    = $val['UnitPrice'];
$Amount    		= $val['Amount'];
$UOM 	= $val['UOM'];
$Amount1 = explode('.', $Amount);

$totalAmount += $Amount;
?>
	<tr>
		<td align="center" class="r-border">
			<?php echo($sn);?>
		</td>
		<td align="center" class="r-border">
			<?php echo($Quantity.' '. $UOM);?>
		</td>
		<td align="left" class="r-border">
			<?php echo($PartName);?><br />
			<?php echo ($PartName2 <> '')?$PartName2.'<br/>':''; ?>
			P/N : <?php echo($PartNumber);?>
		</td>
		<td align="center" class="r-border">
			<?php echo($UnitPrice);?>
		</td>
		<td align="right">
			<?php echo($Amount);?>
		</td>
	</tr>
<?php } 
}else{ 
	
?>
	<tr>
		<td colspan="5" align="center"> <b>No Data</b> </td>
	</tr>
<?php
}

$AdminCost1 =  ($totalAmount / 100)*$AdminCost;
 $AdminCost1 = number_format($AdminCost1, 2, '.', '');
$totalAmount  =$totalAmount + $AdminCost1;
$GSTAmt = $totalAmount * .07;
if($GST == 1){
	$totalAmount = $totalAmount+$GSTAmt+$TTCharges+$FreightCharges;
}else{
	$totalAmount = $totalAmount+$TTCharges+$FreightCharges;
}

$totalAmount = number_format($totalAmount, 2, '.', '');

?>
<?php if($TTCharges <> 0){?>
	<tr>
		<td align="center" class="r-border">
			
		</td>
		<td align="center" class="r-border">
			-
		</td>
		<td align="left" class="r-border">
			TT Charges
		<td align="center" class="r-border">
			-
		</td>
		<td align="right">
			<?php echo($TTCharges);?>
		</td>
	</tr>
	<?php } ?>
	<tr>
		<td align="center" class="r-border">
			&nbsp;
		</td>
		<td align="center" class="r-border">
			&nbsp;
		</td>
		<td align="center" class="r-border">
			&nbsp;
		<td align="center" class="r-border">
			<?php echo number_format($AdminCost, 0, '.', ''); ?>% Admin Cost
		</td>
		<td align="right">
			<?php echo($AdminCost1);?>
		</td>
	</tr>
	<tr>
		<td align="center" class="r-border">
			&nbsp;
		</td>
		<td align="center" class="r-border">
			&nbsp;
		</td>
		<td align="center" class="r-border">
			&nbsp;
		<td align="center" class="br-border" width="20%">
			<?php echo($GST ==1)?'7':'0'; ?>% GST
		</td>
		<td align="right">
			<?php echo(number_format($GSTAmt, 2, '.', ''));?>
		</td>
	</tr>
	<?php if($FreightCharges <> 0){?>
	<tr>
		<td align="center" class="br-border">
			&nbsp;
		</td>
		<td align="center" class="br-border">
			&nbsp;
		</td>
		<td align="left" class="br-border">
			Freight Charges
		<td align="center" class="br-border">
			-
		</td>
		<td align="right" class="b-border">
			<?php   echo($FreightCharges);?>
		</td>
	</tr>
	<?php }
	?>
	<tr>
		<td align="right" colspan=4 class="brt-border">
			<b>TOTAL</b>
		</td>
		<td align="right" class="brt-border">
			<?php echo($totalAmount);   ?>
		</td>
	</tr>
	<tr>
		<td align="left" colspan="5" class="b-border">
			<table width="100%">
				<tr>
					<td >
						<b>REMAKRS :</b>
					</td>
					<td>
						<?php echo($Remarks);?>
					</td>
				</tr>
			<!--		<td>
						d/0:
					</td>
					<td>
						3-2222
					</td>
				</tr>
				<tr>
					<td colspan="3">
						&nbsp;
					</td>
					<td colspan="3">
						&nbsp;
					</td>
					<td>
						dd:
					</td>
					<td>
						18.03.10
					</td>
				</tr>-->
			</table>
		</td>
	</tr>
	<tr>
		<td align="left" colspan=5 valign="middle">	
			<table width="100%">
				<tr>
					<td><b>Name :</b>
					</td>
					<td><?php echo($Name);?>
					</td>
					<td><b>SIGNATURE:</b>
					</td>
					<td>&nbsp;
					</td>
					<td><b>DATE:</b>
					</td>
					<td>
						 <?php echo($DeliveryDate);//echo(date("d M Y"));?>
					</td>
				</tr>
				<tr>
					<td>
						&nbsp;
					</td>
				</tr>
			</table>
			
		<br />
		</td>
	</tr>
</table>
</tr></td></table>
<table style="border:0;font-size: 8px;" border="0" cellspacing="0"  cellpadding="0" width="100%">
<tr><td width="33%" align="left">&nbsp;&nbsp;FM-PRD-742-03</td><td width="33%" align="left">REV : 00</td>
<td align="right"> DATE : 29 Sep'12 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td></tr>
</table>
