<h2 class="title">Setting &raquo; Setup</h2>

<div class="newentry">
	<h3 class="form-title">Set Up</h3>
	<form class="globalform" id="setup-frm" name="setup">
		<ol>
			<div class="lfpane" >
				<li>
					<label>Normal</label>
					<input type="text" id="Normal" name="Normal" class="int"/>
				</li>
				<li>
					<label>JSICost</label>
					<input type="text" id="JSICost" name="JSICost" class="int" />
				</li>
				<li>
					<label>JSIExpense</label>
					<input type="text" id="JSIExpense" name="JSIExpense" class="int" />
				</li>
				<li>
					<label>&nbsp;</label>
					<button id="save-btn" name="save-btn" data="" ><span class="ui-icon ui-icon-disk"></span>Save</button>					
				</li>
			</div>
		</ol>
		<div class="clear"></div>		
	</form>
<script type="text/javascript">
$(document).ready(function(){
	$("input:submit, input:button, button, .button").button().click(function(event){
		event.preventDefault();
	});
	$('#PRNumber').focus();
	firstFoading();
	function firstFoading(){
		ProcessRequest('setting/Setup/getSetupdata',{SONumber:''},'fn_getSetupCallBack');
	}
	
	fn_getSetupCallBack = function(data){
		var today = new Date();      
		var year = today.getFullYear().toString(10).substring(2, 4);
		var normal = 'NORMAL' +'/'+String(year);
		var jsicost = 'JSICOST' +'/'+String(year);
		var jsiexpense = 'JSIEXPENSE' +'/'+String(year);
		
		if(data){
			var obj = jQuery.parseJSON(data);
			
			for (var i=0; i<obj.length; i++){
				if(obj[i]['Value'] == normal){
					$('#Normal').val(obj[i]['Description']);
				}else if(obj[i]['Value'] == jsicost){
					$('#JSICost').val(obj[i]['Description']);
				}else if(obj[i]['Value'] == jsiexpense){
					$('#JSIExpense').val(obj[i]['Description']);
				}
			}
		}
	}
	
	$('#save-btn').click(function(){
		var rules =	[
				"required,Normal,Please enter Normal.",
				"required,JSICost,Please enter JSICost.",
				"required,JSIExpense,Please enter JSIExpense."
				];
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = fn_formSubmit;
		rsv.validate(document.forms['setup'],rules); 
		// console.log($('#Normal').val());
		// console.log($('#JSICost').val());
		// console.log($('#JSIExpense').val());
	});
	
	// Form Submission (add/update)
	function fn_formSubmit(theForm){
		var formValues = $(theForm).serializeArray();
		var action = 'saveSO';
		ProcessRequest('setting/Setup/saveSetupData',formValues,'fn_submitCallback');
	
	
	}
	
	fn_submitCallback = function(data){
		var data = explode("|",data);
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','Normal');	
		if(data[0]=='success'){
			firstFoading();
		}
	}
	$(".int").keydown(function(event) {
			// Allow only backspace, delete, enter, tab
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 ) {

		}else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
});
</script>
