<h2 class="title">Entry &raquo; Customer</h2>
<div class="top-button">
	<button id="add-new"><span class="ui-icon ui-icon-plusthick"></span>Add New Customer</button>
	<button id="back-to-list" style="display:none"><span class="ui-icon ui-icon-arrowthick-1-w"></span>Back to List</button>
</div>
<div class="clear"></div>
<div class="list">
	<table class="display" id="tbl_Customer">
		<thead><tr></tr></thead>
		<tbody></tbody>
		<tfoot><tr></tr></tfoot>
	</table> 
</div>
<div class="newentry" style="display:none">
	<h3 class="form-title">Add Customer</h3>
	<form class="globalform" id="newcustomer-frm" name="newcustomer">
		<ol>
			<div class="lfpane" >
				<li>
					<label>ID</label>
					<input type="text" id="CustomerID" name="CustomerID" value="" />
				</li>
				<li>
					<label>Customer Name</label>
					<input type="text" id="CustomerName" name="CustomerName" value="" />
				</li>
				<li>
					<label>Address 1</label>
					<input type="text" id="CustomerAddress1" name="CustomerAddress1" value="" />
				</li>
				<li>
					<label>Address 2</label>
					<input type="text" id="CustomerAddress2" name="CustomerAddress2" value="" />
				</li>
				<li>
					<label>Address 3</label>
					<input type="text" id="CustomerAddress3" name="CustomerAddress3" value="" />
				</li>
				<li>
					<label>Admin Cost(%)</label>
					<input type="text" id="AdminCost" class="text int" name="AdminCost" value="" maxlength="2" style="width:80px"/>
				</li>
				
				<li>
					<label>Customer Status</label>
					<input type="radio" id="Active" name="Status" value="1" class="retain" checked>Active
					<input type="radio" id="In-Active" name="Status" value="0">In-Active
				</li>
			</div>
			<div class="rfpane" >
				<li>
					<label>Contact No</label>
					<input type="text" id="TelNo" name="TelNo" value="" />
				</li>
				<li>
					<label>Fax</label>
					<input type="text" id="FaxNo" name="FaxNo" value="" />
				</li>
				<li>
					<label>Contact Person</label>
					<input type="text" id="ContactPerson" name="ContactPerson" value="" />
				</li>
				<li>
					<label>Mobile</label>
					<input type="text" id="Mobile" name="Mobile" value="" />
				</li>
				<li>
					<label>Email Address</label>
					<input type="text" id="EmailAddress" name="EmailAddress" value="" />
				</li>
				<li>
					<label>GST Customer</label>
					<Select id="GST" name="GST">
						<Option Value="1"> Yes </option>
						<Option Value="0"> No </option>
					</Select>
				</li>
				<li>
					<label>Inventory Count Day</label>
					<Select id="DayOfCount" name="DayOfCount">
						<Option Value="1"> SUNDAY </option>
						<Option Value="2"> MONDAY </option>
						<Option Value="3"> TUESDAY </option>
						<Option Value="4"> WENSDAY </option>
						<Option Value="5"> THURSDAY </option>
						<Option Value="6"> FRIDAY </option>
						<Option Value="7"> SATURDAY </option>
					</Select>
				</li>
			</div>
			<li class="buttons" style="margin-left: 5%;">
				<label></label>
				<input type="button" id="save-btn" value="Save" data="" />
				<input type="button" id="viewlist-btn" value="View List" />
				<input type="button" id="delete-btn" value="Delete" />
			</li>
		</ol>
		<br class="clear">		
	</form>
</div>
<script type="text/javascript">
var tbl_Customer;
$(document).ready(function() {
	
	$("input:submit, input:button, button, .button").button();
	
// Field to fetch from DB {'DB Column':'Table Header'}
	var Customer_fields = {'CustomerID':'ID',
						  'CustomerName':'Name',
						  'TelNo':'TelNo',
						  'FaxNo':'FaxNo',
						  'ContactPerson':'ContactPerson',
						  'EmailAddress':'EmailAddress',
						  'Mobile':'Mobile',
						  'Status':'Status',
						  'GST':'GST',
						  'AdminCost':'AdminCost'};
							
	
	$.each(Customer_fields,function(i,e){
		$('#tbl_Customer thead tr').append('<th>'+e+'</th>');
		$('#tbl_Customer tfoot tr').append('<th>'+e+'</th>');
	});
	$('#tbl_Customer thead tr').append('<th>Action</th>');
	$('#tbl_Customer tfoot tr').append('<th>Action</th>');	


// Table Initialization
	tbl_Customer = $('#tbl_Customer').dataTable( {
		bJQueryUI: true,
		sScrollX: "100%",
		aLengthMenu: [[10, 25, 50, -1], [10, 25, 50, 'All']],
		sPaginationType: 'full_numbers',
		bDestroy: true,      
		bProcessing: true,
		bServerSide: true,
		sAjaxSource: "entry/Customer/view",
		fnServerData: function ( sSource, aoData, fnCallback ) {
			aoData.push( { "name": "WawiID", "value": "" } );
			aoData.push( { "name": "Fields", "value": implode(",",array_keys(Customer_fields)) } );
			$.ajax( {
				dataType: "json",
				type: "POST",
				url: sSource,
				data: aoData,
				success: fnCallback,
				error: function(request){
					showErrorMessage(request.status);
					if(request.status == 500 && request.statusText == 'Internal Server Error'){					
							msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
					}
				}
			});
		}
	} );
	
    // Buttons
	$('#add-new').click(function(){
		$(this).fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#CustomerID').focus();});});
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Add New Customer');});
		$('#save-btn').val('Save').attr('data','');
		$('#newcustomer-frm').clearForm();
		$('#CustomerID').removeAttr('readonly');
		$('#delete-btn').hide();
		$('#viewlist-btn').show();
	});
		
	$('#viewlist-btn').click(function(){
		$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
	});
	
	$('#back-to-list').click(function(){
		tbl_Customer.fnDraw();
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#viewlist-btn').click(function(){
		tbl_Customer.fnDraw();
		$(this).fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#cancel-btn').click(function(){
		tbl_Customer.fnDraw();
		$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
		$('.newentry').slideToggle(function(){$('.list').slideToggle();});
	});
	
	$('#save-btn').click(function(){
		var rules =	[
				"required,CustomerID,Please enter Customer ID.",
				"required,CustomerName,Please enter Customer Name.",
				"required,CustomerAddress1,Please enter Customer Address 1.",
				"required,TelNo,Please enter Customer Tel No.",
				"required,FaxNo,Please enter Customer Fax.",
				"required,ContactPerson,Please enter Customer Contact Person.",
				"required,EmailAddress,Please enter Customer Email Address.",
				"required,AdminCost,Please enter Customer."
				];
		//	"valid_email,EmailAddress,Please enter valid Email Address.",
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = formSubmit;
		rsv.validate(document.forms['newcustomer'],rules); 
	});
	
	// Form Submission (add/update)
	function formSubmit(theForm){
		var formValues = $(theForm).serializeArray();
			formValues.push({ name: "ID", value: $('#save-btn').attr('data') });
		var action = $('#save-btn').attr('data') == '' ? 'save' : 'update';
		ProcessRequest('entry/Customer/'+action,formValues,'submitCallback');
	}
	
	// Form Submission Callback
	submitCallback = function(data){
		var data = explode("|",data);
		if(data[0] == 'success'){
			tbl_Customer.fnDraw();
			$('#back-to-list').fadeOut('fast',function(){$('#add-new').fadeIn();});
			$('.newentry').slideToggle(function(){$('.list').slideToggle();});
		}
		msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');	
	}
	
	// Todo Function
	todo = function(action,id){
		switch(action){
			case 'edit' : ProcessRequest('entry/Customer/search',{ID:id},'searchCountryCallback'); break;	
			case 'delete' : 
				  var confirm  = ["deleteItem",id];
				  msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirm);				
				break
		}
	}
	
	deleteItem = function(id){
		ProcessRequest('entry/Customer/delete',{ID:id},'deleteCountryCallback');
	}
	
	
	$('#delete-btn').click(function(){
		var confirm  = ["deleteItem",$('#save-btn').attr('data')];
		msgbox('Confirm Deletion','<p class="warning">Are you sure you want to remove this item?</p>',0,confirm);				
	});
// Search Record Callback	
	searchCountryCallback = function(data){
		$('#add-new').fadeOut('fast',function(){$('#back-to-list').fadeIn('fast',function(){$('input#CustomerName').focus().select();});});
		$('#delete-btn').show();
		$('#viewlist-btn').show();
		$('.list').slideToggle(function(){$('.newentry').slideToggle().find('h3.form-title').html('Update Customer');});
		
		if(data.length <= 1){
			var data = explode("|",data);
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','country_name');		
		}else{
			var data = json_decode(data);
			$.each(data,function(i,e){
				if(i == 'status'){
					if(e =='1') $('#Active').attr("checked", "checked");
					else $('#In-Active').attr("checked", "checked");
				}
				else{
					$("#"+i).val(e);
				}
			});
			$('#save-btn').val('Save Changes').attr('data',data.CustomerID);			
			$('#CustomerID').addClass('readonly').attr('readonly','true');
		}
	}
	
	$('#GST').keydown(function(event){
		if ( event.which == 13 ) {
			$('#save-btn').click();
		}
	});
	
	//Delete Callback
	deleteCountryCallback = function(data){
		var data = explode("|",data);
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>');
			tbl_Customer.fnDraw();
	}
	
	$(".int").keydown(function(event) {
		// Allow only backspace, delete, enter, tab
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 ) {
			//
		}else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	});
	
});
</script>
