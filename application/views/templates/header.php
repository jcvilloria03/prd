<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?php echo $title;?></title>
<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600' rel='stylesheet' type='text/css'>
<link href="<?php echo URL_BASE;?>assets/css/reset.css" rel="stylesheet" type="text/css" />
<link href="<?php echo URL_BASE;?>assets/css/jquery-ui-1.9.0.custom.css" rel="stylesheet" type="text/css" />
<link href="<?php echo URL_BASE;?>assets/css/jquery.dataTables.css" rel="stylesheet" type="text/css" />
<link href="<?php echo URL_BASE;?>assets/css/jquery.dataTables_themeroller.css" rel="stylesheet" type="text/css" />
<link href="<?php echo URL_BASE;?>assets/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo URL_BASE;?>assets/css/media-queries.css" rel="stylesheet" type="text/css" />
<link href="<?php echo URL_BASE;?>favicon.ico" rel="shortcut icon"  type="image/x-icon"/>
<!-- CORE -->
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/jquery-1.8.2.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/jquery-ui-1.9.0.custom.min.js"></script>

<!-- UI -->
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/ui/jquery.ui.button.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/ui/jquery.ui.menu.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/ui/jquery.ui.dialog.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/ui/jquery.ui.effect.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/ui/jquery.ui.effect-blind.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/ui/jquery.ui.effect-scale.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/ui/jquery.ui.effect-slide.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/ui/jquery.ui.datepicker.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/ui/jquery.ui.autocomplete.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/ui/jquery.ui.tooltip.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/ui/jquery.ui.combobox.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/ui/jquery.ui.tabs.min.js"></script>

<!-- DATATABLE -->
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/datatable_extras/TableTools/media/js/ZeroClipboard.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/datatable_extras/TableTools/media/js/TableTools.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/datatable_extras/FixedColumns/media/js/FixedColumns.min.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/datatable_extras/Editable/jquery.jeditable.js"></script>


<!-- DIM -->
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/jQueryDim.js"></script>

<!-- Mark -->
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/jquery.maskedinput-1.3.min.js"></script>

<!-- FORM VALIDATOR -->
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/jquery.rsv.js"></script>
<!-- GLOBAL -->
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/jslibrary.js"></script>
<script type="text/javascript" src="<?php echo URL_BASE;?>assets/js/global.js"></script>

<!-- MEDIA QUERIES -->
<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->

</head>
<body>
	<!--<div class="refresh-div">
		<a href="/prd/user/refreshpage" alt="Refresh" class="Refresh">Refresh</a>
	</div>-->
	<div class="wrapper">
		<div class="header">
			<img src="<?php echo URL_BASE;?>assets/images/header-logo.png" alt="PRD System" />
			<div class="top-right-panel">
				<p><?php echo isset($name) && !empty($name) ? "Working as <a id=\"userlogin\" href=\"#\">".$name."</a>" : ""; ?></p>
				<p><?php echo isset($name) && !empty($name) ? date('F j, Y')."  &nbsp;&nbsp;&nbsp; <a class=\"logout\" href=\"".base_url()."user/logout\">Sign-out</a>" : ""; ?></p>
			</div>
		</div>
