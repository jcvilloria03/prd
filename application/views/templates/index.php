<div class="newentry">
<h3 class="form-title">Report</h3>
	<form class="globalform" method="post" action="#">
		<ol>
			<li>
			<label>Report List:</label> 
				<select class="select" id="report">
				</select>
				
			</li>
			<li>
			<label>Field (<em>filter</em>):</label> 
				<select class="select" id="field"></select>
				<select style="display:none; margin-left:5px" class="select" id="filtertype">
					<option selected="selected" value="is">is</option>
					<option value="is not">is not</option>
					<option value="contain">contains</option>
					<option value="notcontain">doesn't contain</option>
					<option value="begin">begins with</option>
					<option value="end">ends with</option>
					<option value="greater than">is greater than</option>
					<option value="less than">is less than</option>
					<option value="greater than or equal to">is greater than or equal to</option>
					<option value="less than or equal to">is less than or equal to</option>
				</select>
				<input type="text" style="display:none; margin-left:5px" title="This is a filter field to seach" id="filter" class="text" value="">
				
			</li>
			<li>
			<label>Range <em>(from</em>):</label> 
				<input type="text" id="range_from" class="text" value="">
				<label style="margin-left: 30px;">Range <em>(to</em>):</label> 
				<input type="text" id="range_to" class="text" value="">
			</li>
			<li>
				<label>Sort by:</label> 
				<select class="select" id="sort_by"></select>
				<label style="margin-left:30px;">Sorting:</label> 
				<select class="select" id="sort_type">
					<option selected="selected" value="ASC">Ascending</option>
					<option value="DESC">Descending</option>
				</select>
			</li>
			
			<li style="display:none" class="buttons">
				<input type="button" value="Submit" id="submit-btn" class="ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false">
				<div class="clr"></div>
			</li>
		</ol>
		<br class="clear">		
    </form>
</div>


<script type="text/javascript">
$(document).ready(function() {

	fn_populate = function(obj){
		console.log(obj);
	}

	ProcessRequest('report/Index/populate',{runfirst:'runfirst'},'fn_populate');
});

</script>