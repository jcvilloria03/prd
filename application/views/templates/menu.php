<script type="text/javascript">
	repeater(function() { checkLogin() }, -1, 60000);
</script>
<div class="menu">
	<ul id="nav">
		<li><a class="home" href="home/dashboard">Dashboard</a></li>
		<?php
			/*
			$cmenu =count($menu);

			$subnav='';
			for ($i = 0; $i <= $cmenu - 1; $i++) {
				if($subnav != $menu[$i]['ProgramName']){
					$subnav = $menu[$i]['ProgramName'];	
					echo('<li><a class="'.strtolower($subnav).'" href="'.strtolower($subnav).'">'.$subnav.'</a><ul class="subnav">');
				}
				$menuurl = $menu[$i]['MenuURL'];
				$menuname = $menu[$i]['MenuName'];
				echo('<li><a href="'.$menuurl.'">'.$menuname.'</a></li>');
				if($cmenu == $i+1){
					echo('</ul></li>');
				}else if($subnav != $menu[$i+1]['ProgramName']){
					echo('</ul></li>');
				}
			}
			*/
		?>
		<li><a class="entry" href="entry">Entry</a>
			<ul class="subnav">
				<li><a href="entry/C_Customer">Customer</a></li>
				<li><a href="entry/Parts">Part</a></li>
				<li><a href="entry/Vendor">Vendor</a></li>
				<li><a href="entry/DeliveryTerms">Delivery Terms</a></li>
				<li><a href="entry/Department">Department</a></li>
				<li><a href="entry/PaymentTerms">Payment Terms</a></li>
				<li><a href="entry/ProductType">Product Type</a></li>
				<li><a href="entry/Addresses">Address</a></li>
				<li><a href="entry/UOM">Unit Of Measurement(UOM)</a></li>
			</ul>
		</li>
		<li>
			<a class="document" href="purchase">Purchase</a>
			<ul class="subnav">
				<li><a href="purchase/PurchaseRequisition">Purchase Requisition </a></li>
				<li><a href="purchase/PurchaseOrder">Purchase Order</a></li>
				<li><a href="purchase/SaleOrder">Sales order</a></li>
				<li><a href="purchase/POReceiving">PO Receiving</a></li>
				
			</ul>
		</li>
		<li>
			<a class="document" href="inventory">Inventory</a>
			<ul class="subnav">
				<li><a href="inventory/InventoryCount">Inventory Count</a></li>
			</ul>
		</li>
		<li><a class="document" href="document">Documents</a>
			<ul class="subnav">
				<li><a href="document/PurchaseRequisition">Purchase Requisition</a></li>
				<li><a href="document/PurchaseOrder">Purchase Order</a></li>
				<li><a href="document/SaleOrder">Sale Order</a></li>
			</ul>
		</li>
		<li><a class="report" href="report/reports">Reports</a>
		</li>
		<!--<li><a class="account" href="account/ChangePassword">Change Password</a></li>-->
	</ul>
</div>
<div class="content">
