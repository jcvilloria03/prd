<div id="login-wrapper">
	<div id="login">
		<h2>Log-In</h2>
		<form name="iLogin_frm" class="globalform" id="iLogin_frm" action="" method="post">
			<em style="font-size: 13px; color:#888"><span class="req">*</span> Required field</em>
			<ol>
				<li>   
					<label>Username</label>
					<input type="text" id="logname" name="logname" />
					<span class="req">*<span>
				</li>
				<li>
					<label>Password</label>
					<input type="password" id="logpass" name="logpass" />
					<span class="req">*<span>
				</li>
				<li class="buttons">
					<label>&nbsp;</label>
					<button id="login-btn" name="login-btn" data="" ><span class="ui-icon ui-icon-key"></span>Login</button>
					<button id="iforgot-btn" name="iforgot-btn" data="" ><span class="ui-icon ui-icon-help"></span>Forgot Password</button>
					<!--<input type="button" value="Login" id="login-btn"  />&nbsp;
					<input type="button" value="Forgot Password?" id="iforgot-btn"  />-->
				</li>
			</ol>
			<div class="clear"></div>
		</form>
	</div>
	<div id="forgot" style="display:none">
		<h2>Forgot Password</h2>
		<form name="iForgot_frm" class="globalform" id="iForgot_frm" action="" method="post">
			<em style="font-size: 13px; color:#888"><span class="req">*</span> Required field</em>
			<ol>
				<li>
					<label>Username</label>
					<input type="text" name="forgotname" id="forgotname" />
					<span class="req">*<span>
				</li>
				<li class="buttons">
					<label>&nbsp;</label>
					<!--<input type="button" value="Send Request" id="forgot-btn"  />
					<input type="button" value="Back to Log In" id="back-btn"  />-->
					<button id="forgot-btn" name="forgot-btn" data="" ><span class="ui-icon ui-icon-help"></span>Send Request</button>
					<button id="back-btn" name="back-btn" data="" ><span class="ui-icon ui-icon-arrowthick-1-w"></span>Back to Log In</button>
					
				</li>
			</ol>
			<div class="clear"></div>
		</form>
	</div>
	<div id="firstlogin" style="display:none;">
		<h2 class="flogintitle">First login? Change your password.</h2>
		<form name="firstLogin_frm" class="globalform" id="firstLogin_frm" action="" method="post" >
			<ol>
				<li>
					<label>Username</label> 
					<input type="text" name="username2" id="username2" />
					<span class="req">*<span>
				</li>
				<li>
					<label>Current Password</label> 
					<input type="password" name="curpassword" id="curpassword" />
					<span class="req">*<span>
				</li>
				<li>
					<label>New Password</label> 
					<input type="password" name="newpassword" id="newpassword" />
					<span class="req">*<span>
				</li>
				<li>
					<label>Confirm New Password</label> 
					<input type="password" name="conpassword" id="conpassword" />
					<span class="req">*<span>
				</li>
				<li class="buttons"> 
					<label>&nbsp;</label> 
					<input type="reset" class="button" value="Clear" id="clear"  />&nbsp; 
					<input type="button" value="Change" id="firstlogin-btn"  />
				</li>
			</ol>
			<div class="clear"></div>
		</form>
		
	</div>
	
</div>

<script type="text/javascript">
$(function() {
	$('#logname').focus();

	var forcss3;
	
	if ($.browser.msie && $.browser.version <= 8) {
		$('#iLogin_frm').html('This page does not support your version browser under IE 9! <br/> '+
							  'Kindly try using another browser like <b>Chrome, Firefox, Safari</b>');
	 }else{
		
	 }
	 
	$("#iforgot-btn").click(function(){
		$("#login").slideToggle(function(){ $("#forgot").slideToggle(); $('#forgotname').focus();});
		
	});
	$("#back-btn").click(function(){
		$("#forgot").slideToggle(function(){ $("#login").slideToggle(); $('#logname').focus();});
	});
	
	
	$('#logpass').keydown(function(event) {
		if ( event.which == 13 ) {if ($(this).val()){$("#login-btn").click(); event.preventDefault()}}
	});
	
	
	$('#login-btn').click(function(){
		var iLogin_rules =	[
				"required,logname,Please enter Username.",
				"required,logpass,Please enter Password."
				];
		rsv.customErrorHandler = formError;	
		rsv.onCompleteHandler = formSubmit;
		rsv.validate(document.forms['iLogin_frm'],iLogin_rules); 
	});
	
	$('#forgot-btn').click(function(){
		var iForgot_rules =	[
					"required,forgotname,Please enter Username."
					];
			rsv.customErrorHandler = formError;	
			rsv.onCompleteHandler = formSubmitForgot;
			rsv.validate(document.forms['iForgot_frm'],iForgot_rules);
		
	});
	
	$('#firstlogin-btn').click(function(){
			var firstLogin_rules =	[
					"required,username2,Please enter your username.",
					"required,curpassword,Please enter your current password.",
					"required,newpassword,Please enter new password.",
					"required,conpassword,Please confirm new password.",
					"same_as,newpassword,conpassword,Please ensure the new passwords you enter are the same.",
					"length>=6,newpassword,Please enter a value that is at least 6 characters long"
					];
			rsv.customErrorHandler = formError;	
			rsv.onCompleteHandler = formSubmit;
			rsv.validate(document.forms['firstLogin_frm'],firstLogin_rules);
		
	});
	
	function formSubmitForgot(theForm){
		var formValues = $(theForm).serializeArray();
		var formId = $(theForm).attr('id');
		var action = formId == 'iLogin_frm' ? 'login' : formId == 'iForgot_frm' ? 'forgot' : 'changepassword';
		ProcessRequest('user/'+action,formValues,'submitForgotCallback');
	}
	
	function formSubmit(theForm){
		var formValues = $(theForm).serializeArray();
		var formId = $(theForm).attr('id');
		var action = formId == 'iLogin_frm' ? 'login' : formId == 'iForgot_frm' ? 'forgot' : 'changepassword';
		ProcessRequest('user/'+action,formValues,'submitCallback');
	}
	
	submitCallback = function(data){
		console.log(data);
		var data = explode("|",data);
		if(data[0]=='success'){
			window.location.href = '<?php echo base_url();?>';
		}else{
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','logname');
		}
	}	
	
	submitForgotCallback = function(data){
		console.log(data);
		var data = explode("|",data);
		if(data[0]=='success'){
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','logname');
			//window.location.href = '<?php echo base_url();?>';
		}else{
			msgbox(msgbox_title[data[0]],'<p class="'+data[0]+'">'+data[1]+'</p>','logname');
		}
	}
	
});
</script>