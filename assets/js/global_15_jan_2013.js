var loadContent,ProcessRequest,ProcessRequestXML,formError,msgbox,menunav,showErrorMessage,msgbox_title,setTableHeader,loadComboBox;
var winHeight = $(window).height();
$(document).ready(function() {
	
	$("input:submit, input:button, button, .button").button();
/* MAIN MENU */
	menunav = $("#nav").menu({
		select: function( event, ui ) {
			var link = ui.item.children( "a:first" );
			loadContent(link.attr( "href" ));
			event.preventDefault();
		}
	}).mouseleave(function(){
		$("#nav").menu('collapseAll',true);
	});
	
/* INDEX MENU */
	$("#nav-index li a").live('click',function(event){
		loadContent($(this).attr( "href" ));
		event.preventDefault();		
	});
	
	$('#userlogin').click(function(){
		loadContent("account/ChangePassword");
	});
	
	$("#nav").removeClass('ui-corner-all').find('li>a').removeClass('ui-corner-all');
	 
/* CONTENT LOADER */
	loadContent = function(href){ 
		$.ajax({
			type: 'POST',
			url: href,
			success: function(data){
				$(".content").html(data);
			},
			beforeSend: function(){				
				$("#nav").menu('collapseAll',true);
				$.dimScreen(1000, 0.5, function() {});
			},
			complete: function(){		
				setInterval(function(){$.dimScreenStop()},2000);
			},
			error: function(request){
				$.dimScreenStop();
				showErrorMessage(request.status);
				if(request.status == 500 && request.statusText == 'Internal Server Error'){					
						msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
				}
			}
		});
	}	
/* CLEAR FORM */
	$.fn.clearForm = function() {
		return this.each(function() {
			var type = this.type, tag = this.tagName.toLowerCase();
			if (tag == 'form')
			  return $(':input',this).clearForm();
			if(!$(this).hasClass('retain')){
			   if (type == 'text' || type == 'password' || tag == 'textarea')
					   this.value = '';
			   else if (type == 'checkbox' || type == 'radio')
				 this.checked = false;
			   else if (tag == 'select')
				 this.selectedIndex = 0;
			}
		});
    };
	
/* FORM ENTER - FOCUS NEXT INPUT */
	$('.globalform').find('input,select').live('keypress',function(event) {
		var index = 0;
			index = $(":input").index(this);
		if($(":input:eq("+ index +")").attr('type') !='button'){
			if (event.keyCode == '13') {
				do {
					index = index + 1;
				} while ($(":input:eq("+ index +")").css('display') == "none");
				$(":input:eq(" + (index) + ")").not(':hidden').focus().select();
				//$(":input:eq(" + ($(":input").index(this) + 1) + ")").not(':hidden').focus();
				return false;							
			}
		}
	});	
	
/* MESSAGE DIALOG BOX */
	msgbox = function(_title,_content,_focus,_confirm,_modal,_width,_height){
		_title = _title ? _title : 'Alert Message';
		_focus = _focus ? _focus : false;
		_width = _width ? _width : 300;
		_height = _height ? _height : 250;
		_modal = _modal ? _modal : true;
		_is_confirm = count(_confirm) > 0 ? true : false;
		if(_is_confirm){
			var _buttons = { 'Yes' : function() {
									$(this).dialog("close");
									var theFunction = eval('(' + _confirm[0] + ')');
										theFunction(_confirm[1]);
									if(_focus) $('#'+_focus).focus().select();
								},
							  'No': function() {
									$(this).dialog("close");
									if(_focus) $('#'+_focus).focus().select();
								}
							}
		}else{
			var _buttons = { 'Close': function() {
									$(this).dialog("close");
									if(_focus) $('#'+_focus).focus().select();
								}
							}
		}
		
		$("#dialog").html(_content).dialog({
            modal: true,
            title: _title,
			show: {effect:"blind",duration:300},
            hide: "scale",
            buttons:_buttons,
			open: function(event,ui){
				$('.ui-button').focus();
			}
        });
		
	}
	
	var window = $('<div />').attr({id:'dialog'}).appendTo(document.body);
	
	/* FORM SUBMIT ERROR PROMPT */
	formError = function(f, errorInfo){		
		var fieldName;
		if(errorInfo.length > 0){
			if (errorInfo[0][0].type == undefined)
				fieldName = errorInfo[0][0][0].name;
			else
				fieldName = errorInfo[0][0].name;			
			msgbox('Error Message','<p class="error">'+errorInfo[0][1]+'</p>',fieldName);
		}
	}
	
	/* PROCESS REQUEST */
	ProcessRequest = function(postURL,postData,postProcess){
		 $.ajax({
			type: 'POST',
			url: postURL,
			data: postData,
			success: function(data){
			 	var theFunction = eval('(' + postProcess + ')');
					theFunction(data);
			},
			beforeSend: function(){
				$.dimScreen(500, 0.3,  function() {
					$('.content').fadeIn();
				});
			   },
			complete: function(){
				 $.dimScreenStop();
			},
			error: function(request){
				$.dimScreenStop();
				showErrorMessage(request.status);
				if(request.status == 500 && request.statusText == 'Internal Server Error'){					
						msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
				}
			}
		 });
	}
	
	ProcessRequestXML = function(postURL,postData,postProcess){
		 $.ajax({
			type: 'POST',
			contentType:'text/xml',
			url: postURL,
			data: postData,
			success: function(data){
			 	var theFunction = eval('(' + postProcess + ')');
					theFunction(data);
			},
			beforeSend: function(){
				$.dimScreen(1000, 0.3, function(){});
			   },
			complete: function(){
				 $.dimScreenStop();
			},
			error: function(request){
				$.dimScreenStop();
				showErrorMessage(request.status);
				if(request.status == 500 && request.statusText == 'Internal Server Error'){					
						msgbox('Error Message','<p class="error">Could not load page.<br>Please contact support.</p>');				
				}
			}
		 });
	}
	
	setTableHeader = function (tableid,msg){
		$('#'+tableid+'_length').after('<span style="width: 200px; margin: 10px 0px 10px 30%;font-weight: bold; ">'+msg+'</span>');
	}
	
	loadComboBoxData = function (id,obj,type){
		$('#' + id)
				.empty()
				.append('<option selected="selected" value=""></option>');
		
		if (obj.length){
			$.each(obj, function(index,item) {
				 $('#' + id)
					 .append($("<option ></option>")
					 .attr("value",item.ID)
					 .text((item.Value != item.ID) ? item.Value + " - " + item.ID : item.ID)); 
			});
		}
		if(type){
			$('#' + id).combobox();	
		}
	}
	
	
	loadbtnAction = function(data){
		$.each(data,function(i,e){
			if($("#"+i).is(":disabled") != e){
				$( "#"+i ).button({ disabled: e });
			}
		});
	}
	
	intKeyDownEvent = function(event){
		// Allow only backspace, delete, enter, tab
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 ) {

		}else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	}
	
	decimalKeyDownEvent = function(event){
		// Allow only backspace, delete, enter, tab
		if ( event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13 || event.keyCode == 9 
		|| event.keyCode == 190 || event.keyCode == 110 ) {
			//

			if(($(this).val().split(".")[0]).indexOf("00")>-1){
				$(this).val($(this).val().replace("00","0"));
			}
		}else {
			// Ensure that it is a number and stop the keypress
			if ((event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105 )) {
				event.preventDefault(); 
			}   
		}
	}
	
/* SHOW ERROR MESSAGE */
	showErrorMessage = function(code){
		switch(code){
			case 500 : loadContent('custom_error/error_500');
		}
	}
	msgbox_title = { 'error' : 'Error Message', 'warning' : 'Warning Message', 'info' : 'Information', 'success' : 'Information' }
	$('.content,#login-wrapper').css('min-height',winHeight - 170);
});



$(window).resize(function() {
});
