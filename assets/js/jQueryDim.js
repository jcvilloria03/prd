//dimming background
jQuery.extend({
    //dims the screen
    dimScreen: function(speed, opacity, callback) {
        if(jQuery('#__dimScreen').size() > 0) return;
        
        if(typeof speed == 'function') {
            callback = speed;
            speed = null;
        }

        if(typeof opacity == 'function') {
            callback = opacity;
            opacity = null;
        }

        if(speed < 1) {
            var placeholder = opacity;
            opacity = speed;
            speed = placeholder;
        }
        
        if(opacity >= 1) {
            var placeholder = speed;
            speed = opacity;
            opacity = placeholder;
        }

        speed = (speed > 0) ? speed : 500;
        opacity = (opacity > 0) ? opacity : 0.5;
		
		var xloader = $(window).width() / 2;
		var yloader = $(window).height() / 2;
		
        return jQuery('<div style="color:#FFF; font-size:12px;"><div class="dimloading" style="margin-left:'+ ( xloader - 40 ) +'px; margin-top:'+ (yloader - 40 ) +'px;" > <span style="margin-left:3px;">Loading...</span><img src="assets/images/dimloader.png" style="vertical-align: middle" /></div></div>').attr({
                id: '__dimScreen'
                ,fade_opacity: opacity
                ,speed: speed
            }).css({
            background: '#000'
            ,height: $(window).height() 
            ,left: '0px'
            ,opacity: 0
            ,position: 'fixed'
            ,top: '0px'
            ,width: $(window).width()
            ,zIndex: 999999
        }).appendTo(document.body).fadeTo(speed, opacity, callback);
    },
    
    //stops current dimming of the screen
    dimScreenStop: function(callback) {
        var x = jQuery('#__dimScreen');
        var opacity = x.attr('fade_opacity');
        var speed = x.attr('speed');
        x.fadeOut(speed, function() {
            x.remove();
            if(typeof callback == 'function') callback();
        });
    }
	
});
